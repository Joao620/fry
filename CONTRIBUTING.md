# Contributing

Here are some ways you can contribute to this project:

[Report bugs and issues](https://gitlab.com/fatscript/fry/issues): If you encounter a problem or error while using `fry`, please let us know! Do you have ideas or suggestions for how we can improve our project? We want to hear from you!

[Explore our YouTube content](https://www.youtube.com/@fatscript): Dive into our tutorials, behind-the-scenes insights, and surrounding topics in the FatScript YouTube channel. By engaging with our content, you help us grow our community, and sharing our videos helps spread the knowledge of FatScript to others!

Spread the word: Help us build the FatScript community by sharing our project with others!

## How to Contribute Code

If you're a developer and you're interested in contributing code to our project, we'd love to have you on board! To get started, please follow these steps:

1. [Open an issue](https://gitlab.com/fatscript/fry/issues) to discuss what you would like to change, point out bugs you have found, or request new features. This will help us ensure that your contributions align with our project goals and existing roadmap.

2. Fork the repository and create a branch for your changes.

3. Write code that adheres to established coding standards and practices. Please see the style guide below for guidance.

4. Write and run tests for your contributions, and ensure that all tests pass before submitting a merge request.

5. Check if [the FatScript specification](https://gitlab.com/fatscript/lang) needs to be updated as well.

6. Open a merge request and submit your changes for review.

7. Participate in the discussion and address any feedback or questions from the community.

8. Celebrate your contribution and add your name and email to the list of contributors below!

## Style Guide

When contributing code to `fry`, please follow these guidelines:

- Use Java-like naming conventions: `PascalCase` for enum values and typedefs, `camelCase` for functions and variables, and `CAPITAL_WORDS` for preprocessor macros.

- Use up to [ISO C17](<https://en.wikipedia.org/wiki/C17_(C_standard_revision)>) features.

- Use 2 spaces for indentation, and keep the opening brace on the same line.

- Limit line length to 80 characters, and consider using a code formatter to enforce these standards, e.g. `clang-format` with Google style, e.g.:

```
"clang-format.style": "{BasedOnStyle: Google, ColumnLimit: 80, IndentWidth: 2, InsertBraces: true}"
```

### Test your changes

- Check all tests in `run_tests.sh` do pass.

- Check also that `auto_check.sh` does not report any errors or warnings.

### Variable declarations

Declare identifiers close to where they are needed, as this allows for more aggressive compiler optimizations.

Try to keep names short but relevant: if code is sound, less comments are necessary.

## Debug options

Some of the debug/trace logs are excluded from regular compilation via `ifdef DEBUG` guards.

To reveal those, build with `DEBUG` flag:

```bash
./compile.sh -DDEBUG
```

## Semantic versioning

Given a version number MAJOR.MINOR.PATCH, increments mean:

- MAJOR version, incompatible changes
- MINOR version, new backwards compatible language features
- PATCH version, backwards compatible bug fixes and library extensions

## Project Structure Overview

Below is an overview of the main directories and their purposes:

- **`src/`**: The heart of `fry`, containing all source code.

  - **`core/`**: Core functionality of the interpreter, including native types and essential operations.
  - **`sdk/`**: Foundational code, such as garbage collection, logging, IO, string manipulation, data encoding, huge numbers support etc.
  - **`libs/`**: FatScript standard libraries, accessible by users of the language and organized to mirror how they are used in the interpreter.
  - **`util/`**: Utility tools for user interaction, including the REPL, formatter, and built-in help.
  - **`lexer.c`, `parser.c`, `interpreter.c`**: High-level components for tokenizing, parsing and interpreting FatScript code.

- **`test/`**: Tests ensuring `fry`'s stability and reliability.

For any questions or clarification on where to place new contributions, please reach out in your issue or merge request discussions.

## Contributors

Antonio Prates - hello@aprates.dev

(when doing a merge request, please, add your name to this list)

Thank you for your support!
