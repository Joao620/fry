# syntax=docker/dockerfile:1

# @file Dockerfile
# @brief Fry dockerfile
# @author Antonio Prates <hello@aprates.dev>
# @version 2.1.0
# @date 2024-01-24
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

# Use an official Alpine Linux runtime as a parent image
FROM alpine:3.16 AS build

# Install build dependencies
RUN apk add --no-cache bash gcc libc-dev curl-dev openssl-dev

# Copy source code to the build container
WORKDIR /app
COPY /linenoise/ ./linenoise/
COPY /src/ ./src/
COPY /test/unit.c ./test/
COPY /compile.sh .

# Set the build argument
ARG CONT_IMG_VER

# Print the value of the build argument
RUN echo "CONT_IMG_VER=$CONT_IMG_VER"

# Compile and install the application
RUN ./compile.sh

# Create a clean Alpine Linux image for the application
FROM alpine:3.16

# Install runtime dependencies
RUN apk add --no-cache curl openssl

# Copy the compiled executable from the build container
WORKDIR /app
COPY --from=build /app/bin/fry /usr/local/bin/fry

# Set the entrypoint for the container
ENTRYPOINT ["fry"]
