#!/usr/bin/env bash

# @file compile.sh
# @brief Build script
# @author Antonio Prates <hello@aprates.dev>
# @version 2.5.0
# @date 2024-03-31
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

useNCurses=0   # set to 1 for ncurses, 0 for fatcurses

if git version &>/dev/null
then
    FRY_VERSION="$(git describe --abbrev=6 --dirty --always --tags)"
elif [ -z "$CONT_IMG_VER" ]
then
    FRY_VERSION="unversioned"  # fallback
else
    FRY_VERSION="$CONT_IMG_VER"
fi

links="-lm -lpthread -lcurl"

sdk="bin/b64.o bin/huge.o bin/rle.o bin/embedded.o bin/logger.o \
bin/memory.o bin/patterns.o bin/sdk.o bin/structures.o bin/sugar.o \
bin/crypto.o"

fry="bin/repl.o bin/help.o bin/formatter.o bin/lexer.o bin/parser.o \
bin/libs.o bin/interpreter.o"

commonFlags="-std=c11 -DFRY_VERSION=\"$FRY_VERSION\" $*"

# choose build mode based on intent and append more flags
case "$@" in
    *"-DDEBUG"*)
        echo 'Building fry [debug mode]:'
        flags="$commonFlags -Wunused -Wdeprecated -Wpedantic \
        -Wfloat-equal -Wall -Wextra -Og -g3 -Wno-overlength-strings"
    ;;
    
    *"-DPROFILE"*)
        echo 'Building fry [profile mode]:'
        flags="$commonFlags -pg -Og -g3"
    ;;
    
    *"-DTURBO"*)
        echo 'Building fry [turbo mode]:'
        flags="$commonFlags -O3 -floop-nest-optimize -flto=auto"
    ;;
    
    *)
        echo 'Building fry:'
        flags="$commonFlags -Winline -O2 -Wno-unused-result"
    ;;
esac

compiler="cc"
#compiler="clang"

# other intresting compile flags available on clang only (override flags):
#flags="$commonFlags -Wmissing-prototypes -Wdocumentation \
#-Wconditional-uninitialized -Wshadow"

# auto-enable SSL support for build if OpenSSL can be found on the system
if echo "int main(){}" | gcc -xc - -lssl -lcrypto -o /dev/null 2>/dev/null
then
    echo $'   OpenSSL lib: \e[32mfound\e[0m'
    links="$links -lssl -lcrypto"
    flags="$flags -DSSL_SUPPORT"
else
    echo $'   OpenSSL lib: \e[33mmissing (no SSL support)\e[0m'
fi

case "$@" in
    *"-DTURBO"*)
        emitFinalBinary="$compiler $flags -fuse-linker-plugin"
    ;;
    
    *)
        emitFinalBinary="$compiler $flags"
    ;;
esac

finishUp() {
    rm bin/*.o
    
    # strip binaries for production, may improve performance (debatable)
    case "$@" in
        *"-DDEBUG"* | *"-DPROFILE"*)
            # do not strip
        ;;
        
        *)
            if command -v strip &>/dev/null
            then
                echo -n '   stripping.'
                strip bin/fry  && echo -n '.'
                strip bin/unit && echo -n '.'
                echo $' \e[34mdone ✔ \e[0m'
            fi
        ;;
    esac
}

# clean target folder
[ -d "bin" ] && rm -r bin
mkdir bin

echo -n '   dependencies'
if echo "int main(){}" | gcc -xc - -lreadline -o /dev/null 2>/dev/null
then
    links="$links -lreadline"
    flags="$flags -DUSE_READLINE"
else
    flags="$flags -I./linenoise"
    $compiler $flags -o bin/linenoise.o -c linenoise/linenoise.c && \
    $compiler $flags -o bin/utf8.o -c linenoise/encodings/utf8.c
    sdk="$sdk bin/linenoise.o bin/utf8.o"
fi
if [ "$useNCurses" -eq 1 ]
then
    links="$links -lncurses"
    flags="$flags -DUSE_NCURSES"
else
    $compiler $flags -o bin/fatcurses.o -c src/sdk/fatcurses.c
    sdk="$sdk bin/fatcurses.o"
fi
echo $' \e[34mdone ✔ \e[0m' && echo -n '   sdk.'                             && \
$compiler $flags -o bin/b64.o         -c src/sdk/b64.c                       && \
$compiler $flags -o bin/rle.o         -c src/sdk/rle.c                       && \
$compiler $flags -o bin/huge.o        -c src/sdk/huge.c                      && \
$compiler $flags -o bin/embedded.o    -c src/sdk/embedded.c   && echo -n '.' && \
$compiler $flags -o bin/logger.o      -c src/sdk/logger.c     && echo -n '.' && \
$compiler $flags -o bin/memory.o      -c src/sdk/memory.c     && echo -n '.' && \
$compiler $flags -o bin/patterns.o    -c src/sdk/patterns.c   && echo -n '.' && \
$compiler $flags -o bin/sdk.o         -c src/sdk/sdk.c        && echo -n '.' && \
$compiler $flags -o bin/structures.o  -c src/sdk/structures.c && echo -n '.' && \
$compiler $flags -o bin/sugar.o       -c src/sdk/sugar.c      && echo -n '.' && \
$compiler $flags -o bin/crypto.o      -c src/sdk/crypto.c     && echo -n '.' && \
echo $' \e[34mdone ✔ \e[0m' && echo -n '   front-end'                        && \
$compiler $flags -o bin/lexer.o       -c src/lexer.c          && echo -n '.' && \
$compiler $flags -o bin/parser.o      -c src/parser.c        && echo -n '..' && \
echo $' \e[34mdone ✔ \e[0m' && echo -n '   runtime'                          && \
$compiler $flags -o bin/libs.o        -c src/libs/libs.c     && echo -n '..' && \
$compiler $flags -o bin/interpreter.o -c src/interpreter.c  && echo -n '...' && \
echo $' \e[34mdone ✔ \e[0m' && echo -n '   tools'                            && \
$compiler $flags -o bin/repl.o        -c src/util/repl.c      && echo -n '.' && \
$compiler $flags -o bin/help.o        -c src/util/help.c      && echo -n '.' && \
$compiler $flags -o bin/formatter.o   -c src/util/formatter.c && echo -n '.' && \
$compiler $flags -o bin/fry.o         -c src/fry.c           && echo -n '..' && \
$compiler $flags -o bin/unit.o        -c test/unit.c         && echo -n '..' && \
echo $' \e[34mdone ✔ \e[0m' && echo -n '   linking.'                         && \
$emitFinalBinary -o bin/fry  bin/fry.o   $fry $sdk $links    && echo -n '..' && \
$emitFinalBinary -o bin/unit bin/unit.o  $fry $sdk $links    && echo -n '..' && \
echo $' \e[34mdone ✔ \e[0m' && finishUp "$@" || (echo ' -> failed!' && exit 1)
