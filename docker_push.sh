#!/usr/bin/env bash

# @file docker_push.sh
# @brief Build and upload fry docker image
# @author Antonio Prates <hello@aprates.dev>
# @version 2.0.0
# @date 2023-12-13
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2023, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

# Halt script on error
set -e

# Init DOCKER_USERNAME and DOCKER_PASSWORD
. docker_credentials

# Setup builder for fatscript cross-platform docker build
if ! docker buildx ls | grep -q frybuilder
then
    # Create the builder if it doesn't exist
    docker buildx create --use --name frybuilder --platform linux/amd64,linux/arm64,linux/386
else
    # Use the existing builder
    docker buildx use frybuilder
fi

# Login to DockerHub
echo 'Logging in to DockerHub...'
echo "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin

# Gather metadata
IMAGE_NAME="$DOCKER_USERNAME/fry"
IMAGE_VERSION="$(git describe --abbrev=6 --dirty --always --tags)"
[[ $IMAGE_VERSION == v* ]] && IMAGE_NAME="$IMAGE_NAME:${IMAGE_VERSION:1}"

# Build a fresh image
echo 'Building fry image...'
docker buildx build \
--platform linux/amd64,linux/arm64,linux/386 \
--build-arg CONT_IMG_VER="$IMAGE_VERSION" \
--tag "$IMAGE_NAME" \
--push .

# Revert back to the default builder
docker buildx use default

# Stop the custom builder
docker buildx stop frybuilder

# End
echo
docker logout
