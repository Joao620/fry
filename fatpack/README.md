# fatpack

## Description

`fatpack` is a FatScript project file packer utility. This script enables users to consolidate files with specific extensions into a single file, as well as to unpack them. Moreover, it can generate self-extracting scripts for the [FatScript Playground](https://fatscript.org/playground).

## Features

- Produce self-extracting scripts for FatScript Playground.
- Consolidate multiple files into a single file.
- Retain the original directory structure during unpacking.
- Select files based on their extensions.
- Operate as both a CLI tool and a library.

## Usage

To employ `fatpack` from the command line:

1. To consolidate a directory:

   ```bash
   fatpack DIR FILE.pack
   ```

2. To generate a self-extracting pack:

   ```bash
   fatpack -z DIR FILE.fpk
   ```

The `-z` parameter is optional for both `.pack` and `.fpk` commands. If provided, `fatpack` will minify FatScript sources and JSON files. This process is not idempotent, meaning it won't be undone upon unpacking. Regardless of the `-z` parameter all files are compressed with RLE and encoded to Base64 and the unpacking logic will first decode the Base64 blob and then decompress using RLE.

3. To unpack a consolidated file:
   ```bash
   fatpack FILE.pack DIR
   ```

## Installation

Assuming you have `fry`, the FatScript interpreter, installed on your system, setting up this utility is straightforward:

```bash
fry -b $HOME/.local/bin/fatpack fatpack.fat
```

## Alternative installation

Assuming you have `python` installed on your system, there is a simplified version capable of `.fpk` command:

```bash
chmod +x fatpack.py
cp $HOME/.local/bin/fatpack fatpack.py
```

Or simply copy it where needed and run:

```bash
python fatpack.py DIR FILE.fpk
```

> this version is mostly intended for Windows users trying to get their project files into [FatScript Playground](https://fatscript.org/playground), it does not implement RLE compression nor the `-z` option for minifying sources

## Supported Extensions

Currently, the script is compatible with the following file extensions:

- `.fat`
- `.json`
- `.csv`
- `.txt`
- `.md`
- `.conf`
- `.xml`
- `.log`
- `.ppm`
- `.flf`
- `.wav`
- `.mp3`

> you can expand the list of permissible extensions in the script as needed

## Notes

- The script will read files as binary and store the data as RLE-Base64 encoded, so the output is web-safe, at the expense of being generally bigger than the input. However fast, RLE compression may not help much in many cases.
- The script's flexibility allows it to function as a library in other projects or scripts.
- Contributions and feedback are always welcome!

## License

[GPLv3](../LICENSE) © 2022-2024 Antonio Prates.
