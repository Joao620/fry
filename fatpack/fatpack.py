#!/usr/bin/env python

# @file fatpack.fat
# @brief Simple FatScript file packer (stripped-down for python)
# @author Antonio Prates <hello@aprates.dev>
# @version 2.5.0
# @date 2024-04-07
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

import os
import base64
import sys

# List of file extensions that the script accepts
allowed_extensions = [
    ".fat",
    ".json",
    ".csv",
    ".txt",
    ".md",
    ".conf",
    ".xml",
    ".log",
    ".ppm",
    ".flf",
    ".wav",
    ".mp3",
    ".ogg",
    # add more as needed
]

# Read .fatignore file if exists
ignore_list = []
if os.path.exists(".fatignore"):
    with open(".fatignore", "r") as f:
        ignore_list = f.read().splitlines()


# Filter files with allowed extensions and not in ignore list.
def filter_allowed(files):
    return [f for f in files if any(f.endswith(ext) for ext in allowed_extensions) and f not in ignore_list]


# Recursively collect all file paths in the given directory.
def get_all_file_paths(directory):
    file_paths = []
    for root, dirs, files in os.walk(directory):
        for file in files:
            file_paths.append(os.path.join(root, file))
    return file_paths


# Pack a single file into base64 format.
def pack_file(file_path):
    with open(file_path, "rb") as file:
        encoded_content = base64.b64encode(file.read()).decode("utf-8")
        encoded_ref = encoded_ref = base64.b64encode(os.path.basename(file_path).encode("utf-8")).decode("utf-8")
        return f"{encoded_ref}:{encoded_content}"


# Pack all allowed files in the directory into a self-extracting FaScript file (.fpk format).
def auto_pack(directory, output_file):
    all_files = filter_allowed(get_all_file_paths(directory))
    packed_files = [pack_file(file) for file in all_files]

    with open(output_file, "w") as out:
        out.write("$result;_<-fat.type._;(->{;filename='")
        out.write(output_file.split("/")[-1] + "';data=[;")
        out.write(",".join(f'"{data}"' for data in packed_files))
        out.write("];exists=path->$exists;write=(path,src)->$write;mkDir=(path,safe=false)->$mkDir;lsDir=path->$lsDir;")
        out.write("log=msg->$log;showProgress=(label,fraction)->$showProgress;decode=b64->$fromBase64;")
        out.write("ensurePath=path->{;nested=path.split('/')(..<-1);~combined='';")
        out.write("nested@dir->{;combined+=dir+'/';!exists(combined)?mkDir(combined);};null;};")
        out.write("fail=code->{log('failed to unpack {filename}'),$exit};")
        out.write("!($isWeb|$args.contains(_=='-f'))?fail(1);")
        out.write("(lsDir('.')@->!_.contains('/')&_!=filename?_).nonEmpty?fail(2);")
        out.write("totalFiles=data.size;..<totalFiles@i->{;")
        out.write("line=data(i);parts=line.split(':');parts.size==2=>{;showProgress('unpacking',i/totalFiles);")
        out.write("currentPath=decode(parts(0)).toText;ensurePath(currentPath);")
        out.write("write(currentPath,decode(parts(1)));};parts.size>2=>fail(3);};")
        out.write("showProgress('unpacking',1);log('done');})();_<-main")


def main():
    if len(sys.argv) != 3:
        print("Usage: python fatpack.py DIR FILE.fpk")
        sys.exit(1)

    directory, output_file = sys.argv[1], sys.argv[2]
    auto_pack(directory, output_file)
    print(f"Packed files from {directory} into {output_file}")


if __name__ == "__main__":
    main()
