#!/usr/bin/env bash

# @file find_non_ascii.sh
# @brief Code inspection tool
# @author Antonio Prates <hello@aprates.dev>
# @version 2.0.0
# @date 2023-12-13
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2023, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

directory_path='src'

find $directory_path -type f | while read -r file
do
    if LC_ALL=C grep --color='auto' -P -n '[^\x00-\x7F]' "$file"
    then
        echo "Non-ASCII character(s) found in file: $file"
    fi
done
