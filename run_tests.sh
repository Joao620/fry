#!/usr/bin/env bash

# @file run_tests.sh
# @brief Fry tests runner
# @author Antonio Prates <hello@aprates.dev>
# @version 2.6.0
# @date 2024-05-26
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

runCmd='./bin/fry'

# LEAK CHEKING
# for valgrind leak checks, first build with DEBUG flag enabled!
# and run like: ./run_tests.sh --leak-check &>leak_check.log
# then check logs like: cat leak_check.log | grep "ERROR SUMMARY"
# if all is reported 0 then we should be good =D

case "$@" in
    *"--leak-check"*)
        runCmd='valgrind --leak-check=full ./bin/fry'
    ;;
esac

formatSources() {
    for file in test/t*.fat
    do
        $runCmd -f "$file"
        exitCode=$?
        
        if [ "$exitCode" != "0" ]
        then
            echo "failed: $file" && exit 1
        fi
    done
}

createBundles() {
    for file in test/t*.fat
    do
        ./bin/fry -o "$file.bun" "$file"
        exitCode=$?
        
        if [ "$exitCode" != "0" ]
        then
            echo "failed: $file" && exit 1
        fi
    done
}

./bin/unit                                                                                 && \
echo    '   formatting sources...' && formatSources                                        && \
echo    '   creating bundles...  ' && createBundles                                        && \
echo -n '   001: greater than               ' && $runCmd -n   40    "$@" test/t001.fat.bun && \
echo -n '   002: fibonacci                  ' && $runCmd -n   50    "$@" test/t002.fat.bun && \
echo -n '   003: case usage                 ' && $runCmd -n   80    "$@" test/t003.fat.bun && \
echo -n '   004: compare scopes             ' && $runCmd -n   60    "$@" test/t004.fat.bun && \
echo -n '   005: import (part 1)            ' && $runCmd -n   32    "$@" test/t005.fat.bun && \
echo -n '   006: import (part 2)            ' && $runCmd -n   32    "$@" test/t006.fat.bun && \
echo -n '   007: import (part 2, alt.)      ' && $runCmd -n   32    "$@" test/t007.fat.bun && \
echo -n '   008: read outer scope (part 1)  ' && $runCmd -n   40    "$@" test/t008.fat.bun && \
echo -n '   009: while loop                 ' && $runCmd -n   60    "$@" test/t009.fat.bun && \
echo -n '   010: map scopes and lists       ' && $runCmd -n  110    "$@" test/t010.fat.bun && \
echo -n '   011: read outer scope (part 2)  ' && $runCmd -n   40    "$@" test/t011.fat.bun && \
echo -n '   012: currying                   ' && $runCmd -n   90    "$@" test/t012.fat.bun && \
echo -n '   013: read txt file              ' && $runCmd -n  110    "$@" test/t013.fat.bun && \
echo -n '   014: import json file           ' && $runCmd -n  110    "$@" test/t014.fat.bun && \
echo -n '   015: call anonymous method      ' && $runCmd -n   32    "$@" test/t015.fat.bun && \
echo -n '   016: get index in list          ' && $runCmd -n   60    "$@" test/t016.fat.bun && \
echo -n '   017: list of texts              ' && $runCmd -n   32    "$@" test/t017.fat.bun && \
echo -n '   018: scope as argument          ' && $runCmd -n   40    "$@" test/t018.fat.bun && \
echo -n '   019: boolean conversions        ' && $runCmd -n  170    "$@" test/t019.fat.bun && \
echo -n '   020: reassign types (primitives)' && $runCmd -n  100 -e "$@" test/t020.fat.bun && \
echo -n '   021: return a scope             ' && $runCmd -n   50    "$@" test/t021.fat.bun && \
echo -n '   022: interpolate texts          ' && $runCmd -n   80    "$@" test/t022.fat.bun && \
echo -n '   023: text subtraction           ' && $runCmd -n   32    "$@" test/t023.fat.bun && \
echo -n '   024: nested scopes              ' && $runCmd -n   50    "$@" test/t024.fat.bun && \
echo -n '   025: nested assign              ' && $runCmd -n   50    "$@" test/t025.fat.bun && \
echo -n '   026: system args                ' && $runCmd        "$@" test/t026.fat.bun x y && \
echo -n '   027: custom types               ' && $runCmd -n  160    "$@" test/t027.fat.bun && \
echo -n '   028: short-circuit operator     ' && $runCmd -n   32    "$@" test/t028.fat.bun && \
echo -n '   029: enigma functions           ' && $runCmd -n  380    "$@" test/t029.fat.bun && \
echo -n '   030: math functions             ' && $runCmd -n  990    "$@" test/t030.fat.bun && \
echo -n '   031: nullish coalescing operator' && $runCmd -n  120    "$@" test/t031.fat.bun && \
echo -n '   032: funny facts about void/any ' && $runCmd -n  340    "$@" test/t032.fat.bun && \
echo -n '   033: text library               ' && $runCmd -n  450    "$@" test/t033.fat.bun && \
echo -n '   034: get by name in scope       ' && $runCmd -n   40    "$@" test/t034.fat.bun && \
echo -n '   035: join lists                 ' && $runCmd -n   70    "$@" test/t035.fat.bun && \
echo -n '   036: join scopes                ' && $runCmd -n   80    "$@" test/t036.fat.bun && \
echo -n '   037: playing with texts         ' && $runCmd -n   90    "$@" test/t037.fat.bun && \
echo -n '   038: prototype to text          ' && $runCmd -n  670 -e "$@" test/t038.fat.bun && \
echo -n '   039: playing with lists         ' && $runCmd -n  170    "$@" test/t039.fat.bun && \
echo -n '   040: destructuring assignment   ' && $runCmd -n  100    "$@" test/t040.fat.bun && \
echo -n '   041: range loops                ' && $runCmd -n  130    "$@" test/t041.fat.bun && \
echo -n '   042: prototype methods          ' && $runCmd -n   90    "$@" test/t042.fat.bun && \
echo -n '   043: dynamic entry naming       ' && $runCmd -n   80    "$@" test/t043.fat.bun && \
echo -n '   044: native types base trait    ' && $runCmd -n  900    "$@" test/t044.fat.bun && \
echo -n '   045: self reference             ' && $runCmd -n  100    "$@" test/t045.fat.bun && \
echo -n '   046: duplicate count            ' && $runCmd -n  440    "$@" test/t046.fat.bun && \
echo -n '   047: give me diamonds           ' && $runCmd -n  190    "$@" test/t047.fat.bun && \
echo -n '   048: remove duplicates          ' && $runCmd -n  260    "$@" test/t048.fat.bun && \
echo -n '   049: stanton measure            ' && $runCmd -n  250    "$@" test/t049.fat.bun && \
echo -n '   050: create phone number        ' && $runCmd -n  290    "$@" test/t050.fat.bun && \
echo -n '   051: count bits                 ' && $runCmd -n  680    "$@" test/t051.fat.bun && \
echo -n '   052: compare two floats         ' && $runCmd -n  680    "$@" test/t052.fat.bun && \
echo -n '   053: error handler              ' && $runCmd -n  150    "$@" test/t053.fat.bun && \
echo -n '   054: rgb to hex convert         ' && $runCmd -n  890    "$@" test/t054.fat.bun && \
echo -n '   055: nested access              ' && $runCmd -n  580    "$@" test/t055.fat.bun && \
echo -n '   056: pig text transformation    ' && $runCmd -n  400    "$@" test/t056.fat.bun && \
echo -n '   057: type aliases               ' && $runCmd -n  490    "$@" test/t057.fat.bun && \
echo -n '   058: inclusions and overrides   ' && $runCmd -n  490    "$@" test/t058.fat.bun && \
echo -n '   059: top three words            ' && $runCmd -n  790    "$@" test/t059.fat.bun && \
echo -n '   060: alphanumeric checks        ' && $runCmd -n  400    "$@" test/t060.fat.bun && \
echo -n '   061: tic-tac-toe                ' && $runCmd -n  740    "$@" test/t061.fat.bun && \
echo -n '   062: time and date              ' && $runCmd -n  250    "$@" test/t062.fat.bun && \
echo -n '   063: regression test (pack a)   ' && $runCmd -n   80    "$@" test/t063.fat.bun && \
echo -n '   064: scope value capturing      ' && $runCmd -n   50    "$@" test/t064.fat.bun && \
echo -n '   065: csv support                ' && $runCmd -n 1200    "$@" test/t065.fat.bun && \
echo -n '   066: json support               ' && $runCmd -n 1180    "$@" test/t066.fat.bun && \
echo -n '   067: method scope self reference' && $runCmd -n   40    "$@" test/t067.fat.bun && \
echo -n '   068: regression test (pack b)   ' && $runCmd -n  460 -e "$@" test/t068.fat.bun && \
echo -n '   069: sdk typeof method          ' && $runCmd -n  240    "$@" test/t069.fat.bun && \
echo -n '   070: biggest number             ' && $runCmd -n   32    "$@" test/t070.fat.bun && \
echo -n '   071: import package folder      ' && $runCmd -n   70    "$@" test/t071.fat.bun && \
echo -n '   072: text padding utils         ' && $runCmd -n  470    "$@" test/t072.fat.bun && \
echo -n '   073: multi-inheritance          ' && $runCmd -n   70    "$@" test/t073.fat.bun && \
echo -n '   074: complex nested access      ' && $runCmd -n  140    "$@" test/t074.fat.bun && \
echo -n '   075: reversing lists            ' && $runCmd -n  250    "$@" test/t075.fat.bun && \
echo -n '   076: finding in lists           ' && $runCmd -n  230    "$@" test/t076.fat.bun && \
echo -n '   077: regex patterns             ' && $runCmd -n  260    "$@" test/t077.fat.bun && \
echo -n '   078: list immutability violation' && $runCmd -n   50    "$@" test/t078.fat.bun && \
echo -n '   079: unique lists               ' && $runCmd -n  250    "$@" test/t079.fat.bun && \
echo -n '   080: quicksort lists            ' && $runCmd -n  330    "$@" test/t080.fat.bun && \
echo -n '   081: system capture             ' && $runCmd -n  260    "$@" test/t081.fat.bun && \
echo -n '   082: composite types            ' && $runCmd -n  410 -e "$@" test/t082.fat.bun && \
echo -n '   083: implicit argument          ' && $runCmd -n  190    "$@" test/t083.fat.bun && \
echo -n '   084: optional parameter         ' && $runCmd -n  190 -e "$@" test/t084.fat.bun && \
echo -n '   085: collection minus           ' && $runCmd -n  190    "$@" test/t085.fat.bun && \
echo -n '   086: math extended              ' && $runCmd -n  780    "$@" test/t086.fat.bun && \
echo -n '   087: accept scope as base type  ' && $runCmd -n  190    "$@" test/t087.fat.bun && \
echo -n '   088: flexible types             ' && $runCmd -n  190    "$@" test/t088.fat.bun && \
echo -n '   089: async workers and tasks    ' && $runCmd -n  590 -e "$@" test/t089.fat.bun && \
echo -n '   090: number to formatted        ' && $runCmd -n  310    "$@" test/t090.fat.bun && \
echo -n '   091: resolve named imports      ' && echo $'   \e[32mskip\e[0m'                && \
echo -n '   092: break map example          ' && $runCmd -n  500    "$@" test/t092.fat.bun && \
echo -n '   093: open and half-open ranges  ' && $runCmd -n  290    "$@" test/t093.fat.bun && \
echo -n '   094: file data store            ' && $runCmd -n  960    "$@" test/t094.fat.bun && \
echo -n '   095: logger mixin               ' && $runCmd -n 1520    "$@" test/t095.fat.bun && \
echo -n '   096: calendar support           ' && $runCmd -n 1810    "$@" test/t096.fat.bun && \
echo -n '   097: index, sort and filter     ' && $runCmd -n  500    "$@" test/t097.fat.bun && \
echo -n '   098: xml support                ' && $runCmd -n 1300    "$@" test/t098.fat.bun && \
echo -n '   099: scoped block syntax        ' && $runCmd -n  120    "$@" test/t099.fat.bun && \
echo -n '   100: binary data                ' && $runCmd -n  390    "$@" test/t100.fat.bun && \
echo -n '   101: extended text proto        ' && $runCmd -n  280    "$@" test/t101.fat.bun && \
echo -n '   102: list head and tail         ' && $runCmd -n  270    "$@" test/t102.fat.bun && \
echo -n '   103: rle schema                 ' && $runCmd -n 1950    "$@" test/t103.fat.bun && \
echo -n '   104: huge int arithmetics       ' && $runCmd -n  500 -e "$@" test/t104.fat.bun && \
echo -n '   105: huge int prototype         ' && $runCmd -n  600 -e "$@" test/t105.fat.bun && \
echo -n '   106: list reduce                ' && $runCmd -n  340    "$@" test/t106.fat.bun && \
echo -n '   107: increment and decrement    ' && $runCmd -n  500    "$@" test/t107.fat.bun && \
echo -n '   108: switch operator            ' && $runCmd -n  500    "$@" test/t108.fat.bun && \
echo -n '   109: nested traps               ' && $runCmd -n  250    "$@" test/t109.fat.bun && \
echo -n '   110: option type                ' && $runCmd -n  900 -e "$@" test/t110.fat.bun && \
echo -n '   111: extra assignment operations' && $runCmd -n  200 -e "$@" test/t111.fat.bun && \
echo -n '   112: indexables patch method    ' && $runCmd -n  700 -e "$@" test/t112.fat.bun && \
echo -n '   113: param validation           ' && $runCmd -n  680 -e "$@" test/t113.fat.bun && \
rm test/*.fat.bun && echo '-> all tests passed' || exit 1
