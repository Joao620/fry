# syntax=docker/dockerfile:1

# @file Dockerfile (games)
# @brief Sample games dockerfile
# @author Antonio Prates <hello@aprates.dev>
# @version 2.1.0
# @date 2024-01-24
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

FROM fatscript/fry

RUN apk add --no-cache alsa-utils
RUN apk --purge --quiet del apk-tools

# Copy Sample Game files to the container
WORKDIR /app
COPY LICENSE .
COPY README.md .
COPY guacano.fat .
COPY hyperlinks.fat .
COPY menu.fat .
COPY pong.fat .
COPY snake.fat .
COPY wave/ wave/

# Set the entrypoint for the container
ENTRYPOINT ["fry", "menu.fat"]
