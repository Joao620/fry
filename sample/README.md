# Sample Games

You can enjoy the games with audio, by running the Docker image and providing the sound device as follows:

```bash
docker run --rm -it --device /dev/snd/ fatscript/games
```

## Usage

- In the menu, use the arrow keys to select a game, then press Enter.
- To exit the menu or a game, press `q`.

## Legal Notes

The project sources are released under [GPLv3](LICENSE) © 2022-2023 Antonio Prates. You can check out the repository [here](https://gitlab.com/fatscript/fry).

However, please note that the sound assets within this project are the intellectual property of Antonio Prates. They are not made available as standalone files or separate downloads, nor are they considered to be in the public domain or under any similar concept. These assets are intended solely for use within the context of the FatScript Sample Games.
