#!/usr/bin/env fry

# @file async-dice.fat
# @brief Example of async processing
# @author Antonio Prates <hello@aprates.dev>
# @version 2.5.0
# @date 2024-03-29
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

# try running with benchmark option, like: fry -c async-dice.fat

_       <- fat.type.List
math    <- fat.math
console <- fat.console
time    <- fat.time
async   <- fat.async

testSize = 1000000
diceSides = 6

rollDice = -> math.floor(random * diceSides) + 1

countSides = (batchSize, id) -> -> {
  console.log('rolling {batchSize} dices (worker {id})...')
  list = 1..batchSize @ rollDice
  console.log('checking distribution (worker {id})...')
  list.reduce((acc, n) -> { acc.[n] += 1, acc }, {})
}

batchSize = math.floor(testSize / diceSides)

# create batch workers
tasks: List/Worker = 1..diceSides @ n -> {
  batchSize = n == 1
    ? batchSize + (testSize % batchSize)
    : batchSize

  Worker(countSides(batchSize, n)).start
}

# aggregate batch results to a total result
countReducer = (acc: Scope, task: Worker): Scope -> {
  count = task.await.result
  1..diceSides @ n -> count(n) ? acc.[n] += count(n)
  acc
}
result: Scope = tasks.reduce(countReducer, {})

# print total results
1..diceSides @ n -> console.log('{n} -> {result(n)} times')
console.log('total = {math.sum(result @ -> result(_))}')
