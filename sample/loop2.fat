#!/usr/bin/env fry

# @file loop2.fat
# @brief Nested loop example, functional programming paradigm (range map)
# @author Antonio Prates <hello@aprates.dev>
# @version 1.3.4
# @date 2023-11-17
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2023, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

_ <- fat._

# Map range 1 to 21, printing each number in a pyramid form
1..21 @ i -> {
  # Inner range loop to print numbers up to current value of 'i'
  1..i @ -> print(i.format('%3.0f'))

  # Print a newline to separate rows
  print('\n')
}
