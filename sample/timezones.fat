#!/usr/bin/env fry

# @file timezones.fat
# @brief Playing with timezones in FatScript
#
# @author Antonio Prates <hello@aprates.dev>
# @version 2.4.1
# @date 2024-03-12
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.
#
# Usage:
# - Copy this file into your project folder (keeping credits/license notes)
# - Import: timezones <- timezones
# - Apply e.g.: time.setZone(timezones.getStandardZoneOffset('Brasilia'))

console <- fat.console
time    <- fat.time
_       <- fat.extra.Duration
_       <- fat.type.List

# Returns the timezone offset in milliseconds
# note: this table is simplified and disregards summer and winter times
getStandardZoneOffset = (tzName: Text): Number -> tzName >> {
  'Pago Pago'           => -Duration(11).hours  # UTC-11
  'Honolulu'            => -Duration(10).hours  # UTC-10
  'Anchorage'           => -Duration(9).hours   # UTC-9
  'Los Angeles'         => -Duration(8).hours   # UTC-8
  'Denver'              => -Duration(7).hours   # UTC-7
  'Chicago'             => -Duration(6).hours   # UTC-6
  'New York'            => -Duration(5).hours   # UTC-5
  'Caracas'             => -Duration(4).hours - Duration(30).minutes
  'Santiago'            => -Duration(4).hours  # UTC-4
  'St. John\'s'         => -Duration(3).hours - Duration(30).minutes
  'Brasilia'            => -Duration(3).hours  # UTC-3
  'Fernando de Noronha' => -Duration(2).hours  # UTC-2
  'Azores'              => -Duration(1).hours  # UTC-1
  'London'              => Duration(0).hours   # UTC+0
  'Paris'               => Duration(1).hours   # UTC+1
  'Athens'              => Duration(2).hours   # UTC+2
  'Moscow'              => Duration(3).hours   # UTC+3
  'Tehran'              => Duration(3).hours + Duration(30).minutes
  'Dubai'               => Duration(4).hours  # UTC+4
  'Kabul'               => Duration(4).hours + Duration(30).minutes
  'Karachi'             => Duration(5).hours  # UTC+5
  'New Delhi'           => Duration(5).hours + Duration(30).minutes
  'Kathmandu'           => Duration(5).hours + Duration(45).minutes
  'Dhaka'               => Duration(6).hours  # UTC+6
  'Yangon'              => Duration(6).hours + Duration(30).minutes
  'Bangkok'             => Duration(7).hours  # UTC+7
  'Beijing'             => Duration(8).hours  # UTC+8
  'Eucla'               => Duration(8).hours + Duration(45).minutes
  'Tokyo'               => Duration(9).hours  # UTC+9
  'Adelaide'            => Duration(9).hours + Duration(30).minutes
  'Sydney'              => Duration(10).hours  # UTC+10
  'Lord Howe Island'    => Duration(10).hours + Duration(30).minutes
  'Noumea'              => Duration(11).hours  # UTC+11
  'Wellington'          => Duration(12).hours  # UTC+12
  'Chatham Islands'     => Duration(12).hours + Duration(45).minutes
  'Tonga'               => Duration(13).hours  # UTC+13
  'Line Islands'        => Duration(14).hours  # UTC+14
  _                     => 0
}

printTimeInZone = (tzName: Text): Void -> {
  currentZone = time.getZone
  time.setZone(getStandardZoneOffset(tzName))
  formattedTime = time.format(time.now, '%H:%M')
  time.setZone(currentZone)  # revert to previous setting

  console.log('current time in {tzName} is {formattedTime}')
}

# print some samples
$isMain => {
  [
    'Los Angeles'
    'New York'
    'Brasilia'
    'London'
    'Athens'
    'Dubai'
    'New Delhi'
    'Beijing'
    'Tokyo'
  ].walk(printTimeInZone)
}
