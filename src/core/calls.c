/**
 * @file calls.c
 * @brief Perform calls of types, indexables, scopes and methods
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-30
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"
#include "../parser.h"

/**
 * Resolve call argument, allowing lazy method evaluation
 */
static Node *resolveArg(Scope *scope, Node *arg, Context *ctx) {
  Node *resolved = IS_FAT_TYPE(arg, FatEntry) ? getCtxValueOf(ctx, arg->val)
                                              : evalNode(scope, arg, ctx);

  // Is argument immutable (pass by reference)
  if (!resolved || !resolved->op) {
    return resolved;
  }

  // Otherwise take a shallow copy and set it as immutable
  resolved = copyNode(resolved, false, ctx);
  resolved->op = false;
  return resolved;
}

/**
 * Add call site arguments keyed by method signature to method scope
 */
static char *loadMethodScope(Scope *callSiteScope, Scope *methodScope,
                             Node *callAst, Node *meth, Context *ctx) {
  // Iterate over each parameter slot in the method signature
  Node *argExpr = callAst->body;
  if (meth->head) {
    for (Node *slotExpr = meth->head; slotExpr; slotExpr = slotExpr->seq) {
      Node *slot = slotExpr;

      // Apply default values
      if (!argExpr) {
        if (slot->type == FatAssign) {
          evalAssign(methodScope, slot->head, slot->body, ctx);
          continue;
        }
        return join2("missing arg" GUIDE, slot->val);
      }
      if (slot->type == FatAssign) {
        if (argExpr->type == FatVoid) {
          evalAssign(methodScope, slot->head, slot->body, ctx);
          argExpr = argExpr->seq;
          continue;
        }
        slot = slot->head;
      }

#ifdef DEBUG
      if (debugLogs) {
        logDebug2(__FILE__, __func__, "resolve", slot->val);
      }
#endif
      Node *resolved = resolveArg(callSiteScope, argExpr, ctx);
      if (!checkAlias(slot->ck, resolved)) {
        return join2(MSG_MISMATCH GUIDE, slot->val);
      }

      // Add the resolved argument to method scope
      addToScope(methodScope, slot->val, resolved);

      argExpr = argExpr->seq;
    }
  } else if (argExpr) {
    Node *implicitArg = resolveArg(callSiteScope, argExpr, ctx);
    addToScope(methodScope, "_", implicitArg);
  }

  return NULL;
}

/**
 * Check for errors that ocurred with loadMethodScope
 */
static Node *getLoadingError(char *err, Context *ctx) {
  Type *type = startsWith(err, MSG_MISMATCH) ? ckTypeError : ckCallError;
  return createError(err, false, type, ctx);
}

/**
 * Leaves loaded parameters GC ready ("unlocks" scope for GC)
 */
static inline void unloadMethodScope(Scope *methodScope) {
  trackScope(methodScope);
  collectScope(methodScope);  // try to immediately deallocate
}

/**
 * Create a curried method instance from a method and a call
 */
static Node *curryCall(Scope *scope, Node *method, Node *call, Context *ctx) {
  if (debugLogs) {
    logDebug(__FILE__, __func__, method->val ? method->val : "(anonymous)");
  }

  // Load call site arguments
  Scope *methodScope = createScope();
  pushStack(ctx, __func__, call, methodScope);
  char *err = loadMethodScope(scope, methodScope, call, method, ctx);

  if (err) {
    unloadMethodScope(methodScope);  // reverse loadMethodScope
    popStack(ctx, 1);
    return getLoadingError(err, ctx);
  }

  // Create a new node and link the body of method as curried method
  Node *curriedMethod = createNode(FatMethod, SRC_RUN, ctx);
  curriedMethod->ck = method->body->ck;
  curriedMethod->head = method->body->head;
  curriedMethod->body = method->body->body;

  // Join args with previously curried scopes, if any
  if (method->scp) {
    Scope *jointScope = mergeScopes(method->scp, methodScope, ctx);
    unloadMethodScope(methodScope);
    methodScope = jointScope;
  }

  bindScope(curriedMethod, methodScope);  // partial apply

  popStack(ctx, 1);
  return curriedMethod;
}

static inline const char *methodName(const Node *call) {
  return call->head && call->head->val ? call->head->val : "(anonymous)";
}

Node *evalMethodCall(Scope *scope, Node *call, Node *method, Context *ctx) {
  if (IS_FAT_TYPE(method->body, FatMethod)) {
    return curryCall(scope, method, call, ctx);
  }

  Memory *localMemBottom = ctx->temp;
  Scope methodLayer = {0};
  char *err = loadMethodScope(scope, &methodLayer, call, method, ctx);
  if (err) {
    wipeScope(&methodLayer, NULL);  // reverse loadMethodScope
    return getLoadingError(err, ctx);
  }

  Node *result = NULL;
  auto_str ref = NULL;
  if (debugLogs) {
    static unsigned int callId = 43690;  // aaaa
    if (asprintf(&ref, "%s #%04x", methodName(call), callId++) == -1) {
      fatalOut(__FILE__, __func__, MSG_OOM);
    }
    logDebug2(__FILE__, __func__, stderrIsTTY ? CL_CYN "begin" : "begin", ref);
  }

  // Execute the method call
  if (method->scp) {
    pushStack(ctx, __func__, method->body, method->scp);
  }
  pushStack(ctx, __func__, method->body, &methodLayer);
  result = interpret(&methodLayer, method->body, ctx);
  popStack(ctx, method->scp ? 2 : 1);
  wipeScope(&methodLayer, NULL);  // reverse loadMethodScope
  if (ctx->tempCount) {
    microGC(result, localMemBottom, ctx);
  }

  // Validate the type of returned value against method signature
  // note: regardless of signature, null and error are always "returnable"
  if (result && !checkAlias(method->ck, result) && !IS_FAT_ERROR(result)) {
    unlockNode(result);
    const char *type = result->ck ? result->ck->name : fatType(result->type);
    char *msg = join3(methodName(call), GUIDE "bad return value" GUIDE, type);
    result = createError(msg, false, ckTypeError, ctx);
  }

  if (debugLogs && ref) {
    logDebug2(__FILE__, __func__, stderrIsTTY ? CL_CYN "end" : "end", ref);
  }

  return result;
}

/**
 * Resolve index(es) of indexables (texts/lists)
 */
static Node *resolveIndexes(Scope *scope, const char *name, Node *arg,
                            Node *entry, NodeTuple *range, Node *aux,
                            Context *ctx) {
  if (!arg) {
    return createError(join2(MSG_I_IDX, name), false, ckIndexError, ctx);
  }

  bool isRange = arg->op == OpRange || arg->op == OpHORange;

  // Extract index arguments of different valid syntaxes
  if (arg->type == FatUnary && isRange) {
    // (..i2) syntax
    aux->type = FatNumber;
    aux->num.f = 0;
    range->_1 = aux;  // fake node
    range->_2 = evalNode(scope, arg->body, ctx);
  } else if (arg->type == FatExpr && isRange) {
    range->_1 = evalNode(scope, arg->head, ctx);
    if (arg->body) {
      // (i1..i2) syntax
      range->_2 = evalNode(scope, arg->body, ctx);
    } else {
      // (i1..) syntax
      aux->type = FatNumber;
      aux->num.f = entry->scp ? (double)entry->scp->size : (double)entry->num.s;
      range->_2 = aux;  // fake node
    }
  } else {
    // (i1) and (i1, i2) syntaxes
    range->_1 = evalNode(scope, arg, ctx);
    range->_2 = arg->seq ? evalNode(scope, arg->seq, ctx) : NULL;
  }

  // Check we have at least one valid index value or fail
  if (!IS_FAT_TYPE(range->_1, FatNumber)) {
    return createError(join2(MSG_I_IDX, name), false, ckIndexError, ctx);
  }

  // In case we do have a second value...
  if (range->_2) {
    // ensure it's valid
    if (range->_2->type != FatNumber) {
      return createError(join2(MSG_I_IDX, name), false, ckIndexError, ctx);
    }
  }

  return NULL;  // success
}

/**
 * Resolve indexed access (for Chunk, Text and List types)
 */
static Node *evalIndexed(Scope *scope, const char *name, Node *arg, Node *entry,
                         Context *ctx) {
  NodeTuple range = {0};
  Node aux = {0};  // node buffer for inferred indexes
  pushStack(ctx, __func__, entry, scope);

  Node *error = resolveIndexes(scope, name, arg, entry, &range, &aux, ctx);
  if (error) {
    popStack(ctx, 1);
    return error;
  }

  if (!range._1) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

  // Select/get item(s) from text/list
  Node *result = NULL;
  if (range._2) {
    long start = (long)range._1->num.f;
    long end = (long)range._2->num.f;

    // Adjust start/end if using half-open range operator
    if (arg->op == OpHORange) {
      if (end) {
        end--;
      } else {
        start = 1;  // nothing to select
      }
    }

    switch (entry->type) {
      case FatChunk:
        result = selectFromChunk(entry, start, end, ctx);
        break;
      case FatText:
        result = selectFromText(entry, start, end, ctx);
        break;
      default:
        result = selectFromList(entry->scp, start, end, ctx);
    }
    unlockNode(range._2);
  } else {
    long index = (long)range._1->num.f;
    switch (entry->type) {
      case FatChunk:
        result = getInChunk(entry, index, ctx);
        break;
      case FatText:
        result = getInText(entry, index, ctx);
        break;
      default:
        result = getInList(entry->scp, index, ctx);
    }
  }
  unlockNode(range._1);

  popStack(ctx, 1);
  return result;
}

/**
 * Get the entry from Scope (see also setDynamic in chaining.c)
 */
static Node *getInScope(Node *scope, Node *arg, Context *ctx) {
  static char key[NUMBER_MAX_LENGTH + 1];

  NodeType type = arg ? arg->type : FatVoid;
  Node *result = NULL;

  switch (type) {
    case FatNumber:
      result = getValueOf(scope->scp, prettyNumber(key, arg->num.f));
      break;

    case FatText:
      result = getValueOf(scope->scp, arg->val);
      break;

    default:
      result = createError(MSG_IDR, true, ckKeyError, ctx);
  }

  unlockNode(scope);
  return result;
}

/**
 * Load default props from given type and/or included types
 */
static void evalDefaults(Scope *instance, Node *type, Context *ctx) {
  // Resolve undefined values from type default values
  for (Node *prop = type->head; prop; prop = prop->seq) {
    const char *propName = prop->val;
    if (!getValueOf(instance, propName)) {
      Node *defaultValue = getValueOf(type->body->scp, propName);
      if (defaultValue) {
        Node *copy = NULL;
        if (defaultValue->scp) {
          if (!initVisit(__func__)) {
            fatalOut(__FILE__, __func__, MSG_U_REC);
          }
          copy = copyNode(defaultValue, true, ctx);
          endVisit();
        } else {
          copy = copyNode(defaultValue, false, ctx);
        }
        addToScope(instance, propName, copy);
      }
    }
  }

  if (!type->ck || !type->ck->include) {
    return;
  }

  // Resolve undefined values from included types (recursively)
  for (int i = 0; type->ck->include[i]; i++) {
    Type *base = resolveAlias(type->ck->include[i]);
    Node *include = base->def;
    if (include) {
      evalDefaults(instance, include, ctx);
    } else {
      auto_str msg = join2(MSG_M_INC ": ", base->name);
      stackOut(__FILE__, __func__, msg, ctx);  // non-trappable
    }
  }
}

static Node *getConstructor(Node *def) {
  Node *constructor = getValueOf(def->tail->scp, "apply");
  if (def->ck && def->ck->include) {
    for (int i = 0; !constructor && def->ck->include[i]; i++) {
      Type *base = resolveAlias(def->ck->include[i]);
      Node *include = base->def;
      if (include) {
        constructor = getConstructor(include);
      }
    }
  }
  return constructor;
}

static Node *failInstance(const char *name, const char *err, int i, Type *ck,
                          Scope *instance, Context *ctx) {
  popStack(ctx, 1);  // instance
  trackScope(instance);
  auto_str pos = ofInt(i);
  char *msg = join5(name, GUIDE, err, " at pos: ", pos);
  return createError(msg, false, ck, ctx);
}

static Node *instantiate(Scope *scope, const char *name, Node *arg, Node *type,
                         Context *ctx) {
  Type *alias = type->ck;

  // Use the base type definition
  type = resolveAlias(alias)->def;

  if (!type) {
    char *msg = join2(MSG_N_FOUND GUIDE, name);
    return createError(msg, false, ckKeyError, ctx);
  }

  Scope *instance = createScope();
  pushStack(ctx, __func__, type, instance);
  instance->ck = alias;  // keep type from alias
  Node *prop = type->head;
  bool mayHaveSkippedSlots = false;

  // Load values from arguments
  for (int i = 1; arg && prop; arg = arg->seq, prop = prop->seq, i++) {
    if (arg->type == FatAssign) {
      mayHaveSkippedSlots = true;
      Node *left = arg->head;
      for (Node *slot = type->head; slot; slot = slot->seq) {
        if (strEq(slot->val, left->val)) {
          left = slot;  // swap left side of expression to slot
          break;        // keep slot's original type and mutability options
        }
      }
      if (left != arg->head || isAlias(type->ck, checkVoid)) {
        evalAssign(instance, left, resolveArg(scope, arg->body, ctx), ctx);
      } else {
        return failInstance(name, MSG_I_ARG, i, ckCallError, instance, ctx);
      }
    } else if (arg->type == FatMethod && arg->val) {
      mayHaveSkippedSlots = true;
      Node *slot = NULL;
      // Check if method exists as parameter in the type definition
      for (slot = type->head; slot; slot = slot->seq) {
        if (strEq(slot->val, arg->val) && isAlias(slot->ck, checkMethod)) {
          break;
        }
      }
      if (slot) {
        Node *method = evalNode(instance, arg, ctx);
        method->op = slot->op;  // keep slot's mutability option
      } else {
        return failInstance(name, MSG_I_ARG, i, ckCallError, instance, ctx);
      }
    } else if (!mayHaveSkippedSlots) {
      Node *aux = resolveArg(scope, arg, ctx);
      if (checkAlias(prop->ck, aux)) {
        evalAssign(instance, prop, aux, ctx);
        unlockNode(aux);
      } else {
        unlockNode(aux);
        return failInstance(name, MSG_MISMATCH, i, ckTypeError, instance, ctx);
      }
    } else {
      // once mayHaveSkippedSlots can't assume order
      return failInstance(name, "unnamed arg", i, ckCallError, instance, ctx);
    }
  }

  // Load default values for missing arguments
  evalDefaults(instance, type, ctx);

  Node *result = NULL;

  // Call apply method (constructor), if available
  Node *constructor = getConstructor(type);
  if (constructor) {
    Node aux = {.type = FatScope, .ck = alias, .scp = instance, .src = SRC_AUX};
    Node *previousSelf = ctx->selfRef;
    ctx->selfRef = &aux;
    result = interpret(instance, constructor->body, ctx);
    ctx->selfRef = previousSelf;
    if (result == &aux) {
      result = copyNode(&aux, false, ctx);  // untangle
    } else {
      trackScope(instance);
    }
  } else {
    result = runtimeCollection(instance, ctx);
  }

  // Set typecheck reference to proper alias (if applicable)
  if (result) {
    switch (result->type) {
      case FatMethod:
      case FatError:
      case FatVoid:
        break;  // do nothing

      case FatScope:
        if (result->scp) {
          result->scp->ck = alias;
        }
        break;

      default:
        result->ck = alias;
    }
  }

  popStack(ctx, 1);  // instance
  return result;
}

Node *evalCall(Scope *scope, Node *call, Context *ctx) {
  Node *head = call->head;
  char *name = "(anonymous)";
  Node *node = NULL;
  if (IS_FAT_TYPE(head, FatEntry)) {
    name = head->val;
    node = getCtxValueOf(ctx, name);
  } else {
    node = evalNode(scope, head, ctx);
  }

  if (!node) {
    char *msg = join2("nothing to call" GUIDE, name);
    return createError(msg, false, ckCallError, ctx);
  }

  Node *result = NULL;

  switch (node->type) {
    case FatType:
      if (node->ck->isComposite) {
        char *msg = join2("can't instantiate composite types" GUIDE, head->val);
        return createError(msg, false, ckCallError, ctx);
      }
      result = instantiate(scope, head->val, call->body, node, ctx);
      break;

    case FatChunk:
    case FatText:
    case FatList:
      result = evalIndexed(scope, name, call->body, node, ctx);
      break;

    case FatScope:
      result = getInScope(node, evalNode(scope, call->body, ctx), ctx);
      break;

    case FatMethod:
      result = evalMethodCall(scope, call, node, ctx);
      break;

    default:
      unlockNode(node);
      return createError(join2("non-callable ", name), false, ckCallError, ctx);
  }
  unlockNode(node);

  // Indexables, Scopes and Methods (nested access)
  for (Node *tail = call->tail; tail; tail = tail->tail) {
    Node *aux = result;
    switch (result ? result->type : FatVoid) {
      case FatChunk:
      case FatText:
      case FatList:
        result = evalIndexed(scope, "(nested)", tail->body, aux, ctx);
        break;

      case FatScope:
        aux = evalNode(scope, tail->body, ctx);
        result = getInScope(result, aux, ctx);
        break;

      case FatMethod:
        result = evalMethodCall(scope, tail, aux, ctx);
        break;

      default:
        unlockNode(aux);
        return createError("non-callable (nested)", true, ckCallError, ctx);
    }
    unlockNode(aux);
  }

  return result;
}
