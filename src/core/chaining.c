/**
 * @file chaining.c
 * @brief Handles dot operator sequences
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.0
 * @date 2024-04-11
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

/**
 * Get dynamic key val e.g: scope.[key], see also getInScope in calls.c
 */
static Node *setDynamic(Scope *scope, Node *dyn, Node *buff, Context *ctx) {
  Node *node = evalNode(scope, dyn->body, ctx);

  NodeType type = node ? node->type : FatVoid;
  Node *result = NULL;

  switch (type) {
    case FatNumber:
      buff->val = prettyNumber(NULL, node->num.f);
      result = buff;
      break;

    case FatText:
      buff->val = copyFragment(node->val, node->num.s);
      result = buff;
      break;

    default:
      result = createError(MSG_IDR, true, ckKeyError, ctx);
  }

  unlockNode(node);
  return result;
}

static inline const char *getEntryName(Node *node) {
  switch (node->type) {
    case FatEntry:
    case FatType:
      return node->val;

    case FatCall:
      return node->head->val;

    default:
      return NULL;
  }
}

static inline bool isDotOp(const Node *node) {
  return IS_FAT_TYPE(node, FatExpr) &&
         (node->op == OpDot || node->op == OpIfDot);
}

static inline Type *getNativeType(const NodeType type) {
  switch (type) {
    case FatVoid:
      return checkVoid;
    case FatBoolean:
      return checkBoolean;
    case FatNumber:
      return checkNumber;
    case FatHugeInt:
      return checkHugeInt;
    case FatChunk:
      return checkChunk;
    case FatText:
    case FatTemp:
      return checkText;
    case FatList:
      return checkList;
    case FatScope:
      return checkScope;
    case FatMethod:
      return checkMethod;
    case FatError:
      return checkError;
    default:
      return NULL;
  }
}

static inline Node *getTypeOf(Node *node) {
  Type *check = NULL;

  if (node->type == FatScope && node->scp) {
    check = node->scp->ck;  // actual type is set inside scope
  } else if (node->type == FatMethod) {
    check = checkMethod;  // method->ck is return type
  } else {
    check = resolveAlias(node->ck);
  }

  if (!check) {
    check = getNativeType(node->type);  // fallback
  }

  return check ? check->def : NULL;
}

static inline Node *getPrototype(Node *def) { return def ? def->tail : NULL; }

static inline Node *autoCall(Scope *scope, Node *method, Context *ctx) {
  Node call = {.type = FatCall, .head = method, .src = SRC_AUX};
  return evalMethodCall(scope, &call, method, ctx);
}

static bool canResolveViaProto(Node *proto, const char *name,
                               NodeTuple *result);

static bool canResolveViaInclude(Type *type, const char *name,
                                 NodeTuple *result) {
  if (!type || !type->include) {
    return false;
  }

  for (int i = 0; type->include[i]; i++) {
    Type *base = resolveAlias(type->include[i]);
    Node *inProto = getPrototype(base->def);
    if (inProto) {
      if (canResolveViaProto(inProto, name, result)) {
        return true;
      }
    } else {
      auto_str msg = join3(type->name, GUIDE MSG_M_INC ": ", base->name);
      logAlert(__FILE__, __func__, msg);
    }
  }
  return false;
}

static bool canResolveViaProto(Node *proto, const char *name,
                               NodeTuple *result) {
  Node *node = getValueOf(proto->scp, name);
  if (node) {
    result->_1 = proto;
    result->_2 = node;
    return true;
  }
  return canResolveViaInclude(proto->ck, name, result);
}

/**
 * Overlay left scope and resolve right side of chained expression
 */
static Node *resolveRight(Node *left, Node *right, Node *node, Context *ctx) {
  Node *result = NULL;

  switch (right->type) {
    case FatCall:
      pushStack(ctx, __func__, right, left->scp);
      result = evalCall(left->scp, right, ctx);
      popStack(ctx, 1);
      break;

    case FatEntry:
      if (!right->op && IS_FAT_TYPE(node, FatMethod) && !node->head) {
        pushStack(ctx, __func__, right, left->scp);
        result = autoCall(left->scp, node, ctx);
        popStack(ctx, 1);
      } else {
        result = node;
      }
      break;

    default:
      break;
  }

  return result;
}

/**
 * Try to resolve property via prototype methods of left's type
 */
static Node *resolveViaProto(Node *left, const char *name, Node *right,
                             Context *ctx) {
  NodeTuple target;

  Node *proto = getPrototype(getTypeOf(left));
  if (proto && name && canResolveViaProto(proto, name, &target)) {
    Node *previousSelf = ctx->selfRef;
    ctx->selfRef = left;
    Node *result = resolveRight(target._1, right, target._2, ctx);
    ctx->selfRef = previousSelf;
    return result;
  }

  if (right->type == FatCall) {
    char *msg = join2(MSG_N_FOUND GUIDE, name ? name : "?");
    return createError(msg, false, ckKeyError, ctx);
  }

  return NULL;
}

static Node *evalDot(Scope *scope, Node *node, Context *ctx);

static Node *resolveLeft(Scope *scope, Node *node, Context *ctx) {
  return isDotOp(node) ? evalDot(scope, node, ctx) : evalNode(scope, node, ctx);
}

static Node *evalDot(Scope *scope, Node *node, Context *ctx) {
#ifdef DEBUG
  if (!node) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
    exit(EXIT_FAILURE);
  }
#endif

  Node *right = node->body;

  if (!right) {
    return NULL;
  }

  Node *result = NULL;

  if (right->type == FatBlock) {
    Node *left = resolveLeft(scope, node->head, ctx);
    Node *previousSelf = ctx->selfRef;
    ctx->selfRef = left;
    if (IS_FAT_TYPE(left, FatScope) && left->scp) {
      pushStack(ctx, __func__, right, left->scp);
      result = interpret(left->scp, right, ctx);
      popStack(ctx, 1);
    } else {
      result = interpret(scope, right, ctx);
    }
    ctx->selfRef = previousSelf;
    return result;
  }

  Node dynBuff = {.type = FatEntry, .op = true, .src = SRC_AUX};
  if (right->type == FatDynamic) {
    right = setDynamic(scope, right, &dynBuff, ctx);
  }

  const char *entryName = getEntryName(right);
  if (!entryName) {
    return createError("bad dynamic", true, ckKeyError, ctx);
  }

  Node *left = resolveLeft(scope, node->head, ctx);

  if (left) {
    pushStack(ctx, __func__, left, scope);

    Node *entry = NULL;
    if (left->type == FatScope && left->scp) {
      entry = getValueOf(left->scp, entryName);
      if (entry) {
        result = resolveRight(left, right, entry, ctx);
      }
    }

    if (!entry && right != &dynBuff) {
      result = resolveViaProto(left, entryName, right, ctx);
    }

    popStack(ctx, 1);  // left
    unlockNode(left);
  } else if (node->op == OpDot) {
    Node *proto = getPrototype(checkVoid->def);  // lift null into Void type
    Node *voidProp = proto ? getValueOf(proto->scp, entryName) : NULL;

    if (voidProp) {  // can resolve via proto
      result = autoCall(proto->scp, voidProp, ctx);
    } else {  // left is expected to be defined
      char *msg = join3("can't resolve scope of '", entryName, "'");
      result = createError(msg, false, ckTypeError, ctx);
    }
  }  // else it's OpIfDot (just ignore, don't emit error)

  free(dynBuff.val);
  return result;
}

/**
 * Assign on chained inner entry, like: outer.inner = right
 */
static Node *innerAssign(Scope *scope, Node *left, Node *right, Context *ctx) {
  if (!left || !left->body) {
    return createError("no left-hand side", true, ckAssignError, ctx);
  }

  Node *inner = left->body;
  Node *outer = resolveLeft(scope, left->head, ctx);
  if (!outer || IS_FAT_ERROR(outer)) {
    return outer;
  }

  pushStack(ctx, __func__, outer, scope);

  Node dynBuff = {.type = FatEntry, .op = true, .src = SRC_AUX};
  if (inner->type == FatDynamic) {
    inner = setDynamic(scope, inner, &dynBuff, ctx);
  }

  Node *result = NULL;
  if (outer->type == FatScope) {
    Node *value = evalNode(scope, right, ctx);
    result = evalAssign(outer->scp, inner, value, ctx);
    unlockNode(value);
  } else {
    result = createError("failed to assign", true, ckAssignError, ctx);
  }

  popStack(ctx, 1);  // outer
  free(dynBuff.val);
  return result;
}
