/**
 * @file chunks.c
 * @brief Binary data handling
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.0
 * @date 2024-04-09
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

char *forceToUtf8(const char *chunk, size_t len) {
  // The worst-case scenario is every byte turns into a replacement
  size_t maxOutputLen = len * 3 + 1;  // +1 for null-terminator
  char *result = FRY_ALLOC(maxOutputLen);

  size_t outputIndex = 0;
  for (size_t i = 0; i < len;) {
    int validBytes = chunk[i] ? utf8bytes(&chunk[i]) : 0;
    if (validBytes > 0 && (i + validBytes) <= len) {
      memcpy(&result[outputIndex], &chunk[i], validBytes);
      outputIndex += validBytes;
      i += validBytes;
    } else {
      // Invalid utf-8 section, insert replacement character and skip over
      memcpy(&result[outputIndex], "\xEF\xBF\xBD", 3);
      outputIndex += 3;
      do {
        i++;
      } while (i < len && (!chunk[i] || !utf8bytes(&chunk[i])));
    }
  }

  result[outputIndex] = '\0';
  return FRY_REALLOC(result, outputIndex + 1);
}

Node *getInChunk(Node *node, long index, Context *ctx) {
#ifdef DEBUG
  if (debugLogs) {
    auto_str num = ofInt(index);
    logDebug2(__FILE__, __func__, MSG_INDEX, num);
  }
#endif

  if (!node->val) {
    return createError(MSG_IS_EMPTY, true, ckIndexError, ctx);
  }

  long length = node->num.s;
  if (index < 0) {
    index = length + index;
  }

  if (index < 0 || index >= length) {
    return createError(MSG_BOUNDS, true, ckIndexError, ctx);
  }

  return runtimeNumber((Byte)node->val[index], ctx);
}

Node *selectFromChunk(Node *node, long start, long end, Context *ctx) {
#ifdef DEBUG
  if (debugLogs) {
    auto_str num1 = ofInt(start);
    auto_str num2 = ofInt(end);
    auto_str msg = join4(MSG_INDEX GUIDE, num1, "..", num2);
    logDebug(__FILE__, __func__, msg);
  }
#endif

  long length = node->num.s;
  if (start < 0) {
    start = length + start;
  }
  if (start < 0) {
    start = 0;
  }
  if (end < 0) {
    end = length + end;
  }
  if (end >= length) {
    end = length - 1;
  }

  if ((!length) || (end < start) || (start >= length) || (end < 0)) {
    return runtimeChunk(NULL, 0, ctx);  // nothing to select
  }

  size_t len = end - start + 1;
  return runtimeChunk(copyFragment(node->val + start, len), len, ctx);
}
