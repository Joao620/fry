/**
 * @file lists.c
 * @brief Hacks list impl. over scope structure
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.0
 * @date 2024-04-09
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

/**
 * Gets the adaptive max number of jumps for a given index
 */
static inline long adaptiveSkipMax(long index) {
  return index <= 16     ? index / 8
         : index <= 32   ? 1 + index / 16
         : index <= 64   ? 2 + index / 32
         : index <= 128  ? 3 + index / 64
         : index <= 256  ? 4 + index / 128
         : index <= 512  ? 5 + index / 256
         : index <= 1024 ? 6 + index / 512
                         : 7 + index / SKIP_LIST_MAX;
}

Node *addToList(Scope *list, Node *node, Context *ctx) {
#ifdef DEBUG
  if (!list->isList) {
    fatalOut(__FILE__, __func__, MSG_NOT_LIST);
  }
#endif

  if (!node) {
    return NULL;
  }

  // If mutable, take copy and ensure copy is immutable
  if (node->op) {
    node = copyNode(node, false, ctx);
    node->op = false;
  }

  Entry *entry = createListEntry(list->size, node);

  lockResource(&list->lock);

  if (list->entries) {
    if (list->ck != node->ck && !checkAlias(list->ck, node)) {
      unlockResource(&list->lock);
      freeListEntry(entry);
      const char *type = fatType(node->type);
      char *msg = join4("adding ", type, " item to List/", list->ck->name);
      ctx->cur->node = node;  // ensure source reference to error
      return createError(msg, false, ckTypeError, ctx);
    }

    if (!adaptiveSkipMod(list->size)) {
      list->quick1->skip = entry;
      list->quick1 = entry;  // points skip-list "next"
    }

    list->quick2->next = entry;
    list->quick2 = entry;  // points "last"

  } else {
    list->ck = node->type == FatMethod ? checkMethod : node->ck;
    list->entries = entry;
    list->quick1 = entry;  // points skip-list "next"
    list->quick2 = entry;  // points "last"
  }
  list->size++;

  unlockResource(&list->lock);

  unlockNode(node);
  return NULL;  // success
}

Scope *evalList(Scope *scope, Node *head, Context *ctx) {
  Scope *list = createList();

  Node *error = NULL;
  for (Node *node = head; node; node = node->seq) {
    error = addToList(list, evalNode(scope, node, ctx), ctx);
    if (error) {
      if (!hasErrorHandler(ctx)) {
        stackOut(__FILE__, __func__, error->val, ctx);
      }
      unlockNode(error);
      break;
    }
  }

  return list;
}

Entry *getByIndex(Scope *list, long index) {
#ifdef DEBUG
  if (!list->isList) {
    fatalOut(__FILE__, __func__, MSG_NOT_LIST);
  }
#endif

  if (index == 0) {
    return list->entries;  // first
  }

  if (index == list->size - 1) {
    return list->quick2;  // last
  }

  if (list->cached) {
    if (index == list->cached->key.i) {
#ifdef DEBUG
      cacheHits++;
#endif
      return list->cached;
    }
    if (index == list->cached->key.i + 1) {
      list->cached = list->cached->next;
#ifdef DEBUG
      cacheHits++;
#endif
      return list->cached;
    }
  }

  long pos = adaptiveSkipMax(index);
  Entry *entry = list->entries;
  for (long i = 0; i < pos; i++) {
    entry = entry->skip;
  }
  pos = adaptiveSkipMod(index);
  for (long i = 0; i < pos; i++) {
#ifdef DEBUG
    if (!entry) {
      fatalOut(__FILE__, __func__, MSG_N_PTR);
      return NULL;
    }
#endif
    entry = entry->next;
  }

  list->cached = entry;
  return entry;
}

Node *getInList(Scope *list, long index, Context *ctx) {
#ifdef DEBUG
  if (!list->isList) {
    fatalOut(__FILE__, __func__, MSG_NOT_LIST);
  }

  if (debugLogs) {
    auto_str num = ofInt(index);
    logDebug2(__FILE__, __func__, MSG_INDEX, num);
  }
#endif

  if (!list->size) {
    return createError(MSG_IS_EMPTY, true, ckIndexError, ctx);
  }

  if (index < 0) {
    index = list->size + index;
  }

  if (index < 0 || index >= list->size) {
    return createError(MSG_BOUNDS, true, ckIndexError, ctx);
  }

  return getByIndex(list, index)->data;
}

Node *selectFromList(Scope *list, long start, long end, Context *ctx) {
#ifdef DEBUG
  if (!list->isList) {
    fatalOut(__FILE__, __func__, MSG_NOT_LIST);
  }

  if (debugLogs) {
    auto_str num1 = ofInt(start);
    auto_str num2 = ofInt(end);
    auto_str msg = join4(MSG_INDEX GUIDE, num1, " : ", num2);
    logDebug(__FILE__, __func__, msg);
  }
#endif

  if (start < 0) {
    start = list->size + start;
  }
  if (start < 0) {
    start = 0;
  }
  if (end < 0) {
    end = list->size + end;
  }

  Scope *result = createList();

  bool isEmpty = !list->size || end < start || start >= list->size || end < 0;

  if (!isEmpty) {
    Entry *entry = getByIndex(list, start);
    while (entry && (start <= end)) {
      addToList(result, entry->data, ctx);
      entry = entry->next;
      start++;
    }
  }

  return runtimeCollection(result, ctx);
}

/**
 * Reverse helper, uses local buffer for up to "chunk size" items at once
 */
__attribute__((noinline))  // prevent buffer allocation in recursive call stack
static void
reverseChunk(Scope *dest, Entry *src, Entry *until, Context *ctx) {
  Node *buffer[SKIP_LIST_MAX];
  int bufferSize = 0;

  // Adds chunk items in reversed order to dest list
  for (; src && src != until; src = src->next) {
    buffer[bufferSize++] = src->data;
  }
  while (bufferSize) {
    addToList(dest, buffer[--bufferSize], ctx);
  }
}

/**
 * Reverse helper, recursively reverses the skip list by chunks
 * (chunks minimize the recursion depth to ~ n / SKIP_LIST_MAX)
 */
static void reverseRecursive(Scope *dest, Entry *src, Context *ctx) {
  if (!src) {
    return;
  }

  reverseRecursive(dest, src->skip, ctx);
  reverseChunk(dest, src, src->skip, ctx);
}

Node *reverseList(Scope *list, Context *ctx) {
#ifdef DEBUG
  if (!list->isList) {
    fatalOut(__FILE__, __func__, MSG_NOT_LIST);
  }
#endif

  Scope *reversed = createList();
  pushStack(ctx, __func__, NULL, reversed);

  reverseRecursive(reversed, list->entries, ctx);

  Node *result = runtimeCollection(reversed, ctx);
  popStack(ctx, 1);
  return result;
}

/**
 * Quicksort helper to compare nodes (assumes nodes of same type)
 */
static bool isGreater(Node *a, Node *b, Node *key) {
#ifdef DEBUG
  if (!a || !b) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
    exit(EXIT_FAILURE);
  }

  if (a->type != b->type) {
    fatalOut(__FILE__, __func__, "unexpected " MSG_MISMATCH);
  }
#endif

  if (key) {
    if (a->scp->isList) {
      long index = (long)key->num.f;
      if (index >= a->scp->size || index >= b->scp->size) {
        return index < b->scp->size;
      }
      a = getByIndex(a->scp, index)->data;
      b = getByIndex(b->scp, index)->data;
    } else {
      a = getValueOf(a->scp, key->val);
      b = getValueOf(b->scp, key->val);
    }

    if (!a || !b) {
      return !!b;
    }

    if (a->type != b->type) {
      return a > b;  // pointer address (arbitrary, but consistent)
    }
  }

  switch (a->type) {
    case FatBoolean:
      return a->num.b > b->num.b;

    case FatNumber:
      return a->num.f > b->num.f;

    case FatHugeInt:
      return hugeIsLess(*b->num.h, *a->num.h);

    case FatText:
    case FatTemp:
      return *a->val > *b->val || strcmp(a->val, b->val) > 0;

    case FatList:
    case FatScope:
      if (a->scp && b->scp) {
        return a->scp->size > b->scp->size;
      }
      return !!b->scp;

    default:
      return a > b;  // pointer address (arbitrary, but consistent)
  }
}

/**
 * Quicksort helper to swap nodes, in-place (no entry re-link is needed)
 */
static inline void swapNodes(Entry *a, Entry *b) {
  Node *aux = a->data;
  a->data = b->data;
  b->data = aux;
}

/**
 * Quicksort helper to find pivot (heuristic optimization)
 * This method is generally good at avoiding worst-case scenario of O(n^2)
 * time complexity, which can occur with a consistently bad pivot choice
 */
static void setRandomPivot(Entry *head, Entry *last) {
  size_t length = 0;
  for (Entry *current = head; current != last; current = current->next) {
    length++;
  }

  size_t randomIndex = (size_t)rnGen() % length;
  Entry *pivot = head;
  for (size_t i = 0; i < randomIndex; i++) {
    pivot = pivot->next;
  }

  swapNodes(head, pivot);
}

/**
 * Quicksort, complexity is O(n log n) in the average case, recursive impl.
 */
void quicksort(Entry *head, Entry *last, Node *key) {
  while (head && last && head != last) {
    setRandomPivot(head, last);

    Entry *i = head;
    for (Entry *j = head->next; j && j != last->next; j = j->next) {
      if (isGreater(head->data, j->data, key)) {
        swapNodes((i = i->next), j);
      }
    }

    if (head != i) {
      swapNodes(head, i);
      quicksort(head, i, key);
    }
    head = i->next;
  }
}
