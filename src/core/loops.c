/**
 * @file loops.c
 * @brief Implements map, range, while loops
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-28
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

static Node *getMapper(Node *body, Context *ctx) {
  if (!body) {
    return NULL;
  }
  if (body->type == FatMethod) {
    return body;
  }
  if (body->type == FatEntry) {
    Node *mapper = getCtxValueOf(ctx, body->val);
    return IS_FAT_TYPE(mapper, FatMethod) ? mapper : NULL;
  }
  return NULL;
}

static inline Node *mapApply(Scope *layer, Entry *arg, Node *fn, Context *ctx) {
  initLayer(layer);
  layer->entries = arg;
  layer->size = 1;
  Node *mapped = interpret(layer, fn, ctx);
  wipeScope(layer, arg);
  layer->entries = NULL;
  layer->quick1 = NULL;
  layer->quick2 = NULL;
  layer->cached = NULL;
  return mapped;
}

static Node *mapFail(const char *func, Node *error, Scope *res, Context *ctx) {
  popStack(ctx, 3);  // result, mapper->scp, layer
  trackScope(res);
  if (debugLogs) {
    logDebug(__FILE__, func, "fail");
  }
  return error;
}

static Node *mapScope(Node *coll, Node *mapper, Context *ctx) {
  if (!mapper) {
    return createError("map" GUIDE MSG_NO_METH, true, ckCallError, ctx);
  }

  // Scopes can only be mapped with methods that have Text as first parameter
  if (mapper->head && mapper->head->ck &&
      !isAlias(checkText, mapper->head->ck)) {
    return createError("mapper" GUIDE MSG_MISMATCH, true, ckTypeError, ctx);
  }

  lockResource(&coll->scp->lock);
  Entry *entry = coll->scp->entries;
  unlockResource(&coll->scp->lock);

  Scope *result = createList();

  if (!entry) {
    return runtimeCollection(result, ctx);
  }

  if (debugLogs) {
    logDebug(__FILE__, __func__, "start");
  }

  char *key = mapper->head ? mapper->head->val : "_";

  Scope layer = {0};
  pushStack(ctx, __func__, coll, result);
  pushStack(ctx, __func__, mapper, mapper->scp);
  pushStack(ctx, __func__, mapper, &layer);

  while (entry) {
    if (!entry->data) {
      lockResource(&coll->scp->lock);
      entry = entry->next;
      unlockResource(&coll->scp->lock);
      continue;  // skip over null values (e.g. import flags)
    }

    Entry arg = {0};
    arg.key.s = key;
    arg.data = runtimeTextDup(entry->key.s, ctx);

    lockResource(&coll->scp->lock);
    coll->scp->cached = entry;
    entry = entry->next;
    unlockResource(&coll->scp->lock);

    Node *mapped = mapApply(&layer, &arg, mapper->body, ctx);
    if (ctx->failureEvent) {
      unlockNode(mapped);
      return mapFail(__func__, ctx->failureEvent, result, ctx);
    }

    Node *error = addToList(result, mapped, ctx);
    if (error) {
      return mapFail(__func__, error, result, ctx);
    }
  }

  Node *listResult = runtimeCollection(result, ctx);
  popStack(ctx, 3);  // result, mapper->scp, layer

  if (debugLogs) {
    logDebug(__FILE__, __func__, "end");
  }

  return listResult;
}

static Node *mapList(Node *coll, Node *mapper, Context *ctx) {
  if (!mapper) {
    return createError("map" GUIDE MSG_NO_METH, true, ckCallError, ctx);
  }

  Scope *result = createList();

  if (!coll->scp->entries) {
    return runtimeCollection(result, ctx);
  }

  // lists can only be processed with methods that match the type of list items
  if (mapper->head && !checkAlias(mapper->head->ck, coll->scp->entries->data)) {
    return createError("mapper" GUIDE MSG_MISMATCH, true, ckTypeError, ctx);
  }

  if (debugLogs) {
    logDebug(__FILE__, __func__, "start");
  }

  Scope layer = {0};
  pushStack(ctx, __func__, coll, result);
  pushStack(ctx, __func__, mapper, mapper->scp);
  pushStack(ctx, __func__, mapper, &layer);

  // note: lists are append only no lock needed to access head
  char *key = mapper->head ? mapper->head->val : "_";
  for (Entry *entry = coll->scp->entries; entry; entry = entry->next) {
    Entry arg = {0};
    arg.key.s = key;
    arg.data = entry->data;

    Node *mapped = mapApply(&layer, &arg, mapper->body, ctx);
    if (ctx->failureEvent) {
      unlockNode(mapped);
      return mapFail(__func__, ctx->failureEvent, result, ctx);
    }

    Node *error = addToList(result, mapped, ctx);
    if (error) {
      return mapFail(__func__, error, result, ctx);
    }
  }

  Node *listResult = runtimeCollection(result, ctx);
  popStack(ctx, 3);  // result, mapper->scp, layer

  if (debugLogs) {
    logDebug(__FILE__, __func__, "end");
  }

  return listResult;
}

static Node *mapRange(IntTuple range, Node *mapper, Context *ctx) {
  if (!mapper) {
    return createError("range" GUIDE MSG_NO_METH, true, ckCallError, ctx);
  }

  // ranges can only be processed with methods that take numbers as argument
  if (mapper->head && mapper->head->ck &&
      !isAlias(checkNumber, mapper->head->ck)) {
    return createError("mapper" GUIDE MSG_MISMATCH, true, ckTypeError, ctx);
  }

  if (debugLogs) {
    logDebug(__FILE__, __func__, "start");
  }

  Scope *result = createList();
  pushStack(ctx, __func__, mapper, result);

  Scope layer = {0};
  pushStack(ctx, __func__, mapper, mapper->scp);
  pushStack(ctx, __func__, mapper, &layer);

  const long start = range._1;
  const long end = range._2;
  const bool isAsc = start < end;
  const long step = isAsc ? 1L : -1L;

  char *key = mapper->head ? mapper->head->val : "_";

  for (long i = start; isAsc ? i <= end : i >= end; i += step) {
    Entry arg = {0};
    arg.key.s = key;
    arg.data = runtimeNumber((double)i, ctx);

    Node *mapped = mapApply(&layer, &arg, mapper->body, ctx);
    if (ctx->failureEvent) {
      unlockNode(mapped);
      return mapFail(__func__, ctx->failureEvent, result, ctx);
    }

    Node *error = addToList(result, mapped, ctx);
    if (error) {
      return mapFail(__func__, error, result, ctx);
    }
  }

  Node *listResult = runtimeCollection(result, ctx);
  popStack(ctx, 3);  // result, mapper->scp, layer

  if (debugLogs) {
    logDebug(__FILE__, __func__, "end");
  }

  return listResult;
}

static inline void adjustHalfOpenRange(IntTuple *range) { range->_2 -= 1L; }

static inline bool isRangeOp(const OpType op) {
  return op == OpRange || op == OpHORange;
}

static inline void verifyContextTimeout(Context *ctx) {
  if (ctx->timeout > 0 && ctx->timeout > getCurrentMs(CLOCK_MONOTONIC_COARSE)) {
    atomic_store(&ctx->isCanceled, true);
  }
}

static Node *evalLoop(Scope *scope, Node *node, Context *ctx) {
  if (!node->head) {
    return NULL;
  }

  Node *head = node->head;
  Node *body = node->body;
  Node *result = NULL;

  switch (head->type) {
    case FatUnary:
      if (isRangeOp(head->op)) {
        // Map over range (..index)
        Node *end = evalNode(scope, head->body, ctx);
        if (IS_FAT_TYPE(end, FatNumber)) {
          IntTuple range;
          range._1 = 0;  // assume zero
          range._2 = (long)end->num.f;
          if (head->op == OpHORange) {
            adjustHalfOpenRange(&range);
            if (range._1 > range._2) {
              result = runtimeCollection(createList(), ctx);
            }
          }
          if (!result) {
            result = mapRange(range, getMapper(body, ctx), ctx);
          }
        } else {
          result = createError(MSG_I_IDX "range", true, ckIndexError, ctx);
        }
        unlockNode(end);
        return result;
      }
      break;
    case FatExpr:
      if (isRangeOp(head->op)) {
        // Map over range (index..index)
        Node *start = evalNode(scope, head->head, ctx);
        Node *end = evalNode(scope, head->body, ctx);
        if (IS_FAT_TYPE(start, FatNumber) && IS_FAT_TYPE(end, FatNumber)) {
          IntTuple range;
          range._1 = (long)start->num.f;
          range._2 = (long)end->num.f;
          if (head->op == OpHORange) {
            adjustHalfOpenRange(&range);
            if (range._1 > range._2) {
              result = runtimeCollection(createList(), ctx);
            }
          }
          if (!result) {
            result = mapRange(range, getMapper(body, ctx), ctx);
          }
        } else {
          result = createError(MSG_I_IDX "range", true, ckIndexError, ctx);
        }
        unlockNode(start);
        unlockNode(end);
        return result;
      }
      break;
    default:
      break;
  }

  Node *entry = evalNode(scope, head, ctx);

  if (!entry) {
    return NULL;
  }

  if (entry->type == FatScope) {
    result = mapScope(entry, getMapper(body, ctx), ctx);

  } else if (entry->type == FatList) {
    result = mapList(entry, getMapper(body, ctx), ctx);

  } else if (IS_FAT_TYPE(body, FatMethod)) {
    result = createError("bad while-loop body", true, ckSyntaxError, ctx);
  } else {
    // While
    if (debugLogs) {
      logDebug(__FILE__, __func__, "start");
    }

    while (booleanOf(entry) && !atomic_load(&ctx->isCanceled)) {
      unlockNode(entry);
      unlockNode(result);
      result = interpret(scope, body, ctx);

      if (ctx->failureEvent) {
        break;
      }

      entry = evalNode(scope, head, ctx);

      verifyContextTimeout(ctx);
    }

    if (debugLogs) {
      logDebug(__FILE__, __func__, "end");
    }
  }

  unlockNode(entry);
  return result;
}
