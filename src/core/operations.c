/**
 * @file operations.c
 * @brief Executes actual unary/binary expression operations
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-28
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

/**
 * Print <primitive a> <op> <primitive b> using debug logger
 */
static void logBinaryOp(const char *func, OpType op, Node *a, Node *b) {
  auto_str valA = toString(a);
  auto_str valB = toString(b);
  auto_str msg = join5(valA, " <", opType(op), "> ", valB);
  logDebug(__FILE__, func, msg);
}

/**
 * Create error for unsupported operation
 */
static Node *unsupportedOp(const char *ref, const NodeType type, Context *ctx) {
  char *msg = join4(MSG_UNSUP " operation" GUIDE, ref, " ", fatType(type));
  return createError(msg, false, ckTypeError, ctx);
}

static inline Type *getTypePromotion(Type *ckA, Type *ckB) {
  return ckA == ckB ? ckA : NULL;
}

static Node *operUnary(Scope *scope, Node *node, Context *ctx) {
  OpType op = node->op;

  if (!node->body) {
    char *err = join2(MSG_M_PAR " (unary)", opType(op));
    return createError(err, false, ckSyntaxError, ctx);
  }

  Node *x = evalNode(scope, node->body, ctx);

  if (debugLogs) {
    auto_str val = toString(x);
    auto_str msg = join3(opType(op), GUIDE, val);
    logDebug(__FILE__, __func__, msg);
  }

  Node *result = NULL;

  switch (op) {
    case OpNot:
      result = RUNTIME_BOOLEAN(!booleanOf(x));
      break;

    case OpBool:
      result = RUNTIME_BOOLEAN(booleanOf(x));
      break;

    case OpMinus:
      if (IS_FAT_TYPE(x, FatNumber)) {
        result = runtimeNumber(-x->num.f, ctx);
        break;
      }
      FALL_THROUGH;

    default:
      result = unsupportedOp(opType(op), x ? x->type : FatVoid, ctx);
  }

  unlockNode(x);
  return result;
}

static Node *operBooleans(OpType op, Node *a, Node *b, Context *ctx) {
  if (debugLogs) {
    logBinaryOp(__func__, op, a, b);
  }

  Node *result = NULL;

  switch (op) {
    case OpPercent:
      result = RUNTIME_BOOLEAN(a->num.b ^ b->num.b);
      break;

    case OpEqual:
      result = RUNTIME_BOOLEAN(a->num.b == b->num.b);
      break;

    case OpNotEq:
      result = RUNTIME_BOOLEAN(a->num.b != b->num.b);
      break;

    default:
      return unsupportedOp(opType(op), FatBoolean, ctx);
  }

  Type *promoted = getTypePromotion(a->ck, b->ck);
  if (promoted) {
    result->ck = promoted;
  }
  return result;
}

static Node *operChunks(OpType op, Node *a, Node *b, Context *ctx) {
  if (debugLogs) {
    logBinaryOp(__func__, op, a, b);
  }

  size_t sizeA = a->num.s;
  size_t sizeB = b->num.s;

  if (op == OpPlus) {
    size_t totalSize = sizeA + sizeB;
    char *newChunk = FRY_ALLOC(totalSize);

    memcpy(newChunk, a->val, sizeA);
    memcpy(newChunk + sizeA, b->val, sizeB);

    Node *result = runtimeChunk(newChunk, totalSize, ctx);

    Type *promoted = getTypePromotion(a->ck, b->ck);
    if (promoted) {
      result->ck = promoted;
    }
    return result;
  }

  if (op == OpEqual) {
    if (sizeA != sizeB) {
      return falseSingleton;
    }
    return RUNTIME_BOOLEAN(memcmp(a->val, b->val, sizeA) == 0);
  }

  if (op == OpNotEq) {
    if (sizeA != sizeB) {
      return trueSingleton;
    }
    return RUNTIME_BOOLEAN(memcmp(a->val, b->val, sizeA) != 0);
  }

  return unsupportedOp(opType(op), FatChunk, ctx);
}

static Node *operNumbers(OpType op, Node *a, Node *b, Context *ctx) {
  if (debugLogs) {
    logBinaryOp(__func__, op, a, b);
  }

  Node *result = NULL;

  switch (op) {
    case OpPlus:
      result = runtimeNumber(a->num.f + b->num.f, ctx);
      break;

    case OpMinus:
      result = runtimeNumber(a->num.f - b->num.f, ctx);
      break;

    case OpMultiply:
      result = runtimeNumber(a->num.f * b->num.f, ctx);
      break;

    case OpPow:
      result = runtimeNumber(pow(a->num.f, b->num.f), ctx);
      break;

    case OpSlash:
      result = runtimeNumber(a->num.f / b->num.f, ctx);
      break;

    case OpPercent:
      result = runtimeNumber(fmod(a->num.f, b->num.f), ctx);
      break;

    case OpEqual:
      return RUNTIME_BOOLEAN(floatEq(a->num.f, b->num.f));

    case OpNotEq:
      return RUNTIME_BOOLEAN(!floatEq(a->num.f, b->num.f));

    case OpLess:
      return RUNTIME_BOOLEAN(a->num.f < b->num.f);

    case OpLessEq:
      return RUNTIME_BOOLEAN(a->num.f <= b->num.f);

    case OpMore:
      return RUNTIME_BOOLEAN(a->num.f > b->num.f);

    case OpMoreEq:
      return RUNTIME_BOOLEAN(a->num.f >= b->num.f);

    default:
      return unsupportedOp(opType(op), FatNumber, ctx);
  }

  Type *promoted = getTypePromotion(a->ck, b->ck);
  if (promoted) {
    result->ck = promoted;
  }
  return result;
}

static Node *operHugeInts(OpType op, Node *a, Node *b, Context *ctx) {
  if (debugLogs) {
    logBinaryOp(__func__, op, a, b);
  }

  Node *result = NULL;
  HugeInt temp = {0};

  switch (op) {
    case OpPlus:
      if (!hugeAdd(temp, *a->num.h, *b->num.h)) {
        return createError(MSG_H_OVER, true, ckValueError, ctx);
      }
      result = runtimeHuge(temp, ctx);
      break;

    case OpMinus:
      if (!hugeSub(temp, *a->num.h, *b->num.h)) {
        return createError(MSG_H_UNDER, true, ckValueError, ctx);
      }
      result = runtimeHuge(temp, ctx);
      break;

    case OpMultiply:
      if (!hugeMul(temp, *a->num.h, *b->num.h)) {
        return createError(MSG_H_OVER, true, ckValueError, ctx);
      }
      result = runtimeHuge(temp, ctx);
      break;

    case OpPow:
      if (!hugeExp(temp, *a->num.h, *b->num.h)) {
        return createError(MSG_H_OVER, true, ckValueError, ctx);
      }
      result = runtimeHuge(temp, ctx);
      break;

    case OpSlash:
      if (hugeEq(*b->num.h, temp)) {
        return createError(MSG_H_DIV_Z, true, ckValueError, ctx);
      }
      hugeDiv(temp, *a->num.h, *b->num.h);
      result = runtimeHuge(temp, ctx);
      break;

    case OpPercent:
      if (hugeEq(*b->num.h, temp)) {
        return createError(MSG_H_MOD_Z, true, ckValueError, ctx);
      }
      hugeMod(temp, *a->num.h, *b->num.h);
      result = runtimeHuge(temp, ctx);
      break;

    case OpEqual:
      return RUNTIME_BOOLEAN(hugeEq(*a->num.h, *b->num.h));

    case OpNotEq:
      return RUNTIME_BOOLEAN(!hugeEq(*a->num.h, *b->num.h));

    case OpLess:
      return RUNTIME_BOOLEAN(hugeIsLess(*a->num.h, *b->num.h));

    case OpLessEq:
      return RUNTIME_BOOLEAN(!hugeIsLess(*b->num.h, *a->num.h));

    case OpMore:
      return RUNTIME_BOOLEAN(hugeIsLess(*b->num.h, *a->num.h));

    case OpMoreEq:
      return RUNTIME_BOOLEAN(!hugeIsLess(*a->num.h, *b->num.h));

    default:
      return unsupportedOp(opType(op), FatHugeInt, ctx);
  }

  Type *promoted = getTypePromotion(a->ck, b->ck);
  if (promoted) {
    result->ck = promoted;
  }
  return result;
}

static inline Node *concatTexts(Node *a, Node *b, Context *ctx) {
  const size_t sizeA = a->num.s;
  const size_t sizeB = b->num.s;
  const size_t totalSize = sizeA + sizeB;
  char *newText = FRY_ALLOC(totalSize + 1);
  memcpy(newText, a->val, sizeA);
  memcpy(newText + sizeA, b->val, sizeB);
  newText[totalSize] = '\0';
  return runtimeText(newText, totalSize, ctx);
}

static Node *operTexts(OpType op, Node *a, Node *b, Context *ctx) {
  if (debugLogs) {
    logBinaryOp(__func__, op, a, b);
  }

  char *result = NULL;

  switch (op) {
    case OpPlus:
      return concatTexts(a, b, ctx);

    case OpMinus:
      result = replaceAll(a->val, b->val, "");
      return runtimeText(result, strlen(result), ctx);

    case OpEqual:
      if (a->num.s != b->num.s) {
        return falseSingleton;
      }
      return RUNTIME_BOOLEAN(FAST_STR_EQ(a->val, b->val));

    case OpNotEq:
      if (a->num.s != b->num.s) {
        return trueSingleton;
      }
      return RUNTIME_BOOLEAN(!FAST_STR_EQ(a->val, b->val));

    case OpLess:
      return RUNTIME_BOOLEAN(strcmp(a->val, b->val) < 0);

    case OpLessEq:
      return RUNTIME_BOOLEAN(strcmp(a->val, b->val) <= 0);

    case OpMore:
      return RUNTIME_BOOLEAN(strcmp(a->val, b->val) > 0);

    case OpMoreEq:
      return RUNTIME_BOOLEAN(strcmp(a->val, b->val) >= 0);

    default:
      return unsupportedOp(opType(op), FatText, ctx);
  }
}

static bool containsValue(Scope *scp, Node *data) {
  Entry *entry = NULL;
  for (entry = scp->entries; entry; entry = entry->next) {
    if (nodeEq(entry->data, data)) {
      break;
    }
  }
  return entry != NULL;
}

static Node *concatLists(Scope *a, Scope *b, Context *ctx) {
  for (Entry *entry = b->entries; entry; entry = entry->next) {
    Node *error = addToList(a, entry->data, ctx);
    if (error) {
      return error;
    }
  }
  return NULL;
}

static Node *diffLists(Scope *result, Scope *a, Scope *b, Context *ctx) {
  for (Entry *entry = a->entries; entry; entry = entry->next) {
    if (!containsValue(b, entry->data) && !containsValue(result, entry->data)) {
      Node *error = addToList(result, entry->data, ctx);
      if (error) {
        return error;
      }
    }
  }
  return NULL;
}

Scope *mergeScopes(Scope *a, Scope *b, Context *ctx) {
  Scope *scope = copyCollection(b, false, ctx);
  lockResource(&a->lock);
  for (Entry *entry = a->entries; entry; entry = entry->next) {
    if (!getValueOf(scope, entry->key.s)) {
      addToScope(scope, entry->key.s, entry->data);
    }
  }
  unlockResource(&a->lock);
  Type *promoted = getTypePromotion(a->ck, b->ck);
  scope->ck = promoted ? promoted : checkScope;
  return scope;
}

static Scope *diffScopes(Scope *a, Scope *b) {
  Scope *scope = createScope();
  scope->ck = a->ck;
  lockResource(&a->lock);
  for (Entry *entry = a->entries; entry; entry = entry->next) {
    Node *aux = getValueOf(b, entry->key.s);
    if (!aux || !nodeEq(entry->data, aux)) {
      addToScope(scope, entry->key.s, entry->data);
    }
  }
  unlockResource(&a->lock);
  Type *promoted = getTypePromotion(a->ck, b->ck);
  scope->ck = promoted ? promoted : checkScope;
  return scope;
}

// generic operation handling for collections (both scopes and lists)
static Node *operCollection(OpType op, Scope *a, Scope *b, Context *ctx) {
  switch (op) {
    case OpEqual:
      return RUNTIME_BOOLEAN(scopeEq(a, b));

    case OpNotEq:
      return RUNTIME_BOOLEAN(!scopeEq(a, b));

    default:
      return unsupportedOp(opType(op), a->isList ? FatList : FatScope, ctx);
  }
}

static Node *operLists(OpType op, Node *a, Node *b, Context *ctx) {
#ifdef DEBUG
  if (debugLogs) {
    logBinaryOp(__func__, op, a, b);  // not very useful
  }
  if (!a->scp || !b->scp) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
  }
#endif

  Scope *list = NULL;
  Node *error = NULL;

  switch (op) {
    case OpPlus:
      list = copyCollection(a->scp, false, ctx);
      error = concatLists(list, b->scp, ctx);
      if (error) {
        trackScope(list);
        return error;
      }
      break;

    case OpMinus:
      list = createList();
      error = diffLists(list, a->scp, b->scp, ctx);
      if (error) {
        trackScope(list);
        return error;
      }
      break;

    default:
      return operCollection(op, a->scp, b->scp, ctx);
  }

  Node *result = runtimeCollection(list, ctx);
  Type *promoted = getTypePromotion(a->ck, b->ck);
  result->ck = promoted ? promoted : checkList;
  return result;
}

static Node *operScopes(OpType op, Node *a, Node *b, Context *ctx) {
#ifdef DEBUG
  if (debugLogs) {
    logBinaryOp(__func__, op, a, b);  // not very useful
  }
  if (!a->scp || !b->scp) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
  }
#endif

  Scope *scope = NULL;

  switch (op) {
    case OpPlus:
      if (a->scp->ck != b->scp->ck) {
        char *msg = join2(MSG_MISMATCH GUIDE, opType(op));
        return createError(msg, false, ckValueError, ctx);
      }
      scope = mergeScopes(a->scp, b->scp, ctx);
      return runtimeCollection(scope, ctx);

    case OpMinus:
      if (a->scp->ck != b->scp->ck) {
        char *msg = join2(MSG_MISMATCH GUIDE, opType(op));
        return createError(msg, false, ckValueError, ctx);
      }
      scope = diffScopes(a->scp, b->scp);
      return runtimeCollection(scope, ctx);

    default:
      return operCollection(op, a->scp, b->scp, ctx);
  }
}

static Node *quickHugeAdd(Node *a, Node *b, Context *ctx) {
  HugeInt temp = {0};
  if (!hugeAdd(temp, *a->num.h, *b->num.h)) {
    return createError(MSG_H_OVER, true, ckValueError, ctx);
  }
  memcpy(*a->num.h, temp, sizeof(HugeInt));
  return a;
}

static Node *quickHugeSub(Node *a, Node *b, Context *ctx) {
  HugeInt temp = {0};
  if (!hugeSub(temp, *a->num.h, *b->num.h)) {
    return createError(MSG_H_UNDER, true, ckValueError, ctx);
  }
  memcpy(*a->num.h, temp, sizeof(HugeInt));
  return a;
}

static Node *quickHugeMul(Node *a, Node *b, Context *ctx) {
  if (!hugeMul(*a->num.h, *a->num.h, *b->num.h)) {
    return createError(MSG_H_OVER, true, ckValueError, ctx);
  }
  return a;
}

static Node *quickHugePow(Node *a, Node *b, Context *ctx) {
  HugeInt temp = {0};
  if (!hugeExp(temp, *a->num.h, *b->num.h)) {
    return createError(MSG_H_OVER, true, ckValueError, ctx);
  }
  memcpy(*a->num.h, temp, sizeof(HugeInt));
  return a;
}

static Node *quickHugeDiv(Node *a, Node *b, Context *ctx) {
  HugeInt temp = {0};
  if (hugeEq(*b->num.h, temp)) {
    return createError(MSG_H_DIV_Z, true, ckValueError, ctx);
  }
  hugeDiv(temp, *a->num.h, *b->num.h);
  memcpy(*a->num.h, temp, sizeof(HugeInt));
  return a;
}

static Node *quickHugeMod(Node *a, Node *b, Context *ctx) {
  const HugeInt hugeZero = {0};
  if (hugeEq(*b->num.h, hugeZero)) {
    return createError(MSG_H_MOD_Z, true, ckValueError, ctx);
  }
  hugeMod(*a->num.h, *a->num.h, *b->num.h);
  return a;
}

static inline Node *quickChunkAdd(Node *a, Node *b) {
  const size_t sizeA = a->num.s;
  const size_t sizeB = b->num.s;
  const size_t totalSize = sizeA + sizeB;
  char *newChunk = FRY_REALLOC(a->val, totalSize);
  memcpy(newChunk + sizeA, b->val, sizeB);
  a->val = newChunk;
  a->num.s = totalSize;
  return a;
}

static inline Node *quickTextAdd(Node *a, Node *b) {
  const size_t sizeA = a->num.s;
  const size_t sizeB = b->num.s;
  const size_t totalSize = sizeA + sizeB;
  char *newText = FRY_REALLOC(a->val, totalSize + 1);
  memcpy(newText + sizeA, b->val, sizeB);
  newText[totalSize] = '\0';
  a->val = newText;
  a->num.s = totalSize;
  return a;
}

static inline Node *quickTextSub(Node *a, Node *b) {
  char *oldVal = a->val;
  a->val = replaceAll(a->val, b->val, "");
  a->num.s = strlen(a->val);
  free(oldVal);
  return a;
}

static inline Node *quickListAdd(Node *a, Node *b, Context *ctx) {
  Node *error = concatLists(a->scp, b->scp, ctx);
  return error ? error : a;
}

static inline Node *quickListSub(Node *a, Node *b, Context *ctx) {
  Scope *diff = createList();
  Node *error = diffLists(diff, a->scp, b->scp, ctx);
  if (error) {
    trackScope(diff);
    return error;
  }
  unbindScope(a);
  bindScope(a, diff);
  return a;
}

static inline Node *quickScopeAdd(Node *a, Node *b, Context *ctx) {
  if (a->scp->ck != b->scp->ck) {
    char *msg = join2(MSG_MISMATCH GUIDE, opType(OpPlus));
    return createError(msg, false, ckValueError, ctx);
  }
  Scope *merged = mergeScopes(a->scp, b->scp, ctx);
  unbindScope(a);
  bindScope(a, merged);
  return a;
}

static inline Node *quickScopeSub(Node *a, Node *b, Context *ctx) {
  if (a->scp->ck != b->scp->ck) {
    char *msg = join2(MSG_MISMATCH GUIDE, opType(OpPlus));
    return createError(msg, false, ckValueError, ctx);
  }
  Scope *merged = diffScopes(a->scp, b->scp);
  unbindScope(a);
  bindScope(a, merged);
  return a;
}

/**
 * Apply quick increment operations (+=), assumes 'a' and 'b' have same type
 */
static inline Node *operIncrement(Node *a, Node *b, Context *ctx) {
  switch (a->type) {
    case FatBoolean:
      a->num.b = b->num.b;
      return a;

    case FatNumber:
      a->num.f += b->num.f;
      return a;

    case FatHugeInt:
      return quickHugeAdd(a, b, ctx);

    case FatChunk:
      return quickChunkAdd(a, b);

    case FatText:
      return quickTextAdd(a, b);

    case FatList:
      return quickListAdd(a, b, ctx);

    case FatScope:
      return quickScopeAdd(a, b, ctx);

    default:
      return NULL;
  }
}

/**
 * Apply quick decrement operations (-=), assumes 'a' and 'b' have same type
 */
static inline Node *operDecrement(Node *a, Node *b, Context *ctx) {
  switch (a->type) {
    case FatNumber:
      a->num.f -= b->num.f;
      return a;

    case FatHugeInt:
      return quickHugeSub(a, b, ctx);

    case FatText:
      return quickTextSub(a, b);

    case FatList:
      return quickListSub(a, b, ctx);

    case FatScope:
      return quickScopeSub(a, b, ctx);

    default:
      return NULL;
  }
}

/**
 * Apply multiply-by operations (*=), assumes 'a' and 'b' have same type
 */
static inline Node *operMultiply(Node *a, Node *b, Context *ctx) {
  switch (a->type) {
    case FatNumber:
      a->num.f *= b->num.f;
      return a;

    case FatHugeInt:
      return quickHugeMul(a, b, ctx);

    default:
      return NULL;
  }
}

/**
 * Apply power-by operations (**=), assumes 'a' and 'b' have same type
 */
static inline Node *operPower(Node *a, Node *b, Context *ctx) {
  switch (a->type) {
    case FatNumber:
      a->num.f = pow(a->num.f, b->num.f);
      return a;

    case FatHugeInt:
      return quickHugePow(a, b, ctx);

    default:
      return NULL;
  }
}

/**
 * Apply divide-by operations (/=), assumes 'a' and 'b' have same type
 */
static inline Node *operDivide(Node *a, Node *b, Context *ctx) {
  switch (a->type) {
    case FatNumber:
      a->num.f /= b->num.f;
      return a;

    case FatHugeInt:
      return quickHugeDiv(a, b, ctx);

    default:
      return NULL;
  }
}

/**
 * Apply modulus-by operations (%=), assumes 'a' and 'b' have same type
 */
static inline Node *operModulus(Node *a, Node *b, Context *ctx) {
  switch (a->type) {
    case FatBoolean:
      a->num.b = a->num.b ^ b->num.b;  // XOR
      return a;

    case FatNumber:
      a->num.f = fmod(a->num.f, b->num.f);
      return a;

    case FatHugeInt:
      return quickHugeMod(a, b, ctx);

    default:
      return NULL;
  }
}

/**
 * Apply a compound assign operations (+=, -=, *=, **=, /=, %=)
 */
static Node *operCompound(OpCode op, Node *a, Node *b, Context *ctx) {
  if (!a->op) {
    char *msg = join2(opType(OpModBy), GUIDE MSG_REASSIGN);
    return createError(msg, false, ckAssignError, ctx);
  }

  switch (op) {
    case OpIncrement:
      return operIncrement(a, b, ctx);

    case OpDecrement:
      return operDecrement(a, b, ctx);

    case OpMulBy:
      return operMultiply(a, b, ctx);

    case OpPowBy:
      return operPower(a, b, ctx);

    case OpDivBy:
      return operDivide(a, b, ctx);

    case OpModBy:
      return operModulus(a, b, ctx);

    default:
      return NULL;
  }
}
