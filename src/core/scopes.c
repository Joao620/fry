/**
 * @file scope.c
 * @brief Implements fat scopes / namespaces
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.1
 * @date 2024-04-25
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

void addToScope(Scope *scope, const char *key, Node *data) {
#ifdef DEBUG
  if (!key) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
  }
#endif

  Entry *toAdd = createScopeEntry(key, data);

  lockResource(&scope->lock);

  if (scope->entries) {
    Entry *entry = scope->entries;
    Entry *prev = NULL;

    // If small list, use simple strategy
    if (scope->size < MIN_QUICK_REF) {
      while (entry) {
        int cmp = strcmp(entry->key.s, key);
#ifdef DEBUG
        if (cmp == 0) {
          // duplicated key, should never happen, crash!
          fatalOut(__FILE__, __func__, join2("repeated key: ", key));
        }
#endif
        if (cmp >= 0) {
          break;  // found the insertion point
        }
        prev = entry;
        entry = entry->next;
      }
    } else {
      // Initialize reference tuple for skip list
      IntTuple refs;
      initQuickRefs(&refs, scope->size);

      long jumpStart = 0;
      Entry *skipList = entry;

      // Optimize the start of the search operation by jumping ahead
      if (scope->quick1 && strcmp(scope->quick1->next->key.s, key) < 0) {
        jumpStart = ((scope->size - 1) / 3) + 1;
        skipList = prev = scope->quick1;
        entry = prev->next;

        // Rebalance quick references
        if (scope->size % 3 == 0) {
          scope->quick1 = entry;
        }
      }

      bool done = false;

      // Start iterating from the (possibly adjusted) start point
      for (long i = jumpStart; entry; i++) {
        // Check if we are not in the range of quick refs
        if (entry->skip && entry->skip->next) {
          if ((i > refs._1 || i < refs._1 - SKIP_SIZE) &&
              (i > refs._2 || i < refs._2 - SKIP_SIZE)) {
            // Check if the key is bigger than the next skip entry's key.
            // If so, simply jump ahead...
            if (strcmp(entry->skip->next->key.s, key) < 0) {
              i += SKIP_SIZE;
              entry = entry->skip;
            }
          }
        }

        // If not done yet, check if key is found or insertion point
        if (!done) {
          int cmp = strcmp(entry->key.s, key);
#ifdef DEBUG
          if (cmp == 0) {
            // duplicated key, should never happen, crash!
            fatalOut(__FILE__, __func__, join2("repeated key: ", key));
          }
#endif
          if (cmp >= 0) {
            done = true;  // found the insertion point
          } else {
            prev = entry;  // walk prev pointer
          }
        }

        // Handle skip and quick reference
        if (i) {
          // If it's time to set a skip link, do it
          if (i % SKIP_SIZE == 0) {
            skipList = (skipList->skip = entry);
          }

          // Update quick references
          if (i >= refs._2) {
            if (i == refs._2) {
              scope->quick2 = entry;
            }
            if (done) {
              break;
            }
          } else if (i == refs._1) {
            scope->quick1 = entry;
          }
        }

        entry = entry->next;
      }
    }

    if (prev) {
      toAdd->next = prev->next;
      prev->next = toAdd;
    } else {
      toAdd->next = scope->entries;
      scope->entries = toAdd;
    }
  } else {
    scope->entries = toAdd;
  }

  scope->size++;
  scope->cached = toAdd;

  unlockResource(&scope->lock);

  unlockNode(data);
}

Scope *evalScope(Node *program, Context *ctx) {
  Scope *result = createScope();

  result->ck = checkScope;
  for (Node *node = program; node; node = node->seq) {
    Node *internalComputationResult =
        node->type == FatEntry
            ? evalAssign(result, node, getCtxValueOf(ctx, node->val), ctx)
            : evalNode(result, node, ctx);  // may fail silently FIXME

    unlockNode(internalComputationResult);
  }

  return result;
}

Node *removeFromScope(Scope *scope, char *key, Context *ctx) {
#ifdef DEBUG
  if (!key) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
  }
#endif

  bool found = false;
  Entry *quick1 = scope->quick1;
  Entry *quick2 = scope->quick2;
  Entry *prev = NULL;

  lockResource(&scope->lock);

  Entry *entry = scope->entries;
  Entry *skipList = entry;
  IntTuple refs;
  initQuickRefs(&refs, scope->size - 1);

  // Search for item and prepare changes
  for (long i = 0; entry; i++) {
    if (!found) {
      if (FAST_STR_EQ(entry->key.s, key)) {
        found = true;
      } else {
        // Rebuild skip list (prevent pointers to removed entry)
        entry->skip = NULL;
        if (i && (i % SKIP_SIZE == 0)) {
          skipList = (skipList->skip = entry);
        }
        prev = entry;
      }
    }

    // Update quick references
    if (i) {
      if (i >= refs._2) {
        if (i == refs._2) {
          quick2 = entry;
        }
        if (found) {
          break;
        }
      } else if (i == refs._1) {
        quick1 = entry;
      }
    }

    entry = entry->next;
  }

  // If found, apply the changes (if it's mutable)
  Node *error = NULL;
  Entry *toRemove = NULL;
  if (found) {
    toRemove = prev ? prev->next : scope->entries;

    if ((toRemove->data && toRemove->data->op) || isUnder(key[0])) {
#ifdef DEBUG
      if (debugLogs) {
        logDebug(__FILE__, __func__, key);
      }
#endif

      if (prev) {
        if (quick1 == toRemove) {
          quick1 = prev;
        }
        if (quick2 == toRemove) {
          quick2 = prev;
        }
        prev->next = toRemove->next;
      } else {
        scope->entries = toRemove->next;
      }

      scope->cached = NULL;  // invalidate cache

      // Apply rebalanced quick references
      if (scope->size > MIN_QUICK_REF) {
        scope->quick1 = quick1;
        scope->quick2 = quick2;
      } else {
        scope->quick1 = scope->quick2 = NULL;
      }
      scope->size--;

    } else {
      char *msg = join2("erase immutable" GUIDE, key);
      error = createError(msg, false, ckAssignError, ctx);
      toRemove = NULL;
    }
  }

  unlockResource(&scope->lock);

  if (toRemove) {
    freeScopeEntry(toRemove);
  }

  return error;
}

/**
 * @brief Traverse entries searching by key using skip list
 */
static Entry *seekEntry(Entry *entry, const char *key) {
  for (; entry; entry = entry->next) {
    // Try to jump via skip list
    for (; entry->skip; entry = entry->skip) {
      if (*entry->skip->key.s > *key) {
        break;
      }
      int cmp = strcmp(entry->skip->key.s, key);
      if (cmp == 0) {
        return entry->skip;
      }
      if (cmp > 0) {
        break;
      }
    }

    // Walk in linked list
    if (*entry->key.s < *key) {
      continue;
    }
    int cmp = strcmp(entry->key.s, key);
    if (cmp == 0) {
      return entry;
    }
    if (cmp > 0) {
      break;
    }
  }
  return NULL;
}

static inline Entry *jumpSeek(Entry *entry, const char *key) {
  return entry ? seekEntry(entry, key) : NULL;
}

/**
 * Seek for key in scope using quick refs
 */
static Entry *quickSeek(Scope *scope, const char *key) {
  Entry *res = jumpSeek(scope->quick2, key);
  if (res) {
    scope->cached = res;
    return res;
  }

  res = jumpSeek(scope->quick1, key);
  if (res) {
    scope->cached = res;
    return res;
  }

  res = seekEntry(scope->entries, key);
  if (res) {
    scope->cached = res;
    return res;
  }

  return NULL;
}

static Entry *getEntryOf(Scope *scope, const char *key) {
#ifdef DEBUG
  if (scope && scope->isList) {
    fatalOut(__FILE__, __func__, MSG_C_LST);
  }
  if (!key) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
  }
#endif

  Entry *cached = scope->cached;
  if (cached && FAST_STR_EQ(cached->key.s, key)) {
#ifdef DEBUG
    cacheHits++;
#endif
    return cached;
  }

  Entry *entry = quickSeek(scope, key);
#ifdef DEBUG
  if (debugLogs) {
    if (entry) {
      auto_str msg = toString(entry->data);
      logDebug2(__FILE__, __func__, key, msg);
    } else {
      logDebug2(__FILE__, __func__, key, MSG_N_FOUND);
    }
  }
#endif
  return entry;
}

Node *getValueOf(Scope *scope, const char *key) {
  Entry *entry = getEntryOf(scope, key);
  return entry ? entry->data : NULL;
}

static Entry *getCtxEntryOf(Context *ctx, const char *key) {
#ifdef DEBUG
  if (!ctx || !key) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
    return NULL;  // mitigate false positive on auto_check.sh
  }
#endif

  Scope *prev[FIND_SCP_DEDUP + 1] = {0};  // null-terminated
  const Frame *stack = ctx->stack;

  for (int i = atomic_load(&ctx->top); i; i--) {
    Scope *scope = stack[i].scp;
    if (scope && !scope->isList) {
      // Check if scope is in prevScopes
      bool alreadyChecked = false;
      for (int j = 0; prev[j]; j++) {
        if (scope == prev[j]) {
          alreadyChecked = true;
          break;
        }
      }
      if (!alreadyChecked) {
        Entry *entry = getEntryOf(scope, key);
        if (entry) {
          return entry;
        }
        // Shift previous and add current scope to the start
        memmove(&prev[1], &prev[0], sizeof(Scope *) * (FIND_SCP_DEDUP - 1));
        prev[0] = scope;
      }
    }
  }
  return NULL;
}

Node *getCtxValueOf(Context *ctx, const char *key) {
  Entry *entry = getCtxEntryOf(ctx, key);
  return entry ? entry->data : NULL;
}

Node *getParameter(Scope *scope, const char *name, NodeType type,
                   Context *ctx) {
  Node *node = getValueOf(scope, name);
  if (!node) {
    char *msg = join3(MSG_M_PAR, GUIDE, name);
    return createError(msg, false, ckCallError, ctx);
  }
  if (node->type != type) {
    char *msg = join3(MSG_MISMATCH, GUIDE, name);
    return createError(msg, false, ckTypeError, ctx);
  }
  return node;
}

static void logStoreEvent(const char *key, Node *val, OpCode isMut) {
  bool isTextType = val->type == FatText;
  auto_str msg = isTextType ? join3("'", val->val, "'") : toString(val);
  logDebug2(__FILE__, isMut ? "storeMut" : "storeImmut", key, msg);
}

/**
 * Apply copy-on-write strategy (reuse value if possible)
 */
static Node *cow(const char *key, Node *val, const OpCode isMut, Context *ctx) {
  // Is inserting mutable value or into a mutable entry?
  if (isMut || val->op) {
    // Is it from source or referenced somewhere already?
    if (!val->meta || atomic_load(&val->meta->refs)) {
      val = copyNode(val, false, ctx);  // untangle
    }

    // Apply mutability option of entry
    val->op = isMut;
  }

  if (debugLogs) {
    logStoreEvent(key, val, isMut);
  }

  return val;
}

static Node *swapEntryData(Entry *entry, Node *newData) {
  if (newData->meta) {
    atomic_fetch_add(&newData->meta->refs, 1);
  }

  Node *oldData = entry->data;
  entry->data = newData;

  if (oldData && oldData->meta) {
    atomic_fetch_sub(&oldData->meta->refs, 1);
  }
  return newData;
}

Node *upsertNode(Scope *scope, const char *key, Node *val, const OpCode isMut,
                 Context *ctx) {
  Entry *entry = getEntryOf(scope, key);

  if (entry) {
    // Is existing special entry (_) or empty
    if (isUnder(*key) || entry->data == NULL) {
      return swapEntryData(entry, cow(key, val, isMut, ctx));
    }

    Node *prevVal = entry->data;

    // Is previous value immutable?
    if (!prevVal->op) {
      char *msg = join2(MSG_REASSIGN GUIDE, key);
      return createError(msg, false, ckAssignError, ctx);
    }

    // Is entry already holding the exact same node?
    if (prevVal == val) {
      return val;  // short-circuit
    }

    // Are we trying to swap the base type?
    if (prevVal->type != val->type) {
      char *msg = join4(MSG_MISMATCH GUIDE, key, ": ", fatType(val->type));
      return createError(msg, false, ckTypeError, ctx);
    }

    val = cow(key, val, true, ctx);
    if (prevVal->ck) {
      val->ck = prevVal->ck;  // keep previous type alias
    }
    return swapEntryData(entry, val);
  }

  val = cow(key, val, isMut, ctx);
  addToScope(scope, key, val);
  return val;
}
