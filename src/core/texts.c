/**
 * @file texts.c
 * @brief Implements texts
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.0
 * @date 2024-04-09
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"
#include "../parser.h"

char *interpolate(char *text, Context *ctx) {
#ifdef DEBUG
  if (!text) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
#endif

  char *buffer = FRY_ALLOC(BUFF_LEN);

  bool isEscaped = false;
  size_t len = 0;  // final length

  for (size_t i = 0; text[i]; i++) {
    if (len >= BUFF_LEN) {
      fatalOut(__FILE__, __func__, MSG_BMO " (a)");
    } else if (isEscaped) {
      isEscaped = false;
      buffer[len] = text[i];
      len++;
    } else if (text[i] == MARK_ESC && text[i + 1] == '{') {
      isEscaped = true;
    } else if (text[i] == '{') {
      // Extract the source out of the template
      int opened = 0;
      size_t start = i + 1;
      while (text[i] && hasBracket(text[i], &opened)) {
        i++;
      }
      auto_str source = copyFragment(text + start, i - start);

      // Effectively execute the source code and keep result as string
      Node *result = evalSource(source, ctx);
      auto_str interpolated = toString(result);
      unlockNode(result);  // allow GC

      // Concatenate the result into the outcome buffer
      size_t iLen = strlen(interpolated);
      if (len + iLen >= BUFF_LEN) {
        fatalOut(__FILE__, __func__, MSG_BMO " (b)");
      }
      memcpy(&buffer[len], interpolated, iLen);
      len += iLen;
    } else {
      buffer[len] = text[i];
      len++;
    }
  }

  buffer[len] = '\0';
  return FRY_REALLOC(buffer, len + 1);
}

Node *getInText(Node *node, long index, Context *ctx) {
#ifdef DEBUG
  if (debugLogs) {
    auto_str num = ofInt(index);
    logDebug2(__FILE__, __func__, MSG_INDEX, num);
  }
#endif

  if (!node->val || !node->val[0]) {
    return createError(MSG_IS_EMPTY, true, ckIndexError, ctx);
  }

  long length = (long)utf8len(node->val);
  if (index < 0) {
    index = length + index;
  }

  if (index < 0 || index >= length) {
    return createError(MSG_BOUNDS, true, ckIndexError, ctx);
  }

  char *ptr = node->val;
  for (long i = 0; i < index; i++) {
    ptr = utf8next(ptr);
  }
  char *result = utf8char(ptr);
  return result ? runtimeText(result, strlen(result), ctx) : NULL;
}

Node *selectFromText(Node *node, long start, long end, Context *ctx) {
#ifdef DEBUG
  if (debugLogs) {
    auto_str num1 = ofInt(start);
    auto_str num2 = ofInt(end);
    auto_str msg = join4(MSG_INDEX GUIDE, num1, "..", num2);
    logDebug(__FILE__, __func__, msg);
  }
#endif

  long length = (long)utf8len(node->val);
  if (start < 0) {
    start = length + start;
  }
  if (start < 0) {
    start = 0;
  }
  if (end < 0) {
    end = length + end;
  }
  if (end >= length) {
    end = length - 1;
  }

  if ((!length) || (end < start) || (start >= length) || (end < 0)) {
    return runtimeText(strDup(""), 0, ctx);  // nothing to select
  }

  char *ptr = node->val;
  for (long i = 0; i < start; i++) {
    ptr = utf8next(ptr);
  }
  const char *head = ptr;
  long diff = end - start;
  for (long i = 0; i <= diff; i++) {
    ptr = utf8next(ptr);
  }
  size_t len = ptr - head;
  return runtimeText(copyFragment(head, len), len, ctx);
}
