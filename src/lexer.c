/**
 * @file lexer.c
 * @brief Builds a token reader from input
 * (inspired by https://lisperator.net/pltut/)
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.0
 * @date 2024-04-09
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "lexer.h"

bool allowDecode = false;

/*
 * Get current char from reader (while tokenizing)
 */
static inline char currentChar(Reader *reader) {
  return reader->source[reader->pos];
}

/**
 * Create fat token (except number)
 */
static inline Token *setValue(TokType type, char *val, char *src) {
  Token *tok = createToken(type, src);
  tok->val = val;
  return tok;
}

/**
 * Create fat number token (only)
 */
static inline Token *setNumber(double num, char *src) {
  Token *tok = createToken(TkNumb, src);
  tok->num = num;
  return tok;
}

/**
 * Read number token and update pos to end
 */
static Token *readNumber(Reader *reader, char *src) {
  char *text = reader->source + reader->pos;
  size_t len = 0;

  // Handle special HugeInt type
  if (isHugeHex(text)) {
    text += 2;
    advanceChar(reader);  // consume '0'
    advanceChar(reader);  // consume 'x'
    while (isHex(text[len])) {
      advanceChar(reader);
      len++;
    }
    if (len > (size_t)(HUGE_INT_SIZE * HUGE_FRAG)) {
      errorOut(__func__, MSG_BMO, src);
      return setValue(TkHuge, strDup("0"), src);
    }
    return setValue(TkHuge, copyFragment(text, len), src);
  }

  // Handle common Number type
  char buffer[NUMBER_MAX_LENGTH + 1];
  bool hasDot = false;
  bool isExpo = false;
  for (; isNumeric(text[0], &hasDot, &isExpo);
       text = reader->source + reader->pos) {
    if (text[0] == '.' && !isDigit(text[1])) {
      break;
    }
    if (len < NUMBER_MAX_LENGTH) {
      buffer[len++] = advanceChar(reader);  // copy
    } else {
      if (len == NUMBER_MAX_LENGTH) {  // error once
        errorOut(__func__, MSG_BMO, src);
      }
      advanceChar(reader);  // skip rest
      len++;
    }
  }
  buffer[len > NUMBER_MAX_LENGTH ? 0 : len] = '\0';
  errno = 0;
  double num = strtod(buffer, NULL);
  if (errno) {
    errorOut(__func__, MSG_I_N_ARG, src);
  }
  return setNumber(num, src);
}

/**
 * Parse single octal character to numeric value
 */
static inline Byte ofOctal(char n) { return n - '0'; }

/**
 * Parse octal octet string into byte (octal literals 000 to 377)
 */
static inline char parseOctet(const char *nnn) {
  return (char)(ofOctal(nnn[0]) * 64 + ofOctal(nnn[1]) * 8 + ofOctal(nnn[2]));
}

static inline bool isOctal(const char ch) { return '0' <= ch && ch <= '7'; }

/**
 * Parse single hexadecimal character to numeric value
 */
static inline Byte ofHex(char n) {
  if (n >= 'a' && n <= 'f') {
    return 10 + (n - 'a');
  }
  if (n >= 'A' && n <= 'F') {
    return 10 + (n - 'A');
  }
  return n - '0';
}

/**
 * Parse hexadecimal octet string into byte (hexadecimal literals 00 to FF)
 */
static inline char parseHex(const char *nn) {
  return (char)(ofHex(nn[0]) * 16 + ofHex(nn[1]));
}

/**
 * Read text token (with delimiter)
 */
static Token *readText(Reader *reader, char *src, bool isRaw) {
  char *buffer = FRY_ALLOC(INIT_B_LEN);
  size_t bufferSize = INIT_B_LEN;
  size_t len = 0;
  bool isEscaped = false;
  bool success = false;
  bool isTemplate = false;
  const char delimiter = isRaw ? MARK_RAW : MARK_TEM;

  advanceChar(reader);  // skip opening delimiter

  while (currentChar(reader)) {
    char ch = advanceChar(reader);

    if (ch == '\n') {
      errorOut(__func__, "line break in text", src);
    }

    if (len == bufferSize) {
      bufferSize *= 2;
      buffer = FRY_REALLOC(buffer, bufferSize);
    }

    if (isEscaped) {
      switch (ch) {
        case 'b':
          buffer[len++] = '\b';  // backspace
          break;
        case 'f':
          buffer[len++] = '\f';  // form feed
          break;
        case 'n':
          buffer[len++] = '\n';  // new line (line feed)
          break;
        case 'r':
          buffer[len++] = '\r';  // carriage return
          break;
        case 't':
          buffer[len++] = '\t';  // tab
          break;
        case 'e':
          buffer[len++] = '\033';  // escape
          break;
        default:
          if ('0' <= ch && ch <= '3') {  // maybe octal octet
            char *byte = reader->source + reader->pos - 1;
            if (isOctal(byte[1]) && isOctal(byte[2])) {
              advanceChar(reader);  // consume 2nd char
              advanceChar(reader);  // consume 3rd char
              buffer[len++] = parseOctet(byte);
            } else {
              buffer[len++] = ch;
            }
          } else if (ch == 'x' || ch == 'X') {  // maybe hexadecimal byte
            char *byte = reader->source + reader->pos;
            if (isHex(byte[0]) && isHex(byte[1])) {
              advanceChar(reader);  // consume 2nd char
              advanceChar(reader);  // consume 3rd char
              buffer[len++] = parseHex(byte);
            } else {
              buffer[len++] = ch;
            }
          } else {
            buffer[len++] = ch;
          }
      }
      isEscaped = false;
    } else if (ch == MARK_ESC && (isRaw || currentChar(reader) != '{')) {
      isEscaped = true;
    } else if (ch == delimiter) {
      success = true;
      break;
    } else {
      if (ch == '{' && !isRaw) {
        isTemplate = true;
      }
      buffer[len++] = ch;
    }
  }
  buffer[len] = '\0';
  buffer = FRY_REALLOC(buffer, len + 1);
  if (!success) {
    errorOut(__func__, MSG_U_EOF, src);
  }

  TokType type = isRaw ? TkRaw : isTemplate ? TkTemp : TkText;
  Token *result = setValue(type, buffer, src);
  result->num = (double)len;  // see also: fromCurrent at parser.c
  return result;
}

/**
 * Read punctuation token (read new lines as a single new line)
 */
static inline Token *readPunctuation(Reader *reader, char *src) {
  return setValue(TkPunct, ofChar(advanceChar(reader)), src);
}

static void decodeSource(Reader *reader) {
  char *source = reader->source;
  const size_t pos = reader->pos;
  const char *blob = source + pos;
  const size_t len = strlen(blob);

  auto_str decoded = xorDecode(bundleKey, blob);
  if (!decoded) {
    fatalOut(reader->name, __func__, MSG_F_DEC " (a)");
    return;  // mitigate false positive on auto_check.sh
  }

  size_t validationLen = readWhile(&isOneLine, decoded);
  if (validationLen != SALT_SIZE) {
    fatalOut(reader->name, __func__, MSG_F_DEC " (b)");
  }

  decoded[validationLen] = '\0';
  if (!isB64Encoded(decoded)) {
    fatalOut(reader->name, __func__, MSG_F_DEC " (c)");
  }

  // Replace in-place (decoded is just 2/3 of encoded size)
  source[pos] = '\0';
  strncat(source + pos, decoded + validationLen + 1, len - 1);  // ignore warn
}

static Token *readNext(Reader *reader);

/**
 * Read comment token (and do magic for special directives inside comments)
 */
static Token *readComment(Reader *reader, char *src) {
  char *source = reader->source + reader->pos + 1;  // look-ahead (#)

  // Check special directives inside comments
  if (startsWith(source, "ver=")) {
    // Alert about fry verion used for creating the bundle if not same
    source += 4;
    const char *verEnd = strstr(source, "\n");
    auto_str ver =
        verEnd ? copyFragment(source, verEnd - source) : strDup("?");
    if (strcmp(ver, getFryV()) != 0) {
      logAlert(reader->name, "bundled with another fry version", ver);
    }
  } else if (startsWith(source, MARK_ENC) && allowDecode) {
    // Decode if obfuscated source and passed checksum
    advanceChar(reader);  // consume ~
    advanceChar(reader);  // consume !
    advanceChar(reader);  // consume @
    advanceChar(reader);  // consume $
    decodeSource(reader);
    free(src);
    return readNext(reader);
  }

  return setValue(TkComm, readPattern(&isOneLine, reader), src);
}

/**
 * Read operator token
 */
static Token *readOperator(Reader *reader, char *src) {
  char buffer[4];  // up to 3 chars + null-terminator
  buffer[0] = advanceChar(reader);
  buffer[1] = '\0';

  if (isOperator(currentChar(reader))) {
    buffer[1] = currentChar(reader);
    buffer[2] = '\0';

    // First, check if the first two chars form a valid 2-character operator
    if (getOpType(buffer)) {
      advanceChar(reader);  // consume 2nd character

      // Then check for a possible 3-character operator
      if (isOperator(currentChar(reader))) {
        buffer[2] = currentChar(reader);
        buffer[3] = '\0';
        OpType op = getOpType(buffer);
        if (op) {               // check if it's a valid 3-character operator
          advanceChar(reader);  // consume 3rd character
          if (op == OpMissing) {
            return setValue(TkKey, strDup(buffer), src);
          }
        } else {
          buffer[2] = '\0';  // revert back to 2-character operator
        }
      }
    } else {
      buffer[1] = '\0';  // revert back to 1-character operator
    }
  }

  return setValue(TkOp, strDup(buffer), src);
}

/*
 * Reads anything else... not space, not comment, not text, not punct or op
 */
static Token *readIdentifier(Reader *reader, char *src) {
  char *head = reader->source + reader->pos;

  size_t len = 0;
  char buff[ENTRY_NAME_MAX];
  while (*head && isIdentifier(*head)) {
    buff[len] = advanceChar(reader);
    head++;
    len++;
    if (len == ENTRY_NAME_MAX) {
      fatalOut(__FILE__, __func__, MSG_BMO);
    }
  }
  buff[len] = '\0';

  // interpret utf-8 void symbol 'ø' as ()
  if (strcmp(buff, "ø") == 0) {
    buff[0] = '(';
    buff[1] = ')';
    buff[2] = '\0';
    len = 2;
  }

  char *val = copyFragment(buff, len);

  if (isUnder(*val)) {
    return setValue(TkUnder, val, src);
  }

  if (isEmbedded(*val)) {
    return setValue(TkEmbed, val, src);
  }

  if (isTypename(val)) {
    return setValue(TkName, val, src);
  }

  if (isKeyword(val)) {
    return setValue(TkKey, val, src);
  }

  return setValue(TkId, val, src);
}

/**
 * Get human readable text position as 'file:line:column'
 */
static inline char *extractSrc(Reader *reader) {
  char *result = NULL;
  const char *path = reader->name;
  size_t line = reader->ln;
  size_t col = reader->col;
  if (asprintf(&result, "%s:%zu:%zu", path, line, col) == -1) {
    fatalOut(__FILE__, __func__, MSG_OOM);
  }
  return result;
}

/**
 * The core of the lexer/tokenizer
 */
static Token *readNext(Reader *reader) {
  skipPattern(&isSpace, reader);
  char *src = extractSrc(reader);

  if (!currentChar(reader)) {
    return createToken(TkEOF, src);
  }

  char ch = currentChar(reader);
  return isCommentStart(ch)  ? readComment(reader, src)
         : isTempStart(ch)   ? readText(reader, src, false)
         : isRawStart(ch)    ? readText(reader, src, true)
         : isDigit(ch)       ? readNumber(reader, src)
         : isPunctuation(ch) ? readPunctuation(reader, src)
         : isOperator(ch)    ? readOperator(reader, src)
                             : readIdentifier(reader, src);
}

/**
 * Basic syntax validation based on token sequence
 */
static inline void checkInvalidSyntax(const Token *prev, const Token *next) {
  auto_str err = NULL;

  // sequence of identical type
  if (prev->type != TkPunct && prev->type != TkOp &&
      (prev->type == next->type)) {
    err = join3("sequence of ", tokType(next->type), "s");
  }

  // number followed by...
  else if (prev->type == TkNumb || prev->type == TkHuge) {
    switch (next->type) {
      case TkKey:
      case TkId:
      case TkName:
      case TkText:
      case TkTemp:
      case TkRaw:
      case TkEmbed:
        err = join2("number followed by ", tokType(next->type));
        break;
      default:
        break;
    }
  }

  // text followed by...
  else if (prev->type == TkRaw || prev->type == TkTemp ||
           prev->type == TkText) {
    switch (next->type) {
      case TkKey:
      case TkId:
      case TkName:
      case TkNumb:
      case TkHuge:
      case TkText:
      case TkTemp:
      case TkRaw:
      case TkEmbed:
        err = join2("text followed by ", tokType(next->type));
        break;
      default:
        break;
    }
  }

  // identifier followed by...
  else if (prev->type == TkKey || prev->type == TkId || prev->type == TkName) {
    switch (next->type) {
      case TkKey:
      case TkId:
      case TkName:
      case TkNumb:
      case TkHuge:
      case TkText:
      case TkTemp:
      case TkRaw:
      case TkEmbed:
        err = join2("identifier followed by ", tokType(next->type));
        break;
      default:
        break;
    }
  }

  if (err) {
    errorOut(__func__, err, next->src);
  }
}

/**
 * Add the next available token from source to tokenized contents
 */
static inline Token *addNextFromSource(Reader *reader) {
  Token *tok = readNext(reader);
  if (reader->head) {
    checkInvalidSyntax(reader->current, tok);
    reader->current = reader->current->next = tok;
  } else {
    reader->current = reader->head = tok;
  }
  return tok;
}

Reader *tokenize(Reader *reader) {
  if (!reader->source) {
    fatalOut(__FILE__, __func__, "no input");
  }

  if (debugLogs) {
    printSourceInput(reader->source);
  }

  while (addNextFromSource(reader)->type != TkEOF) {
    continue;
  }

  reader->current = reader->head;  // reset for parsing
  return reader;
}

Reader *tokenizeFile(char *filepath) {
  Reader *reader = createReader(filepath);
  readFile(&reader->source, filepath, "r");
  return tokenize(reader);
}

/**
 * JSON syntax validation based on token type
 */
static inline bool isValidJsonTok(const Token *tok) {
  switch (tok->type) {
    case TkPunct:
      if (*tok->val != '(' && *tok->val != ')') {
        return true;
      }
      FALL_THROUGH;
    case TkId:
    case TkName:
    case TkUnder:
    case TkOp:
      if (isOpTok(tok, "-")) {
        return true;
      }
      FALL_THROUGH;
    case TkComm:
    case TkEmbed:
      return false;
    default:
      return true;
  }
}

/**
 * JSON syntax validation based on token sequence
 */
static inline bool isValidJsonSeq(const Token *prev, const Token *next) {
  return prev->type == TkPunct || prev->type != next->type;
}

/**
 * Add the next available token from JSON to tokenized contents
 */
static inline Token *addNextFromJson(Reader *reader) {
  Token *tok = readNext(reader);

  // Check for invalid token types
  if (!isValidJsonTok(tok)) {
    logAlert("parseJson", "invalid token", tok->src);
    return NULL;
  }

  // Force no text interpolation
  if (tok->type == TkTemp) {
    tok->type = TkText;
  }

  if (reader->head) {
    if (!isValidJsonSeq(reader->current, tok)) {
      logAlert("parseJson", "invalid syntax", tok->src);
      return NULL;
    }
    reader->current = reader->current->next = tok;
  } else {
    reader->current = reader->head = tok;
  }
  return tok;
}

bool tokenizeJson(Reader *reader) {
  if (!reader->source) {
    return false;
  }

  Token *tok = NULL;
  do {
    tok = addNextFromJson(reader);
    if (!tok) {
      return false;
    }
  } while (tok->type != TkEOF);

  reader->current = reader->head;  // reset for parsing
  return reader;
}
