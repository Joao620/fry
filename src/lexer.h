/**
 * @file lexer.h
 * @brief Builds a token reader from input
 * (inspired by https://lisperator.net/pltut/)
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.2.0
 * @date 2024-02-07
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "sdk/sdk.h"

extern bool allowDecode;  // enables lexer to read obfuscated code

/**
 * @brief Scans the loaded source, converting it into a list of token
 *
 * @param reader Reader* (with source)
 * @return Reader* (with token list)
 */
Reader *tokenize(Reader *reader);

/**
 * @brief Shorthand to read and tokenize from a file
 *
 * @param filepath string
 * @return Reader* (with source)
 */
Reader *tokenizeFile(char *filepath);

/**
 * @brief Scans the loaded JSON, converting it into a JSON-Like AST
 *
 * @param reader Reader* (with JSON source)
 * @return boolean (success)
 */
bool tokenizeJson(Reader *reader);
