/**
 * @file async.c
 * @brief Asynchronous workers and tasks
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-28
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

static const char* LIB_ASYNC =
    "# fat.async - Asynchronous workers and tasks\n"
    "\n"
    "_ <- fat.type.Error\n"
    "\n"
    "## Execute operation atomically\n"
    "atomic = (op: Method): Any -> $atomic\n"
    "\n"
    "## Terminate own thread\n"
    "selfCancel = -> $selfCancel\n"
    "\n"
    "## Get the number of online processors\n"
    "processors = -> $processors\n"
    "\n"
    "Worker = (\n"
    "  ## Argument slot\n"
    "  task: Method\n"
    "  wait: Number = 0  # milliseconds\n"
    "\n"
    "  ## Constructor\n"
    "  apply = (): Worker -> {\n"
    "    !self('task') => CallError('no task provided')\n"
    "    _             => {\n"
    "      self.{\n"
    "        ~ hasStarted = false\n"
    "        ~ hasAwaited = false\n"
    "        ~ isCanceled = false\n"
    "      }\n"
    "      self\n"
    "    }\n"
    "  }\n"
    "\n"
    "  ## Worker prototype methods\n"
    "  start  = (): Worker  -> $asyncStart\n"
    "  cancel = (): Void    -> $asyncCancel\n"
    "  await  = (): Worker  -> $asyncAwait\n"
    "  isDone = (): Boolean -> $asyncIsDone\n"
    "\n"
    "  ## result: Any  # special property created by await\n"
    ")\n";

/**
 * Thread code runner
 */
static void* startWorker(void* arg) {
  // Block SIGINT, SIGHUP, and SIGTERM in this thread
  sigset_t set;
  sigemptyset(&set);
  sigaddset(&set, SIGINT);
  sigaddset(&set, SIGHUP);
  sigaddset(&set, SIGTERM);

  // Apply the signal mask to the thread
  pthread_sigmask(SIG_BLOCK, &set, NULL);

  Worker* worker = (Worker*)arg;
  Context* ctx = worker->ctx;
  Node* task = worker->task;

  // Seed random number generator (this thread)
  initXorshiftStates();

  Node call = {.type = FatCall, .head = task, .src = SRC_AUX};
  ctx->unclaimed = evalMethodCall(globalScope, &call, task, ctx);
  popStack(ctx, 1);  // indicates "is done"
  return ctx->unclaimed;
}

/**
 * Execute operation atomically
 */
static Node* asyncAtomic(Scope* scope, Context* ctx) {
  Node* op = getParameter(scope, "op", FatMethod, ctx);
  if (IS_FAT_ERROR(op)) {
    return op;
  }

  Node atomicCall = {.type = FatCall, .head = op, .src = SRC_AUX};

  lockResource(&atomicLock);
  Node* result = evalMethodCall(scope, &atomicCall, op, ctx);
  unlockResource(&atomicLock);
  return result;
}

/**
 * Begins the task
 */
static Node* asyncStart(Context* ctx) {
  Node* instance = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(instance)) {
    return instance;
  }

  Node* task = getParameter(instance->scp, "task", FatMethod, ctx);
  if (IS_FAT_ERROR(task)) {
    return task;
  }

  Node* hasStarted = getParameter(instance->scp, "hasStarted", FatBoolean, ctx);
  if (IS_FAT_ERROR(hasStarted)) {
    return hasStarted;
  }
  if (hasStarted->num.b) {
    return createError(MSG_W_A_S, true, ckAsyncError, ctx);
  }

  Node* wait = getParameter(instance->scp, "wait", FatNumber, ctx);
  if (IS_FAT_ERROR(wait)) {
    return wait;
  }

  if (debugLogs) {
    logDebug(__FILE__, __func__, "starting worker...");
  }

#ifndef __EMSCRIPTEN__
  // Point to real lock/unlock functions, see initMemoryManagement at memory.c
  lockResource = pthread_mutex_lock;
  unlockResource = pthread_mutex_unlock;

  // Ensure worker can't be started twice
  lockResource(&instance->scp->lock);
  Worker* worker = instance->scp->async;
  if (worker) {
    unlockResource(&instance->scp->lock);
    return createError(MSG_W_A_S, true, ckAsyncError, ctx);
  }

  worker = FRY_ALLOC(sizeof(Worker));
#endif

  Context* threadContext = createContext(0);
  if (wait->num.f > 0) {
    threadContext->timeout = getCurrentMs(CLOCK_MONOTONIC_COARSE) + wait->num.f;
  }
  pushStack(threadContext, __func__, task,
            globalScope);  // indicates "not done"

#ifndef __EMSCRIPTEN__
  // Setup worker
  worker->task = task;
  worker->ctx = threadContext;
  atomic_init(&worker->hasJoined, false);

  if (pthread_create(&(worker->ctx->id), NULL, startWorker, (void*)worker)) {
    popStack(threadContext, 1);
    freeContext(threadContext);
    free(worker);
    unlockResource(&instance->scp->lock);
    return createError("worker failed to start", true, ckAsyncError, ctx);
  }

  instance->scp->async = worker;
  unlockResource(&instance->scp->lock);

  if (debugLogs) {
    logDebug(__FILE__, __func__, "worker created");
  }
#else
  // This is a hack just for compat.: runs the task synchronously

  Node call = {.type = FatCall, .head = task, .src = SRC_AUX};
  Node* result = evalMethodCall(globalScope, &call, task, threadContext);
  popStack(threadContext, 1);

  if (threadContext->isCanceled) {
    Node* isCanceled = getValueOf(instance->scp, "isCanceled");
    if (IS_FAT_TYPE(isCanceled, FatBoolean)) {
      isCanceled->num.b = true;
    }

    // inlined verifyContextTimeout, see loops.c
    if (threadContext->timeout > 0 &&
        threadContext->timeout > getCurrentMs(CLOCK_MONOTONIC_COARSE)) {
      result = createError(MSG_W_T_O, true, ckAsyncError, ctx);
    } else {
      result = createError(MSG_W_W_C, true, ckAsyncError, ctx);
    }
  }

  freeContext(threadContext);

  Node* hasAwaited = getValueOf(instance->scp, "hasAwaited");
  if (IS_FAT_TYPE(hasAwaited, FatBoolean)) {
    hasAwaited->num.b = true;
  }

  if (result && !getValueOf(instance->scp, "result")) {
    addToScope(instance->scp, "result", result);  // store result
  }
#endif

  hasStarted->num.b = true;

  return instance;
}

/**
 * Cancels the task
 */
static Node* asyncCancel(Context* ctx) {
  Node* instance = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(instance)) {
    return instance;
  }

  Node* isCanceled = getParameter(instance->scp, "isCanceled", FatBoolean, ctx);
  if (IS_FAT_ERROR(isCanceled)) {
    return isCanceled;
  }
  if (booleanOf(isCanceled)) {
    return NULL;  // already canceled
  }

  Node* hasStarted = getParameter(instance->scp, "hasStarted", FatBoolean, ctx);
  if (IS_FAT_ERROR(hasStarted)) {
    return hasStarted;
  }
  if (!booleanOf(hasStarted)) {
    return createError(MSG_TNS, true, ckAsyncError, ctx);
  }

  Node* hasAwaited = getParameter(instance->scp, "hasAwaited", FatBoolean, ctx);
  if (IS_FAT_ERROR(hasAwaited)) {
    return hasAwaited;
  }
  if (booleanOf(hasAwaited)) {
    return createError(MSG_TAW, true, ckAsyncError, ctx);
  }

  lockResource(&instance->scp->lock);
  Worker* worker = instance->scp->async;
  unlockResource(&instance->scp->lock);

  if (!worker) {
    return createError(MSG_TNS, true, ckAsyncError, ctx);
  }
  if (atomic_exchange(&worker->hasJoined, true)) {
    return createError(MSG_TAW " (a)", true, ckAsyncError, ctx);
  }

  if (debugLogs) {
    logDebug(__FILE__, __func__, "cancelling worker");
  }

  atomic_store(&worker->ctx->isCanceled, true);

  void* result = NULL;  // ignored
  int joinError = pthread_join(worker->ctx->id, &result);
  if (joinError) {
    logAlert(__FILE__, __func__, strerror(joinError));
    return createError("failed to await (a)", true, ckAsyncError, ctx);
  }

  isCanceled->num.b = true;
  worker->ctx->unclaimed = NULL;
  return NULL;
}

/**
 * Waits for task completion (and extracts/stores result)
 */
static Node* asyncAwait(Context* ctx) {
  Node* instance = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(instance)) {
    return instance;
  }

  Node* maybeError = getValueOf(instance->scp, "result");
  if (IS_FAT_TYPE(maybeError, FatError) && maybeError->ck == ckAsyncError) {
    ctx->failureEvent = maybeError;  // re-raise error
    return maybeError;
  }

  Node* hasAwaited = getParameter(instance->scp, "hasAwaited", FatBoolean, ctx);
  if (IS_FAT_ERROR(hasAwaited)) {
    return hasAwaited;
  }
  if (booleanOf(hasAwaited)) {
    return instance;
  }

  Node* hasStarted = getParameter(instance->scp, "hasStarted", FatBoolean, ctx);
  if (IS_FAT_ERROR(hasStarted)) {
    return hasStarted;
  }
  if (!booleanOf(hasStarted)) {
    return createError(MSG_TNS, true, ckAsyncError, ctx);
  }

  Node* isCanceled = getParameter(instance->scp, "isCanceled", FatBoolean, ctx);
  if (IS_FAT_ERROR(isCanceled)) {
    return isCanceled;
  }
  if (booleanOf(isCanceled)) {
    return createError(MSG_W_W_C, true, ckAsyncError, ctx);
  }

  lockResource(&instance->scp->lock);
  Worker* worker = instance->scp->async;
  unlockResource(&instance->scp->lock);

  if (!worker) {
    // should never happen or "hasAwaited" is lying
    return createError("internal async error", true, ckAsyncError, ctx);
  }
  if (atomic_load(&worker->hasJoined)) {
    for (int trySync = 0; trySync < 10 && !booleanOf(hasAwaited); trySync++) {
      msSleep(GC_THREAD_LAG);  // wait for result to sync
    }
    return instance;
  }
  if (atomic_exchange(&worker->hasJoined, true)) {
    return createError(MSG_TAW " (b)", true, ckAsyncError, ctx);
  }

  if (debugLogs) {
    logDebug(__FILE__, __func__, "awaiting worker");
  }

  // wait for task to complete
  while (atomic_load(&worker->ctx->top) > 0) {
    // inlined verifyContextTimeout, see loops.c
    if (worker->ctx->timeout > 0 &&
        worker->ctx->timeout > getCurrentMs(CLOCK_MONOTONIC_COARSE)) {
      atomic_store(&worker->ctx->isCanceled, true);
    }
    if (atomic_load(&activeMemory) >= memoryLimit - GC_PREMONITION) {
      quickGC(ctx);  // run GC
    }
    msSleep(GC_THREAD_LAG);  // avoid busy wait
  }

  void* result = NULL;
  int joinError = pthread_join(worker->ctx->id, &result);
  if (joinError) {
    logAlert(__FILE__, __func__, strerror(joinError));
    return createError("failed to await (b)", true, ckAsyncError, ctx);
  }

  Node* error = NULL;
  if (worker->ctx->isCanceled) {
    isCanceled->num.b = true;

    // inlined verifyContextTimeout, see loops.c
    if (worker->ctx->timeout > 0 &&
        worker->ctx->timeout > getCurrentMs(CLOCK_MONOTONIC_COARSE)) {
      error = createError(MSG_W_T_O, true, ckAsyncError, ctx);
    } else {
      error = createError(MSG_W_W_C, true, ckAsyncError, ctx);
    }
    result = error;
  }

  if (!maybeError && result) {
    addToScope(instance->scp, "result", (Node*)result);  // store result
  }

  hasAwaited->num.b = true;
  worker->ctx->unclaimed = NULL;
  return error ? error : instance;
}

/**
 * Checks if the task has completed
 */
static Node* asyncIsDone(Context* ctx) {
  Node* instance = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(instance)) {
    return instance;
  }

  // Check if done even if thread has not been joined yet!
  Worker* worker = instance->scp->async;
  if (worker && !atomic_load(&worker->hasJoined)) {
    // we push into context before we start the thread, therefore the only
    // way stack "top" can be zero on a non-joined thread is when done!
    if (atomic_load(&worker->ctx->top) == 0) {
      return trueSingleton;
    }
  }

  // Return unchecked value of "hasAwaited", for swift response
  return getValueOf(instance->scp, "hasAwaited");
}

/**
 * Terminate own thread
 */
static Node* asyncSelfCancel(Context* ctx) {
  atomic_store(&ctx->isCanceled, true);
  return NULL;
}

/**
 * Get the number of online processors
 */
static Node* asyncProcessors(Context* ctx) {
  return runtimeNumber(sysconf(_SC_NPROCESSORS_ONLN), ctx);
}
