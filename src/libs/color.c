/**
 * @file color.c
 * @brief ANSI color codes for console
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-30
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

static const char* LIB_COLOR =
    "# fat.color - ANSI color codes for console\n"
    "\n"
    "## Constants\n"
    "black   = 0\n"
    "red     = 1\n"
    "green   = 2\n"
    "yellow  = 3\n"
    "blue    = 4\n"
    "magenta = 5\n"
    "cyan    = 6\n"
    "white   = 7  # light-gray\n"
    "bright = {\n"
    "  black   =  8  # dark-gray\n"
    "  red     =  9\n"
    "  green   = 10\n"
    "  yellow  = 11\n"
    "  blue    = 12\n"
    "  magenta = 13\n"
    "  cyan    = 14\n"
    "  white   = 15\n"
    "}\n"
    "\n"
    "## Get console color depth support\n"
    "detectDepth = (): Number -> $detectDepth\n"
    "\n"
    "## Color conversion methods\n"
    "### pass either a single hex text or separate r, g, b values\n"
    "\n"
    "## Convert RGB to 8-color mode (approximation)\n"
    "to8 = (xr: Any, g: Number = null, b: Number = null): Number -> $to8\n"
    "\n"
    "## Convert RGB to 16-color mode (approximation)\n"
    "to16 = (xr: Any, g: Number = null, b: Number = null): Number -> $to16\n"
    "\n"
    "## Convert RGB to 256-color mode (approximation)\n"
    "to256 = (xr: Any, g: Number = null, b: Number = null): Number -> $to256\n";

static unsigned int max3(unsigned int a, unsigned int b, unsigned int c) {
  return a > b ? a > c ? a : c : b > c ? b : c;
}

static unsigned int min3(unsigned int a, unsigned int b, unsigned int c) {
  return a < b ? a < c ? a : c : b < c ? b : c;
}

const int baseColors[16][3] = {
    {0, 0, 0},       {128, 0, 0},   {0, 128, 0},   {128, 128, 0},
    {0, 0, 128},     {128, 0, 128}, {0, 128, 128}, {192, 192, 192},
    {128, 128, 128}, {255, 0, 0},   {0, 255, 0},   {255, 255, 0},
    {0, 0, 255},     {255, 0, 255}, {0, 255, 255}, {255, 255, 255}};

/**
 * Ensure hexColor input safety as best as we can
 */
static bool isHexColor(const char* hexColor) {
  if (strlen(hexColor) != 6) {
    return false;
  }
  for (int i = 0; i < 6; i++) {
    char c = hexColor[i];
    if (!((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') ||
          (c >= 'A' && c <= 'F'))) {
      return false;
    }
  }
  return true;
}

/**
 * Helper function to extract RGB values from parameters in scope
 */
static Node* extractColorValues(Scope* scope, unsigned int* red,
                                unsigned int* green, unsigned int* blue,
                                Context* ctx) {
  Node* xr = getValueOf(scope, "xr");
  if (!xr) {
    char* msg = join3(MSG_M_PAR, GUIDE, "xr");
    return createError(msg, false, ckCallError, ctx);
  }

  if (xr->type == FatText) {
    // handle hex string
    char* hexColor = xr->val;

    // ignore pound symbol (#)
    if (*hexColor == '#') {
      hexColor++;
    }

    if (!isHexColor(hexColor) ||
        sscanf(hexColor, "%02x%02x%02x", red, green, blue) != 3) {
      return createError(MSG_B_COLOR, true, ckValueError, ctx);
    }

  } else if (xr->type == FatNumber) {
    // handle r, g, b values
    Node* g = getParameter(scope, "g", FatNumber, ctx);
    if (IS_FAT_ERROR(g)) {
      return g;
    }
    Node* b = getParameter(scope, "b", FatNumber, ctx);
    if (IS_FAT_ERROR(b)) {
      return b;
    }

    *red = (unsigned int)xr->num.f;
    *green = (unsigned int)g->num.f;
    *blue = (unsigned int)b->num.f;

    if (*red > 255) {
      *red = 255;
    }
    if (*green > 255) {
      *green = 255;
    }
    if (*blue > 255) {
      *blue = 255;
    }

  } else {
    char* msg = join3(MSG_MISMATCH, GUIDE, "xr");
    return createError(msg, false, ckTypeError, ctx);
  }

  return NULL;  // No error occurred
}

/**
 * Get console color depth support
 */
static Node* colorDetectDepth(Context* ctx) {
#ifdef __EMSCRIPTEN__
  return runtimeNumber(256, ctx);
#endif

  // Check for no color support
  if (!stdoutIsTTY) {
    return runtimeNumber(0, ctx);
  }

  // Detect once in execution life-time
  static int colorSupport = 0;
  if (colorSupport) {
    return runtimeNumber(colorSupport, ctx);
  }

  // Default to basic color support
  colorSupport = 8;

  char* term = getenv("TERM");
  if (term) {
    if ((strstr(term, "xterm") || strstr(term, "vt100") ||
         strstr(term, "linux") || strstr(term, "ansi") ||
         strstr(term, "konsole") || strstr(term, "vte"))) {
      colorSupport = 16;
    } else if ((strcmp(term, "xterm-256color") == 0 ||
                strcmp(term, "screen-256color") == 0 ||
                strcmp(term, "tmux-256color") == 0 ||
                strcmp(term, "gnome-256color") == 0 ||
                strcmp(term, "konsole-256color") == 0)) {
      colorSupport = 256;
    }
  }

  char* termProgram = getenv("TERM_PROGRAM");
  if (termProgram && (strcmp(termProgram, "iTerm.app") == 0 ||
                      strcmp(termProgram, "Hyper") == 0 ||
                      strcmp(termProgram, "WezTerm") == 0)) {
    colorSupport = 16;
  }

  char* colorTerm = getenv("COLORTERM");
  if (colorTerm && (strcmp(colorTerm, "truecolor") == 0 ||
                    strcmp(colorTerm, "24bit") == 0)) {
    colorSupport = 256;
  }

  char* vteVersion = getenv("VTE_VERSION");
  if (vteVersion && atoi(vteVersion) >= 5000) {
    colorSupport = 256;
  }

  return runtimeNumber(colorSupport, ctx);
}

/**
 * Convert RGB color to 8-color mode (approximation)
 */
static Node* colorTo8(Scope* scope, Context* ctx) {
  unsigned int red = 0;
  unsigned int green = 0;
  unsigned int blue = 0;
  unsigned int color8 = 0;

  Node* error = extractColorValues(scope, &red, &green, &blue, ctx);
  if (error) {
    return error;  // If an error occurred, return it
  }

  double diff = 195075.0;  // maximum possible value of the squared Euclidean
  for (int i = 0; i < 8; i++) {  // loop only over the first 8 colors
    double ri = baseColors[i][0];
    double gi = baseColors[i][1];
    double bi = baseColors[i][2];
    // Euclidean distance in RGB
    double d = pow(red - ri, 2.0) + pow(green - gi, 2.0) + pow(blue - bi, 2.0);
    if (d < diff) {
      diff = d;
      color8 = i;
    }
  }

  return runtimeNumber(color8, ctx);
}

/**
 * Convert RGB color to 16-color mode (approximation)
 */
static Node* colorTo16(Scope* scope, Context* ctx) {
  unsigned int red = 0;
  unsigned int green = 0;
  unsigned int blue = 0;
  unsigned int color16 = 0;

  Node* error = extractColorValues(scope, &red, &green, &blue, ctx);
  if (error) {
    return error;  // If an error occurred, return it
  }

  double diff = 195075.0;  // maximum possible value of the squared Euclidean
  for (int i = 0; i < 16; i++) {
    double ri = baseColors[i][0];
    double gi = baseColors[i][1];
    double bi = baseColors[i][2];
    // Euclidean distance in RGB
    double d = pow(red - ri, 2.0) + pow(green - gi, 2.0) + pow(blue - bi, 2.0);
    if (d < diff) {
      diff = d;
      color16 = i;
    }
  }

  return runtimeNumber(color16, ctx);
}

/**
 * Convert RGB color to 256-color mode (approximation)
 */
static Node* colorTo256(Scope* scope, Context* ctx) {
  unsigned int red = 0;
  unsigned int green = 0;
  unsigned int blue = 0;
  unsigned int color256 = 0;

  Node* error = extractColorValues(scope, &red, &green, &blue, ctx);
  if (error) {
    return error;  // If an error occurred, return it
  }

  if (max3(red, green, blue) - min3(red, green, blue) < 48) {  // if grayish

    if (red < 8) {
      color256 = 16;  // black
    } else if (red > 248) {
      color256 = 231;  // white
    } else {
      color256 = 232 + (int)((float)(red - 8) / 247 * 24);  // gray scale
    }
  } else {
    color256 =  // otherwise, it's a color from the 6x6x6 color cube
        16 + 36 * (int)((float)red / 255 * 5) +
        6 * (int)((float)green / 255 * 5) + (int)((float)blue / 255 * 5);
  }

  return runtimeNumber(color256, ctx);
}
