/**
 * @file curses.c
 * @brief Terminal-based user interface
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-30
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

static const char* LIB_CURSES =
    "# fat.curses - Terminal-based user interface\n"
    "\n"
    "### positions (pos) are of form { x: Number, y: Number }\n"
    "\n"
    "## Draw a box from pos1 to pos2\n"
    "box = (p1: Scope, p2: Scope): Void -> $box\n"
    "\n"
    "## Fill area from pos1 to pos2\n"
    "fill = (p1: Scope, p2: Scope, p: Text = ' '): Void -> $fill\n"
    "\n"
    "## Clear screen buffer\n"
    "clear = (): Void -> $clear\n"
    "\n"
    "## Render screen buffer\n"
    "refresh = (): Void -> $refresh\n"
    "\n"
    "## Return screen size as x, y\n"
    "getMax = (): Scope -> $getMax\n"
    "\n"
    "## Print msg at { x, y } pos\n"
    "printAt = (pos: Scope, msg: Any, width: Number = null): Void -> $printAt\n"
    "\n"
    "## Create a color pair (see fat.color)\n"
    "makePair = (fg: Number = null, bg: Number = null): Number -> $makePair\n"
    "\n"
    "## Apply color pair\n"
    "usePair = (pair: Number): Void -> $usePair\n"
    "\n"
    "## Align view to screen center\n"
    "frameTo = (cols: Number, rows: Number): Void -> $frameTo\n"
    "\n"
    "## Return key pressed\n"
    "readKey = (): Text -> $readKey\n"
    "\n"
    "## Start a text box input\n"
    "readText = (pos: Scope, width: Number, prev: Text = null): Text -> "
    "$readText\n"
    "\n"
    "## Flush input buffer\n"
    "flushKeys = (): Void -> $flushKeys\n"
    "\n"
    "## Exit curses mode\n"
    "endCurses = (): Void -> $endCurses\n";

static int cursesColorPairs = 0;

static int frameOffsetX = 0;
static int frameOffsetY = 0;

/**
 * Print message with ellipsis, considering utf-8 encoding
 */
static void printUtf8Ellipsis(int py, int px, const char* str, int cols) {
  size_t byteOffset = 0;
  int charCount = 0;
  char buffer[5] = {'\0'};  // maximum utf-8 character size (4 + \0)
  size_t bufferIndex = 0;
  while (str[byteOffset] != '\0') {
    if (charCount >= cols - 1) {
      mvaddstr(py, px + charCount, isUtf8Mode ? "…" : "~");
      break;
    }
    if ((str[byteOffset] & 0xC0) != 0x80) {  // first utf-8 byte
      if (bufferIndex > 0) {
        // print and reset buffer
        buffer[bufferIndex] = '\0';
        mvaddstr(py, px + charCount, buffer);
        bufferIndex = 0;
        charCount++;
      }
    }
    if (bufferIndex < 4) {
      buffer[bufferIndex++] = str[byteOffset];
    }
    byteOffset++;
  }
}

/**
 * Star curses mode (used implicitly by all other methods)
 */
static void startCursesMode(Context* ctx) {
  if (!stdoutIsTTY) {
    stackOut(__FILE__, __func__, "stdout is not a TTY", ctx);
  }
  initscr();              // start curses mode
  start_color();          // initialize default colors
  cbreak();               // allow SIGINT
  noecho();               // don't print keys
  nodelay(stdscr, true);  // make getch non-blocking
  keypad(stdscr, true);   // allow getch to capture arrow keys
  curs_set(false);        // hide cursor
  cursesMode = true;
}

/**
 * Exit curses mode
 */
static Node* cursEndCurses(void) {
  endCursesMode();
  return NULL;
}

static void fillArea(int y1, int x1, int y2, int x2, const char* p) {
  const int len = utf8len(p);
  const int areaWidth = x2 - x1 + 1;
  const int diff = areaWidth % len;

  auto_str endFrag = NULL;
  if (diff > 0) {
    char* ptr = (char*)p;
    for (int i = 0; i < diff; i++) {
      ptr = utf8next(ptr);
    }
    endFrag = copyFragment(p, ptr - p);
  }

  for (int i = y1; i <= y2; i++) {
    int j = x1;
    for (; j <= x2 - diff; j += len) {
      mvaddstr(i, j, p);
    }
    if (endFrag) {
      mvaddstr(i, j, endFrag);
    }
  }
}

/**
 * Draw a box from pos1 to pos2 (diagonal)
 */
static Node* cursBox(Scope* scope, Context* ctx, bool fill) {
  Node* p1 = getParameter(scope, "p1", FatScope, ctx);
  if (IS_FAT_ERROR(p1)) {
    return p1;
  }

  Node* p1x = getParameter(p1->scp, "x", FatNumber, ctx);
  if (IS_FAT_ERROR(p1x)) {
    return p1x;
  }

  Node* p1y = getParameter(p1->scp, "y", FatNumber, ctx);
  if (IS_FAT_ERROR(p1y)) {
    return p1y;
  }

  Node* p2 = getParameter(scope, "p2", FatScope, ctx);
  if (IS_FAT_ERROR(p2)) {
    return p2;
  }

  Node* p2x = getParameter(p2->scp, "x", FatNumber, ctx);
  if (IS_FAT_ERROR(p2x)) {
    return p2x;
  }

  Node* p2y = getParameter(p2->scp, "y", FatNumber, ctx);
  if (IS_FAT_ERROR(p2y)) {
    return p2y;
  }

  if (!cursesMode) {
    startCursesMode(ctx);
  }

  int x1 = 0;
  int x2 = 0;
  int y1 = 0;
  int y2 = 0;

  // Ensure x1 is less or equal to x2
  if (p1x->num.f > p2x->num.f) {
    x1 = frameOffsetX + (int)p2x->num.f;
    x2 = frameOffsetX + (int)p1x->num.f;
  } else {
    x1 = frameOffsetX + (int)p1x->num.f;
    x2 = frameOffsetX + (int)p2x->num.f;
  }

  // Ensure y1 is less or equal to y2
  if (p1y->num.f > p2y->num.f) {
    y1 = frameOffsetY + (int)p2y->num.f;
    y2 = frameOffsetY + (int)p1y->num.f;
  } else {
    y1 = frameOffsetY + (int)p1y->num.f;
    y2 = frameOffsetY + (int)p2y->num.f;
  }

  if (fill) {
    Node* p = getParameter(scope, "p", FatText, ctx);
    if (IS_FAT_ERROR(p)) {
      return p;
    }
    fillArea(y1, x1, y2, x2, p->val);
    return NULL;
  }

  int width = x2 - x1;
  int height = y2 - y1;

  // Draw nothing if zero sized
  if (!width && !height) {
    return NULL;
  }

  // Draw horizontal line only, if no height
  if (!height) {
    mvhline(y1, x1, isUtf8Mode ? 0 : '-', width + 1);
    return NULL;
  }

  // Draw vertical line only, if no width
  if (!width) {
    mvvline(y1, x1, isUtf8Mode ? 0 : '|', height + 1);
    return NULL;
  }

  // ...otherwise draw the complete box

  if (isUtf8Mode) {
    // Draw the top and bottom lines of the box
    mvhline(y1, x1 + 1, 0, width - 1);
    mvhline(y2, x1 + 1, 0, width - 1);

    // Draw the left and right lines of the box
    mvvline(y1 + 1, x1, 0, height - 1);
    mvvline(y1 + 1, x2, 0, height - 1);

    // Draw the corners
    mvaddstr(y1, x1, "┌");  // top-left
    mvaddstr(y1, x2, "┐");  // top-right
    mvaddstr(y2, x1, "└");  // bottom-left
    mvaddstr(y2, x2, "┘");  // bottom-right

  } else {  // ascii mode

    // Draw the top and bottom lines of the box
    mvhline(y1, x1 + 1, '-', width - 1);
    mvhline(y2, x1 + 1, '-', width - 1);

    // Draw the left and right lines of the box
    mvvline(y1 + 1, x1, '|', height - 1);
    mvvline(y1 + 1, x2, '|', height - 1);

    // Draw the corners
    mvaddstr(y1, x1, "+");
    mvaddstr(y1, x2, "+");
    mvaddstr(y2, x1, "+");
    mvaddstr(y2, x2, "+");
  }

  return NULL;
}

/**
 * Clear screen buffer
 */
static Node* cursClear(Context* ctx) {
  if (!cursesMode) {
    startCursesMode(ctx);
  }
  clear();
  return NULL;
}

/**
 * "Render" screen buffer (apply changes to console)
 */
static Node* cursRefresh(Context* ctx) {
  if (!cursesMode) {
    startCursesMode(ctx);
  }
  refresh();
  return NULL;
}

/**
 * Get the maximum values for screen as x (cols) and y (rows)
 */
static Node* cursGetMax(Context* ctx) {
#ifdef USE_NCURSES
  const bool wasCursesMode = cursesMode;
  if (!wasCursesMode) {
    startCursesMode(ctx);
  }
#endif

  double x = (double)getmaxx(stdscr);
  double y = (double)getmaxy(stdscr);

#ifdef USE_NCURSES
  if (!wasCursesMode) {
    endCursesMode();
  }
#endif

  Scope* scope = createScope();
  pushStack(ctx, __func__, NULL, scope);

  addToScope(scope, "x", runtimeNumber(x, ctx));
  addToScope(scope, "y", runtimeNumber(y, ctx));

  Node* result = runtimeCollection(scope, ctx);
  popStack(ctx, 1);
  return result;
}

/**
 * Print msg at { x, y } pos
 */
static Node* cursPrintAt(Scope* scope, Context* ctx) {
  Node* pos = getParameter(scope, "pos", FatScope, ctx);
  if (IS_FAT_ERROR(pos)) {
    return pos;
  }

  Node* x = getParameter(pos->scp, "x", FatNumber, ctx);
  if (IS_FAT_ERROR(x)) {
    return x;
  }

  Node* y = getParameter(pos->scp, "y", FatNumber, ctx);
  if (IS_FAT_ERROR(y)) {
    return y;
  }

  if (!cursesMode) {
    startCursesMode(ctx);
  }

  int px = frameOffsetX + (int)x->num.f;
  int py = frameOffsetY + (int)y->num.f;

  auto_str msg = toString(getValueOf(scope, "msg"));

  // optional parameter
  const Node* width = getValueOf(scope, "width");

  if (IS_FAT_TYPE(width, FatNumber)) {
    int cols = (int)width->num.f;
    if ((int)utf8len(msg) > cols) {
      printUtf8Ellipsis(py, px, msg, cols);
    } else {
      mvaddstr(py, px, msg);
    }
  } else {
    mvaddstr(py, px, msg);
  }

  return NULL;
}

/**
 * Ensure the value is within the range [0, 255]
 */
static int clampColorCode(double value) {
  return (int)fmin(fmax(floor(value), 0), 255);
}

/**
 * Create a color pair
 */
static Node* cursMakePair(Scope* scope, Context* ctx) {
  if (!cursesMode) {
    startCursesMode(ctx);
  }

  if (!has_colors()) {
    return createError("no color support", true, NULL, ctx);
  }

  Node* fg = getValueOf(scope, "fg");
  Node* bg = getValueOf(scope, "bg");

  bool hasDefault = use_default_colors() != ERR;

  int defaultFg = hasDefault ? -1 : COLOR_WHITE;
  int defaultBg = hasDefault ? -1 : COLOR_BLACK;
  int fgCode = fg ? clampColorCode(fg->num.f) : defaultFg;
  int bgCode = bg ? clampColorCode(bg->num.f) : defaultBg;

  if (cursesColorPairs >= COLOR_PAIRS - 1) {
    return createError("max number of pairs reached", true, NULL, ctx);
  }

  int colorCode = ++cursesColorPairs;

  if (init_pair(colorCode, fgCode, bgCode) == ERR) {
    return createError("failed to create color pair", true, NULL, ctx);
  }

  return runtimeNumber(colorCode, ctx);
}

/**
 * Set color pair for printAt (activate)
 */
static Node* cursUsePair(Scope* scope, Context* ctx) {
  Node* pair = getParameter(scope, "pair", FatNumber, ctx);
  if (IS_FAT_ERROR(pair)) {
    return pair;
  }

  if (!cursesMode) {
    startCursesMode(ctx);
  }

  if (attron(COLOR_PAIR((int)pair->num.f)) == OK) {
    return NULL;  // success
  }

  return createError("failed to set color pair", true, NULL, ctx);
}

/**
 * Align view to screen center
 */
static Node* cursFrameTo(Scope* scope, Context* ctx) {
  Node* cols = getParameter(scope, "cols", FatNumber, ctx);
  if (IS_FAT_ERROR(cols)) {
    return cols;
  }

  Node* rows = getParameter(scope, "rows", FatNumber, ctx);
  if (IS_FAT_ERROR(rows)) {
    return rows;
  }

  if (!cursesMode) {
    startCursesMode(ctx);
  }

  frameOffsetX = 0;
  frameOffsetY = 0;

  int frameSizeX = (int)cols->num.f;
  if (frameSizeX > 2) {
    int maxX = getmaxx(stdscr);
    if (frameSizeX < maxX) {
      frameOffsetX = (maxX - frameSizeX) / 2;
    }
  }

  int frameSizeY = (int)rows->num.f;
  if (frameSizeY > 2) {
    int maxY = getmaxy(stdscr);
    if (frameSizeY < maxY) {
      frameOffsetY = (maxY - frameSizeY) / 2;
    }
  }

  return NULL;  // success
}

#ifndef __EMSCRIPTEN__
static char keyBuffer[2] = {0};

static const char* keyCodeToText(int code) {
  switch (code) {
    case ERR:
      return NULL;
    case KEY_UP:
      return "up";
    case KEY_DOWN:
      return "down";
    case KEY_LEFT:
      return "left";
    case KEY_RIGHT:
      return "right";
    case KEY_PPAGE:
      return "pageUp";
    case KEY_NPAGE:
      return "pageDown";
    case KEY_HOME:
      return "home";
    case KEY_END:
      return "end";
    case KEY_IC:
      return "insert";
    case KEY_DC:
      return "delete";
    case KEY_BACKSPACE:
    case '\b':
    case '\177':
      return "backspace";
    case KEY_ENTER:
    case '\n':
      return "enter";
    case KEY_SPACE:
      return "space";
    case KEY_STAB:
    case '\t':
      return "tab";
    case KEY_BTAB:
      return "backTab";
    case KEY_RESIZE:
      return "resize";
    case KEY_ESC:
      // esc may cause delay while input looks for a following f-key sequence
      return "esc";
    default:
      if (0 > code || code > 127 || iscntrl(code)) {
        return NULL;  // non-ascii code
      }
      keyBuffer[0] = (char)code;
      return keyBuffer;
  }
}
#endif

/**
 * Return key pressed as text
 */
static Node* cursReadKey(Context* ctx) {
  if (!cursesMode) {
    startCursesMode(ctx);
  }

  Node* result = NULL;

#ifndef __EMSCRIPTEN__
  int input = getch();
  result = runtimeTextDup(keyCodeToText(input), ctx);
#else  // __EMSCRIPTEN__
  emscripten_sleep(JS_IO_LAG);
  char* input = getInputBuffer();
  if (input && *input) {
    result = runtimeText(input, strlen(input), ctx);
  }
#endif

  return result;
}

/**
 * Text box input auxiliary method
 */
static inline bool isPrintable(int code) {
  switch (code) {
    case ERR:
    case KEY_UP:
    case KEY_DOWN:
    case KEY_LEFT:
    case KEY_RIGHT:
    case KEY_PPAGE:
    case KEY_NPAGE:
    case KEY_HOME:
    case KEY_END:
    case KEY_IC:
    case KEY_DC:
    case KEY_BTAB:
    case KEY_RESIZE:
      return false;
    default:
      return !iscntrl(code);
  }
}

static size_t utf8ByteOffset(const char* str, size_t visibleLength) {
  size_t byteOffset = strlen(str);
  size_t charCount = 0;
  while (charCount < visibleLength && byteOffset > 0) {
    byteOffset--;
    if ((str[byteOffset] & 0xC0) != 0x80) {
      charCount++;
    }
  }
  return byteOffset;
}

/**
 * Start a text box input
 */
static Node* cursReadText(Scope* scope, Context* ctx) {
  Node* pos = getParameter(scope, "pos", FatScope, ctx);
  if (IS_FAT_ERROR(pos)) {
    return pos;
  }

  Node* x = getParameter(pos->scp, "x", FatNumber, ctx);
  if (IS_FAT_ERROR(x)) {
    return x;
  }

  Node* y = getParameter(pos->scp, "y", FatNumber, ctx);
  if (IS_FAT_ERROR(y)) {
    return y;
  }

  Node* width = getParameter(scope, "width", FatNumber, ctx);
  if (IS_FAT_ERROR(width)) {
    return width;
  }

  if (!cursesMode) {
    startCursesMode(ctx);
  }

  int px = frameOffsetX + (int)x->num.f;
  int py = frameOffsetY + (int)y->num.f;

  int cols = (int)width->num.f;  // viewable fragment of text

  // optional parameter
  Node* prev = getValueOf(scope, "prev");
  const char* prevVal = IS_FAT_TYPE(prev, FatText) ? prev->val : NULL;

  int len = prevVal ? (int)strlen(prevVal) : 0;  // text total size
  int size = len ? len : cols;
  char* input = len ? strDup(prevVal) : FRY_CALLOC(size + 1);

  // print initial input
  mvprintw(py, px, "%*s", cols, "");  // clear input area
  if ((int)utf8len(input) >= cols) {
    mvaddstr(py, px, input + utf8ByteOffset(input, cols - 1));
    mvaddstr(py, px + cols - 1, "_");
  } else {
    mvprintw(py, px, "%s_", input);
  }
  refresh();

  nodelay(stdscr, false);  // make getch blocking
  while (true) {
    int ch = getch();

    bool isEnter = ch == KEY_ENTER || ch == '\n' || ch == '\r';
    bool isTab = ch == KEY_STAB || ch == '\t';
    if (isEnter || isTab) {
      break;
    }

    if (ch == KEY_ESC || ch == '\004') {
      free(input);
      input = NULL;
      break;
    }

    if (ch == KEY_BACKSPACE || ch == '\b' || ch == '\177') {
      if (len > 0) {
        int byteOffset = len - 1;
        while ((byteOffset > 0) && ((input[byteOffset] & 0xC0) == 0x80)) {
          byteOffset--;
        }
        len = byteOffset;
      }
    } else if (isPrintable(ch)) {  // other characters
      if (len == size) {
        size *= 2;
        input = FRY_REALLOC(input, size + 1);
      }
      input[len++] = (char)ch;
    }
    input[len] = '\0';

    // print updated input
    mvprintw(py, px, "%*s", cols, "");  // clear input area

    if ((int)utf8len(input) >= cols) {
      mvaddstr(py, px, input + utf8ByteOffset(input, cols - 1));
      mvaddstr(py, px + cols - 1, "_");
    } else {
      mvprintw(py, px, "%s_", input);
    }
    refresh();
  }
  nodelay(stdscr, true);  // revert getch to non-blocking

  mvprintw(py, px, "%*s", cols, "");  // clear input area again

  if (!input) {
    refresh();
    return NULL;
  }

  // print final input with ellipsis
  if ((int)utf8len(input) > cols) {
    printUtf8Ellipsis(py, px, input, cols);
  } else {
    mvprintw(py, px, "%s", input);
  }
  refresh();

  return runtimeText(FRY_REALLOC(input, len + 1), len, ctx);
}

/**
 * Flush input buffer
 */
static Node* cursFlushKeys(void) {
  if (cursesMode) {
    flushinp();
  }
  return NULL;
}
