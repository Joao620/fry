/**
 * @file enigma.c
 * @brief Cryptography, hash and UUID methods
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.1.0
 * @date 2024-01-27
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

static const char* LIB_ENIGMA =
    "# fat.enigma - Cryptography, hash and UUID methods\n"
    "\n"
    "## Get 32-bit hash of text\n"
    "getHash = (key: Text): Number -> $getHash\n"
    "\n"
    "## Generate a UUID (version 4)\n"
    "genUUID = (): Text -> $genUUID\n"
    "\n"
    "## Generate a random key of specified length\n"
    "genKey = (len: Number): Text -> $genKey\n"
    "\n"
    "## Simple key derivation function (KDF)\n"
    "derive = (secret: Text): Text -> $derive\n"
    "\n"
    "## Encrypt msg using key (optional)\n"
    "encrypt = (msg: Text, key: Text = null): Text -> $encrypt\n"
    "\n"
    "## Decrypt msg using key (optional)\n"
    "decrypt = (msg: Text, key: Text = null): Text -> $decrypt\n";

/**
 * Build a 32-bit hash of msg via jesteress algorithm
 */
static Node* enigmaGetHash(Scope* scope, Context* ctx) {
  Node* key = getParameter(scope, "key", FatText, ctx);
  if (IS_FAT_ERROR(key)) {
    return key;
  }

  return runtimeNumber(jesteressHash32(key->val, key->num.s), ctx);
}

/**
 * Generate a UUID (version 4)
 */
static Node* enigmaGenUUID(Context* ctx) {
  return runtimeText(generateUuid(), 36, ctx);
}

/**
 * Generate a random key of specified length
 */
static Node* enigmaGenKey(Scope* scope, Context* ctx) {
  Node* len = getParameter(scope, "len", FatNumber, ctx);
  if (IS_FAT_ERROR(len)) {
    return len;
  }

  size_t keyLen = (size_t)len->num.f;
  return runtimeText(generateKey(keyLen), keyLen, ctx);
}

/**
 * Simple key derivation function (KDF)
 */
static Node* enigmaDerive(Scope* scope, Context* ctx) {
  Node* secret = getParameter(scope, "secret", FatText, ctx);
  if (IS_FAT_ERROR(secret)) {
    return secret;
  }

  char* derived = deriveKey(secret->val, secret->num.s);
  return runtimeText(derived, DERIVED_SIZE, ctx);
}

/**
 * Encode msg using secret key to url-safe characters
 */
static Node* enigmaEncrypt(Scope* scope, Context* ctx) {
  Node* msg = getParameter(scope, "msg", FatText, ctx);
  if (IS_FAT_ERROR(msg)) {
    return msg;
  }

  // optional parameter
  Node* key = getValueOf(scope, "key");
  const char* keyVal = IS_FAT_TYPE(key, FatText) ? key->val : NULL;

  char* result = xorEncode(keyVal, msg->val);
  return result ? runtimeText(result, strlen(result), ctx) : NULL;
}

/**
 * Decode msg using secret key to url-safe characters
 */
static Node* enigmaDecrypt(Scope* scope, Context* ctx) {
  Node* msg = getParameter(scope, "msg", FatText, ctx);
  if (IS_FAT_ERROR(msg)) {
    return msg;
  }

  // optional parameter
  Node* key = getValueOf(scope, "key");
  const char* keyVal = IS_FAT_TYPE(key, FatText) ? key->val : NULL;

  char* result = xorDecode(keyVal, msg->val);
  return result ? runtimeText(result, strlen(result), ctx) : NULL;
}
