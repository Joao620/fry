/**
 * @file memo.c
 * @brief Generic memoization utility
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.0
 * @date 2024-04-07
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

static const char* LIB_MEMO =
    "# fat.extra.Memo - Generic memoization utility\n"
    "\n"
    "_ <- fat.type.Method\n"
    "_ <- fat.type.Error\n"
    "\n"
    "Memo = (\n"
    "  ## Argument slot (method to memoize)\n"
    "  method: Method\n"
    "\n"
    "  ## Constructor\n"
    "  apply = -> {\n"
    "    ~method.arity > 1 => ValueError('bad method arity')\n"
    "    _                 => ({ method = ~method, memo = {} })\n"
    "  }\n"
    "\n"
    "  ## Convenience method transformer\n"
    "  asMethod = -> {\n"
    "    curry = self.~method.arity\n"
    "      ? (m: Memo) -> (arg: Any) -> m.call(arg)\n"
    "      : (m: Memo) -> -> m.call\n"
    "    curry(self)\n"
    "  }\n"
    "\n"
    "  ## Memoize call implementation\n"
    "  call = -> {\n"
    "    key = _ == Text ? _ : $stringify  # use arg as key\n"
    "    self.memo(key) ?? (self.memo.[key] = self.method(_))\n"
    "  }\n"
    ")\n";
