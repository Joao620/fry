/**
 * @file param.c
 * @brief Parameter presence and type verification
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-27
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

static const char* LIB_PARAM =
    "# fat.extra.Param - Parameter presence and type verification\n"
    "\n"
    "_ <- fat.type.Text\n"
    "_ <- fat.type.Error\n"
    "\n"
    "Param = (\n"
    "  ## Arguments\n"
    "  _exp: Text  # parameter name to check in context\n"
    "  _typ: Text  # expected type of the evaluated value\n"
    "\n"
    "  ## Constructor\n"
    "  apply = -> {\n"
    "    (_exp == '_' | _exp == '_exp' | _exp == '_typ' |\n"
    "      _exp == '_val' | _exp == 'apply' | _exp == 'get') =>\n"
    "      ValueError('\\'{_exp}\\' is a restricted expression')\n"
    "\n"
    "    (_typ.isEmpty | _typ(0) != _typ(0).toUpper) =>\n"
    "      ValueError('\\'{_typ}\\' is an invalid type name')\n"
    "\n"
    "    _ => self\n"
    "  }\n"
    "\n"
    "  ## Prototype method\n"
    "  get = -> {\n"
    "    _val = (-> $eval)('~' + self._exp)\n"
    "    t = (-> $typeOf)(_val)\n"
    "    { _typ } = self\n"
    "\n"
    "    t == _typ | t.startsWith(_typ + '/') |\n"
    "      _typ == 'Option' & (t == 'Some' | t == 'None') => ~_val\n"
    "\n"
    "    t == 'Void' => KeyError(\n"
    "      '\\'{self._exp}\\' not defined in the execution context'\n"
    "    )\n"
    "    _ => TypeError(\n"
    "      '\\'{self._exp}\\' of type {_typ} expected, found {t}'\n"
    "    )\n"
    "  }\n"
    ")\n"
    "\n"
    "### Utility to disable implicitParameter warnings\n"
    "Using = (_exp, _typ, apply = -> { Param(_exp, _typ).get, () })\n";
