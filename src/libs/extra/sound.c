/**
 * @file sound.c
 * @brief Sound playback interface
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.2.0
 * @date 2024-02-07
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

static const char* LIB_SOUND =
    "# fat.extra.Sound - Sound playback interface\n"
    "\n"
    "### Wrapper for command-line audio players using fork and kill\n"
    "\n"
    "Sound = (\n"
    "  ## Argument slots\n"
    "  path: Text\n"
    "  duration: Number = 0                   # in milliseconds\n"
    "  player: List/Text = [ 'aplay', '-q' ]  # try with: mpg123, afplay...\n"
    "  # ...also [ 'ffplay', '-nodisp', '-autoexit', '-loglevel', 'quiet' ]\n"
    "\n"
    "  ## Constructor\n"
    "  apply = -> {\n"
    "    $exists => self\n"
    "    _       => {\n"
    "      msg = '** Sound: {path} not found'\n"
    "      fg = 3  # yellow color\n"
    "      $stderr\n"
    "      {}\n"
    "    }\n"
    "  }\n"
    "\n"
    "  ## start player, if not already playing\n"
    "  play = -> {\n"
    "    { path, duration, player, pid, started } = self\n"
    "    path != Void & (pid == Void | started + duration < $now) ? {\n"
    "      args = player + [ path ]\n"
    "      out = '/dev/null'\n"
    "      self.~pid = $isWeb ? $soundPlay : $fork\n"
    "      self.~started = $now\n"
    "      ()\n"
    "    }\n"
    "  }\n"
    "\n"
    "  ## stop player, if still playing (estimate on duration)\n"
    "  stop = -> {\n"
    "    { duration, pid, started } = self\n"
    "    pid != Void ? {\n"
    "      started + duration > $now ? $isWeb ? $soundStop : $kill\n"
    "      self.pid = null\n"
    "      self.started = null\n"
    "    }\n"
    "  }\n"
    ")\n";

#ifdef __EMSCRIPTEN__

#include "../../interpreter.h"

// Structure representing a single audio slot
typedef struct {
  Mix_Chunk* sound;  // pointer to the loaded sound chunk
  int channel;       // the audio channel this slot is using
  bool isActive;     // flag to indicate if the slot is currently in use
} AudioSlot;

// Structure representing a FIFO queue for audio slots
typedef struct {
  AudioSlot slots[MIX_CHANNELS];  // array of audio slots
  int nextSlot;                   // index of the next available slot
} AudioFIFO;

// Global instance of the audio FIFO
static AudioFIFO audioFIFO = {0};

// Function to get the next available audio slot (impl. bellow)
static int getAudioSlot(void);

static int playAudioImplementation(const char* filename) {
  int pid = getAudioSlot();
  if (pid < 0) {
    return -1;  // no available slot
  }

  AudioSlot* slot = &audioFIFO.slots[pid];

  // Load WAV file
  slot->sound = Mix_LoadWAV(filename);
  if (!slot->sound) {
    return -1;
  }

  // Play sound
  slot->channel = Mix_PlayChannel(pid, slot->sound, 0);
  if (slot->channel == -1) {
    Mix_FreeChunk(slot->sound);
    slot->sound = NULL;
    return -1;
  }

  slot->isActive = true;
  return pid;
}

static void stopAudioImplementation(int pid) {
  AudioSlot* slot = &audioFIFO.slots[pid];

  if (slot->isActive) {
    Mix_HaltChannel(slot->channel);
    slot->isActive = false;
  }

  if (slot->sound) {
    Mix_FreeChunk(slot->sound);
    slot->sound = NULL;
  }
}

static int getAudioSlot(void) {
  for (int i = 0; i < MIX_CHANNELS; i++) {
    int pid = audioFIFO.nextSlot;
    audioFIFO.nextSlot = (pid + 1) % MIX_CHANNELS;

    AudioSlot* slot = &audioFIFO.slots[pid];

    // Check if slot is available or can be reused
    if (slot->sound) {
      if (!Mix_Playing(slot->channel)) {
        stopAudioImplementation(pid);  // clean up and reuse
        return pid;
      }
    } else {
      return pid;  // use empty slot
    }
    // ...still playing, try next slot
  }
  return -1;
}

static Node* soundPlay(Scope* scope, Context* ctx) {
  if (!hasAudioSupport) {
    runtimeNumber(-1.0, ctx);
  }

  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  int pid = playAudioImplementation(path->val);

  if (pid < 0) {
    char* msg = (char*)Mix_GetError();
    if (!msg || !*msg) {
      msg = "max simultaneous sound channels reached";
    }
    return createError(msg, true, NULL, ctx);
  }

  return runtimeNumber((double)pid, ctx);
}

static Node* soundStop(Scope* scope, Context* ctx) {
  if (!hasAudioSupport) {
    return NULL;
  }

  Node* pid = getParameter(scope, "pid", FatNumber, ctx);
  if (IS_FAT_ERROR(pid)) {
    return pid;
  }

  if (pid->num.f >= 0 && pid->num.f < MIX_CHANNELS) {
    stopAudioImplementation((int)pid->num.f);
  }
  return NULL;
}

#endif
