/**
 * @file failure.c
 * @author Antonio Prates <hello@aprates.dev>
 * @brief Error handling and exception management
 * @version 2.5.1
 * @date 2024-04-19
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

static const char* LIB_FAILURE =
    "# fat.failure Error handling and exception management\n"
    "\n"
    "## Apply generic error handler\n"
    "trap = (): Void -> {\n"
    "  handler = (error: Any): Void -> {\n"
    "    msg = '\\n** {error}', fg = 1, code = 1\n"
    "    $flush, $stderr, $stack, $exit\n"
    "  }\n"
    "  $trap\n"
    "}\n"
    "\n"
    "## Set a handler for errors in context\n"
    "trapWith = (handler: Method): Void -> $trap\n"
    "\n"
    "## Unset error handler in context\n"
    "untrap = (): Void -> $untrap\n";

static const int trapDepth = 4;

static int getTrapLevel(Context* ctx) {
  int trapLevel = atomic_load(&ctx->top) - trapDepth;
  if (trapLevel < 1) {
    trapLevel = 1;
  }
  return trapLevel;
}

/* Set a handler for errors in context */
static Node* failTrap(Scope* scope, Context* ctx) {
  Node* handler = getParameter(scope, "handler", FatMethod, ctx);
  if (IS_FAT_ERROR(handler)) {
    return handler;
  }

  const int trapLevel = getTrapLevel(ctx);

  if (ctx->stack[trapLevel].trap) {
    return createError("scope already trapped", true, ckAssignError, ctx);
  }

  ctx->stack[trapLevel].trap = handler;
  return NULL;
}

/* Unset error handler in context */
static Node* failUntrap(Context* ctx) {
  ctx->stack[getTrapLevel(ctx)].trap = NULL;
  return NULL;
}
