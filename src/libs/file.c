/**
 * @file file.c
 * @brief File input and output operations
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.1
 * @date 2024-04-19
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

static const char* LIB_FILE =
    "# fat.file - File input and output operations\n"
    "\n"
    "FileInfo = (\n"
    "  modTime: Epoch  # last modified\n"
    "  size: Text      # bytes\n"
    ")\n"
    "\n"
    "## Extract base path of app call\n"
    "basePath = (): Text -> $basePath\n"
    "\n"
    "## Check file exists on provided path\n"
    "exists = (path: Text): Boolean -> $exists\n"
    "\n"
    "## Read file from path (text mode)\n"
    "read = (path: Text): Text -> $read\n"
    "\n"
    "## Read file from path (binary mode)\n"
    "readBin = (path: Text): Chunk -> $readBin\n"
    "\n"
    "## Write src to file and return success\n"
    "write = (path: Text, src: Any): Boolean -> $write\n"
    "\n"
    "## Append to file and return success\n"
    "append = (path: Text, src: Any): Boolean -> $append\n"
    "\n"
    "## Remove file and return success\n"
    "remove = (path: Text): Boolean -> $remove\n"
    "\n"
    "## Check if path is a directory\n"
    "isDir = (path: Text): Boolean -> $isDir\n"
    "\n"
    "## Create a directory\n"
    "mkDir = (path: Text, safe: Boolean = false): Boolean -> $mkDir\n"
    "\n"
    "## Get list of files in a directory\n"
    "lsDir = (path: Text): List -> $lsDir\n"
    "\n"
    "## Get file metadata\n"
    "stat = (path: Text): FileInfo -> $stat\n";

/**
 * Extract the base path where the app was called
 */
static Node* fileBasePath(Context* ctx) {
  return runtimeTextDup(basePath ? basePath : "", ctx);
}

/**
 * Check file exists from text path and return boolean
 */
static Node* fileExists(Scope* scope, Context* ctx) {
  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  return RUNTIME_BOOLEAN(existsFile(path->val));
}

/**
 * Read file from text path (text mode) and return content or null
 */
static Node* fileRead(Scope* scope, Context* ctx) {
  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  char* result = NULL;
  long len = readFile(&result, path->val, "r");
  return result ? runtimeText(result, (size_t)len, ctx) : NULL;
}

/**
 * Read file from text path (binary mode) and return content or null
 */
static Node* fileReadBin(Scope* scope, Context* ctx) {
  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  char* result = NULL;
  long len = readFile(&result, path->val, "rb");
  return result ? runtimeChunk(result, (size_t)len, ctx) : NULL;
}

/**
 * Write src to path and return boolean success
 */
static Node* fileWrite(Scope* scope, Context* ctx, bool append) {
  if (isJailMode) {
    return createError("write " MSG_JAIL, true, ckCallError, ctx);
  }

  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  const char* file = path->val;

  Node* source = getValueOf(scope, "src");  // Any
  if (!source) {
    char* msg = join3(MSG_M_PAR, GUIDE, "src");
    return createError(msg, false, ckCallError, ctx);
  }

  char* buffer = NULL;
  size_t len = 0;

  char* mode = append ? "a" : "w";
  bool shouldFree = false;

  switch (source->type) {
    case FatChunk:
      // unless binary data, mode is already set
      mode = append ? "ab" : "wb";
      FALL_THROUGH;
    case FatText:
    case FatTemp:
      buffer = source->val;
      len = source->num.s;
      break;

    default:
      buffer = toJson(source);
      len = strlen(buffer);
      shouldFree = true;
  }

  bool success = writeFile(file, buffer, len, mode);
  if (shouldFree) {
    free(buffer);
  }
  return RUNTIME_BOOLEAN(success);
}

/**
 * Erase file of text path and return boolean success
 */
static Node* fileRemove(Scope* scope, Context* ctx) {
  if (isJailMode) {
    return createError("remove " MSG_JAIL, true, ckCallError, ctx);
  }

  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  return RUNTIME_BOOLEAN(remove(path->val) == 0);
}

/**
 * Check if text path is a directory and return boolean
 */
static Node* fileIsDir(Scope* scope, Context* ctx) {
  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  return RUNTIME_BOOLEAN(isDir(path->val));
}

/**
 * Create directory and return boolean
 * (if safe dir gets 0700 permission instead of 0755)
 */
static Node* fileMkDir(Scope* scope, Context* ctx) {
  if (isJailMode) {
    return createError("mkDir " MSG_JAIL, true, ckCallError, ctx);
  }

  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  Node* safe = getParameter(scope, "safe", FatBoolean, ctx);
  if (IS_FAT_ERROR(safe)) {
    return safe;
  }

  mode_t mode = safe->num.b ? 0700 : 0755;

  if (mkdir(path->val, mode)) {
    logError(path->val, __func__, strerror(errno));
    return falseSingleton;
  }

  return trueSingleton;
}

/**
 * Get list of files in a directory
 */
static Node* fileLsDir(Scope* scope, Context* ctx) {
  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  char** list = listDir(path->val);
  if (!list) {
    return NULL;
  }

  Scope* contents = createList();
  pushStack(ctx, __func__, path, contents);

  for (size_t i = 0; list[i]; i++) {
    addToList(contents, runtimeText(list[i], strlen(list[i]), ctx), ctx);
  }

  Node* result = runtimeCollection(contents, ctx);

  popStack(ctx, 1);
  free(list);

  return result;
}

/**
 * Get file metadata
 */
static Node* fileStat(Scope* scope, Context* ctx) {
  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  struct stat fileInfo;
  if (stat(path->val, &fileInfo)) {
    return createError("unable to get file metadata", true, ckFileError, ctx);
  }

  scope = createScope();
  scope->ck = setType("FileInfo", NULL);
  pushStack(ctx, __func__, path, scope);

  Node* modTime = runtimeNumber((double)fileInfo.st_mtime * 1000, ctx);
  modTime->ck = ckEpoch;
  addToScope(scope, "modTime", modTime);

  Node* size = runtimeNumber((double)fileInfo.st_size, ctx);
  addToScope(scope, "size", size);

  Node* statResponse = runtimeCollection(scope, ctx);

  popStack(ctx, 1);
  return statResponse;
}
