/**
 * @file http.c
 * @brief HTTP handling framework
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-30
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

static const char *LIB_HTTP =
    "# fat.http - HTTP handling framework\n"
    "\n"
    "Route = (\n"
    "  path: Text = '*'      # including wildcard\n"
    "  post: Method          # HttpRequest -> HttpResponse\n"
    "  get: Method           # HttpRequest -> HttpResponse\n"
    "  put: Method           # HttpRequest -> HttpResponse\n"
    "  delete: Method        # HttpRequest -> HttpResponse\n"
    ")\n"
    "\n"
    "HttpRequest = (\n"
    "  method: Text          # POST, GET, PUT, DELETE\n"
    "  path: Text            # all but query parameters\n"
    "  params: Scope/Text    # query parameters\n"
    "  headers: List/Text    # flat list of headers\n"
    "  body: Any             # aka request body\n"
    ")\n"
    "\n"
    "HttpResponse = (\n"
    "  status: Number        # HTTP status code\n"
    "  headers: List/Text    # flat list of headers\n"
    "  body: Any             # aka response body\n"
    ")\n"
    "\n"
    "## CRUD request methods (client mode)\n"
    "setHeaders = (headers: List/Text): Void -> $setHeaders\n"
    "\n"
    "post = (url: Text, body: Any = null, wait: Number = 30000) -> {\n"
    "  method = 'POST'\n"
    "  $request\n"
    "}\n"
    "\n"
    "get = (url: Text, wait: Number = 30000) -> {\n"
    "  method = 'GET'\n"
    "  $request\n"
    "}\n"
    "\n"
    "put = (url: Text, body: Any = null, wait: Number = 30000) -> {\n"
    "  method = 'PUT'\n"
    "  $request\n"
    "}\n"
    "\n"
    "delete = (url: Text, wait: Number = 30000) -> {\n"
    "  method = 'DELETE'\n"
    "  $request\n"
    "}\n"
    "\n"
    "## Auxiliary method (client and server modes)\n"
    "setName = (name: Text): Void -> $setName\n"
    "\n"
    "## SSL configuration (client mode)\n"
    "verifySSL = (enabled: Boolean): Void -> $verifySSL\n"
    "\n"
    "## SSL configuration (server mode)\n"
    "setSSL = (certPath: Text, keyPath: Text): Void -> $setSSL\n"
    "\n"
    "## Endpoint provider (server mode)\n"
    "listen = (port: Number, routes: List/Route): Void -> $listen\n";

typedef struct curl_slist CurlHeaders;

typedef struct CurlHeaderResult {
  Context *ctx;
  Scope *headers;
} CurlHeaderResult;

typedef struct CurlDataResult {
  char *data;
  size_t size;
} CurlDataResult;

typedef CURLcode CurlCode;

typedef CURL CurlSession;

static __thread char *softwareName = NULL;

static __thread CurlHeaders *headers = NULL;

static __thread bool verifySSL = true;

#ifdef SSL_SUPPORT
static bool isSslEnabled = false;

#ifdef DEBUG
static void ssl_info_callback(const SSL *ssl, int where, int ret) {
  if (!debugLogs) {
    return;
  }
  const char *str;
  int w = where & ~SSL_ST_MASK;
  if (w & SSL_ST_CONNECT) {
    str = "SSL_connect";
  } else if (w & SSL_ST_ACCEPT) {
    str = "SSL_accept";
  } else {
    str = "undefined";
  }
  if (where & SSL_CB_LOOP) {
    fprintf(stderr, MRG_STR "%s:%s\n", str, SSL_state_string_long(ssl));
  } else if (where & SSL_CB_ALERT) {
    str = (where & SSL_CB_READ) ? "read" : "write";
    fprintf(stderr, MRG_STR "SSL3 alert %s:%s:%s\n", str,
            SSL_alert_type_string_long(ret), SSL_alert_desc_string_long(ret));
  } else if (where & SSL_CB_EXIT) {
    if (ret == 0) {
      fprintf(stderr, MRG_STR "%s:failed in %s\n", str,
              SSL_state_string_long(ssl));
    } else if (ret < 0) {
      fprintf(stderr, MRG_STR "%s:error in %s\n", str,
              SSL_state_string_long(ssl));
    }
  }
}
#endif

static Node *httpSetSSL(Scope *scope, Context *ctx) {
  if (isSslEnabled) {
    return createError("SSL context already set", true, ckCallError, ctx);
  }

  if (!sslCtx) {
    // Create an SSL context
    sslCtx = SSL_CTX_new(TLS_server_method());
    if (!sslCtx) {
      return createError("failed to create SSL context", true, NULL, ctx);
    }
  }

  // Get configuration parameters (file paths)
  Node *certPath = getParameter(scope, "certPath", FatText, ctx);
  if (IS_FAT_ERROR(certPath)) {
    return certPath;
  }

  Node *keyPath = getParameter(scope, "keyPath", FatText, ctx);
  if (IS_FAT_ERROR(keyPath)) {
    return keyPath;
  }

  // Configure SSL context (load certificates)
  SSL_CTX_use_certificate_file(sslCtx, certPath->val, SSL_FILETYPE_PEM);
  SSL_CTX_use_PrivateKey_file(sslCtx, keyPath->val, SSL_FILETYPE_PEM);
  if (!SSL_CTX_check_private_key(sslCtx)) {
    return createError("SSL failed key validation", true, ckValueError, ctx);
  }

  isSslEnabled = true;
  logMarker("SSL/TLS enabled!");

#ifdef DEBUG
  SSL_CTX_set_info_callback(sslCtx, ssl_info_callback);
#endif

  return NULL;
}
#endif

static char *httpGetName(void) {
  if (!softwareName) {
    softwareName = join2("fry/", getFryV());
  }
  return softwareName;
}

static size_t headerCallback(char *buff, size_t size, size_t count, void *p) {
  CurlHeaderResult *result = (CurlHeaderResult *)p;
  size_t bytes = size * count;

  char *header = strTrim(buff, bytes);
  if (strchr(header, ':')) {
    addToList(result->headers, runtimeText(header, strlen(header), result->ctx),
              result->ctx);
  } else {
    free(header);
  }

  return bytes;
}

static size_t dataCallback(void *buff, size_t size, size_t count, void *p) {
  CurlDataResult *result = (CurlDataResult *)p;
  size_t bytes = size * count;
  result->data = FRY_REALLOC(result->data, result->size + bytes + 1);
  memcpy(result->data + result->size, buff, bytes);
  result->size += bytes;
  result->data[result->size] = '\0';
  return bytes;
}

static inline double getHttpStatus(CurlSession *session) {
  long status = 0;
  curl_easy_getinfo(session, CURLINFO_RESPONSE_CODE, &status);
  return (double)status;
}

/**
 * Set headers of next requests
 */
static Node *httpSetHeaders(Scope *scope, Context *ctx) {
  Node *list = getParameter(scope, "headers", FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  if (!list->scp || !list->scp->size || list->scp->ck != checkText) {
    return createError("invalid headers", true, ckValueError, ctx);
  }

  if (headers) {
    curl_slist_free_all(headers);
    headers = NULL;
  }

  for (Entry *entry = list->scp->entries; entry; entry = entry->next) {
    headers = curl_slist_append(headers, entry->data->val);
  }

  return NULL;
}

/**
 * Toggle SSL certificate verification
 */
static Node *httpVerifySSL(Scope *scope, Context *ctx) {
  Node *enabled = getParameter(scope, "enabled", FatBoolean, ctx);
  if (IS_FAT_ERROR(enabled)) {
    return enabled;
  }

  verifySSL = enabled->num.b;
  return NULL;
}

/**
 * Try to infer if content is text from headers
 */
static bool contentTypeIndicatesText(Scope *headersList) {
  for (Entry *item = headersList->entries; item; item = item->next) {
    const char *header = item->data->val;

    if (strcasestr(header, "Content-Type:")) {
      const char *contentType = header + strlen("Content-Type:");
      while (*contentType == ' ') {
        contentType++;
      }

      // Check if the content type indicates text
      if (strcasestr(contentType, "text/") ||
          strcasestr(contentType, "application/json") ||
          strcasestr(contentType, "application/xml")) {
        return true;
      }

      break;
    }
  }

  return false;
}

/**
 * Extract content length from headers
 */
static long getContentLength(Scope *headersList) {
  for (Entry *item = headersList->entries; item; item = item->next) {
    const char *header = item->data->val;

    if (strcasestr(header, "Content-Length:")) {
      // Parse the value after "Content-Length:"
      const char *contentLengthValue = header + strlen("Content-Length:");

      // Trim leading spaces if necessary
      while (*contentLengthValue == ' ') {
        contentLengthValue++;
      }

      // Convert string to long
      return strtol(contentLengthValue, NULL, 10);
    }
  }

  return -1;  // Not found
}

/**
 * Perform http/https request to url, with optional text body
 */
static Node *httpRequest(Scope *scope, Context *ctx) {
  if (isJailMode) {
    return createError("request " MSG_JAIL, true, ckCallError, ctx);
  }

  Node *method = getParameter(scope, "method", FatText, ctx);
  if (IS_FAT_ERROR(method)) {
    return method;
  }

  Node *url = getParameter(scope, "url", FatText, ctx);
  if (IS_FAT_ERROR(url)) {
    return url;
  }

  Node *wait = getParameter(scope, "wait", FatNumber, ctx);
  if (IS_FAT_ERROR(wait)) {
    return wait;
  }

  // Set content header and body (if defined)
  char *bodyVal = NULL;
  size_t bodyLen = 0;
  bool isBodyTransformed = false;
  char *contentType = "Content-Type: text/plain; charset=UTF-8";
  Node *body = getValueOf(scope, "body");
  if (body) {
    switch (body->type) {
      case FatChunk:
        contentType = "Content-Type: application/octet-stream";
        FALL_THROUGH;
      case FatText:
        bodyVal = body->val;
        bodyLen = body->num.s;
        break;

      default:
        contentType = "Content-Type: application/json; charset=UTF-8";
        bodyVal = toJson(body);
        bodyLen = strlen(bodyVal);
        isBodyTransformed = true;
    }
  }

  // Set default header (as fallback)
  struct curl_slist *defaultHeader = NULL;
  defaultHeader = curl_slist_append(defaultHeader, contentType);

  CurlSession *session = curl_easy_init();
  if (!session) {
    return createError(MSG_NO_SESS, true, NULL, ctx);
  }

  CurlDataResult result;
  result.size = 0;
  result.data = FRY_CALLOC(1);  // will be grown as needed

  CurlHeaderResult resCtx;
  resCtx.ctx = ctx;
  resCtx.headers = createList();

  curl_easy_setopt(session, CURLOPT_USERAGENT, httpGetName());
  if (!verifySSL) {  // should not require self-signed certificate!
    curl_easy_setopt(session, CURLOPT_SSL_VERIFYPEER, 0L);
    curl_easy_setopt(session, CURLOPT_SSL_VERIFYHOST, 0L);
  }
  curl_easy_setopt(session, CURLOPT_HTTPHEADER,
                   headers ? headers : defaultHeader);  // headers
  curl_easy_setopt(session, CURLOPT_BUFFERSIZE, 102400L);
  curl_easy_setopt(session, CURLOPT_TCP_KEEPALIVE, true);
  curl_easy_setopt(session, CURLOPT_NOPROGRESS, true);
  curl_easy_setopt(session, CURLOPT_MAXREDIRS, 50L);
  curl_easy_setopt(session, CURLOPT_TIMEOUT, (long)ceil(wait->num.f / 1000));
  curl_easy_setopt(session, CURLOPT_HEADERFUNCTION, headerCallback);
  curl_easy_setopt(session, CURLOPT_HEADERDATA, (void *)&resCtx);
  curl_easy_setopt(session, CURLOPT_WRITEFUNCTION, dataCallback);
  curl_easy_setopt(session, CURLOPT_WRITEDATA, (void *)&result);
  curl_easy_setopt(session, CURLOPT_CUSTOMREQUEST, method->val);  // method
  curl_easy_setopt(session, CURLOPT_URL, url->val);               // url
  if (bodyVal) {                                                  // body
    curl_easy_setopt(session, CURLOPT_POSTFIELDS, bodyVal);
    curl_easy_setopt(session, CURLOPT_POSTFIELDSIZE, bodyLen);
  }

  CurlCode curlCode = curl_easy_perform(session);
  curl_slist_free_all(defaultHeader);
  if (isBodyTransformed) {
    free(bodyVal);
  }

  if (curlCode != CURLE_OK) {
    char *msg = join3(curl_easy_strerror(curlCode), GUIDE, url->val);
    free(result.data);
    trackScope(resCtx.headers);
    curl_easy_cleanup(session);
    return createError(msg, false, NULL, ctx);
  }

  if (debugLogs) {
    auto_str strSize = ofInt((long)result.size);
    logDebug2(__FILE__, __func__, "bytes", strSize);
  }

  scope = createScope();
  scope->ck = setType("HttpResponse", NULL);
  pushStack(ctx, __func__, url, scope);

  Node *status = runtimeNumber(getHttpStatus(session), ctx);
  addToScope(scope, "status", status);

  Node *resHeaders = runtimeCollection(resCtx.headers, ctx);
  addToScope(scope, "headers", resHeaders);

  if (contentTypeIndicatesText(resCtx.headers)) {
    addToScope(scope, "body", runtimeText(result.data, result.size, ctx));
  } else {
    addToScope(scope, "body", runtimeChunk(result.data, result.size, ctx));
  }

  Node *httpResponse = runtimeCollection(scope, ctx);

  popStack(ctx, 1);
  curl_easy_cleanup(session);

  return httpResponse;
}

/**
 * Set user agent/server name
 */
static Node *httpSetName(Scope *scope, Context *ctx) {
  Node *name = getParameter(scope, "name", FatText, ctx);
  if (IS_FAT_ERROR(name)) {
    return name;
  }

  free(softwareName);
  softwareName = strDup(name->val);
  return NULL;
}

#define TIME_BUFF_LEN 32

static void getCurrentTime(char *buff) {
  struct tm date;
  time_t utc = time(NULL);
  gmtime_r(&utc, &date);
  strftime(buff, TIME_BUFF_LEN, "%a, %d %b %Y %H:%M:%S %Z", &date);
}

static bool writeResponse(int fd, void *ssl, const char *response, size_t len) {
  if (!len) {
    return true;
  }

  ssize_t bytes = 0;

#ifdef SSL_SUPPORT
  size_t totalSent = 0;

  while (totalSent < len) {
    if (isSslEnabled) {
      bytes = SSL_write((SSL *)ssl, response + totalSent, len - totalSent);
      if (bytes <= 0) {
        break;
      }
    } else {
      bytes = write(fd, response + totalSent, len - totalSent);
      if (bytes <= 0) {
        break;
      }
    }
    totalSent += bytes;
  }

  if (totalSent < len) {
    logError(__FILE__, __func__, MSG_F_RESP);
    if (isSslEnabled) {
      SSL_shutdown((SSL *)ssl);
      SSL_free((SSL *)ssl);
    }
    close(fd);
    return false;
  }

  return true;

#else
  (void)ssl;
  bytes = write(fd, response, len);

  if (bytes < 0) {
    logError(__FILE__, __func__, MSG_F_RESP);
    close(fd);
    return false;
  }

  return true;
#endif
}

static void respond(double tick, int fd, void *ssl, int code, Scope *resHeaders,
                    Node *body) {
  char *title = "OK";
  char *defaultMsg = "The request has been successfully processed.";
  char *additional = "";

  switch (code) {
    // Success
    case 200:
      // default values
      break;

    case 201:  // no automatic trigger
      title = "Created";
      defaultMsg = "The request successfully created a new resource.";
      break;

    case 202:  // no automatic trigger
      title = "Accepted";
      defaultMsg = "The request has been accepted, but still processing.";
      break;

    case 204:  // no automatic trigger
      // The request was successful, but no content to return
      title = "No Content";
      defaultMsg = NULL;  // ensure nothing is returned
      break;

    case 205:  // no automatic trigger
      title = "Reset Content";
      defaultMsg = "The client shall reset the view of the content.";
      break;

    // Redirection
    case 301:  // no automatic trigger, use with Location header
      title = "Moved Permanently";
      defaultMsg = "This resource has been permanently moved.";
      break;

    // Client Error
    case 400:
      title = "Bad Request";
      defaultMsg = "Malformed URL could not be understood by the server.";
      break;

    case 401:  // no automatic trigger
      title = "Unauthorized";
      defaultMsg = "The request requires user authentication.";
      break;

    case 403:  // no automatic trigger
      title = "Forbidden";
      defaultMsg = "Client not allowed to access the requested resource.";
      break;

    case 404:
      title = "Not Found";
      defaultMsg = "The resource could not be found on this server.";
      break;

    case 405:
      title = "Method Not Allowed";
      defaultMsg = "The requested method is not allowed on this server.";
      additional = "Allow: POST, GET, PUT, DELETE\r\n";
      break;

    // Sever Error
    case 501:
      title = "Not Implemented";
      defaultMsg = "No server-side handler for this method.";
      break;

    default:
      code = 500;
      title = "Internal Server Error";
      defaultMsg = "An error happened while processing the request.";
  }

  // Obtain current date and time
  char strDate[TIME_BUFF_LEN] = {'\0'};
  getCurrentTime(strDate);

  // Prepare content type header
  char *strHeaders = "Content-Type: text/plain; charset=UTF-8";
  if (resHeaders) {
    strHeaders = joinListItems(resHeaders, "\r\n", &toString);
  } else if (body && body->type == FatChunk) {
    strHeaders = "Content-Type: application/octet-stream";
  } else if (body && body->type != FatText) {
    strHeaders = "Content-Type: application/json; charset=UTF-8";
  }

  // Create the full response header string using asprintf
  char *headerBuff = NULL;
  int headerLen =
      asprintf(&headerBuff,
               "HTTP/1.1 %d %s\r\n"      // http code and title
               "Date: %s\r\n"            // strDate
               "Server: %s\r\n"          // softwareName
               "%s%s\r\n"                // other headers
               "Connection: close\r\n",  // no persistent connections
               code, title, strDate, httpGetName(), additional, strHeaders);
  if (resHeaders) {
    free(strHeaders);  // only free if headers are provided by user
  }
  if (headerLen == -1) {
    fatalOut(__FILE__, __func__, MSG_OOM);
  }
  bool hasWritten = writeResponse(fd, ssl, headerBuff, headerLen);
  free(headerBuff);
  if (!hasWritten) {
    return;  // writeResponse closes connection on error
  }

  // Transmit content length and body (if any)
  char *bodyVal = NULL;
  size_t bodyLen = 0;
  bool isBodyTransformed = false;
  if (body) {
    switch (body->type) {
      case FatChunk:
      case FatText:
        bodyVal = body->val;
        bodyLen = body->num.s;
        break;

      default:
        bodyVal = toJson(body);
        bodyLen = strlen(bodyVal);
        isBodyTransformed = true;
    }
  } else {
    bodyVal = defaultMsg;
    bodyLen = bodyVal ? strlen(bodyVal) : 0;
  }

  if (bodyLen) {
    // Send length header
    headerLen = asprintf(&headerBuff, "Content-Length: %zu\r\n\r\n", bodyLen);
    if (headerLen == -1) {
      fatalOut(__FILE__, __func__, MSG_OOM);
    }
    hasWritten = writeResponse(fd, ssl, headerBuff, headerLen);
    free(headerBuff);
    if (!hasWritten) {
      return;  // writeResponse closes connection on error
    }

    // Send body content
    hasWritten = writeResponse(fd, ssl, bodyVal, bodyLen);
    if (isBodyTransformed) {
      free(bodyVal);
    }
    if (!hasWritten) {
      return;  // writeResponse closes connection on error
    }
  }

  // Close the connection
#ifdef SSL_SUPPORT
  if (isSslEnabled) {
    SSL_shutdown((SSL *)ssl);
    SSL_free((SSL *)ssl);
  }
#endif
  close(fd);

  // Log response
  double ms = getCurrentMs(CLOCK_MONOTONIC) - tick;
  stderrStartLine(code < 300 ? CL_GRN : code < 500 ? CL_YEL : CL_RED);
  fprintf(stderr, MRG_BLT "%s > %d %s, %.0f ms", strDate, code, title, ms);
  stderrEndLine();
}

static bool matchRoute(const char *path, const char *uri) {
  if (*path == '*') {
    if (*(path + 1) == '\0') {
      return true;
    }

    for (; *uri != '\0'; uri++) {
      if (matchRoute(path + 1, uri)) {
        return true;
      }
    }
    return false;
  }

  if (*path == '\0' && *uri == '?') {
    return true;
  }

  if (*path == *uri) {
    return *path == '\0' ? true : matchRoute(path + 1, uri + 1);
  }

  return false;
}

static Scope *findRoute(Scope *routes, const char *uri) {
  Scope *route = NULL;

  // Store configs
  const bool modeDebug = debugLogs;
  debugLogs = false;  // don't log this insanity

  for (Entry *entry = routes->entries; entry; entry = entry->next) {
    if (entry->data && entry->data->scp) {
      Scope *maybeRoute = entry->data->scp;
      const Node *path = getValueOf(maybeRoute, "path");
      if (IS_FAT_TYPE(path, FatText)) {
        if (matchRoute(path->val, uri)) {
          route = maybeRoute;
          break;
        }
      }
    }
  }

  // Restore configs
  debugLogs = modeDebug;

  return route;
}

static Scope *getQueryParams(const char *str, Context *ctx) {
  char *queryString = strchr(str, '?');
  if (!queryString) {
    return NULL;
  }

  if (debugLogs) {
    logDebug(__FILE__, __func__, queryString);
  }

  Scope *result = createScope();
  result->ck = checkScope;
  pushStack(ctx, __func__, NULL, result);

  char *ptr1 = NULL;
  char *ptr2 = NULL;
  char *token = strtok_r(queryString + 1, "&", &ptr1);
  while (token) {
    const char *key = strtok_r(token, "=", &ptr2);
    const char *value = strtok_r(NULL, "=", &ptr2);
    if (key && value && !getValueOf(result, key)) {
      addToScope(result, key, runtimeTextDup(value, ctx));
    }
    token = strtok_r(NULL, "&", &ptr1);
  }
  queryString[0] = '\0';  // hide query params in path value

  popStack(ctx, 1);

  return result;
}

static Node *handle(Scope *scope, const char *method, const char *uri,
                    Scope *reqHeaders, const char *body, Node *handler,
                    Context *ctx) {
  if (debugLogs) {
    logDebug2(__FILE__, "processing", method, uri);
  }

  Scope *requestScope = createScope();
  requestScope->ck = setType("HttpRequest", NULL);
  pushStack(ctx, __func__, NULL, requestScope);

  addToScope(requestScope, "method", runtimeTextDup(method, ctx));

  Scope *args = getQueryParams(uri, ctx);
  addToScope(requestScope, "path", runtimeTextDup(uri, ctx));
  if (args) {
    addToScope(requestScope, "params", runtimeCollection(args, ctx));
  }

  addToScope(requestScope, "headers", runtimeCollection(reqHeaders, ctx));

  long bodyLen = getContentLength(reqHeaders);
  if (body && bodyLen >= 0) {
    char *bodyVal = copyFragment(body, bodyLen);
    if (contentTypeIndicatesText(reqHeaders)) {
      addToScope(requestScope, "body", runtimeText(bodyVal, bodyLen, ctx));
    } else {
      addToScope(requestScope, "body", runtimeChunk(bodyVal, bodyLen, ctx));
    }
  }

  Node call = {.type = FatCall, .src = SRC_AUX};
  call.body = runtimeCollection(requestScope, ctx);
  popStack(ctx, 1);
  return evalMethodCall(scope, &call, handler, ctx);
}

static void logListening(int port) {
  stderrStartLine(CL_GRN);
  fprintf(stderr, MRG_BLT "Listening on port: %d - use Ctrl+C to stop", port);
  stderrEndLine();
}

static int startListening(int port) {
  int serverFd = socket(AF_INET, SOCK_STREAM, 0);
  if (serverFd < 0) {
    fatalOut(__FILE__, __func__, "unable to create socket");
  }

  struct sockaddr_in server;
  server.sin_family = AF_INET;
  server.sin_port = htons(port);
  server.sin_addr.s_addr = htonl(INADDR_ANY);
  socklen_t server_len = sizeof(server);


  if (bind(serverFd, (struct sockaddr *)&server, server_len) < 0) {
    fatalOut(__FILE__, __func__, "unable to bind socket");
  }

  if (listen(serverFd, SOMAXCONN) < 0) {
    fatalOut(__FILE__, __func__, "unable to listen");
  }

  if(port == 0) {  // get the port that was assigned
    if (getsockname(serverFd, (struct sockaddr *)&server, &server_len) == -1) {
      fatalOut(__FILE__, __func__, "unable to get socket address");
    }

    port = ntohs(server.sin_port);
  }

  // All is looking good, log startup
  logListening(port);

  return serverFd;
}

typedef enum {
  HttpNotAllowed,
  HttpPost,
  HttpGet,
  HttpPut,
  HttpDelete
} HttpMethod;

static HttpMethod parseMethod(const char *method) {
  return !method                     ? HttpNotAllowed
         : !strcmp(method, "POST")   ? HttpPost
         : !strcmp(method, "GET")    ? HttpGet
         : !strcmp(method, "PUT")    ? HttpPut
         : !strcmp(method, "DELETE") ? HttpDelete
                                     : HttpNotAllowed;
}

#define AUTO_GC_FREQ 128      // each 128 requests run GC
#define WAIT_LISTEN_RETRY 50  // wait for 50 ms before retry
#define MAX_LISTEN_RETRY 10   // limit to 10 retries, then crash

/**
 * Endpoint provider (server mode)
 */
static Node *httpListen(Scope *scope, Context *ctx) {
#ifdef __EMSCRIPTEN__
  return createError("listen not implemented for web", true, NULL, ctx);
#endif

  Node *port = getParameter(scope, "port", FatNumber, ctx);
  if (IS_FAT_ERROR(port)) {
    return port;
  }

  Node *routes = getParameter(scope, "routes", FatList, ctx);
  if (IS_FAT_ERROR(routes)) {
    return routes;
  }
  if (!routes->scp->size || routes->scp->entries->data->type != FatScope) {
    return createError("invalid routes", true, ckValueError, ctx);
  }

  int serverFd = startListening((int)port->num.f);

  for (size_t errCount = 0, reqCount = 1;; reqCount++) {
    struct sockaddr_in client;
    socklen_t clientLen = sizeof(client);

    // Wait for next request
    int clientFd = accept(serverFd, (struct sockaddr *)&client, &clientLen);
    if (clientFd < 0) {
      logAlert(__FILE__, __func__, strerror(errno));
      if (++errCount == MAX_LISTEN_RETRY) {
        fatalOut(__FILE__, __func__, MSG_U_REQ);
      }
      msSleep(WAIT_LISTEN_RETRY);
      continue;
    }
    errCount = 0;

#ifdef SSL_SUPPORT
    SSL *ssl = NULL;
    if (isSslEnabled) {
      ssl = SSL_new(sslCtx);
      SSL_set_fd(ssl, clientFd);
      if (SSL_accept(ssl) <= 0) {
        unsigned long err;
        while ((err = ERR_get_error())) {
          fprintf(stderr, MRG_STR "SSL Error: %s\n",
                  ERR_error_string(err, NULL));
        }
        logAlert(__FILE__, __func__, "failed SSL handshake");
        SSL_shutdown(ssl);
        SSL_free(ssl);
        close(clientFd);
        continue;
      } else {
        // Handshake successful, log the TLS version
        const char *tlsVersion = SSL_get_version(ssl);
        stderrStartLine(CL_GRN);
        fprintf(stderr, MRG_BLT "SSL handshake using version: %s", tlsVersion);
        stderrEndLine();
      }
    }
#else
    void *ssl = NULL;
#endif

    // Track response time
    double tick = getCurrentMs(CLOCK_MONOTONIC);

    char buffer[BUFF_LEN] = {0};

#ifdef SSL_SUPPORT
    long bytes;
    if (isSslEnabled) {
      bytes = SSL_read(ssl, buffer, sizeof(buffer));
      if (bytes == 0) {
        logAlert(__FILE__, __func__, "SSL connection closed by peer");
        SSL_shutdown(ssl);
        SSL_free(ssl);
        close(clientFd);
        continue;
      } else if (bytes < 0) {
        unsigned long err_code = 0;
        switch (SSL_get_error(ssl, bytes)) {
          case SSL_ERROR_ZERO_RETURN:
            logAlert(__FILE__, __func__, "SSL connection closed by peer");
            break;
          case SSL_ERROR_WANT_READ:
          case SSL_ERROR_WANT_WRITE:
            logAlert(__FILE__, __func__, "SSL read did not complete");
            break;
          case SSL_ERROR_SYSCALL:
            logAlert(__FILE__, __func__, strerror(errno));
            break;
          default:
            err_code = ERR_get_error();
            if (err_code != 0) {
              char err_buf[256];
              ERR_error_string_n(err_code, err_buf, sizeof(err_buf));
              logAlert(__FILE__, __func__, err_buf);
            } else {
              logAlert(__FILE__, __func__, "SSL read unknown error");
            }
        }
        SSL_shutdown(ssl);
        SSL_free(ssl);
        close(clientFd);
        continue;
      } else if (bytes == sizeof(buffer)) {
        logAlert(__FILE__, __func__, "read " MSG_BMO);
        respond(tick, clientFd, ssl, 500, NULL, NULL);
        continue;
      }
    } else {
      bytes = read(clientFd, buffer, sizeof(buffer));
    }
#else
    long bytes = read(clientFd, buffer, sizeof(buffer));
#endif

    if (bytes < 0) {
      logAlert(__FILE__, __func__, strerror(errno));
      close(clientFd);  // close the socket without response
      continue;
    }

    if (bytes == sizeof(buffer)) {
      logAlert(__FILE__, __func__, "read " MSG_BMO);
      respond(tick, clientFd, ssl, 500, NULL, NULL);
      continue;
    }

    buffer[bytes] = '\0';  // ensure null-termination

    // Parse request method
    char *ptr = NULL;
    char *method = strtok_r(buffer, " ", &ptr);
    HttpMethod methodType = parseMethod(method);
    if (methodType == HttpNotAllowed) {
      respond(tick, clientFd, ssl, 405, NULL, NULL);
      continue;
    }

    // Parse request uri and protocol
    char *uri = strtok_r(NULL, " ", &ptr);
    if (!uri || !strtok_r(NULL, "\n", &ptr)) {        // ignore protocol version
      respond(tick, clientFd, ssl, 400, NULL, NULL);  // bad request
      continue;
    }

    // Log request reception
    char strDate[TIME_BUFF_LEN] = {'\0'};
    getCurrentTime(strDate);
    stderrStartLine(CL_BLU);
    fprintf(stderr, MRG_BLT "%s < %s %s", strDate, method, uri);
    stderrEndLine();

    // Parse request headers
    Scope *reqHeaders = createList();
    pushStack(ctx, __func__, NULL, reqHeaders);
    for (char *rawHeader = strtok_r(NULL, "\n", &ptr); rawHeader;
         rawHeader = strtok_r(NULL, "\n", &ptr)) {
      char *header = strTrim(rawHeader, strlen(rawHeader));
      if (*header) {
        addToList(reqHeaders, runtimeText(header, strlen(header), ctx), ctx);
      } else {
        free(header);
        break;
      }
    }

    // Retrieve route handler
    Scope *route = findRoute(routes->scp, uri);

    if (route) {
      Node *handler = NULL;
      switch (methodType) {
        case HttpPost:
          handler = getValueOf(route, "post");
          break;

        case HttpGet:
          handler = getValueOf(route, "get");
          break;

        case HttpPut:
          handler = getValueOf(route, "put");
          break;

        case HttpDelete:
          handler = getValueOf(route, "delete");
          break;

        default:
          break;
      }

      if (IS_FAT_TYPE(handler, FatMethod)) {
        Node *res = handle(scope, method, uri, reqHeaders, ptr, handler, ctx);

        if (IS_FAT_TYPE(res, FatScope) && res->scp) {
          Node *status = getValueOf(res->scp, "status");
          int code = IS_FAT_TYPE(status, FatNumber) ? (int)status->num.f : 200;

          Node *resHeaders = getValueOf(res->scp, "headers");
          if (resHeaders && resHeaders->type != FatList) {
            resHeaders = NULL;
          }

          respond(tick, clientFd, ssl, code,
                  resHeaders ? resHeaders->scp : NULL,
                  getValueOf(res->scp, "body"));  // route handler result

          unbindScope(res);  // facilitate GC (single pass)
        } else {
          respond(tick, clientFd, ssl, 500, NULL, NULL);
        }

        unlockNode(res);  // allow GC
      } else {
        respond(tick, clientFd, ssl, 501, NULL, NULL);  // not implemented
      }
    } else {
      respond(tick, clientFd, ssl, 404, NULL, NULL);  // not found
    }

    popStack(ctx, 1);        // reqHeaders
    trackScope(reqHeaders);  // ensure it's tracked
    if (reqCount % AUTO_GC_FREQ == 0) {
      fullGC(ctx);
    }
  }

  return NULL;  // never reached
}
