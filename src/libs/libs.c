/**
 * @file libs.c
 * @brief Fat embedded libs reference
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-30
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "libs.h"

#include "./extra/date.c"
#include "./extra/duration.c"
#include "./extra/hmap.c"
#include "./extra/logger.c"
#include "./extra/memo.c"
#include "./extra/option.c"
#include "./extra/param.c"
#include "./extra/sound.c"
#include "./extra/storable.c"
#include "./type/boolean.c"
#include "./type/chunk.c"
#include "./type/error.c"
#include "./type/hugeint.c"
#include "./type/list.c"
#include "./type/method.c"
#include "./type/number.c"
#include "./type/scope.c"
#include "./type/text.c"
#include "./type/void.c"
#include "async.c"
#include "color.c"
#include "console.c"
#include "curses.c"
#include "enigma.c"
#include "failure.c"
#include "file.c"
#include "http.c"
#include "math.c"
#include "recode.c"
#include "sdk.c"
#include "system.c"
#include "time.c"

static const char* LIB_EXTRA =
    "_ <- fat.extra.Date\n"
    "_ <- fat.extra.Duration\n"
    "_ <- fat.extra.HashMap\n"
    "_ <- fat.extra.Logger\n"
    "_ <- fat.extra.Memo\n"
    "_ <- fat.extra.Option\n"
    "_ <- fat.extra.Param\n"
    "_ <- fat.extra.Sound\n"
    "_ <- fat.extra.Storable\n";

static const char* LIB_TYPES =
    "_ <- fat.type.Void\n"
    "_ <- fat.type.Boolean\n"
    "_ <- fat.type.Chunk\n"
    "_ <- fat.type.Number\n"
    "_ <- fat.type.HugeInt\n"
    "_ <- fat.type.Text\n"
    "_ <- fat.type.Method\n"
    "_ <- fat.type.List\n"
    "_ <- fat.type.Scope\n"
    "_ <- fat.type.Error\n";

static const char* LIB_LIBS =
    "_ <- fat.type._\n"
    "_ <- fat.extra._\n"
    "_ <- fat.async\n"
    "_ <- fat.color\n"
    "_ <- fat.console\n"
    "_ <- fat.curses\n"
    "_ <- fat.enigma\n"
    "_ <- fat.failure\n"
    "_ <- fat.http\n"
    "_ <- fat.file\n"
    "_ <- fat.math\n"
    "_ <- fat.recode\n"
    "_ <- fat.sdk\n"
    "_ <- fat.system\n"
    "_ <- fat.time\n";

static const char* LIB_STD =
    "# Short-hand for importing all standard libs\n"
    "\n"
    "_       <- fat.type._\n"
    "_       <- fat.extra._\n"
    "async   <- fat.async\n"
    "color   <- fat.color\n"
    "console <- fat.console\n"
    "curses  <- fat.curses\n"
    "enigma  <- fat.enigma\n"
    "failure <- fat.failure\n"
    "http    <- fat.http\n"
    "file    <- fat.file\n"
    "math    <- fat.math\n"
    "recode  <- fat.recode\n"
    "sdk     <- fat.sdk\n"
    "system  <- fat.system\n"
    "time    <- fat.time\n";

static const char* getLibExtra(const char* ref) {
  ref = &ref[6];  // skip "extra/" for next comparisons
  return !strcmp(ref, "Date")       ? LIB_DATE
         : !strcmp(ref, "Duration") ? LIB_DURATION
         : !strcmp(ref, "HashMap")  ? LIB_HMAP
         : !strcmp(ref, "Logger")   ? LIB_LOGGER
         : !strcmp(ref, "Memo")     ? LIB_MEMO
         : !strcmp(ref, "Option")   ? LIB_OPTION
         : !strcmp(ref, "Param")    ? LIB_PARAM
         : !strcmp(ref, "Sound")    ? LIB_SOUND
         : !strcmp(ref, "Storable") ? LIB_STORABLE
         : !strcmp(ref, "_")        ? LIB_EXTRA
                                    : NULL;
}

static const char* getLibType(const char* ref) {
  ref = &ref[5];  // skip "type/" for next comparisons
  return !strcmp(ref, "Boolean")   ? LIB_BOOLEAN
         : !strcmp(ref, "Chunk")   ? LIB_CHUNK
         : !strcmp(ref, "Error")   ? LIB_ERROR
         : !strcmp(ref, "List")    ? LIB_LIST
         : !strcmp(ref, "Method")  ? LIB_METHOD
         : !strcmp(ref, "Number")  ? LIB_NUMBER
         : !strcmp(ref, "HugeInt") ? LIB_HUGE_INT
         : !strcmp(ref, "Scope")   ? LIB_SCOPE
         : !strcmp(ref, "Text")    ? LIB_TEXT
         : !strcmp(ref, "Void")    ? LIB_VOID
         : !strcmp(ref, "_")       ? LIB_TYPES
                                   : NULL;
}

const char* getEmbeddedLib(const char* ref) {
  if (!ref || !startsWith(ref, "fat/")) {
    return NULL;
  }

  ref = &ref[4];  // skip "fat/" for next comparisons
  return startsWith(ref, "extra/")  ? getLibExtra(ref)
         : startsWith(ref, "type/") ? getLibType(ref)
         : !strcmp(ref, "async")    ? LIB_ASYNC
         : !strcmp(ref, "color")    ? LIB_COLOR
         : !strcmp(ref, "console")  ? LIB_CONSOLE
         : !strcmp(ref, "curses")   ? LIB_CURSES
         : !strcmp(ref, "enigma")   ? LIB_ENIGMA
         : !strcmp(ref, "failure")  ? LIB_FAILURE
         : !strcmp(ref, "http")     ? LIB_HTTP
         : !strcmp(ref, "file")     ? LIB_FILE
         : !strcmp(ref, "math")     ? LIB_MATH
         : !strcmp(ref, "recode")   ? LIB_RECODE
         : !strcmp(ref, "sdk")      ? LIB_SDK
         : !strcmp(ref, "std")      ? LIB_STD
         : !strcmp(ref, "system")   ? LIB_SYSTEM
         : !strcmp(ref, "time")     ? LIB_TIME
         : !strcmp(ref, "_")        ? LIB_LIBS
                                    : NULL;
}

Node* callEmbedded(Scope* scope, Node* node, Context* ctx) {
  EbCmd cmd = node->op;
  if (debugLogs) {
    logDebug(__FILE__, __func__, ebCmd(cmd));
  }

  switch (cmd) {
    // Async
    case EbAtomicCall:
      return asyncAtomic(scope, ctx);
    case EbAsyncStart:
      return asyncStart(ctx);
    case EbAsyncCancel:
      return asyncCancel(ctx);
    case EbAsyncAwait:
      return asyncAwait(ctx);
    case EbAsyncIsDone:
      return asyncIsDone(ctx);
    case EbSelfCancel:
      return asyncSelfCancel(ctx);
    case EbProcessors:
      return asyncProcessors(ctx);

    // Color
    case EbDetectDepth:
      return colorDetectDepth(ctx);
    case EbTo8:
      return colorTo8(scope, ctx);
    case EbTo16:
      return colorTo16(scope, ctx);
    case EbTo256:
      return colorTo256(scope, ctx);

    // Console
    case EbLog:
      return consPrint(scope, true, stdout);
    case EbStderr:
      return consPrint(scope, true, stderr);
    case EbPrint:
      return consPrint(scope, false, stdout);
    case EbInput:
      return consInput(scope, ctx);
    case EbFlush:
      return consFlush();
    case EbMoveTo:
      return consMoveTo(scope, ctx);
    case EbShowProgress:
      return consShowProgress(scope, ctx);
    case EbIsTTY:
      return RUNTIME_BOOLEAN(stdoutIsTTY);

    // Curses
    case EbBox:
      return cursBox(scope, ctx, false);
    case EbFill:
      return cursBox(scope, ctx, true);
    case EbClear:
      return cursClear(ctx);
    case EbRefresh:
      return cursRefresh(ctx);
    case EbGetMax:
      return cursGetMax(ctx);
    case EbPrintAt:
      return cursPrintAt(scope, ctx);
    case EbMakePair:
      return cursMakePair(scope, ctx);
    case EbUsePair:
      return cursUsePair(scope, ctx);
    case EbFrameTo:
      return cursFrameTo(scope, ctx);
    case EbReadKey:
      return cursReadKey(ctx);
    case EbReadText:
      return cursReadText(scope, ctx);
    case EbFlushKeys:
      return cursFlushKeys();
    case EbEndCurses:
      return cursEndCurses();

    // Enigma
    case EbGetHash:
      return enigmaGetHash(scope, ctx);
    case EbGenUUID:
      return enigmaGenUUID(ctx);
    case EbGenKey:
      return enigmaGenKey(scope, ctx);
    case EbDerive:
      return enigmaDerive(scope, ctx);
    case EbEncrypt:
      return enigmaEncrypt(scope, ctx);
    case EbDecrypt:
      return enigmaDecrypt(scope, ctx);

    // Failure
    case EbTrap:
      return failTrap(scope, ctx);
    case EbUntrap:
      return failUntrap(ctx);

    // File
    case EbBasePath:
      return fileBasePath(ctx);
    case EbExists:
      return fileExists(scope, ctx);
    case EbRead:
      return fileRead(scope, ctx);
    case EbReadBin:
      return fileReadBin(scope, ctx);
    case EbWrite:
      return fileWrite(scope, ctx, false);
    case EbAppend:
      return fileWrite(scope, ctx, true);
    case EbRemove:
      return fileRemove(scope, ctx);
    case EbIsDir:
      return fileIsDir(scope, ctx);
    case EbMkDir:
      return fileMkDir(scope, ctx);
    case EbLsDir:
      return fileLsDir(scope, ctx);
    case EbStat:
      return fileStat(scope, ctx);

      // Http
    case EbRequest:
      return httpRequest(scope, ctx);
#ifndef __EMSCRIPTEN__
    case EbSetHeaders:
      return httpSetHeaders(scope, ctx);
    case EbSetName:
      return httpSetName(scope, ctx);
    case EbVerifySSL:
      return httpVerifySSL(scope, ctx);
#endif
#ifdef SSL_SUPPORT
    case EbSetSSL:
      return httpSetSSL(scope, ctx);
#endif
    case EbListen:
      return httpListen(scope, ctx);

    // Math
    case EbAbs:
      return mathAbs(scope, ctx);
    case EbCeil:
      return mathCeil(scope, ctx);
    case EbFloor:
      return mathFloor(scope, ctx);
    case EbIsInf:
      return mathIsInf(scope, ctx);
    case EbIsNan:
      return mathIsNan(scope);
    case EbLn:
      return mathLog(scope, ctx);
    case EbRandom:
      return mathRandom(ctx);
    case EbSqrt:
      return mathSqrt(scope, ctx);
    case EbSin:
      return mathSin(scope, ctx);
    case EbCos:
      return mathCos(scope, ctx);
    case EbAsin:
      return mathAsin(scope, ctx);
    case EbAcos:
      return mathAcos(scope, ctx);
    case EbAtan:
      return mathAtan(scope, ctx);
    case EbMax:
      return mathMax(scope, ctx);
    case EbMin:
      return mathMin(scope, ctx);
    case EbSum:
      return mathSum(scope, ctx);

      // Recode
    case EbInferType:
      return recodeInferType(scope, ctx);
    case EbMinify:
      return recodeMinify(scope, ctx);
    case EbToBase64:
      return recodeToBase64(scope, ctx);
    case EbFromBase64:
      return recodeFromBase64(scope, ctx);
    case EbFromJSON:
      return recodeFromJSON(scope, ctx);
    case EbToURL:
      return recodeToURL(scope, ctx);
    case EbFromURL:
      return recodeFromURL(scope, ctx);
    case EbToRLE:
      return recodeToRLE(scope, ctx);
    case EbFromRLE:
      return recodeFromRLE(scope, ctx);

    // SDK
    case EbAst:
      return sdkAst(scope);
    case EbEval:
      return sdkEval(scope, ctx);
    case EbStringify:
      return sdkStringify(scope, ctx);
    case EbRoot:
      return sdkRoot(ctx);
    case EbSelf:
      return sdkSelf(scope, ctx);
    case EbStack:
      return sdkStack(scope, ctx);
    case EbVersion:
      return sdkVersion(ctx);
    case EbWarranty:
      return sdkDisclaimer();
    case EbReadLib:
      return sdkReadLib(scope, ctx);
    case EbTypeOf:
      return sdkTypeOf(scope, ctx);
    case EbGetTypes:
      return sdkGetTypes(ctx);
    case EbGetDef:
      return sdkGetDef(scope, ctx);
    case EbIsMain:
      return RUNTIME_BOOLEAN(ctx->isMainMode);
    case EbGetMeta:
      return sdkGetMeta(ctx);
    case EbSetKey:
      return sdkSetKey(scope, ctx);
    case EbSetMem:
      return sdkSetMem(scope, ctx);
    case EbRunGC:
      return sdkRunGC(ctx);
    case EbQuickGC:
      return sdkQuickGC(ctx);
    case EbSetAutoGC:
      return sdkSetAutoGC(scope, ctx);
    case EbKeepDotFry:
      return sdkKeepDotFry();
    case EbBytesUsage:
      return sdkBytesUsage(ctx);
    case EbNodesUsage:
      return sdkNodesUsage(ctx);

    // System
    case EbArgs:
      return systArgs(ctx);
    case EbExit:
      return systExit(scope, ctx);
    case EbShell:
      return systShell(scope, ctx);
    case EbCapture:
      return systCapture(scope, ctx);
    case EbFork:
      return systFork(scope, ctx);
    case EbKill:
      return systKill(scope, ctx);
    case EbGetEnv:
      return systGetEnv(scope, ctx);
    case EbGetLocale:
      return systGetLocale(ctx);
    case EbSetLocale:
      return systSetLocale(scope);
    case EbGetMacId:
      return systGetMacId(ctx);
    case EbBlockSig:
      return systBlockSig(scope, ctx);

    // Time
    case EbGetZone:
      return timeGetZone(ctx);
    case EbSetZone:
      return timeSetZone(scope, ctx);
    case EbParse:
      return timeParse(scope, ctx);
    case EbFormat:
      return timeFormat(scope, ctx);
    case EbNow:
      return timeNow(ctx);
    case EbWait:
      return timeWait(scope, ctx);

    // Type: Boolean
    case EbBoolApply:
      return boolApply(scope);
    case EbBoolSize:
      return boolSize(ctx);

    // Type: Chunk
    case EbChunApply:
      return chunApply(scope, ctx);
    case EbChunSize:
      return chunSize(ctx);
    case EbChunToBytes:
      return chunToBytes(ctx);
    case EbChunToText:
      return chunToText(ctx);
    case EbChunSeekByte:
      return chunSeekByte(scope, ctx);

    // Type: List
    case EbListApply:
      return listApply(scope, ctx);
    case EbListSize:
      return listSize(ctx);
    case EbListJoin:
      return listJoin(scope, ctx);
    case EbListFlatten:
      return listFlatten(ctx);
    case EbListFind:
      return listFind(scope, ctx);
    case EbListReverse:
      return listReverse(ctx);
    case EbListShuffle:
      return listShuffle(ctx);
    case EbListUnique:
      return listUnique(ctx);
    case EbListSort:
      return listSort(scope, ctx);
    case EbListIdxOf:
      return listIdxOf(scope, ctx);
    case EbListReduce:
      return listReduce(scope, ctx);

    // Type: Method
    case EbMethApply:
      return methApply(scope, ctx);
    case EbMethArity:
      return methArity(ctx);

    // Type: Number
    case EbNumbApply:
      return numbApply(scope, ctx);
    case EbNumbSize:
      return numbSize(ctx);
    case EbNumbFormat:
      return numbFormat(scope, ctx);
    case EbNumbTruncate:
      return numbTruncate(ctx);

      // Type: HugeInt
    case EbHugeApply:
      return hugeApply(scope, ctx);
    case EbHugeSize:
      return hugeSize(ctx);
    case EbHugeModExp:
      return hugeModExp(scope, ctx);
    case EbHugeToNum:
      return hugeToNum(ctx);

    // Type: Scope
    case EbScopApply:
      return scopApply(scope, ctx);
    case EbScopSize:
      return scopSize(ctx);
    case EbScopCopy:
      return scopCopy(ctx);

    // Type: Text
    case EbTextApply:
      return textApply(scope, ctx);
    case EbTextSize:
      return textSize(ctx);
    case EbTextReplace:
      return textReplace(scope, ctx);
    case EbTextIdxOf:
      return textIdxOf(scope, ctx);
    case EbTextCount:
      return textCount(scope, ctx);
    case EbTextSplit:
      return textSplit(scope, ctx);
    case EbTextToLower:
      return textToLower(ctx);
    case EbTextToUpper:
      return textToUpper(ctx);
    case EbTextTrim:
      return textTrim(ctx);
    case EbTextMatch:
      return textMatch(scope, ctx);
    case EbTextRepeat:
      return textRepeat(scope, ctx);
    case EbTextOverlay:
      return textOverlay(scope, ctx);
    case EbToText:
      return textToText(ctx);

    // Type: Error
    case EbErrorApply:
      return errorApply(scope, ctx);

    // other
    case EbIsWeb:
#ifdef __EMSCRIPTEN__
      return trueSingleton;
#else
      return falseSingleton;
#endif
    case EbResult:
      showResult = !showResult;
      return NULL;
    case EbDebug:
      statsLogs = debugLogs = !debugLogs;
      return NULL;
#ifdef DEBUG
    case EbTrace:
      statsLogs = debugLogs = traceLogs = !traceLogs;
      return NULL;
#endif
    case EbBreak:
      if (interactive) {
        breakpoint(scope, node, ctx);
      }
      return NULL;

#ifdef __EMSCRIPTEN__
    case EbSoundPlay:
      return soundPlay(scope, ctx);
    case EbSoundStop:
      return soundStop(scope, ctx);
    case EbStorableSave:
      return storableSave(scope, ctx);
    case EbStorableLoad:
      return storableLoad(scope, ctx);
    case EbStorableList:
      return storableList(scope, ctx);
    case EbStorableErase:
      return storableErase(scope, ctx);
#endif

    // not found
    default:
      if (strcmp(ebCmd(cmd), "unknown") != 0) {
        // we acknowledge the command's existence, but it's disabled by flags
        return createError(MSG_D_EB_CMD, true, ckCallError, ctx);
      }
      return createError(MSG_U_EB_CMD, true, ckCallError, ctx);
  }
}
