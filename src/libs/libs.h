/**
 * @file libs.h
 * @brief Fat embedded libs reference
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.0
 * @date 2024-04-04
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2023, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "../sdk/sdk.h"

/**
 * @brief Gets lib source, as if reading from a virtual file system
 *
 * @param ref path of lib to import
 * @return pointer to hardcoded string (don't use free)
 */
const char* getEmbeddedLib(const char* ref);

/**
 * @brief Call embedded function from command type
 *
 * @param scope Scope* parameters namespace
 * @param node triggering embedded command node
 * @param ctx Context*
 * @return Node*
 */
Node* callEmbedded(Scope* scope, Node* node, Context* ctx);
