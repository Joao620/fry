/**
 * @file recode.c
 * @brief Data conversion between various formats
 * @note XML attributes and self-closing tags are not supported
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.1
 * @date 2024-04-28
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"
#include "../lexer.h"
#include "../parser.h"
#include "../sdk/rle.h"

static const char* LIB_RECODE =
    "# fat.recode - Data conversion between various formats\n"
    "\n"
    "_ <- fat.type._\n"
    "\n"
    "## Regex definition for inferType (deprecated)\n"
    "numeric = "
    "\"^-?[[:digit:]]+(\\\\.[[:digit:]]+)?([eE][+-]?[[:digit:]]+)?$\"\n"
    "\n"
    "## CSV parameters\n"
    "~ csvSeparator = ','\n"
    "~ csvReplacement = ''\n"
    "\n"
    "## XML parameters (deprecated)\n"
    "~ xmlWarnings = true\n"
    "\n"
    "## Convert text to void/boolean/number if applicable\n"
    "inferType = (val: Text): Any -> $inferType\n"
    "\n"
    "## Minifies JSON and FatScript sources\n"
    "minify = (src: Text): Text -> $minify\n"
    "\n"
    "## Encode binary chunk to base64 text\n"
    "toBase64 = (data: Chunk): Text -> $toBase64\n"
    "\n"
    "## Decode base64 text to original format\n"
    "fromBase64 = (b64: Text): Chunk -> $fromBase64\n"
    "\n"
    "## Build JSON from native types\n"
    "toJSON = (_): Text -> $stringify\n"  // same as sdk.stringify
    "\n"
    "## Parse JSON to native types\n"
    "fromJSON = (json: Text): Any -> $fromJSON\n"
    "\n"
    "## Encode text to URL escaped text\n"
    "toURL = (text: Text): Text -> $toURL\n"
    "\n"
    "## Decode URL escaped text to original format\n"
    "fromURL = (url: Text): Text -> $fromURL\n"
    "\n"
    "## Build URL encoded Form Data from scope\n"
    "toFormData = (data: Scope): Text ->\n"
    "  (data @ key -> '{key}={toURL(data(key).toText)}').join('&')\n"
    "\n"
    "## Parse URL encoded Form Data to scope\n"
    "fromFormData = (data: Text): Scope -> {\n"
    "  parsed = {}\n"
    "  data.split('&') @ kv -> {\n"
    "    pair = kv.split('=')\n"
    "    pair.size == 2 ? parsed.[pair(0)] = inferType(fromURL(pair(1)))\n"
    "    ()\n"
    "  }\n"
    "  parsed\n"
    "}\n"
    "\n"
    "## Build CSV from rows\n"
    "toCSV = (header: List/Text, rows: List/Scope): Text -> {\n"
    "  body = rows @ row -> {\n"
    "    selection = header @ key ->\n"
    "      row(key).toText.replace(csvSeparator, csvReplacement).trim\n"
    "    selection.join(csvSeparator)\n"
    "  }\n"
    "  header.join(csvSeparator) + '\\n' + body.join('\\n') + '\\n'\n"
    "}\n"
    "\n"
    "## Parse CSV into rows\n"
    "fromCSV = (csv: Text): List/Scope -> {\n"
    "  lines = csv.trim.split('\\n')\n"
    "  !lines ? [] : {\n"
    "    header = lines.head.split(csvSeparator) @ -> _.trim\n"
    "    1..<lines.size @ i -> {\n"
    "      values = (lines(i) - '\\r').split(csvSeparator)\n"
    "      row = {}\n"
    "      ..<header.size @ k -> {\n"
    "        row.[header(k)] = inferType(values(k).trim)\n"
    "        ()\n"
    "      }\n"
    "      row\n"
    "    }\n"
    "  }\n"
    "}\n"
    "\n"
    "## Build XML from native types (deprecated)\n"
    "toXML = (node: Any, wrap: Text = null): Text -> {\n"
    "  node <= Scope => {\n"
    "    fragments = node @ key -> {\n"
    "      node(key) <= List => toXML(node(key), key)\n"
    "      _                 => '<{key}>' + toXML(node(key)) + '</{key}>'\n"
    "    }\n"
    "    fragments.join('')\n"
    "  }\n"
    "  node <= List => {\n"
    "    fragments = node @ -> {\n"
    "      wrap => '<{key}>' + toXML(_) + '</{key}>'\n"
    "      _    => toXML(_)\n"
    "    }\n"
    "    fragments.join('')\n"
    "  }\n"
    "  _ => node.toText\n"
    "    .replace('&', '&amp;')\n"
    "    .replace('<', '&lt;')\n"
    "    .replace('>', '&gt;')\n"
    "    .replace('\\'', '&apos;')\n"
    "    .replace('\"', '&quot;')\n"
    "}\n"
    "\n"
    "## Parse XML into native types (deprecated)\n"
    "fromXML = (text: Text): Any -> {\n"
    "  ~ xml = text.trim\n"
    "  ~ scope = {}\n"
    "\n"
    "  xml.contains('<') @ {\n"
    "    ~ tagStart = xml.indexOf('<')\n"
    "\n"
    "    xml(tagStart + 1) == '?' ? {\n"
    "      ~ piEnd = xml.indexOf('?>')\n"
    "      piEnd != -1 ? {\n"
    "        xml = xml((piEnd + 2)..)  # skip processing instruction\n"
    "      } : {\n"
    "        ValueError('xml has malformed processing instruction')\n"
    "      }\n"
    "\n"
    "    } : {\n"
    "      ~ tagEnd = xml.indexOf('>')\n"
    "      ~ tagContent = xml((tagStart + 1)..<tagEnd).split(' ')\n"
    "      ~ tagName = tagContent(0)\n"
    "\n"
    "      tagContent.size > 1 ? {\n"
    "        ~ msg = '** xml: ignoring attributes of <{tagName}>'\n"
    "        ~ fg = 3  # yellow color\n"
    "        xmlWarnings ? $stderr\n"
    "      }\n"
    "\n"
    "      xml(tagEnd - 1) == '/' ? {\n"
    "        ~ msg = '** xml: ignoring self-closing tag <{tagName}>'\n"
    "        ~ fg = 3  # yellow color\n"
    "        xmlWarnings ? $stderr\n"
    "\n"
    "        xml = xml((tagEnd + 2)..)  # skip over\n"
    "      } : {\n"
    "        ~ closingTag = '</{tagName}>'\n"
    "        ~ closingIndex = xml.indexOf(closingTag)\n"
    "\n"
    "        closingIndex != -1 ? {\n"
    "          ~ inner = xml((tagEnd + 1)..<closingIndex)\n"
    "          xml = xml((closingIndex + closingTag.size)..)\n"
    "\n"
    "          scope(tagName) ? {\n"
    "            scope(tagName) <= List =>\n"
    "              scope.[tagName] += [ fromXML(inner) ]\n"
    "\n"
    "            _ => {\n"
    "              ~ list = [ scope(tagName), fromXML(inner) ]\n"
    "              scope.[tagName] = null  # reset type\n"
    "              scope.[tagName] = list\n"
    "            }\n"
    "          } : {\n"
    "            scope.[tagName] = fromXML(inner)\n"
    "          }\n"
    "        } : {\n"
    "          ValueError('xml node <{tagName}> has no closing pair')\n"
    "        }\n"
    "      }\n"
    "    }\n"
    "  }\n"
    "\n"
    "  scope.isEmpty ? {\n"
    "    inferType(\n"
    "      xml\n"
    "        .replace('&quot;', '\"')\n"
    "        .replace('&apos;', '\\'')\n"
    "        .replace('&gt;', '>')\n"
    "        .replace('&lt;', '<')\n"
    "        .replace('&amp;', '&')\n"
    "    )\n"
    "  } : {\n"
    "    scope\n"
    "  }\n"
    "}\n"
    "\n"
    "## Compress to RLE schema\n"
    "toRLE = (chunk: Chunk): Chunk -> $toRLE\n"
    "\n"
    "## Decompress from RLE schema\n"
    "fromRLE = (chunk: Chunk): Chunk -> $fromRLE\n";

static inline char* tok2val(Token* tok) {
  switch (tok->type) {
    case TkNumb:
      return prettyNumber(NULL, tok->num);
    case TkHuge:
      return join2("0x", tok->val);
    case TkText:
    case TkTemp:
      return unparseText(tok->val, false);
    case TkRaw:
      return unparseText(tok->val, true);
    case TkEmbed:
      if (strcmp(tok->val, "$break") == 0) {
        return strDup("()");
      }
      FALL_THROUGH;
    default:
      return tok->val;
  }
}

/**
 * Convert text to void/boolean/number if applicable
 */
static Node* recodeInferType(Scope* scope, Context* ctx) {
  Node* val = getParameter(scope, "val", FatText, ctx);
  if (IS_FAT_ERROR(val)) {
    return val;
  }

  size_t len = val->num.s;

  if (len == 0) {
    return val;
  }

  if (len == 4) {
    if (strcasecmp(val->val, "null") == 0) {
      return NULL;
    }
    if (strcasecmp(val->val, "true") == 0) {
      return trueSingleton;
    }
  }

  if (len == 5) {
    if (strcasecmp(val->val, "false") == 0) {
      return falseSingleton;
    }
  }

  char ch = *(val->val);
  if (ch == '-' || isDigit(ch)) {
    errno = 0;
    char* failedChunk = NULL;
    double num = strtod(val->val, &failedChunk);
    if (errno || (failedChunk && *failedChunk != '\0')) {
      // bypass
    } else {
      return runtimeNumber(num, ctx);
    }
  }
  return val;
}

/**
 * Minifies JSON and FatScript sources (simplified from bundle impl.)
 */
static Node* recodeMinify(Scope* scope, Context* ctx) {
  Node* source = getParameter(scope, "src", FatText, ctx);
  if (IS_FAT_ERROR(source)) {
    return source;
  }

  char* buffer = strDup("");

  Reader* reader = createReader(__func__);
  reader->source = source->val;
  tokenize(reader);

  while (hasContent(reader)) {
    char* prevBuff = buffer;
    Token* tok = reader->current;
    char* val = tok2val(tok);

    const Token* next = advanceTok(__func__, reader);
    if (next) {
      char* separator = "";
      if (isPunctEol(next) || isCommTok(next)) {
        separator = ";";
      } else if (isPunctTok(next, ",")) {
        separator = ",";
      }
      buffer = join3(prevBuff, val, separator);
    } else {
      buffer = join2(prevBuff, val);
    }

    // Cleanup
    if (val != tok->val) {
      free(val);
    }
    if (prevBuff != buffer) {
      free(prevBuff);
    }
  }
  freeReaderAll(reader);

  return runtimeText(buffer, strlen(buffer), ctx);
}

/**
 * Encode binary chunk to base64 text
 */
static Node* recodeToBase64(Scope* scope, Context* ctx) {
  Node* data = getParameter(scope, "data", FatChunk, ctx);
  if (IS_FAT_ERROR(data)) {
    return data;
  }

  // Allocate buffer for encoded output
  size_t encodeLen = b64EncodeLength(data->num.s);
  char* encoded = FRY_ALLOC(encodeLen + 1);

  // Base64 encode the original binary data
  size_t len = b64Encode(data->val, data->num.s, encoded);
  encoded[len] = '\0';

  return runtimeText(FRY_REALLOC(encoded, len + 1), len, ctx);
}

/**
 * Decode base64 text to original format
 */
static Node* recodeFromBase64(Scope* scope, Context* ctx) {
  Node* b64 = getParameter(scope, "b64", FatText, ctx);
  if (IS_FAT_ERROR(b64)) {
    return b64;
  }

  // Allocate buffer for decoded output
  size_t decodeLen = b64DecodeLength(b64->num.s);
  char* decoded = FRY_ALLOC(decodeLen);

  // Base64 decode
  size_t len = b64Decode(b64->val, b64->num.s, decoded);

  return runtimeChunk(FRY_REALLOC(decoded, len), len, ctx);
}

/**
 * Decode JSON text to native types
 */
static Node* recodeFromJSON(Scope* scope, Context* ctx) {
  Node* json = getParameter(scope, "json", FatText, ctx);
  if (IS_FAT_ERROR(json)) {
    return json;
  }

  Reader* jsonReader = createReader(__func__);
  jsonReader->source = join3("{", json->val, "}");

  // Tokenize with json-aware tokenizer
  if (!tokenizeJson(jsonReader)) {
    freeReaderAll(jsonReader);
    return createError("invalid json", true, ckValueError, ctx);
  }

  // Parse JSON
  Node* jsonAST = parse(jsonReader);

  // Setup execution context
  Scope layer = {0};
  initLayer(&layer);
  pushStack(ctx, __func__, jsonAST, &layer);

  // Eval JSON (shielded)
  lockResource(&memoryLock);
  useCollector = false;
  trackParsed(jsonAST, jsonReader->bytes, ctx);
  Node* result = interpret(&layer, jsonAST, ctx);
  useCollector = true;
  unlockResource(&memoryLock);

  // Clean up
  wipeScope(&layer, NULL);  // reverse layerScope
  free(jsonReader->source);
  deleteReader(jsonReader);
  popStack(ctx, 1);  // layer

  return result;
}

/**
 * Encode text to URL escaped text
 */
static Node* recodeToURL(Scope* scope, Context* ctx) {
  Node* text = getParameter(scope, "text", FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  char* encoded = FRY_ALLOC(text->num.s * 3 + 1);

  char* e = encoded;
  char* v = text->val;

  while (*v) {
    if (isalnum(*v) || *v == '-' || *v == '_' || *v == '.' || *v == '~') {
      *e++ = *v++;
    } else {
      // Using 4 bytes: 3 for the encoded char + 1 for the null-terminator
      snprintf(e, 4, "%%%02X", (Byte)*v++);
      e += 3;
    }
  }
  *e = '\0';

  size_t len = e - encoded;

  return runtimeText(FRY_REALLOC(encoded, len + 1), len, ctx);
}

/**
 * Decode URL escaped text to original format
 */
static Node* recodeFromURL(Scope* scope, Context* ctx) {
  Node* url = getParameter(scope, "url", FatText, ctx);
  if (IS_FAT_ERROR(url)) {
    return url;
  }

  char* decoded = FRY_ALLOC(url->num.s + 1);

  char* d = decoded;
  char* v = url->val;

  while (*v) {
    if (*v == '%' && isxdigit(*(v + 1)) && isxdigit(*(v + 2))) {
      unsigned int value = 0;
      sscanf(v + 1, "%2x", &value);
      *d++ = (char)value;
      v += 3;
    } else if (*v == '+') {
      *d++ = ' ';
      v++;
    } else {
      *d++ = *v++;
    }
  }
  *d = '\0';

  size_t len = d - decoded;

  return runtimeText(FRY_REALLOC(decoded, len + 1), len, ctx);
}

/**
 * Compress to RLE schema
 */
static Node* recodeToRLE(Scope* scope, Context* ctx) {
  Node* chunk = getParameter(scope, "chunk", FatChunk, ctx);
  if (IS_FAT_ERROR(chunk)) {
    return chunk;
  }

  size_t compSize = 0;
  char* compressed = rleCompress(chunk->val, chunk->num.s, &compSize);

  return runtimeChunk(compressed, compSize, ctx);
}

/**
 * Decompress from RLE schema
 */
static Node* recodeFromRLE(Scope* scope, Context* ctx) {
  Node* chunk = getParameter(scope, "chunk", FatChunk, ctx);
  if (IS_FAT_ERROR(chunk)) {
    return chunk;
  }

  size_t decoSize = 0;
  char* decompressed = rleDecompress(chunk->val, chunk->num.s, &decoSize);

  if (decoSize > 0 && decompressed == NULL) {
    return createError("invalid RLE schema", true, ckValueError, ctx);
  }

  return runtimeChunk(decompressed, decoSize, ctx);
}
