/**
 * @file system.c
 * @brief System-level operations and information
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.1
 * @date 2024-04-22
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

static const char* LIB_SYSTEM =
    "# fat.system - System-level operations and information\n"
    "\n"
    "## ExitCode is exit status or return code (POSIX)\n"
    "\n"
    "CommandResult = (code: ExitCode, out: Text)\n"
    "\n"
    "## Constants\n"
    "successCode = ExitCode * 0\n"
    "failureCode = ExitCode * 1\n"
    "\n"
    "## Return list of args passed from shell\n"
    "args = (): List/Text -> $args\n"
    "\n"
    "## Exit program with provided exit code\n"
    "exit = (code: Number) -> $exit\n"
    "\n"
    "## Get env variable value by name\n"
    "getEnv = (var: Text): Text -> $getEnv\n"
    "\n"
    "## Execute cmd in shell, return exit code\n"
    "shell = (cmd: Text): ExitCode -> $shell\n"
    "\n"
    "## Capture the output of cmd execution\n"
    "capture = (cmd: Text): CommandResult -> $capture\n"
    "\n"
    "## Start background process, return PID\n"
    "fork = (args: List/Text, out: Text = null): Number -> $fork\n"
    "\n"
    "## Send SIGTERM to process by PID\n"
    "kill = (pid: Number): Void -> $kill\n"
    "\n"
    "## Get current locale setting\n"
    "getLocale = (): Text -> $getLocale\n"
    "\n"
    "## Set current locale setting\n"
    "setLocale = (_): Boolean -> $setLocale\n"
    "\n"
    "## Get machine identifier (MAC address)\n"
    "getMacId = (): Text -> $getMacId\n"
    "\n"
    "## Block SIGINT, SIGHUP and SIGTERM signals\n"
    "blockSig = (enabled: Boolean): Void -> $blockSig\n";

/**
 * Build list of args passed after entry file
 */
static Node* systArgs(Context* ctx) {
  Scope* list = createList();
  pushStack(ctx, __func__, NULL, list);
  for (int i = 0; i < fryArgc; i++) {
    addToList(list, runtimeTextDup(fryArgv[i], ctx), ctx);
  }

  Node* result = runtimeCollection(list, ctx);
  popStack(ctx, 1);
  return result;
}

/**
 * Exit program with provided code number
 */
static Node* systExit(Scope* scope, Context* ctx) {
  if (!pthread_equal(ctx->id, mainThreadId)) {
    return createError("exit called from worker", true, ckCallError, ctx);
  }

  const Node* code = getValueOf(scope, "code");

  if (code) {
    if (IS_FAT_ERROR(code)) {
      exitFry(EXIT_FAILURE);
    }
    if (code->type == FatNumber) {
      exitFry((ExitCode)code->num.f);
    }
    if (strEq(code->val, "fail")) {
      exitFry(EXIT_FAILURE);
    }
  }

  if (showResult) {
    puts(isUtf8Mode ? MRG_RES MSG_BYE : MRG_STR MSG_BYE);
  }

  exitFry(EXIT_SUCCESS);
  return NULL;
}

/**
 * Execute command cmd in default shell and return exit code
 */
static Node* systShell(Scope* scope, Context* ctx) {
  if (isJailMode) {
    return createError("shell " MSG_JAIL, true, ckCallError, ctx);
  }

  Node* cmd = getParameter(scope, "cmd", FatText, ctx);
  if (IS_FAT_ERROR(cmd)) {
    return cmd;
  }

  // Restore SIGCHLD signal handler (if it has been disabled by fork)
  signal(SIGCHLD, SIG_DFL);

  ExitCode exitCode = system(cmd->val) / 256;
  Node* result = runtimeNumber((double)exitCode, ctx);
  result->ck = ckExitCode;
  return result;
}

/**
 * Execute command cmd and capture result { code: ExitCode, out: Text }
 * Note that popen would only capture the stdout stream not stderr stream,
 * but as a simple work around (without using dup2) we are joining both
 * streams into a single output by appending " 2>&1" to the command
 */
static Node* systCapture(Scope* scope, Context* ctx) {
  if (isJailMode) {
    return createError("capture " MSG_JAIL, true, ckCallError, ctx);
  }

  Node* cmd = getParameter(scope, "cmd", FatText, ctx);
  if (IS_FAT_ERROR(cmd)) {
    return cmd;
  }

  auto_str command = join2(cmd->val, " 2>&1");  // redirect stderr to stdout
  ExitCode exitCode = -1;
  char* result = strDup("");

  // Restore SIGCHLD signal handler (if it has been disabled by fork)
  signal(SIGCHLD, SIG_DFL);

  FILE* pipe = popen(command, "r");

  size_t resultLen = 0;

  if (pipe) {
    char buffer[PIPE_BUF + 1] = {0};  // ensure null termination
    while (fgets(buffer, PIPE_BUF, pipe)) {
      size_t lineLen = strnlen(buffer, PIPE_BUF);
      resultLen += lineLen;
      result = FRY_REALLOC(result, resultLen + 1);
      strncat(result, buffer, lineLen);
    }
    exitCode = pclose(pipe) / 256;
  }

  scope = createScope();
  scope->ck = setType("CommandResult", NULL);
  pushStack(ctx, __func__, cmd, scope);

  Node* code = runtimeNumber(exitCode, ctx);
  code->ck = ckExitCode;
  addToScope(scope, "code", code);

  Node* out = runtimeText(result, resultLen, ctx);
  addToScope(scope, "out", out);

  Node* commandResult = runtimeCollection(scope, ctx);

  popStack(ctx, 1);
  return commandResult;
}

/**
 * Start process and return the PID
 * (out parameter allows for stdout/stderr redirection)
 */
static Node* systFork(Scope* scope, Context* ctx) {
  if (isJailMode) {
    return createError("fork " MSG_JAIL, true, ckCallError, ctx);
  }

  Node* args = getParameter(scope, "args", FatList, ctx);
  if (IS_FAT_ERROR(args)) {
    return args;
  }

  Node* out = getValueOf(scope, "out");

  if (!args->scp || args->scp->size <= 0) {
    return createError("no arguments to fork", true, ckValueError, ctx);
  }

  // Tell the system to clean up child processes automatically (fire-and-forget)
  signal(SIGCHLD, SIG_IGN);

  pid_t pid = fork();
  if (pid == -1) {
    return createError("fork failed", true, NULL, ctx);
  }

  if (pid > 0) {
    return runtimeNumber(pid, ctx);
  }

  // We are in the child process...
  char** argv = FRY_ALLOC((args->scp->size + 1) * sizeof(char*));

  // Shut down stdin for child process
  close(STDIN_FILENO);

  if (IS_FAT_TYPE(out, FatText)) {
    // Redirect stdout and stderr to "out->val" and set non-blocking mode
    int fd = open(out->val, O_WRONLY | O_CREAT, 0644);
    if (fd != -1) {
      dup2(fd, STDOUT_FILENO);
      dup2(fd, STDERR_FILENO);
      close(fd);
      int flags = fcntl(STDOUT_FILENO, F_GETFL);
      flags |= O_NONBLOCK;
      fcntl(STDOUT_FILENO, F_SETFL, flags);
      flags = fcntl(STDERR_FILENO, F_GETFL);
      flags |= O_NONBLOCK;
      fcntl(STDERR_FILENO, F_SETFL, flags);
    } else {
      logAlert(__FILE__, __func__, "output redirection failed");
    }
  }

  // Fire and forget
  int argc = 0;
  for (Entry* entry = args->scp->entries; entry; entry = entry->next) {
    argv[argc++] = entry->data->val;
  }
  argv[argc] = NULL;  // null-terminate list

  if (argc > 0) {
    execvp(argv[0], argv);
    exit(EXIT_FAILURE);  // execvp only returns on error
  } else {
    exit(EXIT_SUCCESS);  // nothing to run
  }
}

/**
 * Send SIGTERM to process by PID
 */
static Node* systKill(Scope* scope, Context* ctx) {
  if (isJailMode) {
    return createError("kill " MSG_JAIL, true, ckCallError, ctx);
  }

  Node* pid = getParameter(scope, "pid", FatNumber, ctx);
  if (IS_FAT_ERROR(pid)) {
    return pid;
  }

  if (debugLogs) {
    char num[NUMBER_MAX_LENGTH + 1];
    prettyNumber(num, pid->num.f);
    logDebug(__FILE__, __func__, num);
  }

  kill((pid_t)pid->num.f, SIGTERM);
  return NULL;
}

/**
 * Get env variable value by name
 */
static Node* systGetEnv(Scope* scope, Context* ctx) {
  Node* var = getParameter(scope, "var", FatText, ctx);
  if (IS_FAT_ERROR(var)) {
    return var;
  }
  return runtimeTextDup(getenv(var->val), ctx);
}

/**
 * Get current locale setting
 */
static Node* systGetLocale(Context* ctx) {
  return runtimeTextDup(currentLocale, ctx);
}

/**
 * Set current locale setting
 */
static Node* systSetLocale(Scope* scope) {
  Node* locale = getValueOf(scope, "_");
  return RUNTIME_BOOLEAN(setFryLocale(locale ? locale->val : NULL));
}

/**
 * Get MAC address bytes from the given interface address
 */
static void getMacAddressBytes(struct ifaddrs* ifa, Byte** bytes) {
#ifndef __APPLE__
  if (ifa->ifa_addr->sa_family == AF_PACKET) {
    struct sockaddr_ll* s = (struct sockaddr_ll*)ifa->ifa_addr;
    if (s->sll_hatype == ARPHRD_ETHER) {
      *bytes = (Byte*)s->sll_addr;
    }
  }
#else
  if (ifa->ifa_addr->sa_family == AF_LINK &&
      ((struct sockaddr_dl*)ifa->ifa_addr)->sdl_type == IFT_ETHER) {
    struct sockaddr_dl* sdl = (struct sockaddr_dl*)ifa->ifa_addr;
    *bytes = (Byte*)LLADDR(sdl);
  }
#endif
}

/**
 * Get machine identifier (MAC address)
 */
static Node* systGetMacId(Context* ctx) {
  struct ifaddrs* ifaddr = NULL;
  if (getifaddrs(&ifaddr) == -1) {
    return createError("no mac address info", true, NULL, ctx);
  }

  char macAddress[18] = {'\0'};
  Byte* macBytes = NULL;
  for (struct ifaddrs* ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
    if (ifa->ifa_addr == NULL || (ifa->ifa_flags & IFF_LOOPBACK)) {
      continue;
    }
    getMacAddressBytes(ifa, &macBytes);
    if (macBytes) {
      snprintf(macAddress, 18, "%02x:%02x:%02x:%02x:%02x:%02x", macBytes[0],
               macBytes[1], macBytes[2], macBytes[3], macBytes[4], macBytes[5]);
      break;
    }
  }

  freeifaddrs(ifaddr);
  return runtimeText(copyFragment(macAddress, 17), 17, ctx);
}

/**
 * Block SIGINT, SIGHUP and SIGTERM signals
 */
static Node* systBlockSig(Scope* scope, Context* ctx) {
  if (!pthread_equal(ctx->id, mainThreadId)) {
    return createError("not allowed on worker", true, ckCallError, ctx);
  }

  Node* enabled = getParameter(scope, "enabled", FatBoolean, ctx);
  if (IS_FAT_ERROR(enabled)) {
    return enabled;
  }

  if (enabled->num.b) {
    signal(SIGINT, SIG_IGN);   // ignore SIGINT
    signal(SIGHUP, SIG_IGN);   // ignore SIGHUP
    signal(SIGTERM, SIG_IGN);  // ignore SIGTERM
  } else {
    // Register exitFromSignal as the handler for these signals
    signal(SIGINT, exitFromSignal);
    signal(SIGHUP, exitFromSignal);
    signal(SIGTERM, exitFromSignal);
  }
  return NULL;
}
