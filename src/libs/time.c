/**
 * @file time.c
 * @brief Time and date manipulation
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-28
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

static const char* LIB_TIME =
    "# fat.time - Time and date manipulation\n"
    "\n"
    "## Epoch is Unix epoch time in milliseconds\n"
    "_ <- fat.type.Number\n"
    "\n"
    "## Set timezone offset milliseconds\n"
    "setZone = (offset: Number): Void -> $setZone\n"
    "\n"
    "## Get current timezone offset milliseconds\n"
    "getZone = (): Number -> $getZone\n"
    "\n"
    "## Get current UTC in Epoch\n"
    "now = (): Epoch -> $now\n"
    "\n"
    "## Convert Epoch to date format\n"
    "format = (ems: Epoch, fmt: Text = null): Text -> $format\n"
    "\n"
    "## Parse date to Epoch\n"
    "parse = (date: Text, fmt: Text = null): Epoch -> $parse\n"
    "\n"
    "## Wait for milliseconds (sleep)\n"
    "wait = (ms: Number): Void -> $wait\n"
    "\n"
    "## Return elapsed time as text\n"
    "getElapsed = (since: Epoch): Text -> {\n"
    "  ~ x = $now - since\n"  // floor reads x as parameter
    "  x < 1000    => { x.truncate.toText + ' ms' }\n"
    "  x < 60000   => { x = x / 1000, x.truncate.toText + ' s' }\n"
    "  x < 3600000 => { x = x / 60000, x.truncate.toText + ' m' }\n"
    "  _           => { x = x / 3.6e+6, x.truncate.toText + ' h' }\n"
    "}\n";

/**
 * Return time zone offset from global context (in milliseconds)
 */
static Node* timeGetZone(Context* ctx) {
  return runtimeNumber(timeOffset * 1000.0, ctx);
}

/**
 * Set time zone offset to global context (in milliseconds)
 */
static Node* timeSetZone(Scope* scope, Context* ctx) {
  Node* offset = getParameter(scope, "offset", FatNumber, ctx);
  if (IS_FAT_ERROR(offset)) {
    return offset;
  }

  timeOffset = offset->num.f / 1000.0;
  return NULL;
}

/**
 * Convert text date to epoch milliseconds from format
 */
static Node* timeParse(Scope* scope, Context* ctx) {
  Node* date = getParameter(scope, "date", FatText, ctx);
  if (IS_FAT_ERROR(date)) {
    return date;
  }

  char* fmtString = DEFAULT_DATE_FMT;
  bool useMilliseconds = true;
  long ms = 0;

  // optional parameter
  Node* fmt = getValueOf(scope, "fmt");
  if (IS_FAT_TYPE(fmt, FatText) && *fmt->val) {
    fmtString = fmt->val;
    useMilliseconds = false;  // not DEFAULT_DATE_FMT
  }

  struct tm dateBuff;
  if (!strptime(date->val, fmtString, &dateBuff)) {
    return createError(MSG_TCF, true, ckValueError, ctx);
  }

  // milliseconds can only be expected after DEFAULT_DATE_FMT
  if (useMilliseconds && indexOf(date->val, '.') == 19) {
    errno = 0;
    ms = strtol(&date->val[20], NULL, 10);  // parse integer after the dot
    if (errno || ms < 0 || ms > 999) {
      return createError(MSG_TCF, true, ckValueError, ctx);
    }
  }

  double epoch = difftime(timegm(&dateBuff), (time_t)timeOffset);
  Node* result = runtimeNumber(epoch * 1000 + (double)ms, ctx);  // to millis
  result->ck = ckEpoch;
  return result;
}

/**
 * Convert epoch milliseconds to formatted text
 */
static Node* timeFormat(Scope* scope, Context* ctx) {
  Node* ems = getParameter(scope, "ems", FatNumber, ctx);
  if (IS_FAT_ERROR(ems)) {
    return ems;
  }

  char buff[BUFF_LEN];
  char* fmtString = DEFAULT_DATE_FMT;
  bool useMilliseconds = true;

  // optional parameter
  Node* fmt = getValueOf(scope, "fmt");
  if (IS_FAT_TYPE(fmt, FatText) && *fmt->val) {
    fmtString = fmt->val;
    useMilliseconds = false;
  }

  time_t epoch = (time_t)((ems->num.f / 1000) + timeOffset);  // in seconds
  if (!strftime(buff, sizeof(buff) - 5, fmtString, gmtime(&epoch))) {
    return createError(MSG_TCF, true, ckValueError, ctx);
  }

  size_t len = strlen(buff);

  if (useMilliseconds && ems->num.f >= 0) {  // note we reserve 5 bytes above
    snprintf(&buff[len], 5, ".%03ld", (long)fmod(ems->num.f, 1000));
    len += 4;
  }

  return runtimeText(copyFragment(buff, len), len, ctx);
}

/**
 * Get current UTC epoch in milliseconds
 */
static Node* timeNow(Context* ctx) {
  Node* result = runtimeNumber(getCurrentMs(CLOCK_REALTIME), ctx);
  result->ck = ckEpoch;
  return result;
}

/**
 * Wait for ms milliseconds (sleep thread)
 */
static Node* timeWait(Scope* scope, Context* ctx) {
  Node* ms = getParameter(scope, "ms", FatNumber, ctx);
  if (IS_FAT_ERROR(ms)) {
    return ms;
  }

#ifndef __EMSCRIPTEN__
  if (ms->num.f > 0) {
    msSleep((long)ms->num.f);
  }
#else
  emscripten_sleep((int)ms->num.f);
#endif

  return NULL;
}
