/**
 * @file boolean.c
 * @brief Boolean prototype extensions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.1
 * @date 2024-04-19
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../../interpreter.h"

static const char* LIB_BOOLEAN =
    "# fat.type.Boolean - Prototype extensions\n"
    "\n"
    "Boolean = (\n"
    "  ## Argument slot\n"
    "  val: Any\n"
    "\n"
    "  ## Constructor\n"
    "  apply = (): Boolean -> $boolApply\n"
    "\n"
    "  ## Common prototype methods\n"
    "  isEmpty  = (): Boolean -> !self\n"
    "  nonEmpty = (): Boolean -> self\n"
    "  size     = (): Number  -> $boolSize\n"
    "  toText   = (): Text    -> $toText\n"
    ")\n";

/**
 * Coerces node value to boolean
 */
static Node* boolApply(Scope* scope) {
  Node* val = getValueOf(scope, "val");
  return RUNTIME_BOOLEAN(booleanOf(val));
}

/**
 * Returns "length" of boolean pointed by selfRef
 */
static Node* boolSize(Context* ctx) {
  Node* boolean = getInstance(FatBoolean, ctx);
  if (IS_FAT_ERROR(boolean)) {
    return boolean;
  }

  return runtimeNumber(boolean->num.b, ctx);
}
