/**
 * @file chunk.c
 * @brief Chunk prototype extensions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-17
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../../interpreter.h"

static const char* LIB_CHUNK =
    "# fat.type.Chunk - Prototype extensions\n"
    "\n"
    "Chunk = (\n"
    "  ## Argument slot\n"
    "  val: Any\n"
    "\n"
    "  ## Constructor\n"
    "  apply = (): Chunk -> $chunApply\n"
    "\n"
    "  ## Common prototype methods\n"
    "  isEmpty  = (): Boolean -> !self\n"
    "  nonEmpty = (): Boolean -> !!self\n"
    "  size     = (): Number  -> $chunSize\n"
    "  toText   = (): Text    -> $chunToText\n"
    "\n"
    "  ## Chunk prototype special methods\n"
    "  toBytes  = (): List/Number -> $chunToBytes\n"
    "  seekByte = (byte: Number, offset: Number = 0): Number -> $chunSeekByte\n"
    "  patch    = (i: Number, n: Number, val: Chunk = null): Chunk -> {\n"
    "    i < 0 => $chunApply + self\n"
    "    _     => self(..<i) + $chunApply + self(i + n..)\n"
    "  }\n"
    ")\n";

static inline bool isInvalidByte(double n) { return n < 0 || n > 255; }

static inline bool isInvalidBytes(Scope* list) {
  if (list->entries && list->entries->data->type != FatNumber) {
    return true;
  }

  for (Entry* entry = list->entries; entry; entry = entry->next) {
    if (isInvalidByte(entry->data->num.f)) {
      return true;
    }
  }

  return false;
}

static inline char* joinBytesToChunk(Scope* list) {
  char* chunk = FRY_ALLOC((size_t)list->size);

  long i = 0;
  for (Entry* entry = list->entries; entry; entry = entry->next) {
    chunk[i++] = (Byte)entry->data->num.f;
  }
  return chunk;
}

/**
 * Coerces node value to chunk
 */
static Node* chunApply(Scope* scope, Context* ctx) {
  Node* val = getValueOf(scope, "val");

  NodeType type = val ? val->type : FatVoid;

  char* chunk = NULL;

  switch (type) {
    case FatVoid:
      return runtimeChunk(NULL, 0, ctx);

    case FatBoolean:
      chunk = FRY_ALLOC(1);
      *chunk = (Byte)val->num.b;
      return runtimeChunk(chunk, 1, ctx);

    case FatNumber:
      if (isInvalidByte(val->num.f)) {
        return createError(MSG_I_N_ARG, true, ckValueError, ctx);
      }
      chunk = FRY_ALLOC(1);
      *chunk = (Byte)val->num.f;
      return runtimeChunk(chunk, 1, ctx);

    case FatChunk:
      return val;  // bypass

    case FatText:
      chunk = copyFragment(val->val, val->num.s);
      return runtimeChunk(chunk, val->num.s, ctx);

    case FatList:
      if (isInvalidBytes(val->scp)) {
        return createError(MSG_I_N_ARG, true, ckValueError, ctx);
      }
      chunk = joinBytesToChunk(val->scp);
      return runtimeChunk(chunk, (size_t)val->scp->size, ctx);
      break;

    default:
      return createError(MSG_UNSUP, true, ckValueError, ctx);
  }
  return NULL;
}

/**
 * Returns total bytes of chunk pointed by selfRef
 */
static Node* chunSize(Context* ctx) {
  Node* chunk = getInstance(FatChunk, ctx);
  if (IS_FAT_ERROR(chunk)) {
    return chunk;
  }

  return runtimeNumber(chunk->num.s, ctx);
}

/**
 * Returns chunk as list of bytes (numbers)
 */
static Node* chunToBytes(Context* ctx) {
  Node* chunk = getInstance(FatChunk, ctx);
  if (IS_FAT_ERROR(chunk)) {
    return chunk;
  }

  Scope* list = createList();
  pushStack(ctx, __func__, chunk, list);

  const char* data = chunk->val;
  const size_t totalBytes = chunk->num.s;
  for (size_t i = 0; i < totalBytes; i++) {
    Node* byte = runtimeNumber((Byte)data[i], ctx);
    addToList(list, byte, ctx);
  }

  Node* result = runtimeCollection(list, ctx);
  popStack(ctx, 1);

  return result;
}

/**
 * Force chunk to text format
 */
static Node* chunToText(Context* ctx) {
  Node* chunk = getInstance(FatChunk, ctx);
  if (IS_FAT_ERROR(chunk)) {
    return chunk;
  }

  char* text = forceToUtf8(chunk->val, chunk->num.s);
  return runtimeText(text, strlen(text), ctx);
}

/**
 * Find byte index in a chunk after offset, -1 if not found
 */
static Node* chunSeekByte(Scope* scope, Context* ctx) {
  Node* chunk = getInstance(FatChunk, ctx);
  if (IS_FAT_ERROR(chunk)) {
    return chunk;
  }

  Node* byte = getParameter(scope, "byte", FatNumber, ctx);
  if (IS_FAT_ERROR(byte)) {
    return byte;
  }

  if (byte->num.f < 0 || byte->num.f > 255) {
    return createError(MSG_I_N_ARG, true, ckValueError, ctx);
  }

  Byte byteToSeek = (Byte)byte->num.f;

  Node* offset = getParameter(scope, "offset", FatNumber, ctx);
  if (IS_FAT_ERROR(offset)) {
    return offset;
  }

  if (offset->num.f < 0) {
    return createError(MSG_I_N_ARG, true, ckValueError, ctx);
  }

  for (size_t i = (size_t)offset->num.f; i < chunk->num.s; i++) {
    if (byteToSeek == (Byte)chunk->val[i]) {
      return runtimeNumber(i, ctx);
    }
  }

  return runtimeNumber(-1, ctx);
}
