/**
 * @file error.c
 * @brief Error prototype extensions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.0
 * @date 2024-04-11
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../../interpreter.h"

static const char* LIB_ERROR =
    "# fat.type.Error - Prototype extensions\n"
    "\n"
    "Error = (\n"
    "  ## Argument slot\n"
    "  val: Any\n"
    "\n"
    "  ## Constructor\n"
    "  apply = (): Error -> $errorApply\n"
    "\n"
    "  ## Common prototype methods\n"
    "  isEmpty  = (): Boolean -> true\n"
    "  nonEmpty = (): Boolean -> false\n"
    "  size     = (): Number  -> 0\n"
    "  toText   = (): Text    -> $toText\n"
    ")\n"
    "\n"
    "## Set standard error type aliases\n"
    "AssignError = Error\n"
    "AsyncError = Error\n"
    "CallError = Error\n"
    "FileError = Error\n"
    "IndexError = Error\n"
    "KeyError = Error\n"
    "SyntaxError = Error\n"
    "TypeError = Error\n"
    "ValueError = Error\n";

/**
 * Return val as message wrapped into Error type
 */
static Node* errorApply(Scope* scope, Context* ctx) {
  Node* val = getValueOf(scope, "val");
  if (val && IS_FAT_ERROR(val)) {
    return val;
  }

  char* msg = val ? toString(val) : strDup("unknown");
  Type* errorType = ctx->selfRef ? ctx->selfRef->ck : NULL;

  // try to fix src, 'cause it should show where it was called, not here!
  int ctxTop = atomic_load(&ctx->top);
  Frame* prev = ctxTop > 3 ? &ctx->stack[ctxTop - 3] : NULL;
  if (prev && prev->node) {
    ctx->cur->node = prev->node;
  }

  return createError(msg, false, errorType, ctx);
}
