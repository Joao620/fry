/**
 * @file list.c
 * @brief List prototype extensions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-17
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../../interpreter.h"

static const char* LIB_LIST =
    "# fat.type.List - Prototype extensions\n"
    "\n"
    "List = (\n"
    "  ## Argument slot\n"
    "  val: Any\n"
    "\n"
    "  ## Constructor\n"
    "  apply = (): List -> $listApply\n"
    "\n"
    "  ## Common prototype methods\n"
    "  isEmpty  = (): Boolean -> !self\n"
    "  nonEmpty = (): Boolean -> !!self\n"
    "  size     = (): Number  -> $listSize\n"
    "  toText   = (): Text    -> $toText\n"
    "\n"
    "  ## List prototype special methods\n"
    "  join     = (sep: Text): Text    -> $listJoin\n"
    "  flatten  = (): List             -> $listFlatten\n"
    "  find     = (p: Method): Any     -> $listFind\n"
    "  contains = (p: Method): Boolean -> $listFind != Void\n"
    "  filter   = (p: Method): List    -> self @ -> p(_) ? _\n"
    "  reverse  = (): List             -> $listReverse\n"
    "  shuffle  = (): List             -> $listShuffle\n"
    "  unique   = (): List             -> $listUnique\n"
    "  sort     = (): List             -> $listSort\n"
    "  sortBy   = (key: Any): List     -> $listSort\n"
    "  indexOf  = (item: Any): Number  -> $listIdxOf\n"
    "  head     = (): Any              -> self ? self(0)\n"
    "  tail     = (): List             -> self(1..)\n"
    "  map      = (m: Method): List    -> self @ -> m(_)\n"
    "  reduce   = (m: Method, acc: Any = null): Any -> $listReduce\n"
    "  walk     = (m: Method): Void -> { self @ -> { m(_), () }, () }\n"
    "  patch    = (i: Number, n: Number, val: List = []): List -> {\n"
    "    i < 0 => val + self\n"
    "    _     => self(..<i) + val + self(i + n..)\n"
    "  }\n"
    "\n"
    "  ## Option compatibility\n"
    "  headOption = -> {\n"
    "    val = self ? self(0)\n"
    "    val != Void => Some * { val }\n"
    "    _           => None * {}\n"
    "  }\n"
    "  itemOption = (index: Number) -> self(index..index).headOption\n"
    "  findOption = (p: Method) -> [$listFind].headOption\n"
    ")\n";

/**
 * Return val wrapped into list
 */
static Node* listApply(Scope* scope, Context* ctx) {
  Scope* instance = createList();
  Node* val = getValueOf(scope, "val");
  if (val) {
    addToList(instance, val, ctx);
  }
  return runtimeCollection(instance, ctx);
}

/**
 * Join texts from a list using separator
 */
static Node* listJoin(Scope* scope, Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  Node* sep = getParameter(scope, "sep", FatText, ctx);
  if (IS_FAT_ERROR(sep)) {
    return sep;
  }

  char* result = joinListItems(list->scp, sep->val, &toString);
  return runtimeText(result, strlen(result), ctx);
}

/**
 * Join list of lists into flat list
 */
static Node* listFlatten(Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  Scope* original = list->scp;
  if (!original || !original->size) {
    return list;  // already "flat"
  }

  if (original->ck != checkList) {
    return createError("flatten requires matrix", true, ckValueError, ctx);
  }

  Scope* flattened = createList();
  for (Entry* x = original->entries; x; x = x->next) {
    if (x->data->scp && x->data->scp->size) {
      for (Entry* y = x->data->scp->entries; y; y = y->next) {
        Node* error = addToList(flattened, y->data, ctx);
        if (error) {
          trackScope(flattened);
          return error;
        }
      }
    }
  }

  return runtimeCollection(flattened, ctx);
}

/**
 * Return selected item or null
 */
static Node* listFind(Scope* scope, Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  Node* p = getParameter(scope, "p", FatMethod, ctx);
  if (IS_FAT_ERROR(p)) {
    return p;
  }

  if (!list->scp || !list->scp->size) {
    return NULL;
  }

  pushStack(ctx, __func__, p, list->scp);
  pushStack(ctx, __func__, p, p->scp);

  char* key = p->head ? p->head->val : "_";

  Node* result = NULL;
  for (Entry* entry = list->scp->entries; entry; entry = entry->next) {
    Entry arg = {0};
    Scope layer = {0};
    initLayer(&layer);

    arg.key.s = key;
    arg.data = entry->data;
    layer.entries = &arg;
    layer.size = 1;

    // Verify predicate (validator) against entry
    Node* check = interpret(&layer, p->body, ctx);
    if (booleanOf(check)) {
      result = arg.data;
    }
    unlockNode(check);

    // Reverse layerScope
    wipeScope(&layer, &arg);

    if (result) {
      break;
    }
  }

  popStack(ctx, 2);  // p->scp, list->scp

  return result;
}

/**
 * Return a reversed copy of list
 */
static Node* listReverse(Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  return reverseList(list->scp, ctx);
}

/**
 * Return a shuffled copy of list
 */
static Node* listShuffle(Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  if (!list->scp || !list->scp->size) {
    return copyNode(list, false, ctx);
  }

  Scope* shuffled = copyCollection(list->scp, false, ctx);
  long i = 1;
  for (Entry* a = shuffled->entries->next; a; a = a->next, i++) {
    Entry* b = getByIndex(shuffled, (long)(rnGen() % i));
    Node* aux = a->data;
    a->data = b->data;
    b->data = aux;
  }

  return runtimeCollection(shuffled, ctx);
}

/**
 * listUnique auxiliary function
 */
static inline bool hasItemAlready(Entry* list, Entry* item) {
  for (Node* data = item->data; list; list = list->next) {
    if (nodeEq(list->data, data)) {
      return true;
    }
  }
  return false;
}

/**
 * Return only unique items of original list
 */
static Node* listUnique(Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  if (!list->scp || !list->scp->size) {
    return copyNode(list, false, ctx);
  }

  Scope* unique = createList();

  for (Entry* entry = list->scp->entries; entry; entry = entry->next) {
    if (!hasItemAlready(unique->entries, entry)) {
      addToList(unique, entry->data, ctx);
    }
  }

  return runtimeCollection(unique, ctx);
}

/**
 * Return a sorted copy of list (ascending)
 */
static Node* listSort(Scope* scope, Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  if (!list->scp || !list->scp->size) {
    return copyNode(list, false, ctx);
  }

  Node* key = getValueOf(scope, "key");

  if (key) {
    if (isAlias(checkScope, list->scp->ck)) {
      if (key->type != FatText || !*key->val) {
        return createError("invalid key", true, ckValueError, ctx);
      }

    } else if (isAlias(checkList, list->scp->ck)) {
      if (key->type != FatNumber || key->num.f < 0) {
        return createError("invalid key", true, ckValueError, ctx);
      }

    } else {
      return createError(MSG_MISMATCH, true, ckTypeError, ctx);
    }
  }

  Scope* sorted = copyCollection(list->scp, false, ctx);
  quicksort(sorted->entries, sorted->quick2, key);

  return runtimeCollection(sorted, ctx);
}

/**
 * Get item index, -1 if absent
 */
static Node* listIdxOf(Scope* scope, Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  Node* item = getValueOf(scope, "item");
  if (!item) {
    return createError("invalid item", true, ckValueError, ctx);
  }

  if (!list->scp || !list->scp->size) {
    return runtimeNumber(-1, ctx);
  }

  long index = 0;
  for (Entry* entry = list->scp->entries; entry; entry = entry->next) {
    if (nodeEq(entry->data, item)) {
      return runtimeNumber((double)index, ctx);
    }
    index++;
  }

  return runtimeNumber(-1, ctx);
}

/**
 * Functional utility
 */
static Node* listReduce(Scope* scope, Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  Node* acc = getValueOf(scope, "acc");
  if (!list->scp || !list->scp->size) {
    return acc;
  }

  Node* m = getParameter(scope, "m", FatMethod, ctx);
  if (IS_FAT_ERROR(m)) {
    return m;
  }

  if (!(IS_FAT_TYPE(m->head, FatEntry) &&
        IS_FAT_TYPE(m->head->seq, FatEntry))) {
    return createError("invalid reducer", true, ckValueError, ctx);
  }

  // Extract method signature slot names as keys 1 and 2
  char* k1 = m->head->val;
  char* k2 = m->head->seq->val;

  // Check first element in the list as lists can only hold one type
  if (!checkAlias(m->head->seq->ck, list->scp->entries->data)) {
    return createError(join2(MSG_MISMATCH GUIDE, k2), false, ckTypeError, ctx);
  }

  if (debugLogs) {
    logDebug(__FILE__, __func__, "start");
  }

  Scope layer = {0};
  pushStack(ctx, __func__, list, scope);
  pushStack(ctx, __func__, m, m->scp);
  pushStack(ctx, __func__, acc, &layer);
  bool isOrdered = strcmp(k1, k2) < 0;

  // Fallback to first element if no acc, and loop through the list
  Entry* entry = list->scp->entries;
  if (!acc) {
    acc = entry->data;
    entry = entry->next;
  }
  for (; !ctx->failureEvent && entry; entry = entry->next) {
    if (!checkAlias(m->head->ck, acc)) {
      popStack(ctx, 3);  // list, m->scp, layer
      char* msg = join2(MSG_MISMATCH GUIDE, k1);
      return createError(msg, false, ckTypeError, ctx);
    }

    // Inlined load method scope
    initLayer(&layer);
    Entry arg1 = {0};
    arg1.key.s = k1;
    arg1.data = acc;
    Entry arg2 = {0};
    arg2.key.s = k2;
    arg2.data = entry->data;
    if (isOrdered) {
      layer.entries = &arg1;
      arg1.next = &arg2;
    } else {
      layer.entries = &arg2;
      arg2.next = &arg1;
    }
    layer.size = 2;

    // Apply reducer
    acc = interpret(&layer, m->body, ctx);

    // Inlined custom wipeScope
    while (layer.entries) {
      Entry* next = layer.entries->next;
      if (layer.entries != &arg1 && layer.entries != &arg2) {
        freeScopeEntry(layer.entries);
      }
      layer.entries = next;
    }
    pthread_mutex_destroy(&layer.lock);  // created by initLayer

    // Inlined custom refurbishLayer
    layer.quick1 = NULL;
    layer.quick2 = NULL;
    layer.cached = NULL;
  }

  popStack(ctx, 3);  // list, m->scp, layer

  if (debugLogs) {
    logDebug(__FILE__, __func__, "end");
  }

  return ctx->failureEvent ? ctx->failureEvent : acc;
}

/**
 * Return length of list pointed by selfRef
 */
static Node* listSize(Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  return runtimeNumber(list->scp ? (double)list->scp->size : 0, ctx);
}
