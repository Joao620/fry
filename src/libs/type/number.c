/**
 * @file number.c
 * @brief Number prototype extensions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.0
 * @date 2024-04-10
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../../interpreter.h"

static const char* LIB_NUMBER =
    "# fat.type.Number - Prototype extensions\n"
    "\n"
    "Number = (\n"
    "  ## Argument slot\n"
    "  val: Any\n"
    "\n"
    "  ## Constructor\n"
    "  apply = (): Number -> $numbApply\n"
    "\n"
    "  ## Common prototype methods\n"
    "  isEmpty  = (): Boolean -> !self\n"
    "  nonEmpty = (): Boolean -> !!self\n"
    "  size     = (): Number  -> $numbSize\n"
    "  toText   = (): Text    -> $toText\n"
    "\n"
    "  ## Number prototype special methods\n"
    "  format = (fmt: Text): Text -> $numbFormat\n"
    "  truncate = (): Number -> $numbTruncate\n"
    ")\n"
    "\n"
    "## Set standard number type aliases\n"
    "Epoch = Number\n"
    "ExitCode = Number\n"
    "Millis = Number\n";

/**
 * Return text to number or size of val (collections)
 */
static Node* numbApply(Scope* scope, Context* ctx) {
  Node* val = getValueOf(scope, "val");

  if (!val) {
    return runtimeNumber(0, ctx);
  }

  switch (val->type) {
    case FatBoolean:
      return runtimeNumber(val->num.b, ctx);

    case FatNumber:
    case FatError:
      return val;

    case FatText:
      errno = 0;
      char* failedChunk = NULL;
      double num = strtod(val->val, &failedChunk);

      if (errno) {
        return createError(MSG_I_N_ARG, true, ckValueError, ctx);
      }

      if (failedChunk && *failedChunk != '\0') {
        return createError("non-numeric", true, ckValueError, ctx);
      }

      return runtimeNumber(num, ctx);

    case FatScope:
    case FatList:
      return runtimeNumber(val->scp ? (double)val->scp->size : 0, ctx);

    case FatMethod:
      return runtimeNumber(1, ctx);

    default:
      return createError(MSG_UNSUP, true, ckValueError, ctx);
  }
}

/**
 * Returns "length" of number pointed by selfRef, same as math.abs
 */
static Node* numbSize(Context* ctx) {
  Node* number = getInstance(FatNumber, ctx);
  if (IS_FAT_ERROR(number)) {
    return number;
  }

  return runtimeNumber(fabs(number->num.f), ctx);
}

/**
 * Returns number as formatted text (restricted to single number value)
 */
static Node* numbFormat(Scope* scope, Context* ctx) {
  Node* number = getInstance(FatNumber, ctx);
  if (IS_FAT_ERROR(number)) {
    return number;
  }

  Node* fmt = getParameter(scope, "fmt", FatText, ctx);
  if (IS_FAT_ERROR(fmt)) {
    return fmt;
  }

  auto_str formatString =
      *fmt->val == '%' ? strDup(fmt->val) : join3("%", fmt->val, "f");

  if (!matchRegex("^%-?[0-9]*\\.?[0-9]*[fFeEgGaA]$", formatString)) {
    return createError("invalid format specifier", true, ckValueError, ctx);
  }

  char* result = NULL;
  int len = asprintf(&result, formatString, number->num.f);
  if (len < 0) {
    fatalOut(__FILE__, __func__, MSG_OOM);
  }

  return runtimeText(result, (size_t)len, ctx);
}

/**
 * Returns number discarding decimals
 */
static Node* numbTruncate(Context* ctx) {
  Node* number = getInstance(FatNumber, ctx);
  if (IS_FAT_ERROR(number)) {
    return number;
  }

  const double num = number->num.f;
  if (num >= 0) {
    return runtimeNumber(floor(num), ctx);
  }

  return runtimeNumber(ceil(num), ctx);
}
