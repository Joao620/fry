/**
 * @file scope.c
 * @brief Scope prototype extensions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.0
 * @date 2024-04-10
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../../interpreter.h"

static const char* LIB_SCOPE =
    "# fat.type.Scope - Prototype extensions\n"
    "\n"
    "Scope = (\n"
    "  ## Argument slot\n"
    "  val: Any\n"
    "\n"
    "  ## Constructor\n"
    "  apply = (): Scope -> $scopApply\n"
    "\n"
    "  ## Common prototype methods\n"
    "  isEmpty  = (): Boolean -> !self\n"
    "  nonEmpty = (): Boolean -> !!self\n"
    "  size     = (): Number  -> $scopSize\n"
    "  toText   = (): Text    -> $toText\n"
    "\n"
    "  ## Scope prototype special methods\n"
    "  copy     = (): Scope   -> $scopCopy\n"
    "  keys     = (): List    -> self @ -> _\n"
    "\n"
    "  ## Option compatibility\n"
    "  maybe    = (key: Text) -> {\n"
    "    val = self(key)\n"
    "    val != Void => Some * { val }\n"
    "    _           => None * {}\n"
    "  }\n"
    ")\n";

/**
 * Return val wrapped into scope, if not a scope
 */
static Node* scopApply(Scope* scope, Context* ctx) {
  Node* val = getValueOf(scope, "val");
  if (val && (val->type == FatScope || IS_FAT_ERROR(val))) {
    return val;
  }

  Scope* instance = createScope();
  if (val) {
    addToScope(instance, "val", val);
  }
  return runtimeCollection(instance, ctx);
}

/**
 * Returns size of scope pointed by selfRef
 */
static Node* scopSize(Context* ctx) {
  Node* scope = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(scope)) {
    return scope;
  }

  return runtimeNumber(scope->scp ? (double)scope->scp->size : 0, ctx);
}

/**
 * Returns deep copy of scope pointed by selfRef
 */
static Node* scopCopy(Context* ctx) {
  Node* scope = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(scope)) {
    return scope;
  }

  if (!initVisit(__func__)) {
    fatalOut(__FILE__, __func__, MSG_U_REC);
  }
  Node* copy = copyNode(scope, true, ctx);
  endVisit();
  return copy;
}
