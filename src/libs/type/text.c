/**
 * @file text.c
 * @brief Text prototype extensions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-17
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../../interpreter.h"

static const char* LIB_TEXT =
    "# fat.type.Text - Prototype extensions\n"
    "\n"
    "Text = (\n"
    "  ## Argument slot\n"
    "  val: Any\n"
    "\n"
    "  ## Constructor\n"
    "  apply = (): Text -> $textApply\n"
    "\n"
    "  ## Common prototype methods\n"
    "  isEmpty  = (): Boolean -> !self\n"
    "  nonEmpty = (): Boolean -> !!self\n"
    "  size     = (): Number  -> $textSize\n"
    "  toText   = (): Text    -> self\n"
    "\n"
    "  ## Text prototype special methods\n"
    "  replace    = (old: Text, new: Text): Text -> $textReplace\n"
    "  indexOf    = (frag: Text): Number         -> $textIdxOf\n"
    "  contains   = (frag: Text): Boolean        -> $textIdxOf >= 0\n"
    "  count      = (frag: Text): Number         -> $textCount\n"
    "  startsWith = (frag: Text): Boolean        -> {\n"
    "    !frag => true\n"
    "    _     => $textIdxOf == 0\n"
    "  }\n"
    "  endsWith   = (frag: Text): Boolean        -> {\n"
    "    !frag => true\n"
    "    _     => self(-frag.size..) == frag\n"
    "  }\n"
    "  split      = (sep: Text): List            -> $textSplit\n"
    "  toLower    = (): Text                     -> $textToLower\n"
    "  toUpper    = (): Text                     -> $textToUpper\n"
    "  trim       = (): Text                     -> $textTrim\n"
    "  match      = (regex: Text): Boolean       -> $textMatch\n"
    "  repeat     = (n: Number): Text            -> $textRepeat\n"
    "  overlay    = (base: Text, align: Text = 'left'): Text -> $textOverlay\n"
    "  patch      = (i: Number, n: Number, val: Text = ''): Text -> {\n"
    "    i < 0 => val + self\n"
    "    _     => self(..<i) + val + self(i + n..)\n"
    "  }\n"
    ")\n";

/**
 * Return val as text
 */
static Node* textApply(Scope* scope, Context* ctx) {
  Node* val = getValueOf(scope, "val");

  if (val) {
    char* result = toString(val);
    return runtimeText(result, strlen(result), ctx);
  }
  return runtimeText(strDup(""), 0, ctx);
}

/**
 * Return length of text pointed by selfRef
 */
static Node* textSize(Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }
  return runtimeNumber((double)utf8len(text->val), ctx);
}

/**
 * Return new text where oldWord has been replaced by newWord
 */
static Node* textReplace(Scope* scope, Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  Node* oldWord = getParameter(scope, "old", FatText, ctx);
  if (IS_FAT_ERROR(oldWord)) {
    return oldWord;
  }

  Node* newWord = getParameter(scope, "new", FatText, ctx);
  if (IS_FAT_ERROR(newWord)) {
    return newWord;
  }

  char* result = replaceAll(text->val, oldWord->val, newWord->val);
  return runtimeText(result, strlen(result), ctx);
}

/**
 * Get fragment index, -1 if absent
 */
static Node* textIdxOf(Scope* scope, Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  Node* frag = getParameter(scope, "frag", FatText, ctx);
  if (IS_FAT_ERROR(frag)) {
    return frag;
  }

  const char* ptr = utf8strstr(text->val, frag->val);
  if (ptr) {
    size_t index = 0;
    for (char* c = text->val; c < ptr; c = utf8next(c)) {
      index++;
    }
    return runtimeNumber((double)index, ctx);
  }

  return runtimeNumber(-1.0, ctx);
}

/**
 * Get count of frag repetitions in text
 */
static Node* textCount(Scope* scope, Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  Node* frag = getParameter(scope, "frag", FatText, ctx);
  if (IS_FAT_ERROR(frag)) {
    return frag;
  }

  return runtimeNumber(countWord(text->val, frag->val), ctx);
}

/**
 * Return list of texts of splitted parts
 */
static Node* textSplit(Scope* scope, Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  Node* sep = getParameter(scope, "sep", FatText, ctx);
  if (IS_FAT_ERROR(sep)) {
    return sep;
  }

  char** splitted = NULL;
  if (!*sep->val) {
    size_t len = utf8len(text->val);
    char* str = text->val;
    splitted = FRY_ALLOC((len + 1) * sizeof(char*));
    for (size_t i = 0; i < len; i++) {
      splitted[i] = utf8char(str);
      str = utf8next(str);
      if (!str) {
        fatalOut(__FILE__, __func__, "bad utf-8 char");
        free(splitted);  // mitigate false positive on auto_check.sh
        return NULL;     // mitigate false positive on auto_check.sh
      }
    }
    splitted[len] = NULL;
  } else {
    splitted = splitSep(text->val, sep->val);
  }

  // Build list from splitted strings
  Scope* list = createList();
  pushStack(ctx, __func__, ctx->selfRef, list);
  if (splitted) {
    for (size_t i = 0; splitted[i]; i++) {
      Node* fragment = runtimeText(splitted[i], strlen(splitted[i]), ctx);
      addToList(list, fragment, ctx);
    }
    free(splitted);
  }
  Node* result = runtimeCollection(list, ctx);
  popStack(ctx, 1);
  return result;
}

/**
 * Return lowercase version of text
 */
static Node* textToLower(Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  char* result = copyFragment(text->val, text->num.s);
  utf8lower(result);
  return runtimeText(result, text->num.s, ctx);
}

/**
 * Return uppercase version of text
 */
static Node* textToUpper(Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  char* result = copyFragment(text->val, text->num.s);
  utf8upper(result);
  return runtimeText(result, text->num.s, ctx);
}

/**
 * Return trimmed version of text
 */
static Node* textTrim(Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  char* result = strTrim(text->val, text->num.s);
  return runtimeText(result, strlen(result), ctx);
}

/**
 * Return if regular expression is a match
 */
static Node* textMatch(Scope* scope, Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  Node* regex = getParameter(scope, "regex", FatText, ctx);
  if (IS_FAT_ERROR(regex)) {
    return regex;
  }

  return RUNTIME_BOOLEAN(matchRegex(regex->val, text->val));
}

/**
 * Return text repeated n times
 */
static Node* textRepeat(Scope* scope, Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  Node* n = getParameter(scope, "n", FatNumber, ctx);
  if (IS_FAT_ERROR(n)) {
    return n;
  }

  size_t len = n->num.f > 0 ? (size_t)n->num.f * text->num.s : 0;
  char* repeated = FRY_ALLOC(len + 1);
  for (size_t i = 0; i < len; i++) {
    repeated[i] = text->val[i % text->num.s];
  }
  repeated[len] = '\0';

  return runtimeText(repeated, len, ctx);
}

/**
 * Return text layed over base (with optional alignment)
 */
static Node* textOverlay(Scope* scope, Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  Node* base = getParameter(scope, "base", FatText, ctx);
  if (IS_FAT_ERROR(base)) {
    return base;
  }

  Node* align = getParameter(scope, "align", FatText, ctx);
  if (IS_FAT_ERROR(align)) {
    return align;
  }

  size_t start = 0;
  size_t shift = 0;

  if (strcmp(align->val, "left") != 0) {
    if (base->num.s >= text->num.s) {
      if (strcmp(align->val, "center") == 0) {
        start = (base->num.s - text->num.s) / 2;
      } else if (strcmp(align->val, "right") == 0) {
        start = base->num.s - text->num.s;
      } else {
        return createError(MSG_B_A_A, true, ckValueError, ctx);
      }
    } else {
      if (strcmp(align->val, "center") == 0) {
        shift = (text->num.s - base->num.s) / 2;
      } else if (strcmp(align->val, "right") == 0) {
        shift = text->num.s - base->num.s;
      } else {
        return createError(MSG_B_A_A, true, ckValueError, ctx);
      }
    }
  }

  char* combined = copyFragment(base->val, base->num.s);
  for (size_t i = 0; shift + i < text->num.s && start + i < base->num.s; i++) {
    combined[start + i] = text->val[shift + i];
  }

  return runtimeText(combined, base->num.s, ctx);
}

/**
 * Return self as text (generic, used by other native types)
 */
static Node* textToText(Context* ctx) {
  char* result = toString(ctx->selfRef);
  return runtimeText(result, strlen(result), ctx);
}
