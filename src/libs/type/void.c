/**
 * @file void.c
 * @brief Void prototype extensions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.0
 * @date 2024-04-11
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

static const char* LIB_VOID =
    "# fat.type.Void - Prototype extensions\n"
    "\n"
    "Void = (\n"
    "  ## Argument slot\n"
    "  val: Any\n"
    "\n"
    "  ## Constructor\n"
    "  apply = (): Void -> null\n"
    "\n"
    "  ## Common prototype methods\n"
    "  isEmpty  = (): Boolean -> true\n"
    "  nonEmpty = (): Boolean -> false\n"
    "  size     = (): Number  -> 0\n"
    "  toText   = (): Text    -> $toText\n"
    ")\n";
