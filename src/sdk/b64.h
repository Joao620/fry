/**
 * @file b64.h
 * @brief Base-64 encoding/decoding implementation.
 *
 * This is based on the libb64 project, in the public domain.
 * For details, see http://sourceforge.net/projects/libb64
 *
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.1.0
 * @date 2024-01-27
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>

extern const char* b64encoding;

/**
 * @brief Check if data is within base64 encoding characters.
 *
 * @param data string
 * @return boolean
 */
bool isB64Encoded(const char* data);

/**
 * @brief Calculate required length of buffer for encoded string.
 *
 * @param len raw input length
 * @return size_t encoded length or zero for failure
 */
size_t b64EncodeLength(size_t len);

/**
 * @brief Encodes a complete block of binary data into base64 format.
 *
 * @param input data block to be encoded
 * @param inLen length of the input data block in bytes
 * @param output output buffer for the encoded data
 * @return size_t bytes written to the buffer, excluded the null terminator
 */
size_t b64Encode(const char* input, size_t inLen, char* output);

/**
 * @brief Calculate a maximum required buffer length for decoded output.
 *
 * @param encodedLen encoded size
 * @return size_t (note: the actual decoded size may be a bit less)
 */
size_t b64DecodeLength(size_t encodedLen);

/**
 * @brief Decodes a block of base64 encoded data back into original format.
 *
 * @param input block of base64 encoded data
 * @param inLen length of the base64 encoded data block in bytes
 * @param output output buffer for the decoded binary data
 * @return size_t actual total bytes written to the output buffer
 */
size_t b64Decode(const char* input, size_t inLen, char* output);
