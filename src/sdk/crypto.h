/**
 * @file crypto.h
 * @brief A very simple solution to obfuscate data with bitwise-XOR.
 *
 * Based on work I've done together with team mate Joao Condack back in
 * mid-2005, at PUC-Rio University. Now with standard base-64 encoding.
 *
 * Used as back-end for the FatScript enigma library.
 *
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.1.0
 * @date 2024-01-27
 * @note This schema is not cryptographically safe!
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "sugar.h"

/**
 * @brief Base64 encode bitwise-XOR key over message.
 *
 * @param key secret
 * @param text to decode
 * @return string (use free)
 */
char* xorEncode(const char* key, const char* text);

/**
 * @brief Base64 decode bitwise-XOR key over message.
 *
 * @param key secret
 * @param zData to decode
 * @return string (use free)
 */
char* xorDecode(const char* key, const char* zData);

/**
 * @brief Generates a random text from url-safe characters.
 *
 * @param len size_t
 * @return string (use free)
 */
char* generateKey(size_t len);

/**
 * @brief Generates a random UUID (version 4 RFC 4122 format).
 *
 * @return string (use free)
 */
char* generateUuid(void);

/**
 * @brief Function to derive a key using the base64 alphabet.
 *
 * @param secret password of pass-phrase
 * @param len size of secret
 * @return string (use free)
 */
char* deriveKey(const char* secret, size_t len);
