/**
 * @file embedded.c
 * @brief Embedded commands boilerplate
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.1
 * @date 2024-04-29
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "sdk.h"

// Embedded command lookup table
static const EbMap ebMap[] = {

    // Async
    {EbAtomicCall, "atomic"},
    {EbAsyncStart, "asyncStart"},
    {EbAsyncCancel, "asyncCancel"},
    {EbAsyncAwait, "asyncAwait"},
    {EbAsyncIsDone, "asyncIsDone"},
    {EbSelfCancel, "selfCancel"},
    {EbProcessors, "processors"},

    // Color
    {EbDetectDepth, "detectDepth"},
    {EbTo8, "to8"},
    {EbTo16, "to16"},
    {EbTo256, "to256"},

    // Console
    {EbLog, "log"},
    {EbStderr, "stderr"},
    {EbPrint, "print"},
    {EbInput, "input"},
    {EbFlush, "flush"},
    {EbMoveTo, "moveTo"},
    {EbShowProgress, "showProgress"},
    {EbIsTTY, "isTTY"},

    // Curses
    {EbBox, "box"},
    {EbFill, "fill"},
    {EbClear, "clear"},
    {EbRefresh, "refresh"},
    {EbGetMax, "getMax"},
    {EbPrintAt, "printAt"},
    {EbMakePair, "makePair"},
    {EbUsePair, "usePair"},
    {EbFrameTo, "frameTo"},
    {EbReadKey, "readKey"},
    {EbReadText, "readText"},
    {EbFlushKeys, "flushKeys"},
    {EbEndCurses, "endCurses"},

    // Enigma
    {EbGetHash, "getHash"},
    {EbGenUUID, "genUUID"},
    {EbGenKey, "genKey"},
    {EbDerive, "derive"},
    {EbEncrypt, "encrypt"},
    {EbDecrypt, "decrypt"},

    // Failure
    {EbTrap, "trap"},
    {EbUntrap, "untrap"},

    // File
    {EbBasePath, "basePath"},
    {EbExists, "exists"},
    {EbRead, "read"},
    {EbReadBin, "readBin"},
    {EbWrite, "write"},
    {EbAppend, "append"},
    {EbRemove, "remove"},
    {EbIsDir, "isDir"},
    {EbMkDir, "mkDir"},
    {EbLsDir, "lsDir"},
    {EbStat, "stat"},

    // Http
    {EbRequest, "request"},
    {EbSetHeaders, "setHeaders"},
    {EbSetName, "setName"},
    {EbVerifySSL, "verifySSL"},
    {EbSetSSL, "setSSL"},
    {EbListen, "listen"},

    // Math
    {EbAbs, "abs"},
    {EbCeil, "ceil"},
    {EbFloor, "floor"},
    {EbIsInf, "isInf"},
    {EbIsNan, "isNaN"},
    {EbLn, "ln"},
    {EbRandom, "random"},
    {EbSqrt, "sqrt"},
    {EbSin, "sin"},
    {EbCos, "cos"},
    {EbAsin, "asin"},
    {EbAcos, "acos"},
    {EbAtan, "atan"},
    {EbMax, "max"},
    {EbMin, "min"},
    {EbSum, "sum"},

    // Recode
    {EbInferType, "inferType"},
    {EbMinify, "minify"},
    {EbToBase64, "toBase64"},
    {EbFromBase64, "fromBase64"},
    {EbFromJSON, "fromJSON"},
    {EbToURL, "toURL"},
    {EbFromURL, "fromURL"},
    {EbToRLE, "toRLE"},
    {EbFromRLE, "fromRLE"},

    // SDK
    {EbAst, "ast"},
    {EbEval, "eval"},
    {EbStringify, "stringify"},
    {EbRoot, "root"},
    {EbSelf, "self"},
    {EbStack, "stack"},
    {EbVersion, "version"},
    {EbWarranty, "warranty"},
    {EbReadLib, "readLib"},
    {EbTypeOf, "typeOf"},
    {EbGetTypes, "getTypes"},
    {EbGetDef, "getDef"},
    {EbIsMain, "isMain"},
    {EbGetMeta, "getMeta"},
    {EbSetKey, "setKey"},
    {EbSetMem, "setMem"},
    {EbRunGC, "runGC"},
    {EbQuickGC, "quickGC"},
    {EbSetAutoGC, "setAutoGC"},
    {EbKeepDotFry, "keepDotFry"},
    {EbBytesUsage, "bytesUsage"},
    {EbNodesUsage, "nodesUsage"},

    // System
    {EbArgs, "args"},
    {EbExit, "exit"},
    {EbShell, "shell"},
    {EbCapture, "capture"},
    {EbFork, "fork"},
    {EbKill, "kill"},
    {EbGetEnv, "getEnv"},
    {EbGetLocale, "getLocale"},
    {EbSetLocale, "setLocale"},
    {EbGetMacId, "getMacId"},
    {EbBlockSig, "blockSig"},

    // Time
    {EbGetZone, "getZone"},
    {EbSetZone, "setZone"},
    {EbParse, "parse"},
    {EbFormat, "format"},
    {EbNow, "now"},
    {EbWait, "wait"},

    // Type: Boolean
    {EbBoolApply, "boolApply"},
    {EbBoolSize, "boolSize"},

    // Type: Chunk
    {EbChunApply, "chunApply"},
    {EbChunSize, "chunSize"},
    {EbChunToBytes, "chunToBytes"},
    {EbChunToText, "chunToText"},
    {EbChunSeekByte, "chunSeekByte"},

    // Type: List
    {EbListApply, "listApply"},
    {EbListSize, "listSize"},
    {EbListJoin, "listJoin"},
    {EbListFlatten, "listFlatten"},
    {EbListFind, "listFind"},
    {EbListReverse, "listReverse"},
    {EbListShuffle, "listShuffle"},
    {EbListUnique, "listUnique"},
    {EbListSort, "listSort"},
    {EbListIdxOf, "listIdxOf"},
    {EbListReduce, "listReduce"},

    // Type: Method
    {EbMethApply, "methApply"},
    {EbMethArity, "methArity"},

    // Type: Number
    {EbNumbApply, "numbApply"},
    {EbNumbSize, "numbSize"},
    {EbNumbFormat, "numbFormat"},
    {EbNumbTruncate, "numbTruncate"},

    // Type: HugeInt
    {EbHugeApply, "hugeApply"},
    {EbHugeSize, "hugeSize"},
    {EbHugeModExp, "hugeModExp"},
    {EbHugeToNum, "hugeToNum"},

    // Type: Scope
    {EbScopApply, "scopApply"},
    {EbScopSize, "scopSize"},
    {EbScopCopy, "scopCopy"},

    // Type: Text
    {EbTextApply, "textApply"},
    {EbTextSize, "textSize"},
    {EbTextReplace, "textReplace"},
    {EbTextIdxOf, "textIdxOf"},
    {EbTextCount, "textCount"},
    {EbTextSplit, "textSplit"},
    {EbTextToLower, "textToLower"},
    {EbTextToUpper, "textToUpper"},
    {EbTextTrim, "textTrim"},
    {EbTextMatch, "textMatch"},
    {EbTextRepeat, "textRepeat"},
    {EbTextOverlay, "textOverlay"},
    {EbToText, "toText"},

    // Type: Error
    {EbErrorApply, "errorApply"},

    // Other
    {EbIsWeb, "isWeb"},
    {EbResult, "result"},
    {EbDebug, "debug"},
    {EbTrace, "trace"},
    {EbBreak, "break"},

    // Emscripten only
    {EbSoundPlay, "soundPlay"},
    {EbSoundStop, "soundStop"},
    {EbStorableSave, "storableSave"},
    {EbStorableLoad, "storableLoad"},
    {EbStorableList, "storableList"},
    {EbStorableErase, "storableErase"},
};

const char* ebCmd(EbCmd cmd) {
  for (size_t i = 0; i < sizeof(ebMap) / sizeof(EbMap); i++) {
    if (ebMap[i].cmd == cmd) {
      return ebMap[i].name;
    }
  }
  return "unknown";  // not found
}

EbCmd getEbCmd(const char* name) {
  for (size_t i = 0; i < sizeof(ebMap) / sizeof(EbMap); i++) {
    if (FAST_STR_EQ(ebMap[i].name, name)) {
      return ebMap[i].cmd;
    }
  }
  return EbUnknown;  // not found
}
