/**
 * @file embedded.h
 * @brief Embedded commands boilerplate
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.1
 * @date 2024-04-29
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "sugar.h"

/**
 * @brief Embedded command type.
 */
typedef enum {
  EbUnknown,

  // Async
  EbAtomicCall,   // asyncAtomic
  EbAsyncStart,   // asyncStart
  EbAsyncCancel,  // asyncCancel
  EbAsyncAwait,   // asyncAwait
  EbAsyncIsDone,  // asyncIsDone
  EbSelfCancel,   // asyncSelfCancel
  EbProcessors,   // asyncProcessors

  // Color
  EbDetectDepth,  // colorMaxColors
  EbTo8,          // colorTo8
  EbTo16,         // colorTo16
  EbTo256,        // colorTo256

  // Console
  EbLog,           // consPrint (true, stdout)
  EbStderr,        // consPrint (false, stderr)
  EbPrint,         // consPrint (false, stdout)
  EbInput,         // consInput
  EbFlush,         // consFlush
  EbMoveTo,        // consMoveTo
  EbShowProgress,  // consShowProgress
  EbIsTTY,         // stdoutIsTTY

  // Curses
  EbBox,        // cursBox (false)
  EbFill,       // cursBox (true)
  EbClear,      // cursClear
  EbRefresh,    // cursRefresh
  EbGetMax,     // cursGetMax
  EbPrintAt,    // cursPrintAt
  EbMakePair,   // cursMakePair
  EbUsePair,    // cursUsePair
  EbFrameTo,    // cursFrameTo
  EbReadKey,    // cursReadKey
  EbReadText,   // cursReadText
  EbFlushKeys,  // cursFlushKeys
  EbEndCurses,  // cursEndCurses

  // Enigma
  EbGetHash,  // enigmaGetHash
  EbGenUUID,  // enigmaGenUUID
  EbGenKey,   // enigmaGenKey
  EbDerive,   // enigmaDerive
  EbEncrypt,  // enigmaEncrypt
  EbDecrypt,  // enigmaDecrypt

  // Failure
  EbTrap,    // failTrap
  EbUntrap,  // failUntrap

  // File
  EbBasePath,  // fileBasePath
  EbExists,    // fileExists
  EbRead,      // fileRead
  EbReadBin,   // fileReadBin
  EbWrite,     // fileWrite (false)
  EbAppend,    // fileWrite (true)
  EbRemove,    // fileRemove
  EbIsDir,     // fileIsDir
  EbMkDir,     // fileMkDir
  EbLsDir,     // fileLsDir
  EbStat,      // fileStat

  // Http
  EbRequest,     // httpRequest
  EbSetHeaders,  // httpSetHeaders
  EbSetName,     // httpSetName
  EbVerifySSL,   // httpVerifySSL
  EbSetSSL,      // httpSetSSL
  EbListen,      // httpListen

  // Math
  EbAbs,     // mathAbs
  EbCeil,    // mathCeil
  EbFloor,   // mathFloor
  EbIsInf,   // mathIsInf
  EbIsNan,   // mathIsNan
  EbLn,      // mathLog
  EbRandom,  // mathRandom
  EbSqrt,    // mathSqrt
  EbSin,     // mathSin
  EbCos,     // mathCos
  EbAsin,    // mathAsin
  EbAcos,    // mathAcos
  EbAtan,    // mathAtan
  EbMax,     // mathMax
  EbMin,     // mathMin
  EbSum,     // mathSum

  // Recode
  EbInferType,   // recodeInferType
  EbMinify,      // recodeMinify
  EbToBase64,    // recodeToBase64
  EbFromBase64,  // recodeFromBase64
  EbFromJSON,    // recodeFromJSON
  EbToURL,       // recodeToURL
  EbFromURL,     // recodeFromURL
  EbToRLE,       // recodeToRLE
  EbFromRLE,     // recodeFromRLE

  // SDK
  EbAst,         // sdkAst
  EbEval,        // sdkEval
  EbStringify,   // sdkStringify
  EbRoot,        // sdkRoot
  EbSelf,        // sdkSelf
  EbStack,       // sdkStack
  EbVersion,     // sdkVersion
  EbWarranty,    // sdkDisclaimer
  EbReadLib,     // sdkReadLib
  EbTypeOf,      // sdkTypeOf
  EbGetTypes,    // sdkGetTypes
  EbGetDef,      // sdkGetDef
  EbIsMain,      // sdkIsMain
  EbGetMeta,     // sdkGetMeta
  EbSetKey,      // sdkSetKey
  EbSetMem,      // sdkSetMem
  EbRunGC,       // sdkRunGC
  EbQuickGC,     // sdkQuickGC
  EbSetAutoGC,   // sdkSetAutoGC
  EbKeepDotFry,  // sdkKeepDotFry
  EbBytesUsage,  // sdkBytesUsage
  EbNodesUsage,  // sdkNodesUsage

  // System
  EbArgs,       // systArgs
  EbExit,       // systExit
  EbShell,      // systShell
  EbCapture,    // systCapture
  EbFork,       // systFork
  EbKill,       // systKill
  EbGetEnv,     // systGetEnv
  EbGetLocale,  // systGetLocale
  EbSetLocale,  // systSetLocale
  EbGetMacId,   // systGetMacId
  EbBlockSig,   // systBlockSig

  // Time
  EbGetZone,  // timeGetZone
  EbSetZone,  // timeSetZone
  EbParse,    // timeParse
  EbFormat,   // timeFormat
  EbNow,      // timeNow
  EbWait,     // timeWait

  // Type: Boolean
  EbBoolApply,  // boolApply
  EbBoolSize,   // boolSize

  // Type: Chunk
  EbChunApply,     // chunApply
  EbChunSize,      // chunSize
  EbChunToBytes,   // chunToBytes
  EbChunToText,    // chunToText
  EbChunSeekByte,  // chunSeekByte

  // Type: List
  EbListApply,    // listApply
  EbListSize,     // listSize
  EbListJoin,     // listJoin
  EbListFlatten,  // listFlatten
  EbListFind,     // listFind
  EbListReverse,  // listReverse
  EbListShuffle,  // listShuffle
  EbListUnique,   // listUnique
  EbListSort,     // listSort
  EbListIdxOf,    // listIdxOf
  EbListReduce,   // listReduce

  // Type: Method
  EbMethApply,  // methApply
  EbMethArity,  // methArity

  // Type: Number
  EbNumbApply,     // numbApply
  EbNumbSize,      // numbSize
  EbNumbFormat,    // numbFormat
  EbNumbTruncate,  // numbTruncate

  // Type: HugeInt
  EbHugeApply,   // hugeApply
  EbHugeSize,    // hugeSize
  EbHugeModExp,  // hugeModExp
  EbHugeToNum,   // hugeToNum

  // Type: Scope
  EbScopApply,  // scopApply
  EbScopSize,   // scopSize
  EbScopCopy,   // scopCopy

  // Type: Text
  EbTextApply,    // textApply
  EbTextSize,     // textSize
  EbTextReplace,  // textReplace
  EbTextIdxOf,    // textIdxOf
  EbTextCount,    // textCount
  EbTextSplit,    // textSplit
  EbTextToLower,  // textToLower
  EbTextToUpper,  // textToUpper
  EbTextTrim,     // textTrim
  EbTextMatch,    // textMatch
  EbTextRepeat,   // textRepeat
  EbTextOverlay,  // textOverlay
  EbToText,       // textToText

  // Type: Error
  EbErrorApply,  // errorApply

  // other
  EbIsWeb,
  EbResult,
  EbDebug,
  EbTrace,
  EbBreak,

  // emscripten only
  EbSoundPlay,
  EbSoundStop,
  EbStorableSave,
  EbStorableLoad,
  EbStorableList,
  EbStorableErase,

} EbCmd;

/**
 * @brief Embedded command tuple for lookup table (ebMap).
 */
typedef struct EbMap {
  EbCmd cmd;
  const char* name;
} EbMap;

/**
 * @brief Return string name of embedded command.
 *
 * @param cmd EbCmd (type id)
 * @return string
 */
const char* ebCmd(EbCmd cmd);

/**
 * @brief Get embedded command type id from a string.
 *
 * @param name string name of embedded command
 * @return EbCmd
 */
EbCmd getEbCmd(const char* name);
