/**
 * @file fatcurses.c
 * @brief FatScript cursor runtime support (ncurses inspired)
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-30
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "sdk.h"

static bool isNoDelay = true;

int termCols = 80;
int termRows = 24;

#ifndef __EMSCRIPTEN__

static bool isEchoOn = true;
static bool isBlockOn = true;

static bool updateTermSize(void) {
  if (!stdoutIsTTY) {
    return false;
  }

  // Throttle ioctl polling
  static double lastTimeChecked = 0.0;
  double now = getCurrentMs(CLOCK_MONOTONIC_COARSE);
  if (lastTimeChecked + RESIZE_POLL_MS_INTERVAL > now) {
    return false;
  }
  lastTimeChecked = now;

  // Perform actual term size query and check for changes
  struct winsize w;
  if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &w) == 0 &&
      (w.ws_col != termCols || w.ws_row != termRows)) {
    termCols = w.ws_col;
    termRows = w.ws_row;
    return true;
  }
  return false;
}

static void setTerminalMode(void) {
  struct termios tty;

  if (tcgetattr(STDIN_FILENO, &tty) < 0) {
    errorOut(__FILE__, __func__, "tcgetattr (a)");
    return;
  }

  if (isEchoOn) {
    tty.c_lflag |= ECHO;
  } else {
    tty.c_lflag &= ~ECHO;
  }

  if (isBlockOn) {
    tty.c_lflag |= ICANON;
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 0;
  } else {
    tty.c_lflag &= ~ICANON;
    tty.c_cc[VMIN] = 0;
    tty.c_cc[VTIME] = 0;
  }

  if (tcsetattr(STDIN_FILENO, TCSANOW, &tty) < 0) {
    errorOut(__FILE__, __func__, "tcgetattr (b)");
  }
}

void setEcho(bool isOn) {
  isEchoOn = isOn;
  setTerminalMode();
}

void setBreak(bool isOn) { (void)isOn; }

void setBlock(bool isOn) {
  isBlockOn = isOn;
  setTerminalMode();
}

int getchImpl(void) {
  if (updateTermSize()) {
    return KEY_RESIZE;
  }

  char ch = '\0';
  int readBytes = read(STDIN_FILENO, &ch, 1);

  if (readBytes <= 0) {
    return ERR;
  }

  if (ch == '\033') {  // maybe escape sequence
    char seq[4] = {0};

    if (read(STDIN_FILENO, seq, 1) == 0) {
      return '\033';  // not an escape sequence, just KEY_ESC
    }
    if (read(STDIN_FILENO, seq + 1, 1) == 0) {
      return ERR;
    }

    if (seq[0] == '[') {
      switch (seq[1]) {
        case '1':
        case 'H':
          return KEY_HOME;
        case '2':
          read(STDIN_FILENO, seq + 2, 1);  // read the '~'
          return KEY_IC;
        case '3':
          read(STDIN_FILENO, seq + 2, 1);  // read the '~'
          return KEY_DC;
        case '4':
        case 'F':
          return KEY_END;
        case '5':
          read(STDIN_FILENO, seq + 2, 1);  // read the '~'
          return KEY_PPAGE;
        case '6':
          read(STDIN_FILENO, seq + 2, 1);  // read the '~'
          return KEY_NPAGE;

        case 'A':
          return KEY_UP;
        case 'B':
          return KEY_DOWN;
        case 'C':
          return KEY_RIGHT;
        case 'D':
          return KEY_LEFT;

        case 'Z':
          return KEY_BTAB;

        default:
          break;
      }
    }

    return ERR;  // unhandled escape sequence
  }

  return (int)ch;
}

#endif

static void move(int y, int x);

static void resetColor(void) { directPrint(CL_RST, stdout); }

int clear(void) {
  directPrint("\033[H\033[2J\033[3J\n", stdout);
  resetColor();
  return OK;
}

int refresh(void) {
  fflush(stdout);
  return OK;
}

int flushinp(void) {
  tcflush(STDIN_FILENO, TCIFLUSH);
  return OK;
}

int getch(void) {
#ifdef __EMSCRIPTEN__
  emscripten_sleep(JS_IO_LAG);
  while (!isNoDelay && isEmptyBuffer()) {
    emscripten_sleep(JS_IO_LAG);
  }
#endif

  int result = getchImpl();

#ifndef __EMSCRIPTEN__
  while (!isNoDelay && result == ERR) {
    msSleep(20);
    result = getchImpl();
  };
#endif

  return result;
}

WINDOW* initscr(void) {
#ifndef __EMSCRIPTEN__
  updateTermSize();
#endif
  clear();
  setEcho(false);
  setBreak(false);
  setBlock(false);
  directPrint("\033[?25l", stdout);  // hide cursor escape sequence
  fflush(stdout);
  return NULL;
}

int endwin(void) {
  clear();
  setEcho(true);
  setBreak(true);
  setBlock(true);
  directPrint("\033[?25h", stdout);  // show cursor escape sequence
  fflush(stdout);
  return OK;
}

int start_color(void) { return OK; }
bool has_colors(void) { return true; }
int use_default_colors(void) { return OK; }

static ColorPair colorPairs[COLOR_PAIRS] = {0};

static ColorPair* currentColor = NULL;

int init_pair(int pair, int fg, int bg) {
  if (pair <= 0 || pair >= COLOR_PAIRS) {
    return ERR;
  }

  colorPairs[pair].fg = fg;
  colorPairs[pair].bg = bg;
  return OK;
}

int attron(int pair) {
  if (pair >= COLOR_PAIRS) {
    return ERR;
  }

  currentColor = pair >= 0 ? &colorPairs[pair] : NULL;
  return OK;
}

static void printColorCodes(void) {
  resetColor();
  if (currentColor) {
    printFgColor(currentColor->fg, stdout);
    printBgColor(currentColor->bg, stdout);
  }
}

static void move(int y, int x) {
  int correctedX = (x < 0 ? 1 : x + 1);
  int correctedY = (y < 0 ? 1 : y + 1);

  char buffer[64] = {'\0'};
  snprintf(buffer, sizeof(buffer), "\033[%d;%dH", correctedY, correctedX);
  directPrint(buffer, stdout);
  printColorCodes();
}

int mvprintw(int y, int x, const char* format, ...) {
  char buffer[1024] = {'\0'};

  va_list args;
  va_start(args, format);

  move(y, x);
  int result = vsnprintf(buffer, sizeof(buffer), format, args);
  directPrint(buffer, stdout);
  resetColor();

  va_end(args);

  return result >= 0 ? OK : ERR;
}

int mvaddstr(int y, int x, const char* str) {
  if (!str) {
    return ERR;
  }

  move(y, x);
  directPrint(str, stdout);
  resetColor();
  return OK;
}

int mvhline(int y, int x, char ch, int length) {
  if (length < 0) {
    return ERR;
  }

  char buffer[2] = {0};
  buffer[0] = ch;

  const char* str = ch ? buffer : "─";

  move(y, x);
  for (int i = 0; i < length; i++) {
    directPrint(str, stdout);
  }
  resetColor();
  return OK;
}

int mvvline(int y, int x, char ch, int length) {
  if (length < 0) {
    return ERR;
  }

  char buffer[2] = {0};
  buffer[0] = ch;

  const char* str = ch ? buffer : "│";

  for (int i = 0; i < length; i++) {
    move(y + i, x);
    directPrint(str, stdout);
    resetColor();
  }
  return OK;
}

int getmaxx(WINDOW* win) {
  (void)win;
#ifndef __EMSCRIPTEN__
  updateTermSize();
#endif
  return termCols;
}

int getmaxy(WINDOW* win) {
  (void)win;
  // note: getmaxx always is called first, no need for updateTermSize
  return termRows;
}

int cbreak(void) { return OK; }

int noecho(void) { return OK; }

int nodelay(WINDOW* win, bool b) {
  (void)win;
  isNoDelay = b;
#ifdef __EMSCRIPTEN__
  setBlock(!b);
#endif
  return OK;
}

int keypad(WINDOW* win, bool b) {
  (void)win;
  (void)b;
  return OK;
}

int curs_set(int mode) {
  (void)mode;
  return OK;
}
