/**
 * @file fatcurses.h
 * @brief FatScript cursor runtime support (ncurses inspired)
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-28
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2023, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include <stdbool.h>
#include <stdio.h>

#define RESIZE_POLL_MS_INTERVAL 40.0

// curses

#ifndef __EMSCRIPTEN__

/*
 * Definitions copied from NCURSES
 */
#define KEY_BACKSPACE 0407
#define KEY_STAB 0524  // set-tab
#define KEY_BTAB 0541  // back-tab
#define KEY_ENTER 0527
#define KEY_UP 0403
#define KEY_DOWN 0402
#define KEY_LEFT 0404
#define KEY_RIGHT 0405
#define KEY_PPAGE 0523  // prev-page (up)
#define KEY_NPAGE 0522  // next-page (down)
#define KEY_HOME 0406
#define KEY_END 0550
#define KEY_IC 0513      // insert
#define KEY_DC 0512      // delete
#define KEY_RESIZE 0632  // resize event
#define KEY_F0 0410      // function keys

#else

/*
 * Definitions tested with Chrome browser only
 */
#define KEY_BACKSPACE 8
#define KEY_STAB 9
#define KEY_ENTER 13
#define KEY_UP 38
#define KEY_DOWN 40
#define KEY_LEFT 37
#define KEY_RIGHT 39
#define KEY_PPAGE 33
#define KEY_NPAGE 34
#define KEY_HOME 36
#define KEY_END 35
#define KEY_IC 45
#define KEY_DC 46
#define KEY_RESIZE (-2)  // FIXME: needs an event listener on JS side
#define KEY_BTAB (-3)    // FIXME: the Shift key is detected separately

#endif

#define COLOR_BLACK 0
#define COLOR_RED 1
#define COLOR_GREEN 2
#define COLOR_YELLOW 3
#define COLOR_BLUE 4
#define COLOR_MAGENTA 5
#define COLOR_CYAN 6
#define COLOR_WHITE 7
#define COLOR_PAIRS 256
#define COLOR_PAIR(n) (n)

#define WINDOW void
#define stdscr NULL
#define OK 0
#define ERR (-1)

extern int termCols;
extern int termRows;

typedef struct {
  int fg;
  int bg;
} ColorPair;

int clear(void);
int refresh(void);
int flushinp(void);
int getch(void);

WINDOW *initscr(void);
int endwin(void);

int start_color(void);
bool has_colors(void);
int use_default_colors(void);

int init_pair(int pair, int fg, int bg);
int attron(int pair);

int mvprintw(int y, int x, const char *format, ...);
int mvaddstr(int y, int x, const char *str);
int mvhline(int y, int x, char ch, int length);
int mvvline(int y, int x, char ch, int length);

int getmaxx(WINDOW *);
int getmaxy(WINDOW *);

int cbreak(void);
int noecho(void);
int nodelay(WINDOW *, bool);
int keypad(WINDOW *, bool);
int curs_set(int);
