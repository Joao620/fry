/**
 * @file huge.c
 * @brief HugeInt arithmetic library for numbers up to 4096 bits
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-15
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "huge.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Shift right (divide by two)
 * (modifies the input HugeInt directly)
 */
static void hugeShiftRight(HugeInt a) {
  uint32_t carry = 0;
  for (int i = HUGE_INT_SIZE - 1; i >= 0; i--) {
    uint32_t nextCarry = a[i] & 1;
    a[i] = (a[i] >> 1) | (carry << 31);
    carry = nextCarry;
  }
}

/**
 * Shift left (multiply by two)
 * (modifies the input HugeInt directly)
 */
static void hugeShiftLeft(HugeInt a, const int shift) {
  if (shift <= 0) {
    return;
  }

  int wordShift = shift / 32;
  int bitShift = shift % 32;

  // Shift whole words
  for (int i = HUGE_INT_SIZE - 1; i >= 0; --i) {
    a[i] = (i - wordShift >= 0) ? a[i - wordShift] : 0;
  }

  // Handle remaining bit shifts
  if (bitShift > 0) {
    for (int i = HUGE_INT_SIZE - 1; i > 0; --i) {
      a[i] = (a[i] << bitShift) | (a[i - 1] >> (32 - bitShift));
    }
    a[0] <<= bitShift;
  }
}

/**
 * Check if a bit is set in HugeInt
 */
static inline bool bitIsSet(const HugeInt num, const int bitIndex) {
  int elementIndex = bitIndex / 32;
  int bitPosition = bitIndex % 32;
  return (num[elementIndex] >> bitPosition) & 1;
}

/**
 * Set a bit in HugeInt
 * (modifies the input HugeInt directly)
 */
static inline void hugeSetBit(HugeInt num, const int bitIndex) {
  int elementIndex = bitIndex / 32;
  int bitPosition = bitIndex % 32;
  num[elementIndex] |= (1U << bitPosition);
}

int minBitsToRepresent(const HugeInt num) {
  for (int group = HUGE_INT_SIZE - 1; group >= 0; group--) {
    if (num[group] != 0) {
      for (int bit = 31; bit >= 0; bit--) {
        if (bitIsSet(num, group * 32 + bit)) {
          return group * 32 + bit + 1;  // + 1 because bit positions start at 0
        }
      }
    }
  }
  return 0;  // number is zero
}

bool hugeAdd(HugeInt result, const HugeInt a, const HugeInt b) {
  bool carry = false;
  for (int i = 0; i < HUGE_INT_SIZE; i++) {
    uint32_t temp = a[i] + carry;
    carry = temp < a[i];
    result[i] = temp + b[i];
    if (!carry) {
      carry = result[i] < temp;
    }
  }
  return !carry;  // returns false if overflow has happened
}

// bits should be max(aBits, bBits), but if you don't know use simply hugeSub
static bool quickHugeSub(HugeInt result, const HugeInt a, const HugeInt b,
                         const int bits) {
  const int iterMax = (bits + 31) / 32;
  bool borrow = false;
  for (int i = 0; i < iterMax; i++) {
    uint32_t temp = a[i] - borrow;
    borrow = temp > a[i];
    result[i] = temp - b[i];
    if (!borrow) {
      borrow = result[i] > temp;
    }
  }
  return !borrow;  // returns false if underflow has occurred
}

bool hugeSub(HugeInt result, const HugeInt a, const HugeInt b) {
  return quickHugeSub(result, a, b, HUGE_INT_SIZE * 32);
}

bool hugeMul(HugeInt result, const HugeInt a, const HugeInt b) {
  HugeInt tempResult = {0};

  for (int i = 0; i < HUGE_INT_SIZE; i++) {
    if (a[i] == 0) {
      continue;  // skip if current digit is zero (fast-track)
    }

    uint64_t carry = 0;
    for (int j = 0; j < HUGE_INT_SIZE - i; j++) {
      uint64_t product = (uint64_t)a[i] * b[j] + tempResult[i + j] + carry;

      tempResult[i + j] = (uint32_t)(product & 0xFFFFFFFF);  // lower 32 bits
      carry = (product >> 32);                               // upper 32 bits

      if (j == HUGE_INT_SIZE - i - 1 && carry != 0) {
        return false;  // overflow occurred
      }
    }
  }

  memcpy(result, tempResult, sizeof(HugeInt));
  return true;  // no overflow
}

bool hugeEq(const HugeInt a, const HugeInt b) {
  return memcmp(a, b, sizeof(HugeInt)) == 0;
}

bool hugeSet(HugeInt num, const char *hexString) {
  size_t hexLength = strlen(hexString);

  // Check if the string length exceeds the HugeInt capacity
  if (hexLength > (size_t)(HUGE_INT_SIZE * HUGE_FRAG)) {
    return false;  // string is too long
  }

  memset(num, 0, sizeof(HugeInt));
  char buffer[HUGE_FRAG + 1];  // to hold HUGE_FRAG digits + '\0'

  // Process the string in chunks of HUGE_FRAG digits
  for (size_t i = 0; i * HUGE_FRAG < hexLength; i++) {
    size_t end = hexLength - i * HUGE_FRAG;
    size_t start =
        (i + 1) * HUGE_FRAG > hexLength ? 0 : hexLength - (i + 1) * HUGE_FRAG;

    strncpy(buffer, &hexString[start], end - start);
    buffer[end - start] = '\0';

    char *endPtr = NULL;
    uint32_t value = strtoul(buffer, &endPtr, 16);
    if (*endPtr != '\0') {
      return false;  // invalid hexadecimal number
    }
    num[i] = value;
  }

  return true;  // success
}

// bits should be max(aBits, bBits), but if you don't know use simply hugeIsLess
static bool quickHugeIsLess(const HugeInt a, const HugeInt b, const int bits) {
  for (int i = (bits - 1) / 32; i >= 0; i--) {
    if (a[i] < b[i]) {
      return true;
    }
    if (a[i] > b[i]) {
      return false;
    }
  }

  return false;  // a is equal to b
}

bool hugeIsLess(const HugeInt a, const HugeInt b) {
  return quickHugeIsLess(a, b, HUGE_INT_SIZE * 32);
}

void hugeMod(HugeInt result, const HugeInt a, const HugeInt modulus) {
  memmove(result, a, sizeof(HugeInt));

  const int aBits = minBitsToRepresent(a);
  const int modulusBits = minBitsToRepresent(modulus);
  if (modulusBits == 0) {
    return;
  }

  const int shift = aBits - modulusBits;
  if (shift < 0) {
    return;
  }

  HugeInt scaledModulus;
  memcpy(scaledModulus, modulus, sizeof(HugeInt));

  // Scale up the modulus to get close to 'a'
  hugeShiftLeft(scaledModulus, shift);

  // Scale modulus back down, subtracting from result as needed
  for (int i = shift; i >= 0; i--) {
    if (!quickHugeIsLess(result, scaledModulus, aBits)) {
      quickHugeSub(result, result, scaledModulus, aBits);
    }
    hugeShiftRight(scaledModulus);
  }
}

void hugeDiv(HugeInt result, const HugeInt dividend, const HugeInt divisor) {
  memset(result, 0, sizeof(HugeInt));

  const int dividendBits = minBitsToRepresent(dividend);
  const int divisorBits = minBitsToRepresent(divisor);
  if (divisorBits == 0) {
    return;
  }

  int shift = dividendBits - divisorBits;
  if (shift < 0) {
    return;
  }

  HugeInt tempRemainder;
  memcpy(tempRemainder, dividend, sizeof(HugeInt));

  HugeInt shiftedDivisor;
  memcpy(shiftedDivisor, divisor, sizeof(HugeInt));

  // Scale up the divisor to get close to the dividend
  hugeShiftLeft(shiftedDivisor, shift);

  // Iterate over each bit of the dividend
  for (int i = shift; i >= 0; i--) {
    if (!quickHugeIsLess(tempRemainder, shiftedDivisor, dividendBits)) {
      quickHugeSub(tempRemainder, tempRemainder, shiftedDivisor, dividendBits);
      hugeSetBit(result, i);
    }
    hugeShiftRight(shiftedDivisor);  // scale down divisor
  }
}

bool hugeExp(HugeInt result, const HugeInt base, const HugeInt exponent) {
  // Initialize result as 1
  memset(result, 0, sizeof(HugeInt));
  result[0] = 1;

  HugeInt tempBase;
  memcpy(tempBase, base, sizeof(HugeInt));

  // Iterate over each bit of the exponent, starting from LSB
  const int msb = minBitsToRepresent(exponent);
  for (int i = 0; i < msb; i++) {
    if (bitIsSet(exponent, i)) {
      if (!hugeMul(result, result, tempBase)) {
        return false;
      }
    }

    // Prepare tempBase for the next iteration by squaring it
    if (i < msb - 1) {  // avoid extra squaring
      if (!hugeMul(tempBase, tempBase, tempBase)) {
        return false;
      }
    }
  }

  return true;
}

bool hugeModExpImpl(HugeInt result, const HugeInt base, const HugeInt exponent,
                    const HugeInt modulus) {
  HugeInt tempBase;
  memcpy(tempBase, base, sizeof(HugeInt));

  // Initialize result as 1
  memset(result, 0, sizeof(HugeInt));
  result[0] = 1;

  // Iterate over each bit of the exponent, starting from LSB
  const int msb = minBitsToRepresent(exponent);
  for (int i = 0; i < msb; i++) {
    if (bitIsSet(exponent, i)) {
      if (!hugeMul(result, result, tempBase)) {
        return false;
      }
      hugeMod(result, result, modulus);
    }

    if (!hugeMul(tempBase, tempBase, tempBase)) {
      return false;
    }
    hugeMod(tempBase, tempBase, modulus);
  }

  return true;
}
