/**
 * @file logger.c
 * @brief Simple log levels and using ANSI colors
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-30
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "sdk.h"

bool debugLogs = false;
bool statsLogs = false;
int indMax = IND_MAX;
bool stdoutIsTTY = false;
bool stderrIsTTY = false;

#ifdef DEBUG
bool traceLogs = false;
#endif

static void startLineDumbImpl(const char* color) { (void)color; }

static void stdoutStartLineColorImpl(const char* color) {
  fputs(color, stdout);
}
static void stdoutEndLineDumbImpl(void) { fputc('\n', stdout); }
static void stdoutEndLineColorImpl(void) { fputs(CL_RST "\n", stdout); }
static void stdoutLnDumbImpl(const char* msg, const char* color) {
  (void)color;
  fputs(msg, stdout);
  fputc('\n', stdout);
}
static void stdoutLnColorImpl(const char* msg, const char* color) {
  fputs(color, stdout);
  fputs(msg, stdout);
  fputs(CL_RST "\n", stdout);
}

static void stderrStartLineColorImpl(const char* color) {
  fputs(color, stderr);
}
static void stderrEndLineDumbImpl(void) { fputc('\n', stderr); }
static void stderrEndLineColorImpl(void) { fputs(CL_RST "\n", stderr); }
static void stderrLnDumbImpl(const char* msg, const char* color) {
  (void)color;
  fputs(msg, stderr);
  fputc('\n', stderr);
}
static void stderrLnColorImpl(const char* msg, const char* color) {
  fputs(color, stderr);
  fputs(msg, stderr);
  fputs(CL_RST "\n", stderr);
}

StartLineFunc stdoutStartLine = startLineDumbImpl;
StartLineFunc stderrStartLine = startLineDumbImpl;
EndLineFunc stdoutEndLine = stdoutEndLineDumbImpl;
EndLineFunc stderrEndLine = stderrEndLineDumbImpl;
PrintLineFunc stdoutLn = stdoutLnDumbImpl;
PrintLineFunc stderrLn = stderrLnDumbImpl;

void initLogger(void) {
  const char* term = getenv("TERM");

  // TERM=dumb disables colored output, curses etc.
  if (!term || strcmp(term, "dumb") != 0) {
    stdoutIsTTY = isatty(STDOUT_FILENO);
    stderrIsTTY = isatty(STDERR_FILENO);
  }

  if (stdoutIsTTY) {
    stdoutStartLine = stdoutStartLineColorImpl;
    stdoutEndLine = stdoutEndLineColorImpl;
    stdoutLn = stdoutLnColorImpl;
  }

  if (stderrIsTTY) {
    stderrStartLine = stderrStartLineColorImpl;
    stderrEndLine = stderrEndLineColorImpl;
    stderrLn = stderrLnColorImpl;
  }
}

/**
 * @brief Keeps only the last part of filepath (after last slash)
 *
 * @param filepath to extract the filename from
 * @return string (use free)
 */
static char* extractFilename(const char* filepath) {
  if (!filepath[0]) {
    return strDup("");
  }
  size_t i = strlen(filepath) - 1;
  for (; i; i--) {
    if (filepath[i] == '/') {
      i++;
      break;
    }
  }
  return replaceAll(&filepath[i], ".c", "");
}

/**
 * @brief Print limited number of chars and reset color to default
 *
 * @param text to print
 * @param limit max length
 */
static void printLimited(const char* text, int limit) {
  int i = 0;
  for (; i < limit && text[i]; i++) {
    if (iscntrl(text[i])) {
      limit -= 1;  // because of extra backslash char
      switch (text[i]) {
        case '\b':
          fputs("\\b", stderr);
          break;
        case '\f':
          fputs("\\f", stderr);
          break;
        case '\n':
          fputs("\\n", stderr);
          break;
        case '\r':
          fputs("\\r", stderr);
          break;
        case '\t':
          fputs("\\t", stderr);
          break;
        case '\033':
          fputs("\\e", stderr);
          break;
        default:
          fprintf(stderr, "\\%03o", (Byte)text[i]);  // print as octal
          limit -= 2;
      }
    } else {
      fputc(text[i], stderr);
    }
  }
  if (i >= limit) {
    fputs(isUtf8Mode ? "…" : "~", stderr);
  }
  stderrEndLine();
}

static inline const char* getFrameType(Frame* frame) {
  return frame->node ? fatType(frame->node->type) : "Block";
}

#ifdef DEBUG

static inline const char* getFrameSrc(Frame* frame) {
  return frame->node ? frame->node->src : SRC_AUX;
}

void traceRun(const char* func, Frame* cur) {
  const char* src = getFrameSrc(cur);
  stderrStartLine(CL_MGT);
  fprintf(stderr, MRG_STR LB_TRACE ": %11.11s > %s > ", func, src);
  fputs(getFrameType(cur), stderr);
  stderrEndLine();
}

void traceMemory(const char* action, Node* node) {
  stderrStartLine(CL_MGT);
  fprintf(stderr, MRG_STR LB_TRACE ":      memory > %10.10s > %7s > %" PRIxPTR,
          action, node->src, (uintptr_t)node);
  stderrEndLine();
}

void traceScope(const char* action, Scope* scp) {
  stderrStartLine(CL_MGT);
  fputs(MRG_STR LB_TRACE, stderr);
  fprintf(stderr, ":      memory > %10.10s >   scope > %" PRIxPTR, action,
          (uintptr_t)scp);
  stderrEndLine();
}

void logTrace2(const char* file, const char* func, const char* ref,
               const char* msg) {
  if (debugLogs && traceLogs) {
    auto_str name = extractFilename(file);
    stderrStartLine(CL_MGT);
    fputs(MRG_STR LB_TRACE, stderr);
    fprintf(stderr, ": %11.11s > %10.10s > %7s > ", name, func, ref);
    printLimited(msg, indMax * 2 - 48);
  }
}

void traceStackDepth(const Context* ctx) {
  int ctxTop = atomic_load(&ctx->top);
  stderrStartLine(CL_MGT);
  fprintf(stderr, MRG_STR LB_TRACE ": stack depth > layer > %2d > %d%% of %d",
          ctxTop, (int)(ctxTop * 100 / stackDepth), (int)stackDepth);
  stderrEndLine();
}

char* ofPointer(void* p) {
  if (!p) {
    return strDup("null");
  }
  char* result = NULL;
  if (asprintf(&result, "%" PRIxPTR, (uintptr_t)p) == -1) {
    fatalOut(__FILE__, __func__, MSG_OOM);
  }
  return result;
}

#endif

void logBlock(int block) {
  stderrStartLine(CL_GRN);
  fprintf(stderr, MRG_STR "- Block %d", block);
  stderrEndLine();
}

void logHints(const char* func, const char* msg, const char* src) {
  stderrStartLine(CL_CYN);
  fprintf(stderr, MRG_HNT LB_HINTS ": %s > %s > %s", func, msg, src);
  stderrEndLine();
}

void logDebug(const char* file, const char* func, const char* msg) {
  auto_str name = extractFilename(file);
  stderrStartLine(CL_BLU);
  fprintf(stderr, MRG_STR LB_DEBUG ": %11.11s > %10.10s > ", name, func);
  printLimited(msg, indMax * 2 - 38);
}

void logDebug2(const char* file, const char* func, const char* ref,
               const char* msg) {
  auto_str name = extractFilename(file);
  stderrStartLine(CL_BLU);
  fputs(MRG_STR LB_DEBUG, stderr);
  fprintf(stderr, ": %11.11s > %10.10s > %7s > ", name, func, ref);
  printLimited(msg, indMax * 2 - 48);
}

static void printHumanizedTime(double seconds) {
  if (seconds < 10) {
    printf("%7.0f ms     ", seconds * 1000);
  } else {
    printf("%7.0f sec    ", seconds);
  }
}

void printHumanizedCount(long count, bool compact, FILE* stream) {
  char* approx = compact ? "" : "  ";
  char* unit = "";

  if (count > 999) {
    approx = "~";
    count = ceil((double)count / 1000);
    unit = "k";
    if (count > 999) {
      count = ceil((double)count / 1000);
      unit = "m";
    }
  }

  if (compact) {
    fprintf(stream, "%s%ld%s", approx, count, unit);
  } else {
    fprintf(stream, "%s%4ld%s", approx, count, unit);
  }
}

void logStats(clock_t t, double w) {
  endCursesMode();

  double act = (double)atomic_load(&activeMemory);
  double lim = (double)memoryLimit;
  double all = (double)maxAllocated;
  double bytes = realBytesUsage();  // heavy operation
  double kb = bytes / 1000;
  double maxKb = kb * all / act;  // estimative

  puts("");
  stdoutLn(MRG_BLT "CLOCK / STATS", CL_GRN);

  if (warnHighGC) {
    fflush(stdout);
    logAlert("GC", "high use",
             "too much time spent!\n"
             "Your program seems to be running low on memory, try to increase\n"
             "mem limit for better GC performance, e.g. fry -n <more nodes>");
  }

  // cpu-time

  fputs(MRG_STR "total cpu-time     >", stdout);
  printHumanizedTime((double)t / CLOCKS_PER_SEC);

  // wall-time

  fputs("\n" MRG_STR "total wall-time    >", stdout);
  printHumanizedTime(w / 1000);

  // memory (in bytes)

  fputs("\n" MRG_STR "max ram use (est.) >", stdout);
  printf("%7.0f", maxKb < 1000.0 ? maxKb : maxKb / 1000.0);
  puts(maxKb < 1000.0 ? " kb" : " mb");

  fputs(MRG_STR "current ram use    >", stdout);
  printf("%7.0f", kb < 1000.0 ? kb : kb / 1000.0);
  puts(kb < 1000.0 ? " kb" : " mb");

  fputs(MRG_STR "average node size  >", stdout);
  printf("%7.0f bytes\n", bytes / act);

  // gc time / nodes use

  if (!floatEq(gcTotalTimeMs, 0.0)) {
    fputs(MRG_STR "time spent with GC >", stdout);
    printHumanizedTime(gcTotalTimeMs / 1000.0);
    double tMs = ((double)t / CLOCKS_PER_SEC) * 1000.0;
    printf("| %3.0f%%\n", gcTotalTimeMs * 100.0 / tMs);

    fputs(MRG_STR "current node count > ", stdout);
    printHumanizedCount((long)act, false, stdout);
    printf(" nodes  | %3d%%\n", (int)(act * 100.0 / lim));
  }

  fputs(MRG_STR "max nodes tracked  > ", stdout);
  printHumanizedCount(maxAllocated, false, stdout);
  printf(" nodes  | %3d%%\n", (int)(all * 100.0 / lim));

  // stack usage (if available)

  fputs(MRG_STR "max stack depth    >", stdout);
  printf("%7d layers | %3d%%\n", maxStackUsed,
         (maxStackUsed * 100) / stackDepth);

  // scope registry

  fputs(MRG_STR "active scope count > ", stdout);
  printHumanizedCount(trackedScopes, false, stdout);
  puts(" scopes");

  fputs(MRG_STR "max scopes tracked > ", stdout);
  printHumanizedCount(maxTrackedScopes, false, stdout);
  puts(" scopes");

#ifdef DEBUG
  // lookup cache
  fputs(MRG_STR "lookup cache stats > ", stdout);
  printHumanizedCount(cacheHits, false, stdout);
  puts(" hits");
#endif
}

void logAlert(const char* file, const char* func, const char* msg) {
  endCursesMode();
  stderrStartLine(CL_YEL);
  fprintf(stderr, MRG_BLT LB_ALERT ": %s > %s > %s", file, func, msg);
  stderrEndLine();
}

void logError(const char* file, const char* func, const char* msg) {
  endCursesMode();
  stderrStartLine(CL_RED);
  fprintf(stderr, MRG_ERR LB_ERROR ": %s > %s > %s", file, func, msg);
  stderrEndLine();
  hasFoundErrors = true;
}

void errorOut(const char* file, const char* func, const char* msg) {
  logError(file, func, msg);
  if (crashOnError) {
    exitFry(EXIT_FAILURE);  // maybe exits
  }
}

noreturn void fatalOut(const char* file, const char* func, const char* msg) {
  endCursesMode();
  stderrStartLine(CL_RED);
  fputs(MRG_ERR LB_FATAL, stderr);
  fprintf(stderr, ": %s > %s > %s", file, func ? func : "?", msg);
  stderrEndLine();
  exitFry(EXIT_FAILURE);  // always exits
}

void logMarker(const char* msg) {
  endCursesMode();
  stderrStartLine(CL_GRN);
  fprintf(stderr, MRG_BLT "%s", msg);
  stderrEndLine();
}

void indentPrint(const char* text, int level) {
  if (level > 0) {
    bool maxedOut = level > indMax;
    level = maxedOut ? indMax : level;
    fputs(maxedOut ? "~" : " ", stderr);
    if (level >= 7) {
      if (isUtf8Mode) {
        fputs(level == 7 ? "   " MRG_NTH : "   " MRG_OPN, stderr);
      } else {
        fputs("    | ", stderr);
      }
      for (int i = 7; i < level; i++) {
        fputs(i % 2 ? "." : " ", stderr);
      }
    } else {
      for (int i = 1; i < level; i++) {
        fputs(" ", stderr);
      }
    }
  }
  printLimited(text, indMax * 2 - level);
}

void printSourceInput(const char* text) {
  int line = 1;
  logMarker("INPUT");
  stderrStartLine(CL_BLU);
  fputs(" 1 ", stderr);
  stderrStartLine(CL_CYN);
  for (int i = 0; text[i]; i++) {
    if (text[i] == '\n') {
      line++;
      if (!text[i + 1]) {
        break;  // skip last blank line
      }
      stderrEndLine();
      stderrStartLine(CL_BLU);
      fprintf(stderr, "%2d ", line);
      stderrStartLine(CL_CYN);
    } else {
      fputc(text[i], stderr);
    }
  }
  stderrEndLine();
}

void logStack(Context* ctx, int depth) {
#ifdef DEBUG
  if (!ctx) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
    exit(EXIT_FAILURE);
  }
#endif

  int count = 0;

  // i > 1, why? because root layer is always empty
  for (int i = atomic_load(&ctx->top); i > 1 && count < depth; i--) {
    Frame* frame = &ctx->stack[i];
    Node* n = frame->node;
    bool isEbStack = n && n->type == FatEmbed && n->op == EbStack;
    bool isCall = n && n->type == FatCall;
    const char* callTo = isCall && n->head && n->head->val ? n->head->val : "?";
    bool isMethodCall = !!strstr(frame->func, "evalMethodCall");
    bool isPrintStack = isCall && strcmp(callTo, "printStack") == 0;

    if (count) {
      if (!isEbStack && !isMethodCall && !isPrintStack) {
        bool isBinary = n && n->type == FatExpr;
        const char* func = frame->func;
        const char* src = n ? n->src : "(unknown)";
        if (*src != '$') {  // see: SRC_RUN and SRC_AUX
          const char* type = getFrameType(frame);
          const char* desc = isCall ? callTo : isBinary ? opType(n->op) : "";
          stderrStartLine(CL_RED);
          fputs(MRG_STR LB_STACK, stderr);
          fprintf(stderr, ": %11.11s > %4s > %s %s", func, src, type, desc);
          stderrEndLine();
          count++;
        }
      }
    } else {
      count = 1;
    }
  }
}

void stackOut(const char* file, const char* func, const char* msg,
              Context* ctx) {
  logError(file, func, msg);
  logStack(ctx, STACK_TRACE);
  if (crashOnError) {
    exitFry(EXIT_FAILURE);  // maybe exits
  }
  stderrLn(MRG_END "continuing...", CL_RED);
  hasFoundErrors = true;
}

static void indentAst(const char* text, int level, bool isResponse) {
  if (isResponse && level == IND_BASE) {
    fputs(isUtf8Mode ? MRG_RES : MRG_STR, stderr);
    stderrStartLine(CL_GRN);
    indentPrint(text, 0);
  } else {
    stderrStartLine(CL_CYN);
    indentPrint(text, level);
  }
}

static void indentLabel(const char* text, int level) {
  stderrStartLine(CL_BLU);
  indentPrint(text, level);
}

static bool isPrintingMethodArgs = false;

void printNode(Node* node, int level, bool isResponse) {
  if (!node) {
    indentAst(isResponse ? isUtf8Mode ? "ø" : "()" : "null", level, isResponse);
    return;
  }

#ifdef DEBUG
  char* id = NULL;  // only used with default case
#endif

  char* label = NULL;
  char* text = NULL;
  char num[NUMBER_MAX_LENGTH + 1];
  const char* val = node->val ? node->val : "null";
  Type* ck = node->ck;

  switch (node->type) {
    case FatVoid:
    case FatEntry:
      label = node->op ? join2("~ ", val) : strDup(val);  // is mutable?
      text = node->ck ? join3(label, ": ", node->ck->name) : strDup(label);
      indentAst(text, node->op ? level - 2 : level, isResponse);
      break;

    case FatBoolean:
      text = join3(ck ? ck->name : "Boolean", ": ", ofBool(node->num.b));
      indentAst(text, level, isResponse);
      break;

    case FatNumber:
      prettyNumber(num, node->num.f);
      text = join3(ck ? ck->name : "Number", ": ", num);
      indentAst(text, level, isResponse);
      break;

    case FatHugeInt:
      label = unparseHugeInt(*node->num.h, true);
      text = join3(ck ? ck->name : "HugeInt", ": ", label);
      indentAst(text, level, isResponse);
      break;

    case FatChunk:
      text = join2(ck ? ck->name : "Raw", ": (101010)");
      indentAst(text, level, isResponse);
      break;

    case FatText:
      text = join4(ck ? ck->name : "Text", ": '", val, "'");
      indentAst(text, level, isResponse);
      break;

    case FatTemp:
      text = join3("Template: '", val, "'");
      indentAst(text, level, isResponse);
      break;

    case FatList:
      label = strDup(node->ck->name);
      if (node->scp && node->scp->ck) {
        const char* inner =
            node->scp->ck->isComposite ? "*" : node->scp->ck->name;
        char* check = join3(label, "/", inner);
        if (node->val) {
          text = join3(node->val, ": ", check);
          free(check);
        } else {
          text = check;
        }
      } else {
        if (node->val) {
          text = join3(node->val, ": ", label);
        } else {
          text = strDup(label);
        }
      }
      free(label);
      label = NULL;
      indentAst(text, level, isResponse);
      if (node->scp) {
        if (node->scp->entries) {
          auto_str size = ofInt(node->scp->size);
          label = join3("items (", size, ")" GUIDE);
          indentLabel(label, level + INDENT);
          printScope(node->scp, level + INDENT * 2);
        } else {
          indentLabel("(empty)", level + INDENT);
        }
      }
      if (node->body) {
        indentLabel("body" GUIDE, level + INDENT);
        printNodes(node->body, level + INDENT * 2);
      }
      break;

    case FatScope:
      if (node->scp && node->scp->ck) {
        indentAst(node->scp->ck->name, level, isResponse);
      } else {
        indentAst("Scope", level, isResponse);
      }
      if (node->scp) {
        if (node->scp->entries) {
          char* size = ofInt(node->scp->size);
          label = join3("entries (", size, ")" GUIDE);
          indentLabel(label, level + INDENT);
          free(size);
          printScope(node->scp, level + INDENT * 2);
        } else {
          indentLabel("(empty)", level + INDENT);
        }
      }
      if (node->body) {
        indentLabel("body" GUIDE, level + INDENT);
        printNodes(node->body, level + INDENT * 2);
      }
      break;

    case FatMethod:
      label = join3("Method/", ck ? ck->name : "Any", ": ");
      text = join2(label, node->val ? node->val : "(anonymous)");
      indentAst(text, level, isResponse);
      if (node->head) {
        indentLabel("args" GUIDE, level + INDENT);
        isPrintingMethodArgs = true;
        printNodes(node->head, level + INDENT * 2);
        isPrintingMethodArgs = false;
      }
      indentLabel("body" GUIDE, level + INDENT);
      if (node->body) {
        Node* body = node->body;
        if (body->type == FatBlock || body->type == FatVoid) {
          if (body->scp) {
            printScope(body->scp, level + INDENT * 2);
          } else if (body->body) {
            printNodes(body->body, level + INDENT * 2);
          } else {
            indentLabel("(no-op)", level + INDENT * 2);
          }
        } else {
          printNodes(body, level + INDENT * 2);
        }
      } else {
        indentLabel("(no-op)", level + INDENT * 2);
      }
      break;

    case FatError:  // poetic license to print in red!
      label = strDup(node->ck ? node->ck->name : "Error");
      text = join3(label, ": ", node->val ? node->val : "(no message)");
      stderrStartLine(CL_RED);
      fputs(isResponse && isUtf8Mode ? MRG_RES : "", stderr);
      indentPrint(text, isResponse && isUtf8Mode ? 0 : level);
      free(text);
      text = join2(" at: ", node->src ? node->src : "(unknown)");
      stderrStartLine(CL_RED);
      indentPrint(text, level + INDENT);
      break;

    case FatType:
      text = join2(val, ": Type");
      indentAst(text, level, isResponse);
      if (ck && ck->alias) {
        if (ck->isComposite) {
          char* desc = strDup(ck->alias->name);
          for (int i = 0; ck->include[i]; i++) {
            char* aux = join3(desc, "/", ck->include[i]->name);
            free(desc);
            desc = aux;
          }
          label = join2("composite" GUIDE, desc);
          free(desc);
        } else {
          label = join2("alias" GUIDE, ck->alias->name);
        }
        indentLabel(label, level + INDENT);
      }
      if (ck && ck->include && !ck->isComposite) {
        indentLabel("includes" GUIDE, level + INDENT);
        for (int i = 0; ck->include[i]; i++) {
          indentAst(ck->include[i]->name, level + INDENT * 2, false);
        }
      }
      if (node->head) {
        indentLabel("props" GUIDE, level + INDENT);
        printNodes(node->head, level + INDENT * 2);
      }
      if (node->body && node->body->scp && node->body->scp->entries) {
        indentLabel("defaults" GUIDE, level + INDENT);
        printScope(node->body->scp, level + INDENT * 2);
      }
      if (node->tail && node->tail->scp && node->tail->scp->entries) {
        indentLabel("methods" GUIDE, level + INDENT);
        printScope(node->tail->scp, level + INDENT * 2);
      }
      break;

    case FatBlock:
      if (node->body) {
        printNodes(node->body, level + (level <= IND_BASE ? INDENT : 0));
      } else {
        printScope(node->scp, level + (level <= IND_BASE ? INDENT : 0));
      }
      break;

    case FatCall:
      if (node->head && node->head->val) {
        text = join2("Call: ", node->head->val);
        indentAst(text, level, isResponse);
      } else {
        indentAst("Call", level, isResponse);
      }
      if (node->scp) {
        indentLabel("scope" GUIDE, level + INDENT);
        printScope(node->scp, level + INDENT * 2);
      }
      if (node->body) {
        indentLabel("args" GUIDE, level + INDENT);
        printNodes(node->body, level + INDENT * 2);
      }
      if (node->tail) {
        indentLabel("with" GUIDE, level + INDENT);
        printNodes(node->tail, level + INDENT * 2);
      }
      break;

    case FatAssign:
      if (isPrintingMethodArgs) {
        Node* arg = node->head;
        label = join3(arg->val, ": ", arg->ck ? arg->ck->name : "Any");
        text = toString(node->body);
        auto_str fallbackArg = join3(label, " = ", text);
        indentAst(fallbackArg, level, isResponse);
      } else if (node->head && node->head->type == FatScope) {
        indentAst("Destructure", level, isResponse);
        indentLabel("props" GUIDE, level + INDENT);
        printNodes(node->head->body, level + INDENT * 2);
        indentLabel("of" GUIDE, level + INDENT);
        printNodes(node->body, level + INDENT * 2);
      } else {
        indentAst("Assign", level, isResponse);
        indentLabel("left" GUIDE, level + INDENT);
        printNodes(node->head, level + INDENT * 2);
        indentLabel("right" GUIDE, level + INDENT);
        printNodes(node->body, level + INDENT * 2);
      }
      break;

    case FatUnary:  // prints Eval for better ast understanding
      text = join2("Eval: ", opType(node->op));
      indentAst(text, level, isResponse);
      indentLabel("unary" GUIDE, level + INDENT);
      printNodes(node->body, level + INDENT * 2);
      break;

    case FatExpr:  // prints Eval for better ast understanding
      text = join2("Eval: ", opType(node->op));
      indentAst(text, level, isResponse);
      indentLabel("left" GUIDE, level + INDENT);
      printNodes(node->head, level + INDENT * 2);
      indentLabel("right" GUIDE, level + INDENT);
      printNodes(node->body, level + INDENT * 2);
      break;

    case FatCase:
      indentAst("Case", level, isResponse);
      indentLabel("if" GUIDE, level + INDENT);
      printNodes(node->head, level + INDENT * 2);
      indentLabel("then" GUIDE, level + INDENT);
      printNodes(node->body, level + INDENT * 2);
      if (node->tail) {
        Node* other = node->tail;
        while (other && other->type == FatCase) {
          indentLabel("elif" GUIDE, level + INDENT);
          printNodes(other->head, level + INDENT * 2);
          indentLabel("then" GUIDE, level + INDENT);
          printNodes(other->body, level + INDENT * 2);
          other = other->tail;
        }
        if (other) {
          indentLabel("else" GUIDE, level + INDENT);
          printNodes(other, level + INDENT * 2);
        }
      }
      break;

    case FatSwitch:
      indentAst("Switch", level, isResponse);
      indentLabel("value" GUIDE, level + INDENT);
      printNodes(node->head, level + INDENT * 2);
      indentLabel("match" GUIDE, level + INDENT);
      printNodes(node->body, level + INDENT * 2);
      break;

    case FatLoop:
      indentAst("Loop", level, isResponse);
      indentLabel("input" GUIDE, level + INDENT);
      printNodes(node->head, level + INDENT * 2);
      indentLabel("output" GUIDE, level + INDENT);
      printNodes(node->body, level + INDENT * 2);
      break;

    case FatEmbed:
      text = join3(fatType(node->type), ": $", ebCmd(node->op));
      indentAst(text, level, isResponse);
      break;

    case FatDynamic:
      indentAst("Dynamic", level, isResponse);
      indentLabel("eval" GUIDE, level + INDENT);
      printNodes(node->body, level + INDENT * 2);
      break;

    default:
#ifdef DEBUG
      // here we try to catch anything that is really invalid/unknown...
      id = ofInt((long)node->type);
      label = ofPointer((void*)node);
      text = join5(fatType(node->type), "[type=", id, "]" GUIDE, label);
      free(id);
      stderrStartLine(CL_RED);
      indentPrint(text, level);
#else
      indentAst(fatType(node->type), level, isResponse);
#endif
  }
  free(text);
  free(label);
}

void printNodes(Node* node, int level) {
  for (; node; node = node->seq) {
    printNode(node, level, false);
  }
}

static void printEntry(const char* text, int level, bool isMutable) {
  if (isMutable) {
    auto_str aux = join2("~ ", text);
    indentAst(aux, level - 2, false);
  } else {
    indentAst(text, level, false);
  }
}

static inline bool isPrimitive(const Node* node) {
  switch (node->type) {
    case FatVoid:
    case FatBoolean:
    case FatNumber:
    case FatHugeInt:
    case FatChunk:
    case FatText:
      return true;
    default:
      return false;
  }
}

// this is generally not thread safe, but why would anyone log scope contents
// in a multi-threaded program? no point, won't fix...
void printScope(Scope* scope, int level) {
  if (!scope || !scope->entries) {
    indentLabel("(empty)", level);
    return;
  }

  if (isFirstVisit(scope)) {
    long i = 0;
    for (Entry* entry = scope->entries; entry; entry = entry->next, i++) {
      char* key = scope->isList ? ofInt(i) : strDup(entry->key.s);
      Node* node = entry->data;

      Type* ck = NULL;
      if (node) {
        Scope* scp = node->scp;
        ck = scp && !scp->isList ? scp->ck : node->ck;
      }

      char* text = NULL;
      if (node) {
        if (isPrimitive(node)) {
          auto_str primitive = toString(node);
          const char* s =
              node->type == FatText ? " = '" : " = ";        // start with...
          const char* e = node->type == FatText ? "'" : "";  // end with...
          text = ck ? join6(key, ": ", ck->name, s, primitive, e)
                    : join4(key, s, primitive, e);
          printEntry(text, level, entry->key.s && node->op);
        } else if (node->type == FatMethod) {
          Node* method = node;
          text = method->ck ? join3(key, ": Method/", method->ck->name)
                            : join2(key, ": Method/Any");
          printEntry(text, level, entry->key.s && node->op);
          if (method->head) {
            indentLabel("args" GUIDE, level + INDENT);
            isPrintingMethodArgs = true;
            printNodes(method->head, level + INDENT * 2);
            isPrintingMethodArgs = false;
          }
          indentLabel("body" GUIDE, level + INDENT);
          if (method->body) {
            printNodes(method->body, level + INDENT * 2);
          } else {
            indentLabel("(no-op)", level + INDENT * 2);
          }
        } else if (node->type == FatType) {
          printNode(node, level, false);
        } else {
          text = join3(key, ": ", ck ? ck->name : fatType(node->type));
          printEntry(text, level, entry->key.s && node->op);
          if (node->scp) {
            if (node->scp->entries) {
              auto_str size = ofInt(node->scp->size);
              free(text);
              const char* label = node->scp->isList ? "items (" : "entries (";
              text = join3(label, size, ")" GUIDE);
              indentLabel(text, level + INDENT);
              printScope(node->scp, level + INDENT * 2);
            } else {
              indentLabel("(empty)", level + INDENT);
            }
          }
        }
      } else if (startsWith(key, "!")) {
        // import flag
        stderrStartLine(CL_WHT);
        indentPrint(key, level);
      } else {
        indentAst(key, level, false);
      }

      free(text);
      free(key);
    }
  } else {
    indentLabel("(recursive)", level + INDENT);
  }
}
