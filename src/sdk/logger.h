/**
 * @file logger.h
 * @brief Simple log levels and using ANSI colors
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-30
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "messages.h"
#include "structures.h"
#include "sugar.h"

// ascii margin "icons"
#define IND_BASE 3     // margin indent for "icons"
#define MRG_STR "   "  // margin blank string
#define MRG_HNT " # "  // margin for hints
#define MRG_BLT " * "  // margin bullet
#define MRG_ERR " ! "  // margin error
#define MRG_END "-- "  // margin ending line
#define MRG_INP " > "  // input margin (ascii mode)

// utf-8 margin "icons" (we just hope it works)
#define MRG_1ST " ┌ "  // margin first input
#define MRG_NTH " ├ "  // margin next input
#define MRG_OPN " │ "  // margin open input
#define MRG_RES " └ "  // margin response

// labels
#define LB_HINTS "Hints"
#define LB_STACK "Stack"
#define LB_CLOCK "Clock"
#define LB_TRACE "Trace"
#define LB_DEBUG "Debug"
#define LB_ALERT "Alert"
#define LB_ERROR "Error"
#define LB_FATAL "Fatal"

// ANSI color codes
#define CL_RST "\033[0m"   // reset color code
#define CL_BLK "\033[30m"  // black color code
#define CL_RED "\033[31m"  // red color code
#define CL_GRN "\033[32m"  // green color code
#define CL_YEL "\033[33m"  // yellow/yellow color code
#define CL_BLU "\033[34m"  // blue color code
#define CL_MGT "\033[35m"  // magenta color code
#define CL_CYN "\033[36m"  // cyan color code
#define CL_WHT "\033[37m"  // white/gray color code
#define CL_GRA "\033[90m"  // dark gray color code

// log level options
extern bool debugLogs;
extern bool statsLogs;
extern int indMax;  // dynamic indentation limit for repl
extern bool stdoutIsTTY;
extern bool stderrIsTTY;

/**
 * Utility methods to set/unset color of line output on TTY.
 */
typedef void (*StartLineFunc)(const char*);
typedef void (*EndLineFunc)(void);
typedef void (*PrintLineFunc)(const char*, const char*);
extern StartLineFunc stdoutStartLine;
extern StartLineFunc stderrStartLine;
extern EndLineFunc stdoutEndLine;
extern EndLineFunc stderrEndLine;
extern PrintLineFunc stdoutLn;
extern PrintLineFunc stderrLn;

/**
 * @brief Logger initializer, call at the very beginning of main.
 */
void initLogger(void);

#ifdef DEBUG

extern bool traceLogs;

/**
 * @brief Trace log node type of ast during run, in magenta.
 *
 * @param func function reference
 * @param cur Frame*
 */
void traceRun(const char* func, Frame* cur);

/**
 * @brief Trace log memory references, in magenta.
 *
 * @param action what is being done
 * @param node ref of node
 */
void traceMemory(const char* action, Node* node);

/**
 * @brief Trace log memory references, in magenta.
 *
 * @param action what is being done
 * @param scp ref of scope
 */
void traceScope(const char* action, Scope* scp);

/**
 * @brief Trace log reference and message, in magenta.
 *
 * @param file file reference
 * @param func function reference
 * @param ref message reference
 * @param msg message
 */
void logTrace2(const char* file, const char* func, const char* ref,
               const char* msg);

/**
 * @brief Trace log about stack usage, in blue.
 *
 * @param ctx Context*
 */
void traceStackDepth(const Context* ctx);

/**
 * @brief Convert a Void pointer to a new string (as hex).
 *
 * @param p void* (pointer)
 * @return string (use free)
 */
char* ofPointer(void* p);

#endif

/**
 * @brief Debug log Block being parsed, in green.
 *
 * @param block of code
 */
void logBlock(int block);

/**
 * @brief Hints log message, in cyan.
 *
 * @param func function reference
 * @param msg message
 * @param src issue code source
 */
void logHints(const char* func, const char* msg, const char* src);

/**
 * @brief Debug log message, in blue.
 *
 * @param file file reference
 * @param func function reference
 * @param msg message
 */
void logDebug(const char* file, const char* func, const char* msg);

/**
 * @brief Debug log reference and message, in blue.
 *
 * @param file file reference
 * @param func function reference
 * @param ref message reference
 * @param msg message
 */
void logDebug2(const char* file, const char* func, const char* ref,
               const char* msg);

/**
 * @brief Log adaptive units in thousands (k) or millions (m).
 *
 * @param count raw count
 * @param compact do not reserve preceding alignment space if true
 * @param stream where to print (stdout/stderr)
 */
void printHumanizedCount(long count, bool compact, FILE* stream);

/**
 * @brief Log time, with additional profiling info.
 *
 * @param t time in cpu clock ticks
 * @param w wall-time in epoch ms
 */
void logStats(clock_t t, double w);

/**
 * @brief Log warning, in yellow.
 *
 * @param file file reference
 * @param func function reference
 * @param msg message
 */
void logAlert(const char* file, const char* func, const char* msg);

/**
 * @brief Log to stderr, in red.
 *
 * @param file file reference
 * @param func function reference
 * @param msg message
 */
void logError(const char* file, const char* func, const char* msg);

/**
 * @brief Log to stderr, in red, and exit (if crash enabled).
 *
 * @param file file reference
 * @param func function reference
 * @param msg message
 */
void errorOut(const char* file, const char* func, const char* msg);

/**
 * @brief Log to stderr, in red, and exit (always).
 *
 * @param file file reference
 * @param func function reference
 * @param msg message
 */
noreturn void fatalOut(const char* file, const char* func, const char* msg);

/**
 * @brief Log header, with a bullet, in green.
 *
 * @param msg message
 */
void logMarker(const char* msg);

/**
 * @brief Print line of text with indentation.
 *
 * @param text to be printed
 * @param level number of blank spaces to prepend
 */
void indentPrint(const char* text, int level);

/**
 * @brief Print source skipping blank lines, with line numbers, in cyan.
 *
 * @param text source
 */
void printSourceInput(const char* text);

/**
 * @brief Print stacktrace, with depth, in red.
 *
 * @param ctx Context*
 * @param depth count from top
 */
void logStack(Context* ctx, int depth);

/**
 * @brief Print error and stacktrace, in red, and exit (if crash enabled).
 *
 * @param file file reference
 * @param func function reference
 * @param msg message
 * @param ctx Context*
 */
void stackOut(const char* file, const char* func, const char* msg,
              Context* ctx);

/**
 * @brief Print abstract syntax tree recursively, with indentation.
 *
 * @param node root node to print
 * @param level indentation of root
 * @param isResponse margin type
 */
void printNode(Node* node, int level, bool isResponse);

/**
 * @brief Print sequence of nodes, with indentation.
 *
 * @param node head of node sequence to print
 * @param level indentation
 */
void printNodes(Node* node, int level);

/**
 * @brief Print collection entries, with indentation.
 *
 * @param scope collection to print
 * @param level indentation
 */
void printScope(Scope* scope, int level);
