/**
 * @file memory.c
 * @brief Memory management and garbage collector functions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-30
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "sdk.h"

long memoryLimit = DEFAULT_MEM;
long maxAllocated = 0;
atomic_long activeMemory;
atomic_long newNodesSinceGC;
bool useCollector = true;
bool memoryRedefined = false;
Memory *memRegistry = NULL;
long autoGCN = 0;

Scope **scopesRegistry[SCOPE_HASH] = {0};
int scpRegistryCap[SCOPE_HASH] = {0};
int trackedScopes = 0;
int maxTrackedScopes = 0;
#ifdef DEBUG
long cacheHits = 0;
Node *parsedPrograms = NULL;
#endif

atomic_size_t parsedBytes;

double gcTotalTimeMs = 0;
bool warnHighGC = false;
atomic_bool hasLoggedWorkerBlocked;

pthread_mutex_t memoryLock;
pthread_mutexattr_t recursiveMutex;

pthread_mutex_t atomicLock;

atomic_int threadCounter;

#ifndef DEBUG
static int dummy_mutex_noop(pthread_mutex_t *ignoredLock) {
  (void)ignoredLock;
  return 0;  // do nothing
}
#endif

int (*lockResource)(pthread_mutex_t *) = NULL;
int (*unlockResource)(pthread_mutex_t *) = NULL;

void initMemoryManagement(void) {
  atomic_init(&activeMemory, 0);
  atomic_init(&newNodesSinceGC, 0);
  atomic_init(&parsedBytes, 0);
  atomic_init(&hasLoggedWorkerBlocked, false);
  atomic_init(&threadCounter, 0);

#ifndef DEBUG
  // Initially point to dummy function, see asyncStart at async.c
  lockResource = dummy_mutex_noop;
  unlockResource = dummy_mutex_noop;
#else
  // use real lock/unlock functions from the start, when built with debug flag
  // helps verify locking logic, but slows down single-threaded code by ~30%
  lockResource = pthread_mutex_lock;
  unlockResource = pthread_mutex_unlock;
#endif

  // Initialize the mutex attributes
  if (pthread_mutexattr_init(&recursiveMutex)) {
    fatalOut(__FILE__, __func__, "failed to initialize attr");
  }

  // Set the mutex type to recursive
  if (pthread_mutexattr_settype(&recursiveMutex, PTHREAD_MUTEX_RECURSIVE)) {
    fatalOut(__FILE__, __func__, "failed to set mutex type");
  }

  // Initialize global mutexes
  if (pthread_mutex_init(&memoryLock, &recursiveMutex)) {
    fatalOut(__FILE__, __func__, MSG_F_MUTEX);
  }
  if (pthread_mutex_init(&atomicLock, &recursiveMutex)) {
    fatalOut(__FILE__, __func__, MSG_F_MUTEX);
  }
}

void cleanMemoryLock(void) {
  pthread_mutex_destroy(&atomicLock);
  pthread_mutex_destroy(&memoryLock);
  pthread_mutexattr_destroy(&recursiveMutex);
}

// only call this function with global memory locked
static inline void addToGlobalMemory(Memory *memory) {
  if (memRegistry) {
    memRegistry->prev = memory;
  }
  memory->next = memRegistry;
  memRegistry = memory;
}

void moveLocalToGlobalMemory(Context *ctx) {
  lockResource(&memoryLock);
  Memory *memory = ctx->temp;
  while (memory) {
    Memory *next = memory->next;
    addToGlobalMemory(memory);
    memory = next;
  }
  ctx->tempCount = 0;
  ctx->temp = NULL;
  unlockResource(&memoryLock);
}

static inline void addToContextMemory(Memory *memory, Context *ctx) {
  memory->next = ctx->temp;
  ctx->temp = memory;
  ctx->tempCount++;

  if (ctx->tempCount == MAX_CONTEXT_MEM) {
    moveLocalToGlobalMemory(ctx);
  }
}

void createMemory(Node *node, Context *ctx) {
  Memory *memory = FRY_ALLOC(sizeof(Memory));

  // Initialize members
  memory->prev = NULL;
  memory->data = node;
  atomic_init(&memory->refs, 0);  // see: bind/unbind Scope methods
  memory->visited = false;
  memory->locked = true;

  node->meta = memory;

  if (ctx) {
    addToContextMemory(memory, ctx);  // contention-less

    // perform autoGC if enabled with some memory pressure heuristics
    long recentNewNodes = atomic_fetch_add(&newNodesSinceGC, 1);
    if (autoGCN > 0 && pthread_equal(ctx->id, mainThreadId) &&
        recentNewNodes >= autoGCN) {
      long activeNodes = atomic_load(&activeMemory);
      long freeNodes = memoryLimit - activeNodes;
      if (freeNodes > autoGCN) {
        quickGC(ctx);
      } else {
        fullGC(ctx);
      }
    }
  } else {
    lockResource(&memoryLock);
    addToGlobalMemory(memory);
    unlockResource(&memoryLock);
  }

#ifdef DEBUG
  if (traceLogs) {
    traceMemory("created", node);
  }
#endif
}

/**
 * Frees node (and it's string value), if not null
 * (also if type definition, dereferences itself)
 */
static void deleteNode(Node *node) {
  if (node->type == FatType) {
    Type *type = getType(node->val);
    if (type && type->def == node) {
      if (debugLogs) {
        logDebug2(__FILE__, __func__, "type definition", type->name);
      }
      type->def = NULL;
    }
  } else if (node->type == FatHugeInt) {
    free(node->num.h);
  }

  unbindScope(node);
  free(node->val);

#ifdef DEBUG
  if (traceLogs) {
    traceMemory("removed", node);
  }
#endif
  if (*node->src != '$') {
    free(node->src);  // see also: cpSrc
  }

  free(node);
}

static void removeMemory(Memory *memory) {
#ifdef DEBUG
  if (!memRegistry) {
    fatalOut(__FILE__, __func__, MSG_IS_EMPTY);
    exit(EXIT_FAILURE);
  }
#endif

  // Delete the actual node value
  Node *node = memory->data;
  if (node) {
    deleteNode(node);
  }

  // Remove metadata from registry
  Memory *prev = memory->prev;
  Memory *next = memory->next;
  if (next) {
    next->prev = prev;
  }
  if (prev) {
    prev->next = next;
  } else {
    memRegistry = next;
  }
  atomic_fetch_sub(&activeMemory, 1);
  free(memory);
}

static void visitNode(Node *node, bool follow);

static void visitScope(Scope *scope);

static void visitStack(Context *ctx) {
#ifdef DEBUG
  if (!ctx) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
    return;
  }
#endif

  visitNode(ctx->failureEvent, false);
  visitNode(ctx->unclaimed, false);

  Scope *prev[FIND_SCP_DEDUP + 1] = {0};  // null-terminated
  const Frame *stack = ctx->stack;

  for (int i = atomic_load(&ctx->top); i; i--) {
    visitNode(stack[i].node, true);

    Scope *scope = stack[i].scp;
    if (scope) {
      // Check if scope is in previous scopes
      bool alreadyChecked = false;
      for (int j = 0; prev[j]; j++) {
        if (scope == prev[j]) {
          alreadyChecked = true;
          break;
        }
      }
      if (!alreadyChecked) {
        visitScope(scope);
        // Shift previous and add current scope to the start
        memmove(&prev[1], &prev[0], sizeof(Scope *) * (FIND_SCP_DEDUP - 1));
        prev[0] = scope;
      }
    }
  }
}

static void visitScope(Scope *scope) {
  if (!isFirstVisit(scope)) {
    return;
  }
  for (Entry *entry = scope->entries; entry; entry = entry->next) {
    visitNode(entry->data, false);
  }
  if (scope->async) {
    visitStack(scope->async->ctx);
  }
}

static void visitNode(Node *node, bool follow) {
  if (!node || !node->meta || node->meta->visited) {
    return;
  }
  node->meta->visited = true;
  visitNode(node->head, true);
  visitNode(node->body, true);
  visitNode(node->tail, true);
  if (node->scp) {
    visitScope(node->scp);
  }
  if (follow) {
    for (node = node->seq; node; node = node->seq) {
      visitNode(node, false);
    }
  }
}

static void logGcElapsedTime(const char *func, double elapsedMs, Context *ctx) {
  if (!pthread_equal(ctx->id, mainThreadId)) {
    if (!atomic_exchange(&hasLoggedWorkerBlocked, true)) {
      stderrLn(MRG_STR LB_CLOCK ": async workers blocked by GC", CL_MGT);
    }
    return;
  }

  atomic_store(&hasLoggedWorkerBlocked, false);
  const double act = (double)atomic_load(&activeMemory);
  const double lim = (double)memoryLimit;

  stderrStartLine(CL_MGT);
  fprintf(stderr, MRG_STR LB_CLOCK ": %s took %.0f ms ", func, elapsedMs);
  fprintf(stderr, "(kept: %d%% nodes / ", (int)(act * 100.0 / lim));
  printHumanizedCount(trackedScopes, true, stderr);
  fputs(" scopes)", stderr);
  stderrEndLine();
}

static inline void addToTrapsCache(HandlersCache *cache, Node *trap) {
  if (cache->size >= cache->capacity) {
    cache->capacity *= 2;  // double the capacity
    cache->handlers =
        FRY_REALLOC(cache->handlers, cache->capacity * sizeof(Node *));
    if (!cache->handlers) {
      fatalOut(__FILE__, __func__, MSG_OOM);
      return;  // mitigate false positive on auto_check.sh
    }
  }
  cache->handlers[cache->size++] = trap;
}

static inline void updateTrapsCache(Context *ctx) {
  HandlersCache *cache = &(ctx->trapsCache);
  cache->size = 0;
  Frame *stack = ctx->stack;
  for (int i = atomic_load(&ctx->top); i; i--) {
    if (stack[i].trap) {
      addToTrapsCache(cache, stack[i].trap);
    }
  }
}

static bool isNodeProtected(Node *node, Context *ctx) {
  if (node == ctx->failureEvent || node == ctx->unclaimed) {
    return true;
  }
  HandlersCache *cache = &(ctx->trapsCache);
  if (!cache->isUpdated) {
    updateTrapsCache(ctx);
    cache->isUpdated = true;
  }
  for (int i = 0; i < cache->size; i++) {
    if (node == cache->handlers[i]) {
      return true;
    }
  }
  return false;
}

void microGC(Node *keep, Memory *until, Context *ctx) {
  Memory *next = NULL;
  Memory *prev = NULL;
  for (Memory *mem = ctx->temp; mem && mem != until; mem = next) {
    next = mem->next;
    Node *node = mem->data;
    if (node && node != keep && !atomic_load(&mem->refs) &&
        !isNodeProtected(node, ctx)) {
      atomic_fetch_sub(&newNodesSinceGC, 1);
      atomic_fetch_sub(&activeMemory, 1);
      ctx->tempCount--;
      if (prev) {
        prev->next = next;
      } else {
        ctx->temp = next;
      }
      deleteNode(node);
      free(mem);
      continue;
    }
    prev = mem;
  }

  // Defer cache building until it is actually necessary each time
  ctx->trapsCache.isUpdated = false;
}

/**
 * @brief Perform a garbage collection recursion through all nodes
 *        still accessible from global scope or bound to context
 *        (partial step / incremental)
 *
 * @param ctx Context*
 * @return elapsed time in milliseconds
 */
static double markAndSweep(Context *ctx) {
  if (!useCollector || !ctx) {
    return 0;
  }

  // Prepare for GC, move thread registered nodes (local) to global metaspace
  moveLocalToGlobalMemory(ctx);

  if (!pthread_equal(ctx->id, mainThreadId)) {
    msSleep(GC_THREAD_LAG);  // hope for main thread to run GC asap
    return GC_THREAD_LAG;
  }

  if (!initVisit(__func__)) {
    return 0;  // prevent GC while some other traversal is taking place
  }

#ifdef DEBUG
  if (debugLogs) {
    logDebug(__FILE__, __func__, "start");
  }
#endif

  clock_t tack = clock();

  if (atomic_load(&activeMemory) > memoryLimit - (GC_PREMONITION * 2 / 3)) {
    warnHighGC = true;
  }

  // Unmark node references
  Memory *mem = memRegistry;
  for (; mem; mem = mem->next) {
    mem->visited = false;
  }

  // Mark stack references
  visitStack(ctx);

  // Mark metaSpace references
  for (int hash = 0; hash < META_HASH; hash++) {
    for (Type *ck = metaSpace[hash]; ck; ck = ck->next) {
      visitNode(ck->def, false);
    }
  }

  // Collect "lost" scopes
  for (int hash = 0; hash < SCOPE_HASH; hash++) {
    for (int i = 0; i < scpRegistryCap[hash]; i++) {
      if (isFirstVisit(scopesRegistry[hash][i])) {
        collectScope(scopesRegistry[hash][i]);
      }
    }
  }

  // Collect "orphan" nodes (2nd pass)
  Memory *next = NULL;
  for (mem = memRegistry; mem; mem = next) {
    next = mem->next;  // mem is freed during delete node
    if (!mem->visited && !mem->locked && !atomic_load(&mem->refs)) {
      removeMemory(mem);
    }
  }

  atomic_store(&newNodesSinceGC, 0);
  endVisit();

#ifdef DEBUG
  if (debugLogs) {
    logDebug(__FILE__, __func__, "end");
  }
#endif

  // Track time spent with garbage collector
  double elapsedMs = (double)(clock() - tack) * 1000 / CLOCKS_PER_SEC;
  gcTotalTimeMs += elapsedMs;
  return elapsedMs;
}

double fullGC(Context *ctx) {
  double totalTime = 0;
  long activeNodes = atomic_load(&activeMemory);

  for (long prevCount = 0; activeNodes != prevCount;
       activeNodes = atomic_load(&activeMemory)) {
    prevCount = activeNodes;
    totalTime += markAndSweep(ctx);
  }

  if (statsLogs) {
    logGcElapsedTime(__func__, totalTime, ctx);
  }
  return totalTime;
}

double quickGC(Context *ctx) {
  double totalTime = markAndSweep(ctx);
  if (statsLogs) {
    logGcElapsedTime(__func__, totalTime, ctx);
  }
  return totalTime;
}

size_t getNodeBytes(Node *node) {
  size_t usage = sizeof(Node);

  if (node->meta) {
    usage += sizeof(Memory);
  }
  if (node->val) {
    switch (node->type) {
      case FatChunk:
      case FatText:
      case FatTemp:
        usage += node->num.s;
        break;

      default:
        usage += strlen(node->val);
    }
    usage++;  // '\0' char
  } else if (node->type == FatHugeInt) {
    usage += sizeof(HugeInt);
  }
  if (node->src && *node->src != '$') {
    usage += strlen(node->src) + 1;
  }
  if (node->scp && isFirstVisit(node->scp)) {
    usage += sizeof(Scope);
    lockResource(&node->scp->lock);
    for (Entry *entry = node->scp->entries; entry; entry = entry->next) {
      if (!node->scp->isList) {
        usage += strlen(entry->key.s) + 1;
      }
      usage += sizeof(Entry);
    }
    if (node->scp->async) {
      usage += sizeof(Worker);
    }
    unlockResource(&node->scp->lock);
  }

  return usage;
}

void addMemory(Node *node, Context *ctx) {
  if (atomic_fetch_add(&activeMemory, 1) >= memoryLimit - GC_PREMONITION) {
    quickGC(ctx);  // run GC
  }

  long act = atomic_load(&activeMemory);

  if (act > memoryLimit) {
    fatalOut(__FILE__, __func__, "limit reached");
    return;
  }

  if (act > maxAllocated) {
    maxAllocated = act;  // update stats
  }

  if (ctx) {
    createMemory(node, ctx);  // don't track source level nodes (with no ctx)
  }
}

void trackScope(Scope *scope) {
  if (!scope || scope->isTracked) {
    return;
  }

  if (scope == globalScope || scope->isLayer) {
    return;
  }

  int hash = (int)((uintptr_t)scope % SCOPE_HASH);
  lockResource(&memoryLock);
  if (scpRegistryCap[hash] % SCOPE_BLOCK == 0) {
    if (scpRegistryCap[hash] == 0) {
      scopesRegistry[hash] = FRY_ALLOC(sizeof(Scope *) * SCOPE_BLOCK);
    } else {
      scopesRegistry[hash] =
          FRY_REALLOC(scopesRegistry[hash],
                      sizeof(Scope *) * (scpRegistryCap[hash] + SCOPE_BLOCK));
    }
  }
  scopesRegistry[hash][scpRegistryCap[hash]++] = scope;
  trackedScopes++;
  unlockResource(&memoryLock);

#ifdef DEBUG
  if (traceLogs) {
    traceScope("tracked", scope);
  }
#endif
  if (trackedScopes > maxTrackedScopes) {
    maxTrackedScopes = trackedScopes;
  }
  scope->isTracked = true;
}

void bindScope(Node *node, Scope *scope) {
  if (!scope) {
    return;
  }

  node->scp = scope;
  atomic_fetch_add(&scope->refs, 1);
  trackScope(scope);
}

void unbindScope(Node *node) {
  if (!node->scp) {
    return;
  }

  atomic_fetch_sub(&node->scp->refs, 1);
  node->scp = NULL;
}

void collectScope(Scope *scope) {
  if (!useCollector || scope == globalScope || scope->isLayer) {
    return;
  }

  if (atomic_load(&scope->refs)) {
    return;  // can't collect scope still referenced
  }

  if (scope->async && !atomic_load(&scope->async->hasJoined)) {
    return;  // can't collect an active async scope
  }

  int hash = (int)((uintptr_t)scope % SCOPE_HASH);
  for (int i = 0; i < scpRegistryCap[hash]; i++) {
    if (scopesRegistry[hash][i] == scope) {
      lockResource(&memoryLock);
      scpRegistryCap[hash]--;
      if (i < scpRegistryCap[hash]) {
        memmove(&scopesRegistry[hash][i], &scopesRegistry[hash][i + 1],
                (scpRegistryCap[hash] - i) * sizeof(Scope *));
      }
      trackedScopes--;

      // Check if the bucket should be scaled down
      if ((scpRegistryCap[hash] % SCOPE_BLOCK == 0)) {
        if (scpRegistryCap[hash]) {
          int size =
              (((scpRegistryCap[hash] - 1) / SCOPE_BLOCK) + 1) * SCOPE_BLOCK;
          scopesRegistry[hash] =
              FRY_REALLOC(scopesRegistry[hash], size * sizeof(Scope *));
        } else {
          free(scopesRegistry[hash]);
        }
      }
      unlockResource(&memoryLock);

      if (scope->async) {
        freeContext(scope->async->ctx);
        free(scope->async);
        scope->async = NULL;
      }
      wipeScope(scope, NULL);
#ifdef DEBUG
      if (traceLogs) {
        traceScope("untracked", scope);
      }
#endif
      free(scope);
      return;
    }
  }
}

static void handleStackOverflow(void) {
  endCursesMode();
  stderrStartLine(CL_YEL);
  fputs(MRG_BLT LB_ALERT ": max stack depth is set to ", stderr);
  fprintf(stderr, "%d", stackDepth);
  stderrEndLine();
  fatalOut(__FILE__, __func__, "stack overflow");
}

static void increaseStackSize(Context *ctx) {
  if (ctx->currentMax * 2 <= stackDepth) {
    ctx->currentMax *= 2;
    ctx->stack = FRY_REALLOC(ctx->stack, sizeof(Frame) * ctx->currentMax);
  } else if (ctx->currentMax != stackDepth) {
    ctx->currentMax = stackDepth;
    ctx->stack = FRY_REALLOC(ctx->stack, sizeof(Frame) * stackDepth);
  } else {
    handleStackOverflow();
  }
}

void pushStack(Context *ctx, const char *func, Node *node, Scope *scp) {
#ifdef DEBUG
  if (!ctx) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
    return;  // mitigate false positive on auto_check.sh
  }
#endif

  // Check for stack size and resize stack if needed/allowed
  int ctxTop = atomic_fetch_add(&ctx->top, 1) + 1;
  if (ctxTop == ctx->currentMax) {
    increaseStackSize(ctx);
  }

  // Increment stack top
  if (ctxTop > maxStackUsed) {
    maxStackUsed = ctxTop;
  }

  // Update stack frame references
  ctx->cur = &ctx->stack[ctxTop];
  ctx->cur->node = node;
  ctx->cur->scp = scp;
  ctx->cur->trap = NULL;
  ctx->cur->func = func;

#ifdef DEBUG
  if (traceLogs) {
    traceRun(func, ctx->cur);
  }
#endif
}

void popStack(Context *ctx, int n) {
#ifdef DEBUG
  if (n < 0) {
    fatalOut(__FILE__, __func__, "invalid count");
  }
#endif

  // Decrement stack top
  int ctxTop = atomic_fetch_sub(&ctx->top, n) - n;
  ctx->cur = &ctx->stack[ctxTop];

#ifdef DEBUG
  if (ctxTop < 0) {
    fatalOut(__FILE__, __func__, "stack underflow");
  }
#endif
}

void resetSession(void) {
  lockResource(&memoryLock);
  while (memRegistry) {
    removeMemory(memRegistry);
  }
  unlockResource(&memoryLock);
}

long safeValue(double value, long max, const char *ref) {
  if (INI_DEPTH <= value && value <= max) {
    return (long)value;
  }
  char num[NUMBER_MAX_LENGTH + 1];
  prettyNumber(num, max);
  auto_str msg = join4("auto-set to ", num, " ", ref);
  logAlert(__FILE__, __func__, msg);
  return max;
}

double realBytesUsage(void) {
  size_t usage = 0;

  if (!initVisit(__func__)) {
    fatalOut(__FILE__, __func__, MSG_U_REC);
  }
  for (Memory *mem = memRegistry; mem; mem = mem->next) {
    usage += getNodeBytes(mem->data);
  }
  for (int hash = 0; hash < META_HASH; hash++) {
    for (Type *ck = metaSpace[hash]; ck; ck = ck->next) {
      usage += sizeof(Type);
      usage += strlen(ck->name) + 1;
      if (ck->include) {
        for (int i = 0; ck->include[i]; i++) {
          usage += sizeof(Type *);
        }
        usage += sizeof(Type *);  // null termination
      }
    }
  }
  endVisit();

  return (double)atomic_load(&parsedBytes) + (double)usage;
}

void unlockNode(Node *node) {
  if (node && node->meta) {
    node->meta->locked = false;
  }
}

static void trackSource(Node *node, bool follow, Context *ctx) {
  // see extractSwitchOrCases for why we need to check if meta
  // is defined (avoid meta pointing to same node duplicates)
  if (!node || node->meta || node == trueSingleton || node == falseSingleton) {
    return;
  }

  createMemory(node, ctx);
  node->meta->locked = false;  // unlockNode (inlined)

  trackSource(node->head, true, ctx);
  trackSource(node->body, true, ctx);
  trackSource(node->tail, true, ctx);
  if (follow) {
    for (node = node->seq; node; node = node->seq) {
      trackSource(node, false, ctx);
    }
  }
}

void trackParsed(Node *program, size_t bytes, Context *ctx) {
  if (!program) {
    return;
  }

  // Fix stats (deduplicate size count)
  atomic_fetch_sub(&parsedBytes, bytes);

  trackSource(program, true, ctx);
#ifdef DEBUG
  lockResource(&memoryLock);
  // Unlink node from programs linked list (used for leak check)
  if (program == parsedPrograms) {
    parsedPrograms = program->programs;
    unlockResource(&memoryLock);
    return;
  }

  Node *prev = NULL;
  Node *aux = parsedPrograms;
  while (aux && aux != program) {
    prev = aux;
    aux = aux->programs;
  }
  if (!aux) {
    fatalOut(__FILE__, __func__, "twice on same program");
  }

  prev->programs = program->programs;
  unlockResource(&memoryLock);
#endif
}
