/**
 * @file messages.h
 * @brief fry log messages and labels
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.0
 * @date 2024-04-11
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#define MSG_OOM "out of memory"
#define MSG_NOT_LIST "not a list"
#define MSG_MISMATCH "type mismatch"
#define MSG_INDEX "index"
#define MSG_I_IDX "invalid index for "
#define MSG_FOUND "found"
#define MSG_N_FOUND "not found"
#define MSG_BOUNDS "out of bounds"
#define MSG_IS_EMPTY "is empty"
#define MSG_CORRUPTED "memory is corrupted"
#define MSG_BMO "buffer maxed out"
#define MSG_UNSUP "unsupported"
#define MSG_REASSIGN "reassignment to immutable"
#define MSG_U_EOF "unexpected end of file"
#define MSG_NO_SESS "curl session failure"
#define MSG_M_PAR "missing parameter"
#define MSG_M_INC "missing include"
#define MSG_CS "can't store"
#define MSG_M_PATH "missing file path"
#define MSG_I_OP "invalid option"
#define MSG_F_DEC "failed to decode"
#define MSG_N_PTR "null pointer exception"
#define MSG_B_DES "bad destructuring"
#define MSG_NO_METH "no method to apply"
#define MSG_I_COL "invalid color code"
#define MSG_C_LST "can't process lists"
#define MSG_IDR "invalid dynamic reference"
#define MSG_TCF "time conversion failed"
#define MSG_B_COLOR "bad hex color code"
#define MSG_I_CT "invalid composite type"
#define MSG_D_DEF "diverging definition"
#define MSG_TNS "task not started"
#define MSG_TAW "task has already been awaited"
#define MSG_CAST_C "can't cast to composite type"
#define MSG_CAST_N "can't cast null to another type"
#define MSG_I_ARG "invalid argument"
#define MSG_I_N_ARG "invalid numerical argument"
#define MSG_U_REQ "unable to accept requests"
#define MSG_F_MUTEX "failed to initialize mutex"
#define MSG_D_EB_CMD "disabled in this build"
#define MSG_U_EB_CMD "unknown embedded command"
#define MSG_H_OVER "overflow"
#define MSG_H_UNDER "underflow"
#define MSG_H_DIV_Z "division by zero"
#define MSG_H_MOD_Z "modulus by zero"
#define MSG_F_RESP "failed to send response"
#define MSG_BYE "Goodbye\n"
#define MSG_I_IMP "invalid import"
#define MSG_U_REC "unexpected recursion"
#define MSG_W_A_S "worker already started"
#define MSG_W_T_O "worker has timed out"
#define MSG_W_W_C "worker was canceled"
#define MSG_F_F_N "failed to format number"
#define MSG_JAIL "blocked by jail mode"
#define MSG_B_A_A "bad align argument"
