/**
 * @file parameters.h
 * @brief Hardcoded "configurable" parameters
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.4.0
 * @date 2024-02-18
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

// RC_FILE: sets where from should initFry run commands at precook phase
#define RC_FILE ".fryrc"  // (filename)

// REPL_FILE: sets where should fry save session commands (for -s option)
#define REPL_FILE "repl.fat"  // (filename)

// DEFAULT_MEM: sets initial memory limit, can be overridden by CLI options
#define DEFAULT_MEM 10000000L  // (node count) recommended range [2m~20m]

// INI_DEPTH: sets the initial value of stack allocation
// DEF_DEPTH: default max size, sets "recursive call" limit (approx.)
// MAX_DEPTH: hard limit for stack setting (via CLI options)
#define INI_DEPTH 32    // (frames) <= DEF_DEPTH
#define DEF_DEPTH 896   // (frames) can be overridden by CLI options
#define MAX_DEPTH 8192  // (frames) >= DEF_DEPTH

// STACK_TRACE: sets how many lines of stack trace to print on errors
#define STACK_TRACE 16  // (lines) set as many as you wish

// MIN_QUICK_REF: sets the minimum size of a scope where quick refs will be
// used to balance scope nodes access at 1/3 and 2/3 of collection indexes
#define MIN_QUICK_REF 6

// SKIP_SIZE: defines a skip-list for better search performance, trade-off:
// - smaller [8~16] -> good for small scopes, but horrible for big scopes
// - medium [24~48] -> balanced performance gains for variated scope sizes
// - bigger [64~96] -> bad for small scopes, but minor gains for big ones
// 32 was kind of sweet spot for scopes up to 10k items lookup on my laptop
#define SKIP_SIZE 32  // (items) jump size in linked list during lookup

// SKIP_LIST_MAX: defines the maximum amount of list items to skip at once
// note: at 1024-skip-step, seek time balances out a bigger jump based on
// benchmarking I did on my personal computer...
// (this will most likely have to do with page misses and CPU caching)
#define SKIP_LIST_MAX 1024  // (items) skip-list chunk maximum size

// META_HASH: all types are stored in a single hash-table, the smaller the
// size the more collisions you may have, the bigger the size, more memory
// is used for book keeping, it depends on how many types you are defining,
// ideally value should be bigger than the number of types your project has
#define META_HASH 389  // prime: https://planetmath.org/goodhashtableprimes

// SCOPE_HASH and SCOPE_BLOCK: is a matrix compound of hashed-index buckets,
// scope visit/tracking is used by "GC", "ast loggers" and "bytes usage"...
// defines the total number of scopes that can be instantiated by any program
// the buckets are increased as needed always in multiples of SCOPE_BLOCK
#define SCOPE_HASH 6151  // prime: https://planetmath.org/goodhashtableprimes
#define SCOPE_BLOCK 32   // (pointers) base to avoid realloc for simple uses

// FIND_SCP_DEDUP size of scopes cache to de-duplicate on lexical scope search
#define FIND_SCP_DEDUP 6  // (pointers) recommended [2~12]

// GC_PREMONITION: allows GC to kick in before final memory limit is reached,
// this is specially necessary for enabling interpolation or node evaluation
// with a constrained amount of node allowance, if you are parsing big chunks
// via eval, you can try to increase this, to avoid some random OOM errors
// a higher value could also help prevent async operation thread bottle neck
#define GC_PREMONITION 256  // (node count) before memory limit is reached

// GC_THREAD_LAG: hold thread for milliseconds if memory bottle necking
#define GC_THREAD_LAG 10  // milliseconds

// MAX_CONTEXT_MEM: maximum amount of nodes that can be temporarily hidden from
// garbage collector inside a thread context (improves parallelism performance)
// it also roughly means "maximum amount of computations before lock and sync"
// a higher value may reduce the thread contention, but there are other factors
// note: premonition/max = number of threads that may endure memory starvation
#define MAX_CONTEXT_MEM 64  // (node count) a divisor of GC premonition

// TYPE_MAX: prevents inclusions abuse, 'cause fry is interpreted and type
// resolution can get VERY expensive at runtime! that is why compiled languages
// have the upper-hand on performance, if you want to make it more go for it...
// see also: canResolveViaProto at chaining.c and storType at scopes.c
#define TYPE_MAX 10  // (pointers) be warned, performance degradation risk

// IND_MAX: controls the default number of columns indentation can grow, when
// printing ast and logs, sets how long lines can be printed until ellipsis...
// (the value 39 assumes we could use up to full width of 80 columns terminal)
// see also: initFry at fry.c where indMax uses IND_MAX as fallback
#define IND_MAX 39  // (columns) ideally half of console width - 1

// INDENT: defines both AST print and fry's formatter (-f) indentation size
#define INDENT 2  // (columns) recommended range [2~4]

// ARGS_MAX_STYLE: alerts when too many args are used on a method signature
// LOCAL_MAX_STYLE: alerts when method local entries exceed a sensible amount
#define ARG_STYLE_MAX 5     // (pointers) it's just as a guideline reference
#define LOCAL_STYLE_MAX 10  // (pointers) it's just as a guideline reference

// DEFAULT_DATE_FMT: the default date format used by the embedded time methods
#define DEFAULT_DATE_FMT "%Y-%m-%d %H:%M:%S"  // see: man strftime

// INIT_B_LEN: initial buffer length for dynamically allocated text buffers
#define INIT_B_LEN 256  // (bytes) recommended range [128~1024]

// BUFF_LEN: sets the maximum length that a text can have in some cases:
// formatted date output, interpolation result, console.input, REPL input...
// this also sets the maximum size of read buffer on http.listen (server mode)
#define BUFF_LEN 16384  // (bytes) recommended range [4096~32768]

// ENTRY_NAME_MAX: sets the maximum length that a key may have unless dynamic
#define ENTRY_NAME_MAX 128

// DIR_LIST_MAX: do you have that many files in a directory? you messy
#define DIR_LIST_MAX 4096  // (pointers) recommended value [1024~8192]

// BUN_STACK and IMPORT_MAX: max scopes of imports, used only when bundling
#define BUN_STACK 1024   // (pointers) max layers, see: pushImportScope
#define IMPORT_MAX 1024  // (pointers) per layer, used by addImport

// SALT_SIZE and PEPPER_MAX: used for obfuscation, used when bundling
#define SALT_SIZE 24   // (bytes) recommended range [16-64]
#define PEPPER_MAX 24  // (bytes) recommended range [16-128]

// DERIVED_SIZE used for password hashing via deriveKey embedded command
#define DERIVED_SIZE 32  // (bytes) must be divisible by 4

// MAX_TO_INT max safe value for double to integer conversion (n < 2^53)
#define MAX_TO_INT 9007199254740992.0
