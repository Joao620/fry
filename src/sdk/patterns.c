/**
 * @file patterns.c
 * @brief Utilities for the lexer
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.4.1
 * @date 2024-03-14
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "sdk.h"

bool isSpace(const char ch) { return strchr(" \r\t\v\f", ch); }

bool isPunctuation(const char ch) { return strchr(":,;\n(){}[]", ch); }

bool isOperator(const char ch) { return strchr(".+-*/%=&|<>!?@~", ch); }

// ctype isdigit is broken on some platforms, don't use it
bool isDigit(const char ch) { return '0' <= ch && ch <= '9'; }

bool isHex(const char ch) {
  return ('0' <= ch && ch <= '9') || ('a' <= ch && ch <= 'f') ||
         ('A' <= ch && ch <= 'F');
}

bool isTempStart(const char ch) { return ch == MARK_TEM; }

bool isRawStart(const char ch) { return ch == MARK_RAW; }

bool isCommentStart(const char ch) { return ch == MARK_COM; }

bool isOneLine(const char ch) { return ch && ch != '\n'; }

bool isUnder(const char ch) { return ch == MARK_UND; }

// ctype isalnum is broken on some platforms, trust me on this
bool isIdentifier(const char ch) {
  return isalpha(ch) || isDigit(ch) ||
         // not comment, space, punct, op or text delimiter
         !strchr("# \n\r\t\v\f:,;(){}[].+-*/%=&|<>!?@~'\"", ch);
}

bool isEmbedded(const char ch) { return ch == MARK_EBD; }

bool isNumeric(const char ch, bool *hasDot, bool *isExpo) {
  if (*isExpo) {
    *isExpo = false;
    return ch == '+' || ch == '-';
  }
  if (*hasDot && !*isExpo && (ch == 'e' || ch == 'E')) {
    *isExpo = true;
    return true;
  }
  if (ch == '.') {
    if (*hasDot) {
      return false;
    }
    *hasDot = true;
    return true;
  }
  return isDigit(ch);
}

bool hasBracket(const char ch, int *opened) {
  if (ch == '{') {
    *opened += 1;
  } else if (ch == '}') {
    *opened -= 1;
  }
  return *opened != 0;
}

bool isHugeHex(const char *hex) {
  return hex[0] == '0' && hex[1] == 'x' && isHex(hex[2]);
}

bool isTypename(const char *word) { return isupper(word[0]); }

bool isKeyword(const char *word) {
  return strcmp(word, "null") == 0 || strcmp(word, "false") == 0 ||
         strcmp(word, "true") == 0 || strcmp(word, "infinity") == 0;

  // Note: However 'self' and 'root' are also keywords they are handled
  // at parser abstraction level and not by the lexer itself. Those are
  // special keywords because they are not primitive values, but macros
  // that are lifted into FatScript's embedded command $self and $root.
}

size_t readWhile(bool (*const pattern)(char), const char *input) {
  size_t len = 0;
  while (input[len] && pattern(input[len])) {
    len++;
  }
  return len;
}

bool isPunctTok(const Token *tok, const char *val) {
  return tok->type == TkPunct && strcmp(tok->val, val) == 0;
}

bool isPunctEol(const Token *tok) {
  return isPunctTok(tok, "\n") || isPunctTok(tok, ";");
}

bool isPunctSep(const Token *tok) {
  return isPunctEol(tok) || isPunctTok(tok, ",");
}

bool isCommTok(const Token *tok) { return tok->type == TkComm; }

bool isOpTok(const Token *tok, const char *val) {
  return tok->type == TkOp && strcmp(tok->val, val) == 0;
}

bool isKeyTok(const Token *tok, const char *val) {
  return tok->type == TkKey && strcmp(tok->val, val) == 0;
}

size_t skipPattern(bool (*const pattern)(char), Reader *reader) {
  size_t len = 0;
  char *source = reader->source + reader->pos;
  while (*source && pattern(*source)) {
    advanceChar(reader);
    source++;
    len++;
  }
  return len;
}

char *readPattern(bool (*const pattern)(char), Reader *reader) {
  const char *source = reader->source + reader->pos;
  return copyFragment(source, skipPattern(pattern, reader));
}
