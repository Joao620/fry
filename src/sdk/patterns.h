/**
 * @file patterns.h
 * @brief Utilities for the lexer
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.1.0
 * @date 2024-01-20
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "structures.h"
#include "sugar.h"

// token markers
#define MARK_COM '#'     // comment
#define MARK_TEM '\''    // text template
#define MARK_RAW '"'     // raw text
#define MARK_ESC '\\'    // escape sequence
#define MARK_EBD '$'     // embedded
#define MARK_UND '_'     // magic entry
#define MARK_ENC "~!@$"  // encoded

/**
 * @brief Check if char belongs to pattern...
 *
 * @param ch char
 * @return boolean
 */
bool isSpace(char ch);
bool isPunctuation(char ch);
bool isOperator(char ch);
bool isDigit(char ch);
bool isHex(char ch);
bool isTempStart(char ch);
bool isRawStart(char ch);
bool isCommentStart(char ch);
bool isOneLine(char ch);
bool isUnder(char ch);
bool isIdentifier(char ch);
bool isEmbedded(char ch);

/**
 * @brief Helps check if digits (including dot) is a number
 *
 * @param ch char (current)
 * @param hasDot state while scanning
 * @param isExpo is parsing an exponent + or -
 * @return boolean
 */
bool isNumeric(char ch, bool *hasDot, bool *isExpo);

/**
 * @brief Helps check all brackets were closed
 *
 * @param ch char (current)
 * @param opened count while scanning (passed by reference)
 * @return boolean
 */
bool hasBracket(char ch, int *opened);

/**
 * @brief Check if hex represents a hex number as string
 *
 * @param hex string
 * @return boolean
 */
bool isHugeHex(const char *hex);

/**
 * @brief Check if word respects Type casing
 *
 * @param word string
 * @return boolean
 */
bool isTypename(const char *word);

/**
 * @brief Check if word is a keyword (null, false or true)
 *
 * @param word string
 * @return boolean
 */
bool isKeyword(const char *word);

/**
 * @brief Return length until when a pattern is valid
 *
 * @param pattern (function pointer)
 * @param input string
 * @return size_t
 */
size_t readWhile(bool (*pattern)(char), const char *input);

/**
 * @brief Check if token is a specific punctuation symbol
 *
 * @param tok Token*
 * @param val string (punctuation symbol)
 * @return boolean
 */
bool isPunctTok(const Token *tok, const char *val);

/**
 * @brief Check if token is end of line
 *
 * @param tok Token*
 * @return boolean
 */
bool isPunctEol(const Token *tok);

/**
 * @brief Check if token is separator (eol + commas)
 *
 * @param tok Token*
 * @return boolean
 */
bool isPunctSep(const Token *tok);

/**
 * @brief Check if token is comment
 *
 * @param tok Token*
 * @return boolean
 */
bool isCommTok(const Token *tok);

/**
 * @brief Check if token is a specific operation symbol
 *
 * @param tok Token*
 * @param val string (operation symbol)
 * @return boolean
 */
bool isOpTok(const Token *tok, const char *val);

/**
 * @brief Check if token is a specific keyword
 *
 * @param tok Token*
 * @param val string (keyword)
 * @return boolean
 */
bool isKeyTok(const Token *tok, const char *val);

/**
 * @brief Advance reader where a pattern is valid (return fragment size)
 *
 * @param pattern function pointer
 * @param reader Reader* to read from
 * @return size_t
 */
size_t skipPattern(bool (*pattern)(char), Reader *reader);

/**
 * @brief Return contents where a pattern is valid
 *
 * @param pattern function pointer
 * @param reader Reader* to read from
 * @return string
 */
char *readPattern(bool (*pattern)(char), Reader *reader);
