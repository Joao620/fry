/**
 * @file rle.h
 * @brief Flat run-length-encoded format
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.1
 * @date 2024-05-09
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "rle.h"

/**
 * Return uncompressed data, flagged
 */
static size_t rleFlat(const char *input, char *output, size_t size) {
  *output = 0;  // flag indicating uncompressed data
  memcpy(output + 1, input, size);
  return size + 1;
}

/**
 * Estimate maximum RLE compressed size (adding 1 byte per chunk)
 */
static size_t rleMaxSize(size_t size) {
  return 1 + (size / RLE_CHUNK_SIZE) + size;
}

/**
 * Get number of times a pattern repeats sequentially on input
 */
static unsigned char repeatsNbytes(const char *input, size_t size, size_t n) {
  // Ensure the input is at least twice as long as the pattern length
  if (size < 2 * n) {
    return 0;
  }

  // Check if the first 'n' bytes are repeated
  if (memcmp(input, input + n, n) != 0) {
    return 0;  // no immediate repetition
  }

  unsigned char repeatCount = 1;  // we already know the pattern repeats once

  // Check for further repetitions of the first 'n' bytes
  for (size_t i = 2 * n; i <= size - n; i += n) {
    if (memcmp(input, input + i, n) == 0) {
      repeatCount++;
      if (repeatCount == RLE_ENCODE) {  // handle maximum count for pattern
        break;
      }
    } else {
      break;  // stop when a non-repeating segment is found
    }
  }

  return repeatCount;
}

/**
 * Compress with RLE schema (or flag and store uncompressed)
 * Note: output buffer must be pre-allocated with size + 1
 */
static size_t rleCompressChunk(const char *input, char *output, size_t size) {
  size_t outPos = 0;
  size_t lenPos = 0;

  for (size_t i = 0; i < size; i++) {
    // Count occurrences of 1-4 bytes repeated pattern (if any)

    unsigned char repeated = 0;
    size_t remaining = size - i;

    for (size_t n = 1; n <= (128 / RLE_ENCODE); n++) {
      repeated = repeatsNbytes(input + i, remaining, n);

      if (repeated) {
        if (outPos >= size - (n + 1)) {
          return rleFlat(input, output, size);
        }

        output[outPos++] =
            (unsigned char)(127 + (RLE_ENCODE * (n - 1)) + repeated);
        memcpy(output + outPos, input + i, n);

        i += ((repeated + 1) * n) - 1;
        outPos += n;
        lenPos = outPos;
        break;
      }
    }

    if (repeated) {
      continue;
    }

    // ...else, count non-repeated input sequence (1-127 bytes)

    if (outPos == lenPos) {
      outPos++;
    }

    if (outPos >= size - 1) {
      return rleFlat(input, output, size);
    }

    unsigned char chunkSize = (unsigned char)(outPos - lenPos);

    output[lenPos] = chunkSize;  // update sequence size
    output[outPos++] = input[i];

    if (chunkSize == 127) {
      lenPos = outPos;  // start new sequence
    }
  }

  return outPos;
}

char *rleCompress(const char *input, size_t size, size_t *compSize) {
  if (size == 0) {
    *compSize = 0;
    return NULL;
  }

  // Allocate memory for the compressed data
  char *output = FRY_ALLOC(rleMaxSize(size));

  if (size < 3) {
    *compSize = rleFlat(input, output, size);
    return output;
  }

  size_t inPos = 0;
  size_t outPos = 0;
  size_t remaining = size;

  do {
    size_t chunkSize = remaining > RLE_CHUNK_SIZE ? RLE_CHUNK_SIZE : remaining;
    outPos += rleCompressChunk(input + inPos, output + outPos, chunkSize);
    inPos += chunkSize;
    remaining -= chunkSize;
  } while (remaining);

  *compSize = outPos;
  return FRY_REALLOC(output, outPos);
}

char *rleDecompress(const char *compressed, size_t size, size_t *decoSize) {
  if (size == 0 || size == 1) {
    *decoSize = size;
    return NULL;
  }

  size_t reservedSize = size * 2;  // heuristic size :P

  char *decompressed = FRY_ALLOC(reservedSize);

  size_t outPos = 0;
  size_t chunkSize = 0;
  for (size_t i = 0; i < size; i += chunkSize + 1) {
    chunkSize = (unsigned char)compressed[i];
    size_t inPos = i + 1;

    // Handle non-run-length-encoded sequence
    if (chunkSize <= 127) {
      // Handle non-compressed chunk
      if (chunkSize == 0) {
        size_t remaining = size - i - 1;
        chunkSize = remaining > RLE_CHUNK_SIZE ? RLE_CHUNK_SIZE : remaining;
      }

      // Bail out before buffer overrun
      if (inPos + chunkSize > size) {
        free(decompressed);
        *decoSize = inPos;  // indicating failure position
        return NULL;
      }

      size_t newSize = outPos + chunkSize;

      if (newSize > reservedSize) {
        char *decompressedR = FRY_REALLOC(decompressed, newSize);
        decompressed = decompressedR;
      }

      memcpy(decompressed + outPos, compressed + inPos, chunkSize);
      outPos += chunkSize;
      continue;
    }

    // Handle run-length-encoded sequence
    size_t n = 1 + (chunkSize - 128) / RLE_ENCODE;

    // Bail out before buffer overrun
    if (inPos + n > size) {
      free(decompressed);
      *decoSize = inPos;  // indicating failure position
      return NULL;
    }

    size_t runLength = chunkSize - 127 - (RLE_ENCODE * (n - 1)) + 1;
    size_t newSize = outPos + (runLength * n);

    if (newSize > reservedSize) {
      char *decompressedR = FRY_REALLOC(decompressed, newSize);
      decompressed = decompressedR;
    }

    if (n == 1) {
      unsigned char c = (unsigned char)compressed[inPos];
      memset(decompressed + outPos, c, runLength);
      outPos += runLength;
    } else {
      for (size_t j = 0; j < runLength; j++) {
        memcpy(decompressed + outPos, compressed + inPos, n);
        outPos += n;
      }
    }

    chunkSize = n;  // ensure that the loop advances correctly
  }

  *decoSize = outPos;
  if (outPos) {
    return FRY_REALLOC(decompressed, outPos);
  }

  // Should never happen
  free(decompressed);
  return NULL;
}
