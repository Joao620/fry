/**
 * @file sdk.c
 * @brief fry development toolkit
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-30
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "sdk.h"

char *bundleKey = NULL;
bool crashOnError = true;
bool hasFoundErrors = false;
bool configOnce = false;
bool cursesMode = false;
bool isAstOnly = false;
Scope *globalScope = NULL;
char *currentLocale = NULL;
char *basePath = NULL;
bool isUtf8Mode = false;
int stackDepth = DEF_DEPTH;
int maxStackUsed = 0;

Type *metaSpace[META_HASH] = {0};
Type *ckTypeType = NULL;
Type *checkVoid = NULL;
Type *checkBoolean = NULL;
Type *checkNumber = NULL;
Type *checkHugeInt = NULL;
Type *checkChunk = NULL;
Type *checkText = NULL;
Type *checkList = NULL;
Type *checkScope = NULL;
Type *checkMethod = NULL;
Type *checkError = NULL;

Type *ckEpoch = NULL;
Type *ckExitCode = NULL;

Type *ckAssignError = NULL;
Type *ckAsyncError = NULL;
Type *ckCallError = NULL;
Type *ckFileError = NULL;
Type *ckIndexError = NULL;
Type *ckKeyError = NULL;
Type *ckSyntaxError = NULL;
Type *ckTypeError = NULL;
Type *ckValueError = NULL;

// initialize boolean singletons for runtime use
static Node truthNode = {.type = FatBoolean, .src = SRC_RUN, .num.b = true};
static Node falseNode = {.type = FatBoolean, .src = SRC_RUN, .num.b = false};
Node *trueSingleton = &truthNode;
Node *falseSingleton = &falseNode;

Scope **visited[SCOPE_HASH] = {0};
int visitCap[SCOPE_HASH];
static char *visitingFunc = NULL;

pthread_t mainThreadId;

#ifdef SSL_SUPPORT
SSL_CTX *sslCtx = NULL;
#endif

const char *getFryV(void) {
  static const char *version = FRY_VERSION;
  return version[0] == 'v' ? &version[1] : version;
}

void printVersion(VersionMode mode) {
  if (mode == VerMinimal) {
    puts(getFryV());
    return;
  }

  if (mode == VerDebugger) {
    stdoutLn("FRY " FRY_VERSION PLATFORM ARCHITECTURE " - FatScript Debugger\n",
             CL_GRA);
    return;
  }

  puts("FRY " FRY_VERSION PLATFORM ARCHITECTURE " - FatScript Interpreter\n");

  // ...and maybe additional info bellow:
  if (mode == VerWarranty) {
    puts(
        "Copyright (c) 2022-2024, Antonio Prates.\n"
        "fatscript/fry project <https://gitlab.com/fatscript/fry>.\n\n"
        "This program is free software: you can redistribute it and/or "
        "modify"
        "\nit under the terms of the GNU General Public License as "
        "published "
        "by\nthe Free Software Foundation, version 3.\n\n"
        "This program is distributed in the hope that it will be useful, but"
        "\nWITHOUT ANY WARRANTY; without even the implied warranty of\n"
        "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU\n"
        "General Public License for more details.\n\n"
        "You should have received a copy of the GNU General Public License\n"
        "along with this program. If not, see "
        "<http://www.gnu.org/licenses/>.\n"
#ifndef __EMSCRIPTEN__
#ifndef USE_READLINE
        "\n"
        "Additional Notices:\n"
        "This program includes a modified version of the linenoise library,\n"
        "which is covered under its own BSD-style license:\n"
        "<raw.githubusercontent.com/fatscript/linenoise/utf8-support/"
        "LICENSE>\n"
#endif
#endif
    );
  }
}

static inline char *handleInfinity(char *buff, const double f) {
  const char *inf = f > 0 ? "infinity" : "-infinity";
  const int ret = buff ? sprintf(buff, "%s", inf) : asprintf(&buff, "%s", inf);
  if (ret == -1) {
    fatalOut(__FILE__, __func__, MSG_F_F_N);
    return NULL;  // mitigate false positive on auto_check.sh
  }
  return buff;
}

char *prettyNumber(char *buff, const double f) {
  if (isinf(f)) {
    return handleInfinity(buff, f);
  }

  char *result = ofFloat(buff, f);

  if (f > NUMBER_INT_ONLY) {
    // replace random mantissa with zeros
    char *mantissa = &result[16];
    for (int i = 0; mantissa[i]; i++) {
      mantissa[i] = '0';
    }
    return result;
  }

  int zeros = 0;
  int nines = 0;
  bool onlyZeros = true;

  for (size_t i = strlen(result) - 1, dec = 0; i > 0; i--, dec++) {
    char digit = result[i];

    // Exit on integer part
    if (digit == '.') {
      if (zeros > 5) {
        result[i] = '\0';
      } else if (nines > 6) {
        result[i + 7] = '\0';
      }
      break;
    }

    // Try to reconstruct representation with some "artistic" freedom
    if (digit == '0') {
      if (onlyZeros || zeros > 6 || (dec < 6 && zeros > 1)) {
        result[i] = '\0';  // trim
      }
      zeros++;
      nines = 0;
    } else {
      onlyZeros = false;
      if (digit == '9') {
        if (nines > 6) {
          result[i + 7] = '\0';  // trim with 6 trailing 9s
        } else if (dec < 6 && nines > 2) {
          result[i + 3] = '\0';  // trim with 2 trailing 9s
        }
        nines++;
        zeros = 0;
      } else if (nines > 5 || (dec < 6 && nines > 1)) {
        result[i + 1] = '\0';  // trim at previous digit
        result[i] += 1;        // round up current digit
        nines = result[i] == '9';
        zeros = 0;
      } else {
        zeros = nines = 0;
      }
    }
  }

  // Ensure no trailing zeroes after decimal point
  char *decimalPoint = strchr(result, '.');
  if (!decimalPoint) {
    return result;
  }

  char *end = decimalPoint + strlen(decimalPoint) - 1;
  while (end > decimalPoint && *end == '0') {
    *end = '\0';
    end--;
  }
  if (end == decimalPoint) {
    *end = '\0';
  }
  return result;
}

bool hasErrorHandler(Context *ctx) {
  for (int i = atomic_load(&ctx->top); i; i--) {
    if (ctx->stack[i].trap) {
      return true;
    }
  }
  return false;
}

Node *createError(char *msg, bool copy, Type *ck, Context *ctx) {
#ifdef DEBUG
  if (!msg || !ctx) {
    fatalOut(__FILE__, __func__, "malformed error");
    exit(EXIT_FAILURE);
  }
#endif

  char *src = ctx->cur->node ? cpSrc(ctx->cur->node) : SRC_RUN;

  if (debugLogs) {
    logDebug2(__FILE__, __func__, src, msg);
  }

  Node *error = createNode(FatError, src, ctx);
  error->ck = ck ? ck : checkError;
  error->val = copy ? strDup(msg) : msg;

  if (!pthread_equal(ctx->id, mainThreadId)) {
    return error;
  }

  if (ctx->isHandlingFailure) {
    logError(__FILE__, __func__, msg);
    logStack(ctx, STACK_TRACE);
    msg = "an error ocurred while handling a failure";
    fatalOut(__FILE__, __func__, msg);
  } else if (hasErrorHandler(ctx)) {
    if (debugLogs) {
      logDebug(__FILE__, __func__, "failure event created");
    }
    ctx->failureEvent = error;
  } else if (crashOnError) {
    endCursesMode();
    stderrStartLine(CL_RED);
    fprintf(stderr, MRG_ERR "%s: %s, at %s", error->ck->name, msg, error->src);
    stderrEndLine();
    logStack(ctx, STACK_TRACE);
    exitFry(EXIT_FAILURE);
  }

  return error;
}

long adaptiveSkipMod(long index) {
  return index <= 16     ? index % 8
         : index <= 32   ? index % 16
         : index <= 64   ? index % 32
         : index <= 128  ? index % 64
         : index <= 256  ? index % 128
         : index <= 512  ? index % 256
         : index <= 1024 ? index % 512
                         : index % SKIP_LIST_MAX;
}

char *toString(const Node *node) {
  NodeType type = node ? node->type : FatVoid;

  switch (type) {
    case FatVoid:
      return strDup("null");

    case FatBoolean:
      return strDup(ofBool(node->num.b));

    case FatNumber:
      return prettyNumber(NULL, node->num.f);

    case FatChunk:
      return strDup("(101010)");  // the Answer to the Ultimate Question :P

    case FatHugeInt:
      return unparseHugeInt(*node->num.h, false);

    case FatText:
    case FatTemp:
      return copyFragment(node->val, node->num.s);

    case FatError:
      return join3(node->ck ? node->ck->name : "Error", ": ",
                   node->val ? node->val : "unkown");

    case FatScope:
      if (node->scp && node->scp->ck) {
        return strDup(node->scp->ck->name);
      }
      FALL_THROUGH;

    default:
      return strDup(fatType(node->type));
  }
}

char *unparseHugeInt(const HugeInt num, bool prefix) {
  char *buffer = FRY_ALLOC(HUGE_INT_SIZE * HUGE_FRAG + 2 + 1);  // n+"0x"+'\n'

  char *current = buffer;
  if (prefix) {
    current += snprintf(current, 3, "0x");
  }

  bool hasPrintedFirstGroup = false;
  for (int i = HUGE_INT_SIZE - 1; i >= 0; i--) {
    if (hasPrintedFirstGroup) {
      current += snprintf(current, HUGE_FRAG + 1, "%08x", num[i]);
    } else if (num[i]) {
      current += snprintf(current, HUGE_FRAG + 1, "%x", num[i]);
      hasPrintedFirstGroup = true;
    } else {
      // skip leading zeroes
    }
  }

  // In case the number is 0
  if (!hasPrintedFirstGroup) {
    current += snprintf(current, 2, "0");
  }

  return FRY_REALLOC(buffer, current - buffer + 1);
}

// see also: readText at lexer.c
char *unparseText(const char *text, bool isRaw) {
  char *buffer = FRY_ALLOC(INIT_B_LEN);

  size_t bufferSize = INIT_B_LEN;
  const char delimiter = isRaw ? MARK_RAW : MARK_TEM;

  buffer[0] = delimiter;  // add opening delimiter
  size_t pos = 1;

  for (size_t i = 0; text[i]; i++) {
    if (pos >= bufferSize - 6) {  // account for octal + delimiter + \0
      bufferSize *= 2;
      buffer = FRY_REALLOC(buffer, bufferSize);
    }

    if (text[i] == '\\' && (isRaw || text[i + 1] != '{')) {
      buffer[pos++] = '\\';
      buffer[pos++] = '\\';
    } else if (text[i] == delimiter) {
      buffer[pos++] = '\\';
      buffer[pos++] = delimiter;
    } else if (text[i] == '\b') {
      buffer[pos++] = '\\';
      buffer[pos++] = 'b';
    } else if (text[i] == '\f') {
      buffer[pos++] = '\\';
      buffer[pos++] = 'f';
    } else if (text[i] == '\n') {
      buffer[pos++] = '\\';
      buffer[pos++] = 'n';
    } else if (text[i] == '\r') {
      buffer[pos++] = '\\';
      buffer[pos++] = 'r';
    } else if (text[i] == '\t') {
      buffer[pos++] = '\\';
      buffer[pos++] = 't';
    } else if (text[i] == '\033') {
      buffer[pos++] = '\\';
      buffer[pos++] = 'e';
    } else if (iscntrl(text[i])) {
      pos += snprintf(&buffer[pos], BUFF_LEN - pos, "\\%03o", (Byte)text[i]);
    } else {
      buffer[pos++] = text[i];
    }
  }
  buffer[pos++] = delimiter;
  buffer[pos++] = '\0';

  return FRY_REALLOC(buffer, pos);
}

char *joinListItems(Scope *list, const char *sep, char *strFn(const Node *)) {
  // return empty string for no contents
  if (!list || list->size <= 0) {
    return strDup("");
  }

  char **strs = FRY_ALLOC((list->size + 1) * sizeof(char *));

  long i = 0;
  for (Entry *entry = list->entries; entry; entry = entry->next) {
    strs[i++] = strFn(entry->data);
  }
  strs[i] = NULL;  // null-terminate list

  char *result = joinSep(strs, sep);

  // clean up auxiliary list of strings
  freeStrList(strs);

  return result;
}

static char *toJsonList(Scope *list) {
  auto_str aux = joinListItems(list, ",", &toJson);
  char *result = join3("[", aux, "]");
  return result;
}

static char *toJsonScope(Scope *scope) {
  if (!scope || scope->size <= 0) {
    return strDup("{}");
  }

  char **strs = FRY_ALLOC((scope->size + 1) * sizeof(char *));

  long i = 0;
  for (Entry *entry = scope->entries; entry; entry = entry->next) {
    char *key = unparseText(entry->key.s, true);
    char *val = toJson(entry->data);
    strs[i++] = join3(key, ":", val);
    free(key);
    free(val);
  }
  strs[i] = NULL;  // null-terminate list

  auto_str aux = joinSep(strs, ",");
  char *result = join3("{", aux, "}");

  // clean up
  freeStrList(strs);

  return result;
}

static inline char *toJsonError(const Node *error) {
  auto_str aux = toString(error);
  char *result = unparseText(aux, true);
  return result;
}

char *toJson(const Node *node) {
  NodeType type = node ? node->type : FatVoid;

  switch (type) {
    case FatBoolean:
      return strDup(ofBool(node->num.b));

    case FatNumber:
      return prettyNumber(NULL, node->num.f);

    case FatHugeInt:
      return unparseHugeInt(*node->num.h, true);

    case FatText:
    case FatTemp:
      return unparseText(node->val, true);

    case FatList:
      return toJsonList(node->scp);

    case FatScope:
      return toJsonScope(node->scp);

    case FatError:
      return toJsonError(node);

    default:
      return strDup("null");
  }
}

void initLayer(Scope *layer) {
#ifdef DEBUG
  if (!layer) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
  }
#endif

  // Init Scope
  if (pthread_mutex_init(&layer->lock, &recursiveMutex)) {
    fatalOut(__FILE__, __func__, MSG_F_MUTEX);
  }
  atomic_init(&layer->refs, 0);
  layer->isLayer = true;
}

// Define a function pointer type for freeing entries
typedef void (*FreeEntryFunc)(Entry *);

void wipeScope(Scope *scope, const Entry *keep) {
#ifdef DEBUG
  if (!scope) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
#endif

  // Determine the appropriate free function to use
  FreeEntryFunc freeFunc = scope->isList ? freeListEntry : freeScopeEntry;

  Entry *garbage = scope->entries;
  while (garbage) {
    Entry *next = garbage->next;
    if (garbage != keep) {
      freeFunc(garbage);
    } else {
      unlockNode(garbage->data);
    }
    garbage = next;
  }

  pthread_mutex_destroy(&scope->lock);
}

Node *runtimeNumber(double num, Context *ctx) {
  Node *result = createNode(FatNumber, SRC_RUN, ctx);
  result->num.f = num;
  result->ck = checkNumber;
  return result;
}

Node *runtimeHuge(HugeInt num, Context *ctx) {
  Node *result = createNode(FatHugeInt, SRC_RUN, ctx);
  result->num.h = copyHuge(num);
  result->ck = checkHugeInt;
  return result;
}

Node *runtimeChunk(char *val, size_t len, Context *ctx) {
  Node *result = createNode(FatChunk, SRC_RUN, ctx);
  result->num.s = len;
  result->val = val;
  result->ck = checkChunk;
  return result;
}

Node *runtimeText(char *val, size_t len, Context *ctx) {
#ifdef DEBUG
  if (!val) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
    return NULL;
  }
#endif

  Node *result = createNode(FatText, SRC_RUN, ctx);
  result->num.s = len;
  result->val = val;
  result->ck = checkText;
  return result;
}

Node *runtimeTextDup(const char *val, Context *ctx) {
  if (!val) {
    return NULL;
  }

  size_t len = strlen(val);

  Node *result = createNode(FatText, SRC_RUN, ctx);
  result->num.s = len;
  result->val = copyFragment(val, len);
  result->ck = checkText;
  return result;
}

Node *runtimeCollection(Scope *scope, Context *ctx) {
  if (!scope) {
    return NULL;
  }

  Node *result = NULL;
  if (scope->isList) {
    result = createNode(FatList, SRC_RUN, ctx);
    result->ck = checkList;
  } else {
    result = createNode(FatScope, SRC_RUN, ctx);
    result->ck = checkScope;
  }
  bindScope(result, scope);
  return result;
}

static bool isHugeZero(HugeInt num) {
  const HugeInt hugeZero = {0};
  return hugeEq(num, hugeZero);
}

bool booleanOf(Node *node) {
  if (!node) {
    return false;
  }

  switch (node->type) {
    case FatVoid:
      return false;

    case FatBoolean:
      return node->num.b;

    case FatNumber:
      return !floatEq(node->num.f, 0);

    case FatHugeInt:
      return !isHugeZero(*node->num.h);

    case FatChunk:
    case FatText:
      return node->num.s != 0;

    case FatMethod:
    case FatType:
      return true;

    default:
      if (node->scp && node->scp->size) {
        return true;
      }
      return false;
  }
}

static inline bool nodeNumEq(Node *a, Node *b) {
  switch (a->type) {
    case FatBoolean:
      return a->num.b == b->num.b;

    case FatNumber:
      return floatEq(a->num.f, b->num.f);

    case FatHugeInt:
      return hugeEq(*a->num.h, *b->num.h);

    case FatText:
    case FatTemp:
    case FatChunk:
      return a->num.s == b->num.s;

    default:
      return true;
  }
}

static inline bool nodeValEq(Node *a, Node *b) {
  if (a->type == FatChunk) {
    return memcmp(a->val, b->val, a->num.s) == 0;
  }
  return strEq(a->val, b->val);
}

bool nodeEq(Node *a, Node *b) {
  // Perform pointer shallow check
  if (a == b) {
    return true;
  }
  if (!a || !b) {
    return false;
  }

  // Perform inexpensive data check
  if (a->type != b->type || !nodeNumEq(a, b) || !nodeValEq(a, b)) {
    return false;
  }

  // Perform recursive deep check
  return nodeEq(a->head, b->head) && nodeEq(a->body, b->body) &&
         nodeEq(a->tail, b->tail) && scopeEq(a->scp, b->scp);
}

bool scopeEq(Scope *a, Scope *b) {
  // Perform pointer shallow check
  if (a == b) {
    return true;
  }
  if (!a || !b) {
    return false;
  }
  if (a->entries == b->entries) {
    return true;
  }
  if (!a->entries || !b->entries) {
    return false;
  }

  // Perform inexpensive size check
  if (a->size != b->size) {
    return false;
  }

  // Perform recursive deep check
  Entry *ea = a->entries;
  Entry *eb = b->entries;
  while (ea && eb) {
    if (!nodeEq(ea->data, eb->data)) {
      return false;
    }
    ea = ea->next;
    eb = eb->next;
  }
  return !ea && !eb;  // both sides ended together?
}

Type *createComposite(const char *name, char **types) {
  Type *composite = NULL;
  if (name) {
    // Prevent recursive composition
    for (int i = 0; types[i]; i++) {
      if (strcmp(types[i], name) == 0) {
        logAlert(__func__, "recursive mix", name);
        return NULL;
      }
    }
    composite = setType(name, NULL);
  } else {
    auto_str auxName = joinSep(types, "/");
    composite = setType(auxName, NULL);
  }

  // If existing, validate it's identical, or fail creation
  size_t count = listCount((void **)types);
  if (composite->alias) {
    if (strchr(composite->name, '/')) {
      return composite;  // bypass check hack
    }

    bool isSame = true;
    if (!composite->isComposite || composite->alias != getType(types[0]) ||
        listCount((void **)composite->include) != count - 1) {
      isSame = false;
    } else {
      for (size_t i = 1; i < count; i++) {
        if (composite->include[i - 1] != getType(types[i])) {
          isSame = false;
        }
      }
    }

    if (!isSame) {
      logAlert(__func__, MSG_D_DEF, composite->name);
      return NULL;
    }

    return composite;
  }

  // Allocate memory for composite definition
  composite->include = FRY_ALLOC(sizeof(Type *) * (count));

  // Store the composite mix
  composite->isComposite = true;
  composite->alias = setType(types[0], NULL);
  for (size_t i = 1; i < count; i++) {
    composite->include[i - 1] = setType(types[i], NULL);
  }
  composite->include[count - 1] = NULL;

  return composite;
}

Type *resolveAlias(Type *ck) {
  Type *base = ck;
  for (; ck; ck = ck->alias) {
    base = ck;
  }
  return base;
}

bool isAlias(const Type *base, Type *alias) {
  for (; alias; alias = alias->alias) {
    if (alias == base) {
      return true;
    }
  }
  return false;
}

/**
 * Compares types resolving aliases
 */
static inline bool matchTypes(Type *a, Type *b) {
  a = resolveAlias(a);
  b = resolveAlias(b);
  if (a && b) {
    return a == b;
  }
  return true;  // FatAny implied on a or b
}

static bool checkTypeMethod(const Type *ck, Node *node) {
  if (!ck->isComposite) {
    return ck == checkMethod;
  }

  if (ck->alias != checkMethod) {
    return false;
  }

  if (!node->ck) {
    return false;  // no annotation and no type inference
  }

  if (!node->ck->isComposite) {
    return ck->include[0] == node->ck && ck->include[1] == NULL;
  }

  if (ck->include[0]->isComposite && ck->include[0] == node->ck) {
    return true;  // fnx: Method/Matrix = fn, where fn = (x): Matrix -> x
  }

  if (ck->include[0] != node->ck->alias) {
    return false;
  }

  for (int i = 1; ck->include[i] && node->ck->include[i - 1]; i++) {
    if (ck->include[i] != node->ck->include[i - 1]) {
      return false;
    }
  }

  return true;
}

static bool checkTypeList(const Type *ck, Node *node) {
  if (!ck->isComposite) {
    return ck == checkList;
  }

  if (ck->alias != checkList) {
    return false;
  }

  Node *head = NULL;
  for (int i = 0; ck->include[i]; i++, node = head) {
    Scope *list = node->scp && node->scp->isList ? node->scp : NULL;

    if (list) {
      lockResource(&list->lock);
      head = list->entries ? list->entries->data : NULL;
      unlockResource(&list->lock);

      if (head) {
        if (!checkAlias(ck->include[i], head)) {
          return false;
        }
      } else {
        break;  // can't check type on empty list
      }
    } else {
      logAlert(__FILE__, __func__, MSG_UNSUP " deep type check (a)");
      break;  // limitation: only checks lists of lists of...
    }
  }

  return true;
}

static bool checkTypeScope(const Type *ck, const Node *node) {
  Scope *scope = node->scp;

  if (ck->isComposite) {
    if (!checkTypeScope(ck->alias, node)) {
      return false;
    }

    const Type *sub = ck->include[0];

    lockResource(&scope->lock);
    for (Entry *entry = scope->entries; entry; entry = entry->next) {
      if (!checkAlias(sub, entry->data)) {
        unlockResource(&scope->lock);
        return false;
      }
    }
    unlockResource(&scope->lock);

    // Limitation: only checks at depth 0
    if (ck->include[1]) {
      logAlert(__FILE__, __func__, MSG_UNSUP " deep type check (b)");
    }

    return true;
  }

  if (isAlias(ck, scope->ck)) {
    return true;
  }

  if (ck == checkScope) {
    return true;
  }

  if (scope->ck && scope->ck->include) {
    Type **incl = scope->ck->include;

    // node has only one include and it is ck?
    if (incl[0] == ck && !incl[1]) {
      return true;
    }

    // base types are a match?
    if (ck->include) {
      for (int i = 0; ck->include[i] || incl[i]; i++) {
        if (ck->include[i] != incl[i]) {
          return false;
        }
      }
      return true;
    }
  }

  return false;
}

bool checkType(const Type *ck, Node *node) {
  if (!ck || !node || node->type == FatVoid) {
    return true;  // Any implied
  }

  if (node->type == FatType) {
    Type *ckPtr = (Type *)ck;
    return ck == ckTypeType || node->ck == ckTypeType ||
           matchTypes(ckPtr, node->ck);
  }

  if (node->type == FatMethod) {
    return ck == checkMethod;
  }

  if (node->type == FatList) {
    return ck == checkList;
  }

  if (node->type == FatScope && node->scp && node->scp->ck) {
    return ck == node->scp->ck;
  }

  return ck == node->ck;
}

bool checkAlias(const Type *ck, Node *node) {
  if (!ck || !node || node->type == FatVoid) {
    return true;  // Any implied
  }

  if (node->type == FatType) {
    return ck == ckTypeType;
  }

  if (node->type == FatMethod) {
    return checkTypeMethod(ck, node);
  }

  if (node->type == FatList) {
    return checkTypeList(ck, node);
  }

  if (node->type == FatScope && node->scp) {
    return checkTypeScope(ck, node);
  }

  return isAlias(ck, node->ck);
}

int getMethodArity(Node *method) {
  int arity = 0;
  for (Node *arg = method->head; arg; arg = arg->seq) {
    arity++;
  }
  return arity;
}

char *cpSrc(Node *node) {
  return *node->src == '$' ? node->src : strDup(node->src);
}

void endCursesMode(void) {
  if (cursesMode) {
    cursesMode = false;
    endwin();
  }
}

static void exitHooks(const char *ref) {
#ifndef DEBUG
  (void)ref;
#endif

  // Only the main thread should run exitHooks
  if (!pthread_equal(pthread_self(), mainThreadId)) {
    abort();
  }

  // Try to cancel other threads still executing, if any
  for (int hash = 0; hash < SCOPE_HASH; hash++) {
    for (int i = 0; i < scpRegistryCap[hash]; i++) {
      Worker *worker = scopesRegistry[hash][i]->async;
      if (worker && worker->ctx && !atomic_load(&worker->hasJoined)) {
        atomic_store(&worker->ctx->isCanceled, true);
        void *result = NULL;  // ignored
        int joinError = pthread_join(worker->ctx->id, &result);
        if (joinError) {
          logAlert(__FILE__, __func__, strerror(joinError));
        }
      }
    }
  }

#ifdef DEBUG
  if (traceLogs) {
    logDebug2(__FILE__, __func__, ref, "cleaning up...");
  }
  // this cleanup is useful for tracking memory leaks, don't remove
  if (globalScope) {
    wipeScope(globalScope, NULL);
    resetSession();  // collect all nodes
    free(globalScope);
    globalScope = NULL;
  }
  Type *next = NULL;
  for (int hash = 0; hash < META_HASH; hash++) {
    for (Type *ck = metaSpace[hash]; ck; ck = next) {
      next = ck->next;
      logTrace2(__FILE__, __func__, "dispose", ck->name);
      free((char *)ck->name);
      if (ck->include) {
        free(ck->include);
      }
      if (ck->alias && ck->def) {
        free(ck->def->val);
        free(ck->def);
      }
      free(ck);
    }
  }
#endif

  if (configOnce) {
    curl_global_cleanup();
    cleanMemoryLock();
#ifdef __EMSCRIPTEN__
    Mix_CloseAudio();
    SDL_Quit();
#endif
#ifdef SSL_SUPPORT
    if (sslCtx) {
      SSL_CTX_free(sslCtx);
    }
    EVP_cleanup();
    ERR_free_strings();
#endif
  }
}

noreturn void exitFromSignal(ExitCode sig) {
  // Only the main thread should receive signals
  if (!pthread_equal(pthread_self(), mainThreadId)) {
    abort();
  }

  // Ignore new termination signals while shutting down
  signal(SIGINT, SIG_IGN);
  signal(SIGHUP, SIG_IGN);
  signal(SIGTERM, SIG_IGN);

  endCursesMode();

  // Not entirely safe on signal handler, but adds a nice touch :P
  long count = atomic_load(&activeMemory);
  char *approx = "";
  char *unit = "";
  if (count > 999) {
    approx = "~";
    count = ceil((double)count / 1000);
    unit = "k";
    if (count > 999) {
      count = ceil((double)count / 1000);
      unit = "m";
    }
  }
  stderrEndLine();
  fputs(MRG_END "pid ", stderr);
  fprintf(stderr, "%u terminated with ", (unsigned int)getpid());
  fprintf(stderr, "%s%ld%s active nodes and ", approx, count, unit);
  count = (long)trackedScopes;
  approx = "";
  unit = "";
  if (count > 999) {
    approx = "~";
    count = ceil((double)count / 1000);
    unit = "k";
    if (count > 999) {
      count = ceil((double)count / 1000);
      unit = "m";
    }
  }
  fprintf(stderr, "%s%ld%s active scopes!\n\n", approx, count, unit);
  // End of unsafe section

  exitHooks(__func__);
  exit(sig);
}

noreturn void exitFry(ExitCode code) {
  endCursesMode();
  exitHooks(__func__);
  exit(abs(code % 256));  // ensure exit code between 0 and 255 (POSIX)
}

static bool isLocaleUTF8(const char *locale) {
  return strcasestr(locale, "utf-8") || strcasestr(locale, "utf8");
}

bool setFryLocale(const char *locale) {
  free(currentLocale);

  currentLocale = strDup(setlocale(LC_ALL, locale));

  if (currentLocale) {
    isUtf8Mode = isLocaleUTF8(currentLocale);
    return true;
  }

  // else setlocale failed, thus returned null, fallback to system default
  currentLocale = strDup(setlocale(LC_ALL, NULL));
  isUtf8Mode = currentLocale && isLocaleUTF8(currentLocale);
  return false;
}

bool canExpandPath(const char *path) {
  size_t len = strlen(path);
  return len > 2 && strcmp(&path[len - 2], "/_") == 0;
}

static inline void removeFromList(char **paths) {
  free(paths[0]);
  for (int i = 0; paths[i]; i++) {
    paths[i] = paths[i + 1];
  }
}

char *getBasePath(const char *filepath) {
  const char *lastSlash = strrchr(filepath, '/');
  if (lastSlash) {
    return copyFragment(filepath, lastSlash - filepath + 1);
  }
  return NULL;
}

char *getSourceForPath(const char *path) {
  auto_str importPath = join3(basePath ? basePath : "", path, ".fat");
  size_t len = strlen(importPath);

  bool isValid = existsFile(importPath);
  if (!isValid && len > 4) {
    importPath[len - 4] = '\0';  //  try without ".fat"
  }

  if (isValid || existsFile(importPath)) {
    if (debugLogs) {
      logDebug2(__FILE__, __func__, "importing", importPath);
    }

    // Return the source from file
    char *sourceFromFile = NULL;
    readFile(&sourceFromFile, importPath, "r");
    return sourceFromFile;
  }

  if (!canExpandPath(path)) {
    if (debugLogs) {
      logDebug2(__FILE__, __func__, "can't resolve", path);
    }
    return NULL;
  }

  // Generate a list of imports from folder
  char *source = NULL;
  auto_str trimmed = copyFragment(path, strlen(path) - 2);  // remove "/_"
  auto_str dirPath = join3(basePath ? basePath : "", trimmed, "/");
  if (isDir(dirPath)) {
    if (debugLogs) {
      logDebug2(__FILE__, __func__, "expanding", dirPath);
    }

    char **paths = listDir(dirPath);
    if (paths && paths[0]) {
      size_t i = 0;
      for (; paths[i]; i++) {
        len = strlen(paths[i]);
        while (len > 1 && paths[i][len - 1] == '/') {  // is dir?
          removeFromList(&paths[i]);
          if (paths[i]) {
            len = strlen(paths[i]);  // get len of "next" path
          } else {
            if (i) {
              i--;
            }
            len = 0;  // break
          }
        }
        if (len > 4 && paths[i][len - 4] == '.') {  // hacky way to check and
          paths[i][len - 4] = '\0';                 // trim fat file extension
        }
      }

      // replace slashes back to dots in trimmed path base
      char *aux = trimmed;
      while (*aux) {
        if (*aux == '/') {
          *aux = '.';
        }
        aux++;
      }

      auto_str statementBase = join3("\n_ <- ", trimmed, ".");
      aux = joinSep(paths, statementBase);
      source = join3(&statementBase[1], aux, "\n");  // synthetic import list

      free(aux);
      freeStrList(paths);
    } else {
      if (debugLogs) {
        logDebug2(__FILE__, __func__, "nothing in dir", dirPath);
      }
      source = strDup("");  // don't throw an error
    }
  } else if (debugLogs) {
    logDebug2(__FILE__, __func__, "not a folder", dirPath);
  }

  return source;
}

bool initVisit(const char *func) {
  lockResource(&memoryLock);
  if (visitingFunc) {
#ifdef DEBUG
    if (strcmp(func, "markAndSweep") != 0) {
      auto_str msg = join2("called while" GUIDE, visitingFunc);
      logAlert(__func__, func, msg);
    }
#endif
    unlockResource(&memoryLock);
    return false;  // not ok to proceed!
  }

  visitingFunc = (char *)func;
  memset(visitCap, 0, sizeof(visitCap));
  return true;
}

void endVisit(void) {
  for (int hash = 0; hash < SCOPE_HASH; hash++) {
    if (visited[hash]) {
      free(visited[hash]);
      visited[hash] = NULL;
    }
  }
  visitingFunc = NULL;
  unlockResource(&memoryLock);
}

bool isFirstVisit(Scope *scope) {
  int i = 0;
  int hash = (int)((uintptr_t)scope % SCOPE_HASH);

  for (; i < visitCap[hash]; i++) {
    if (visited[hash][i] == scope) {
      return false;
    }
  }

  if (visitCap[hash] % SCOPE_BLOCK == 0) {
    if (visitCap[hash] == 0) {
      visited[hash] = FRY_ALLOC(sizeof(Scope *) * (SCOPE_BLOCK));
    } else {
      visited[hash] = FRY_REALLOC(
          visited[hash], sizeof(Scope *) * (visitCap[hash] + SCOPE_BLOCK));
    }
  }

  // not yet visited, register visit
  visited[hash][i] = scope;
  visitCap[hash]++;
  return true;
}

void printFgColor(int fgCode, FILE *stream) {
  char buffer[16] = {'\0'};
  if (fgCode < 0) {
    // ignore
  } else if (fgCode < 8) {
    snprintf(buffer, sizeof(buffer), "\033[3%dm", fgCode);
  } else if (fgCode < 16) {
    snprintf(buffer, sizeof(buffer), "\033[9%dm", fgCode - 8);
  } else if (fgCode < 256) {
    snprintf(buffer, sizeof(buffer), "\033[38:5:%dm", fgCode);
  }  // else also ignore
  directPrint(buffer, stream);
}

void printBgColor(int bgCode, FILE *stream) {
  char buffer[16] = {'\0'};
  if (bgCode < 0) {
    // ignore
  } else if (bgCode < 8) {
    snprintf(buffer, sizeof(buffer), "\033[4%dm", bgCode);
  } else if (bgCode < 16) {
    snprintf(buffer, sizeof(buffer), "\033[10%dm", bgCode - 8);
  } else if (bgCode < 256) {
    snprintf(buffer, sizeof(buffer), "\033[48:5:%dm", bgCode);
  }  // else also ignore
  directPrint(buffer, stream);
}

char *getFryMeta(void) {
  char *list[450] = {0};
  size_t pos = 0;

  asprintf(&list[pos++], "{\n  fryVersion    = '%s'\n",
           FRY_VERSION PLATFORM ARCHITECTURE);
#ifdef __EMSCRIPTEN__
  asprintf(&list[pos++], "  buildMode     = 'web'\n");
#elif DEBUG
  asprintf(&list[pos++], "  buildMode     = 'debug'\n");
#elif TURBO
  asprintf(&list[pos++], "  buildMode     = 'turbo'\n");
#else
  asprintf(&list[pos++], "  buildMode     = 'default'\n");
#endif

  asprintf(&list[pos++], "  buildDate     = '%s'\n", __DATE__);
#ifdef USE_NCURSES
  asprintf(&list[pos++], "  tuiBackend    = 'ncurses'\n");
#else
  asprintf(&list[pos++], "  tuiBackend    = 'fatcurses'\n");
#endif

#ifdef USE_READLINE
  asprintf(&list[pos++], "  editBackend   = 'readline'\n");
#elif __EMSCRIPTEN__
  asprintf(&list[pos++], "  editBackend   = 'webenv'\n");
#else
  asprintf(&list[pos++], "  editBackend   = 'linenoise'\n");
#endif

  asprintf(&list[pos++], "  rcFile        = '%s'\n", RC_FILE);
  asprintf(&list[pos++], "  replFile      = '%s'\n", REPL_FILE);
  asprintf(&list[pos++], "  dateFmt       = '%s'\n", DEFAULT_DATE_FMT);
  asprintf(&list[pos++], "  maxToInt      = %.0f\n", MAX_TO_INT);
  asprintf(&list[pos++], "  defaultMem    = %ld\n", DEFAULT_MEM);
  asprintf(&list[pos++], "  memoryLimit   = %ld\n", memoryLimit);
  asprintf(&list[pos++], "  iniDepth      = %d\n", INI_DEPTH);
  asprintf(&list[pos++], "  defDepth      = %d\n", DEF_DEPTH);
  asprintf(&list[pos++], "  maxDepth      = %d\n", MAX_DEPTH);
  asprintf(&list[pos++], "  stackDepth    = %d\n", stackDepth);
  asprintf(&list[pos++], "  stackTrace    = %d\n", STACK_TRACE);
  asprintf(&list[pos++], "  minQuickRef   = %d\n", MIN_QUICK_REF);
  asprintf(&list[pos++], "  skipSize      = %d\n", SKIP_SIZE);
  asprintf(&list[pos++], "  skipListMax   = %d\n", SKIP_LIST_MAX);
  asprintf(&list[pos++], "  metaHash      = %d\n", META_HASH);
  asprintf(&list[pos++], "  scopeHash     = %d\n", SCOPE_HASH);
  asprintf(&list[pos++], "  findScpDedup  = %d\n", FIND_SCP_DEDUP);
  asprintf(&list[pos++], "  gcPremonition = %d\n", GC_PREMONITION);
  asprintf(&list[pos++], "  gcThreadLag   = %d\n", GC_THREAD_LAG);
  asprintf(&list[pos++], "  maxContextMem = %d\n", MAX_CONTEXT_MEM);
  asprintf(&list[pos++], "  typeMax       = %d\n", TYPE_MAX);
  asprintf(&list[pos++], "  indent        = %d\n", INDENT);
  asprintf(&list[pos++], "  argsStyleMax  = %d\n", ARG_STYLE_MAX);
  asprintf(&list[pos++], "  localStyleMax = %d\n", LOCAL_STYLE_MAX);
  asprintf(&list[pos++], "  initBuffLen   = %d\n", INIT_B_LEN);
  asprintf(&list[pos++], "  buffLen       = %d\n", BUFF_LEN);
  asprintf(&list[pos++], "  entryNameMax  = %d\n", ENTRY_NAME_MAX);
  asprintf(&list[pos++], "  dirListMax    = %d\n", DIR_LIST_MAX);
  asprintf(&list[pos++], "  bunStack      = %d\n", BUN_STACK);
  asprintf(&list[pos++], "  importMax     = %d\n", IMPORT_MAX);
  asprintf(&list[pos++], "  derivedSize   = %d\n", DERIVED_SIZE);
  asprintf(&list[pos++], "  sizeOfToken   = %d\n", (int)sizeof(Token));
  asprintf(&list[pos++], "  sizeOfReader  = %d\n", (int)sizeof(Reader));
  asprintf(&list[pos++], "  sizeOfNode    = %d\n", (int)sizeof(Node));
  asprintf(&list[pos++], "  sizeOfMeta    = %d\n", (int)sizeof(Memory));
  asprintf(&list[pos++], "  sizeOfScope   = %d\n", (int)sizeof(Scope));
  asprintf(&list[pos++], "  sizeOfEntry   = %d\n", (int)sizeof(Entry));
  asprintf(&list[pos++], "  sizeOfContext = %d\n", (int)sizeof(Context));
  asprintf(&list[pos++], "  sizeOfWorker  = %d\n", (int)sizeof(Worker));
  asprintf(&list[pos++], "  sizeOfHugeInt = %d\n", (int)sizeof(HugeInt));
  asprintf(&list[pos++], "  isAstOnly     = %s\n", ofBool(isAstOnly));
  asprintf(&list[pos++], "  debugLogs     = %s\n", ofBool(debugLogs));
  asprintf(&list[pos++], "  crashOnError  = %s\n", ofBool(crashOnError));
  if (configOnce) {
    asprintf(&list[pos++], "  randomSeedA   = 0x%" PRIx64 "\n", xorshiftStateA);
    asprintf(&list[pos++], "  randomSeedB   = 0x%" PRIx64 "\n", xorshiftStateB);
    asprintf(&list[pos++], "  randomSeedC   = 0x%" PRIx64 "\n", xorshiftStateC);
  }

#ifdef SSL_SUPPORT
  asprintf(&list[pos++], "  sslSupport    = true\n");
#else
  asprintf(&list[pos++], "  sslSupport    = false\n");
#endif

  asprintf(&list[pos++], "  ebCmdSupport  = [");

  EbCmd lastCmd = EbStorableErase;

  for (EbCmd i = 1; i <= lastCmd; i++) {
    const char *cmd = ebCmd(i);
    asprintf(&list[pos++], "'%s'", cmd);
    if (i == lastCmd) {
      asprintf(&list[pos++], "]\n");
    } else {
      asprintf(&list[pos++], ",");
    }
  }
  asprintf(&list[pos++], "}\n");

  // Sanity check with leeway for configOnce and debug comment
  int totalItems = pos + 5;
  int totalSize = sizeof(list) / sizeof(char *);
  if (totalItems >= totalSize) {
    fatalOut(__FILE__, __func__, MSG_BMO);
  }

#ifdef DEBUG
  // Add debug comment
  asprintf(&list[pos++], "#%d/%d\n", totalItems, totalSize);
#endif

  char *result = mkString(list);

  // Clean up
  freeEachStrInList(list);

  return result;
}
