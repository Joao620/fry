/**
 * @file structures.h
 * @brief fry interpreter internal data structures
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-30
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "huge.h"
#include "parameters.h"
#include "sugar.h"

#define SRC_RUN "$heap"  // runtime computation result
#define SRC_AUX "$aux"   // auxiliary stack allocated

/**
 * @brief Token type.
 */
typedef enum {
  TkEOF,    // end of file
  TkPunct,  // punctuation
  TkKey,    // keyword
  TkId,     // identifier
  TkName,   // typename
  TkUnder,  // underscore
  TkOp,     // operator
  TkNumb,   // number
  TkHuge,   // huge integer (as hex string)
  TkText,   // text (generic, treated as raw)
  TkTemp,   // text template (single quotes)
  TkRaw,    // raw text (double quotes)
  TkComm,   // comment
  TkEmbed   // embedded command
} TokType;

/**
 * @brief Return string name of token type.
 *
 * @param type TokType
 * @return string
 */
const char *tokType(TokType type);

/**
 * @brief AST node type.
 */
typedef enum {
  FatInvalid,  // invalid
  FatVoid,     // nothing
  FatAny,      // anything (not used/placeholder)
  FatBoolean,  // primitive
  FatNumber,   // primitive
  FatHugeInt,  // primitive
  FatChunk,    // primitive (binary blob)
  FatText,     // primitive
  FatTemp,     // text template to interpolate
  FatEntry,    // identifier
  FatList,     // list container
  FatScope,    // scope container
  FatMethod,   // function
  FatError,    // exception
  FatType,     // type definition
  FatBlock,    // syntax/wrapper
  FatDynamic,  // syntax/logic node
  FatCall,     // syntax/logic node
  FatAssign,   // syntax/logic node
  FatUnary,    // syntax/logic node
  FatExpr,     // syntax/logic node
  FatCase,     // syntax/logic node
  FatSwitch,   // syntax/logic node
  FatLoop,     // syntax/logic node
  FatEmbed     // embedded command
} NodeType;

/**
 * @brief Return string name of fat node type.
 *
 * @param type NodeType
 * @return string
 */
const char *fatType(NodeType type);

/**
 * @brief Operation type.
 */
typedef enum {
  OpInvalid,    // (???)
  OpAssign,     // =
  OpOr,         // |
  OpAnd,        // &
  OpNot,        // !
  OpBool,       // !!
  OpLess,       // <
  OpMore,       // >
  OpLessEq,     // <=
  OpMoreEq,     // >=
  OpEqual,      // ==
  OpNotEq,      // !=
  OpPlus,       // +
  OpIncrement,  // +=
  OpDecrement,  // -=
  OpMulBy,      // *=
  OpPowBy,      // **=
  OpDivBy,      // /=
  OpModBy,      // %=
  OpMinus,      // -
  OpMultiply,   // *
  OpPow,        // **
  OpSlash,      // /
  OpPercent,    // %
  OpLambda,     // ->
  OpImport,     // <-
  OpCase,       // =>
  OpSwitch,     // >>
  OpIf,         // ? (:)
  OpDot,        // .
  OpRange,      // ..
  OpHORange,    // ..<
  OpIfDot,      // ?.
  OpCoalesce,   // ??
  OpCoAssign,   // ??=
  OpMissing,    // ???
  OpLoop,       // @
  OpMutable     // ~
} OpType;

/**
 * @brief Return string name of operation type.
 *
 * @param op OpType
 * @return string
 */
const char *opType(OpType op);

/**
 * @brief Get operation type from a string.
 *
 * @param op string value of operation
 * @return OpType
 */
OpType getOpType(const char *op);

/**
 * @brief Lexical token consumed by parser.
 */
typedef struct Token {
  TokType type;        // token type
  double num;          // number value
  char *val;           // string value
  struct Token *next;  // like a linked list
  char *src;           // for tracing
} Token;

/**
 * @brief Create a Token object.
 *
 * @param type TokType
 * @param src source reference
 * @return Token*
 */
Token *createToken(TokType type, char *src);

#ifdef DEBUG
/**
 * @brief Convert Token to string (for trace purpose).
 *
 * @param tok token
 * @return string (use free)
 */
char *ofToken(Token *tok);
#endif

typedef struct Reader {
  char *name;      // filename or reference
  char *source;    // source code buffer
  size_t pos;      // next index in source
  size_t ln;       // line number
  size_t col;      // column
  Token *head;     // first token of source, once tokenized
  Token *current;  // current token being consumed by parser
  size_t bytes;    // node bytes generated by this reader
  bool isCase;     // reader state during parsing
  bool isIfElse;   // reader state during parsing
  bool isMethod;   // reader state during parsing
  bool isParen;    // reader state during parsing
  bool isCall;     // reader state during parsing
} Reader;

/**
 * @brief Create a Reader object.
 *
 * @param name constant string
 * @return Reader*
 */
Reader *createReader(const char *name);

/**
 * @brief Frees reader and it's token structure, for parsed reader (consumed).
 *
 * @param reader Reader*
 */
void deleteReader(Reader *reader);

/**
 * @brief Frees non-parsed reader (when contents hasn't been assigned to nodes).
 * @note Don't use this after parse, as values are assigned by parser into AST.
 *
 * @param reader Reader*
 */
void freeReaderAll(Reader *reader);

/**
 * @brief Consume current char from reader (and return it).
 *
 * @param reader Reader*
 * @return char
 */
char advanceChar(Reader *reader);

/**
 * @brief Consume current token from reader, return next.
 *
 * @param func function reference
 * @param reader Reader*
 * @return Token*
 */
Token *advanceTok(const char *func, Reader *reader);

/**
 * @brief Skip current token from reader, freeing token val.
 *
 * @param func function reference
 * @param reader Reader*
 */
void skipTok(const char *func, Reader *reader);

struct Scope;    // recursive struct, see bellow
struct Type;     // recursive struct, see bellow
struct Memory;   // recursive struct, see memory.h
struct Context;  // ahead declaration, see bellow

typedef unsigned int OpCode;  // should be the same type of enum

typedef union {
  size_t s;    // size of FatText (and FatBlock index)
  double f;    // floating point FatNumber
  bool b;      // value of FatBoolean
  HugeInt *h;  // pointer to dynamically allocated buffer
} NumLike;

/**
 * @brief Node is the main AST structure.
 * @note On an attempt to avoid adding diverse structs and
 * also to not to let it bloated, some fields have different
 * use depending on the NodeType, for example:
 * - op, for FatUnary and FatExpr means OpType
 * - op, for FatEmbed means EbCmd
 * - op, for FatEntry means a flag like "isMutable"
 * Similar reuse happens with num, val, head, body, tail and next
 */
typedef struct Node {
  NodeType type;        // node type (internal/primitive)
  OpCode op;            // (see note above)
  NumLike num;          // based on node type, see: NumLike
  char *val;            // text -> value, raw or metadata
  struct Type *ck;      // type check
  struct Node *head;    // case -> cond, assign/binary -> left, method -> args
  struct Node *body;    // case -> then, assign/binary -> right, method -> body
  struct Node *tail;    // case -> else, call -> curried, type -> proto
  struct Node *seq;     // program next block / instruction
  char *src;            // for tracing (needs to be handled as a const)
  struct Scope *scp;    // collection structure
  struct Memory *meta;  // garbage collector metadata about this node
#ifdef DEBUG
  struct Node *programs;  // allows leak-check with valgrind
#endif
} Node;

/**
 * @brief Auxiliary indexes tuple.
 */
typedef struct {
  long _1;
  long _2;
} IntTuple;

/**
 * @brief Auxiliary nodes tuple.
 */
typedef struct {
  Node *_1;
  Node *_2;
} NodeTuple;

/**
 * @brief Initialize quick references (1/3 and 2/3 of size).
 *
 * @param refs IntTuple*
 * @param size number
 */
void initQuickRefs(IntTuple *refs, long size);

typedef union {
  long i;   // index
  char *s;  // string
} KeyLike;

/**
 * @brief Value wrapper for Scope/List structure.
 */
typedef struct Entry {
  KeyLike key;         // entry name/index
  Node *data;          // node data or collection meta
  struct Entry *next;  // prog -> next block, scope/list -> next item
  struct Entry *skip;  // used exclusively for skip-list
} Entry;

/**
 * @brief Create a Scope Entry object - use with global memory lock.
 *
 * @param key entry name (string)
 * @param data Node*
 */
Entry *createScopeEntry(const char *key, Node *data);

/**
 * @brief Create a List Entry object - use with global memory lock.
 *
 * @param index long
 * @param data Node*
 */
Entry *createListEntry(long index, Node *data);

/**
 * @brief Destroy a Scope Entry object - use with global memory lock.
 *
 * @param entry to free (Scope)
 */
void freeScopeEntry(Entry *entry);

/**
 * @brief Destroy a List Entry object - use with global memory lock.
 *
 * @param entry to free (List)
 */
void freeListEntry(Entry *entry);

struct Worker;

/**
 * @brief Collection base (List and Scope).
 */
typedef struct Scope {
  Entry *entries;        // head of linked list
  Entry *quick1;         // entry 1/3 of scope / last skip-list item
  Entry *quick2;         // entry 2/3 of scope / last list item
  struct Type *ck;       // type check
  Entry *cached;         // lookup cache
  struct Worker *async;  // thread wrapper
  pthread_mutex_t lock;  // async handling (should be locked on write)
  long size;             // number of entries in scope
  atomic_long refs;      // meta for garbage collecting
  bool isTracked;        // meta for garbage collecting
  bool isLayer;          // meta for garbage collecting
  bool isList;           // access mode (by index if true / by name if false)
} Scope;

/**
 * @brief Typechecking meta entry.
 */
typedef struct Type {
  const char *name;       // unique name on metaSpace
  Node *def;              // definition
  struct Type *alias;     // type forwarding
  struct Type **include;  // extends from (list of other types)
  struct Type *next;      // metaSpace linked list
  bool isComposite;       // used for typechecking inner structure
} Type;

/**
 * @brief Checks if typename is reserved word.
 *
 * @param name unique typename
 * @return boolean
 */
bool isReservedName(const char *name);

/**
 * @brief Get the Type object by name.
 *
 * @param name unique typename
 * @return Type*
 */
Type *getType(const char *name);

/**
 * @brief Create, if not exists, a Type object (upsert).
 *
 * @param name unique typename
 * @param def Node *
 * @return Type*
 */
Type *setType(const char *name, Node *def);

/**
 * @brief Create a Scope object.
 *
 * @return Scope*
 */
Scope *createScope(void);

/**
 * @brief Create a Scope object with isList set as true.
 *
 * @return Scope*
 */
Scope *createList(void);

/**
 * @brief Node metadata for garbage collection.
 */
typedef struct Memory {
  struct Memory *prev;  // doubly linked list
  struct Memory *next;  // doubly linked list
  Node *data;           // assigned at creation, never changes (thread-safe)
  atomic_long refs;     // how many scopes point to this node
  bool visited;         // only used by markAndSweep GC (single-tread)
  bool locked;          // value not yet consumed by some operation?
} Memory;

/**
 * @brief Stack frame.
 */
typedef struct Frame {
  Node *node;
  Scope *scp;
  Node *trap;  // failure handler (points to method when set)
  const char *func;
} Frame;

/**
 * @brief Auxiliary structure for microGC.
 */
typedef struct HandlersCache {
  Node **handlers;
  int size;
  int capacity;
  bool isUpdated;
} HandlersCache;

typedef struct Context {
  Frame *stack;            // stack
  Frame *cur;              // stack
  atomic_int top;          // stack
  int currentMax;          // stack
  Node *selfRef;           // called instance (matched by type)
  Node *failureEvent;      // error handling
  Node *unclaimed;         // async or errorHandler result (GC protected)
  bool isHandlingFailure;  // error handling
  bool hasHandledFailure;  // error handling
  bool isMainMode;         // used to determinate if main or module
  atomic_bool isCanceled;  // async handling
  pthread_t id;            // async handling
  double timeout;          // async handling
  Memory *temp;            // temporary memory (no locking)
  size_t tempCount;        // how many nodes in temp memory
  char *importPath;        // to handle recursive import paths
  struct HandlersCache trapsCache;
} Context;

/**
 * @brief Create a Node object.
 *
 * @param type NodeType
 * @param at source reference
 * @param ctx Context*
 * @return Node*
 */
Node *createNode(NodeType type, char *at, Context *ctx);

/**
 * @brief Create a Context object.
 *
 * @param id thread id
 * @return Context*
 */
Context *createContext(pthread_t id);

/**
 * @brief Destroy a Context object.
 *
 * @param ctx Context*
 */
void freeContext(Context *ctx);

/**
 * @brief Returns pointer to dynamically allocated copy of HugeInt.
 *
 * @param num huge int to copy
 * @return HugeInt*
 */
HugeInt *copyHuge(HugeInt num);

/**
 * @brief Returns a copy of fat node.
 * @note Use initVisit before deep copy!
 *
 * @param node Node*
 * @param isDeep also duplicates nodes if true
 * @param ctx Context*
 * @return Node*
 */
Node *copyNode(Node *node, bool isDeep, Context *ctx);

/**
 * @brief Returns a copy of fat collection.
 * @note Use initVisit before deep copy!
 *
 * @param coll Scope*
 * @param isDeep also duplicates nodes if true
 * @param ctx Context*
 * @return Scope*
 */
Scope *copyCollection(Scope *coll, bool isDeep, Context *ctx);

/**
 * @brief Worker execution wrapper.
 */
typedef struct Worker {
  Node *task;    // FatMethod
  Context *ctx;  // thread context/stack
  atomic_bool hasJoined;
} Worker;
