/**
 * @file webenv.h
 * @brief Glue code for Emscripten (replacement for curl and readline)
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-28
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include <emscripten/fetch.h>
#endif

#include <stdbool.h>
#include <stdio.h>

// JS defined functions
extern bool isEmptyBuffer(void);
extern bool hasNewline(void);
extern char *getInputBuffer(void);
extern int getchImpl(void);
extern void setEcho(bool isOn);
extern void setBreak(bool isOn);
extern void setBlock(bool isOn);
extern int directPrint(const char *str, FILE *stream);
extern int getTermCols(void);
extern int getTermRows(void);
extern bool localStoreSet(const char *, const char *);
extern char *localStoreGet(const char *);
extern char *localStoreList(const char *);
extern bool localStoreRemove(const char *);

#define JS_IO_LAG 0  // prevent JS lockup (seems to do fine with 0)

extern bool hasAudioSupport;

// curl

#define CURLcode int
#define CURL void
#define CURLINFO int
#define CURLoption int

typedef enum {
  CURL_GLOBAL_ALL,
  CURLE_OK,
  CURLE_ERR,
  CURLINFO_RESPONSE_CODE,
  CURLOPT_USERAGENT,
  CURLOPT_HTTPHEADER,
  CURLOPT_BUFFERSIZE,
  CURLOPT_TCP_KEEPALIVE,
  CURLOPT_NOPROGRESS,
  CURLOPT_MAXREDIRS,
  CURLOPT_TIMEOUT,
  CURLOPT_HEADERFUNCTION,
  CURLOPT_HEADERDATA,
  CURLOPT_WRITEFUNCTION,
  CURLOPT_WRITEDATA,
  CURLOPT_CUSTOMREQUEST,
  CURLOPT_URL,
  CURLOPT_POSTFIELDS,
  CURLOPT_POSTFIELDSIZE,
  CURLOPT_SSL_VERIFYPEER,
  CURLOPT_SSL_VERIFYHOST
} CurlMockConstants;

typedef void *curl_slist;

typedef struct {
  char *url;
  char *method;
  char *postFields;
  size_t postSize;
} EmscriptenCURL;

struct curl_slist *curl_slist_append(struct curl_slist *list, const char *data);
void curl_slist_free_all(struct curl_slist *list);
CURLcode curl_global_init(long flags);
void curl_global_cleanup(void);
CURL *curl_easy_init(void);
void curl_easy_cleanup(CURL *curl);
CURLcode curl_easy_getinfo(CURL *curl, CURLINFO info, ...);
CURLcode curl_easy_setopt(CURL *curl, CURLoption option, ...);
CURLcode curl_easy_perform(CURL *curl);
const char *curl_easy_strerror(CURLcode);

// readline

char *readline(const char *prompt);
void add_history(const char *input);
