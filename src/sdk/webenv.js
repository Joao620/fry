/**
 * @file webenv.js
 * @brief Glue code for Emscripten
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-28
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

/**
 * see also: ../web/script.js
 */

mergeInto(LibraryManager.library, {
  // Check if empty
  isEmptyBuffer: () => !inputBuffer,

  // Check if Enter was pressed
  hasNewline: () => inputBuffer.includes("\n"),

  // Get the input buffer (ensuring no chars after new line)
  getInputBuffer: () => {
    if (cursorPosition) {
      const index = inputBuffer.indexOf("\n");
      if (index >= 0) {
        inputBuffer = inputBuffer.slice(0, index + 1);
      }
      cursorPosition = 0;
    }

    const input = stringToNewUTF8(inputBuffer);
    inputBuffer = ""; // flush input
    return input;
  },

  // Mock getch implementation (this is far from perfect)
  getchImpl: () => {
    if (inputBuffer && inputBuffer.length) {
      const ch = inputBuffer.charCodeAt(0);
      inputBuffer = inputBuffer.slice(1);
      return ch;
    }
    return -1; // ERR
  },

  // Toggle console echo mode
  setEcho: (isOn) => {
    isEchoOn = isOn;
    term.options.cursorStyle = isOn ? "block" : "underline";
  },

  // Toggle console break line mode
  setBreak: (isOn) => {
    isBreakOn = isOn;
  },

  // Toggle console non-block mode
  setBlock: (isOn) => {
    isBlockOn = isOn;

    // flush input on mode switch
    inputBuffer = "";
    cursorPosition = 0;
  },

  // Bypass Module.print and directly write to term
  directPrint: (str, _) => {
    term.write(UTF8ToString(str).replaceAll("\n", "\r\n"));
    return 0; // non-negative value (always succeed)
  },

  // Get number of columns available on terminal
  getTermCols: () => term.options.cols,

  // Get number of rows available on terminal
  getTermRows: () => term.options.rows,

  // Storable utils

  // Function to set a value in local storage
  // assumes path is of exact form: <folder>/<name>
  // and path must also not contain commas (not checked here)
  localStoreSet: (path, value) => {
    try {
      localStorage.setItem(UTF8ToString(path), UTF8ToString(value));
      return true;
    } catch (e) {
      console.error("Storage failed: " + e);
      return false;
    }
  },

  // Retrieve value from local storage by full path (or null)
  localStoreGet: (path) => {
    const value = localStorage.getItem(UTF8ToString(path));
    if (value === null) return null;
    return stringToNewUTF8(value);
  },

  // Retrieves the list of "filenames" of a given "folder"
  // assumes path is of exact form: <folder>/<name> (not checked here)
  // Returns the list as a comma separated C string
  localStoreList: (base) => {
    let substrings = [];
    const folder = UTF8ToString(base) + "/";
    for (let i = 0; i < localStorage.length; i++) {
      const key = localStorage.key(i);
      if (key.startsWith(folder)) {
        const [_, name] = key.split("/");
        substrings.push(name);
      }
    }
    return stringToNewUTF8(substrings.join(","));
  },

  // Removes value from path
  localStoreRemove: (path) => {
    const key = UTF8ToString(path);
    const value = localStorage.getItem(key);
    if (value === null) return false; // was not there
    try {
      localStorage.removeItem(key);
      return localStorage.getItem(key) === null; // was it removed?
    } catch (e) {
      console.error("Storage removal failed: " + e);
      return false;
    }
  },
});
