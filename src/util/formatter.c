/**
 * @file formatter.c
 * @brief Source code formatting utils
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-30
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "formatter.h"

#include "../lexer.h"
#include "../libs/libs.h"
#include "../parser.h"

// formatter global vars
static int stackTop = -1;
static const int stackBottom = 0;
static char** imports[BUN_STACK] = {0};
static char* ctxPath = NULL;
static bool isBundlingMain = true;

/*
 * Append text to a path buffer safely
 */
static void appendPath(char* path, const char* text) {
  size_t usedLen = strnlen(path, PATH_MAX);
  if (usedLen + strlen(text) >= PATH_MAX) {
    fatalOut(__FILE__, __func__, MSG_BMO);
  }
  strncat(&path[usedLen], text, PATH_MAX - usedLen - 1);
}

static void addLocalImport(char* path) {
  size_t nextIndex = listCount((void**)imports[stackTop]);
  if (nextIndex == IMPORT_MAX - 1) {
    fatalOut(__FILE__, __func__, MSG_BMO);
  }
  imports[stackTop][nextIndex] = strDup(path);
}

static void addNamedImport(char* path) {
  size_t nextIndex = listCount((void**)imports[stackBottom]);
  if (nextIndex == IMPORT_MAX - 1) {
    fatalOut(__FILE__, __func__, MSG_BMO);
  }
  imports[stackBottom][nextIndex] = strDup(path);
}

static bool hasImported(char* path) {
  for (int layer = stackTop; layer >= 0; layer--) {
    for (int i = 0; imports[layer][i]; i++) {
      if (strcmp(imports[layer][i], path) == 0) {
        return true;
      }
    }
  }
  return false;
}

static Reader* getReader(char* filepath, char* embedded) {
  if (!filepath) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
  }
  Reader* reader = createReader(filepath);
  reader->source = embedded ? embedded : getSourceForPath(filepath);
  if (!reader->source) {
    fatalOut(__FILE__, __func__, join2("bad path" GUIDE, filepath));
  }
  return tokenize(reader);
}

static char* tokToVal(Token* tok, bool isBundling) {
  switch (tok->type) {
    case TkNumb:
      return prettyNumber(NULL, tok->num);
    case TkHuge:
      return join2("0x", tok->val);
    case TkText:
    case TkTemp:
      return unparseText(tok->val, false);
    case TkRaw:
      return unparseText(tok->val, true);
    case TkEmbed:
      if (isBundling) {
        if (strcmp(tok->val, "$isMain") == 0) {
          logAlert("$isMain evaluated as", ofBool(isBundlingMain), tok->src);
          return strDup(ofBool(isBundlingMain));
        }
        if (strcmp(tok->val, "$break") == 0) {
          logAlert("$break replaced by", "no-op", tok->src);
          return strDup("()");
        }
      }
      return tok->val;
    default:
      return isPunctTok(tok, ";") ? strDup("\n") : tok->val;
  }
}

static bool isStaticPath(const Token* tok) {
  switch (tok->type) {
    case TkId:
    case TkName:
    case TkUnder:
      return true;
    default:
      return false;
  }
}

static bool isDynamicPath(const Token* tok) {
  switch (tok->type) {
    case TkText:
    case TkTemp:
    case TkRaw:
      return true;
    default:
      return false;
  }
}

// note: symbol must be of size 2 (exactly)
static long indexOfSymbol(const char* line, const char* symbol) {
  for (long i = 0; line[i] && line[i] != MARK_COM; i++) {
    if (line[i] == symbol[0] && line[i + 1] == symbol[1]) {
      return i;
    }
  }
  return -1;
}

static void group(char** lines, long first, long last, long max,
                  const char* symbol) {
  for (long i = first; i <= last; i++) {
    long pos = indexOfSymbol(lines[i], symbol);
    if (pos < max) {
      char* whites = createWhites((int)(max - pos));
      char* replacement = join2(whites, symbol);
      char* new = replaceFirst(lines[i], symbol, replacement);
      free(lines[i]);
      free(whites);
      free(replacement);
      lines[i] = new;
    }
  }
}

static bool isPaired(char* line) {
  int open = 1;
  while (*line && hasBracket(*line, &open)) {
    line++;
  }
  return open == 1;
}

static long getTextEnd(const char* text, char delimiter) {
  char prev = '\0';
  for (long i = 0; text[i]; i++) {
    if (prev != '\\' && text[i] == delimiter) {
      return i;
    }
    prev = text[i];
  }
  return -1;
}

static bool isWithinText(const char* line, long pos);

static bool isInDelimited(const char* text, long pos, char delimiter) {
  long start = indexOf(text, delimiter);

  if (start == -1 || pos < start) {
    return false;
  }

  long end = start + getTextEnd(&text[start + 1], delimiter) + 1;

  if (end > start) {
    if (pos > start && pos < end) {
      return true;
    }
    return isWithinText(&text[end + 1], pos - end - 1);
  }

  return false;
}

static bool isWithinText(const char* line, long pos) {
  if (pos < 0) {
    return false;
  }

  return isInDelimited(line, pos, '"') || isInDelimited(line, pos, '\'');
}

static void reflowSymbol(long linesCount, char** lines, const char* symbol,
                         const char* unless) {
  long first = -1;
  long last = -1;
  long max = -1;

  for (long i = 0; i < linesCount; i++) {
    long unlessPos = unless ? indexOfSymbol(lines[i], unless) : -1;
    long pos = isPaired(lines[i]) ? indexOfSymbol(lines[i], symbol) : -1;

    // disables symbol grouping if symbol is inside a quoted text
    bool disable = isWithinText(lines[i], pos);

    if (pos >= 0 && !(unlessPos != -1 && pos > unlessPos) && !disable) {
      max = pos > max ? pos : max;
      if (first < 0) {
        first = i;
      } else {
        last = i;
      }
    } else {
      if (last >= 0) {
        group(lines, first, last, max, symbol);
      }
      first = last = max = -1;
    }
  }
  if (last >= 0) {
    group(lines, first, last, max, symbol);
  }
}

bool isBufferBlank(const char* buffer) {
  while (*buffer) {
    if (!isspace(*buffer)) {
      return false;
    }
    buffer++;
  }
  return true;
}

static void clearBlankLines(long linesCount, char** lines) {
  for (long i = 0; i < linesCount; i++) {
    if (isBufferBlank(lines[i])) {
      lines[i] = FRY_REALLOC(lines[i], 1);
      lines[i][0] = '\0';
    }
  }
}

static char* reflowLines(char* buffer) {
  char** lines = splitSep(buffer, "\n");
  free(buffer);

  long linesCount = (long)listCount((void**)lines);
  reflowSymbol(linesCount, lines, "<-", "=>");
  reflowSymbol(linesCount, lines, "==", "=>");
  reflowSymbol(linesCount, lines, "!=", "=>");
  reflowSymbol(linesCount, lines, " &", "=>");
  reflowSymbol(linesCount, lines, " |", "=>");
  reflowSymbol(linesCount, lines, "==", NULL);
  reflowSymbol(linesCount, lines, "!=", NULL);
  reflowSymbol(linesCount, lines, "=>", NULL);
  reflowSymbol(linesCount, lines, " #", NULL);
  clearBlankLines(linesCount, lines);
  buffer = joinSep(lines, "\n");

  freeStrList(lines);
  return buffer;
}

static inline bool canImportTo(const Token* target, const Token* source) {
  return target && source && isStaticPath(target) && isStaticPath(source);
}

char* createWhites(int size) {
  if (size < 0) {
    size = 0;
  }

  char* text = FRY_ALLOC(size + 1);
  memset(text, ' ', size);
  text[size] = '\0';
  return text;
}

void pushImportScope(void) {
  stackTop++;
  if (stackTop == BUN_STACK) {
    fatalOut(__FILE__, __func__, MSG_BMO);
  }
  imports[stackTop] = FRY_CALLOC(IMPORT_MAX * sizeof(char*));
}

void popImportScope(void) {
  freeStrList(imports[stackTop]);
  stackTop--;
}

/**
 * Log the failed filepath and exit execution with error code
 */
static void failBundling(char* filepath) {
  stderrStartLine(CL_RED);
  fprintf(stderr, MRG_ERR "failed: %s", filepath);
  stderrEndLine();
  exitFry(EXIT_FAILURE);
}

/*
 * Just check if can parse the file with no errors
 * (ideally should do some proper linting in the future)
 */
static void performBasicAnalysis(char* srcPath) {
  Reader* probeReader = getReader(srcPath, NULL);
  Node* program = parse(probeReader);
  free(probeReader->source);
  trackParsed(program, probeReader->bytes, NULL);
  deleteReader(probeReader);
}

/*
 * Actual recursive bundling implementation
 */
char* bundle(char* filepath, bool recursive) {
  bool prevIsBundlingMain = isBundlingMain;

  char* buffer = strDup("");
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"         // drop const qualifier
  char* embedded = (char*)getEmbeddedLib(filepath);  // don't free...
#pragma GCC diagnostic pop

  char* prevPath = ctxPath;
  char* addPath = NULL;

  // Embedded imports aren't actual files, but we bundle them along to
  // streamline the already complex bundling process...
  if (!embedded) {
    if (!canExpandPath(filepath)) {
      addPath = getBasePath(filepath);
    }

    // push path into context (set next state)
    if (addPath) {
      ctxPath = prevPath ? join2(prevPath, addPath) : addPath;
    }
  }

  // try to read the file from combined path
  auto_str srcPath = prevPath ? join2(prevPath, filepath) : strDup(filepath);
  Reader* reader = getReader(srcPath, embedded);
  if (!embedded) {
    performBasicAnalysis(srcPath);
    if (hasFoundErrors) {
      failBundling(filepath);
    }
  }

  while (hasContent(reader)) {
    char* prevBuff = buffer;
    Token* tok = reader->current;
    TokType type = tok->type;
    char* val = tokToVal(tok, true);

    // handle brace contexts
    if (isPunctTok(tok, "{")) {
      pushImportScope();
    } else if (isPunctTok(tok, "}")) {
      popImportScope();
    }

    const Token* next = advanceTok(__func__, reader);

    if (next) {
      bool isImport = recursive && isOpTok(next, "<-");

      // If this is a static import path, join the content!
      if (isImport && canImportTo(tok, next->next)) {
        Token* part = advanceTok(__func__, reader);  // consume import sign "<-"
        char path[PATH_MAX] = {'\0'};

        while (isStaticPath(part) || isOpTok(part, ".")) {
          appendPath(path, isStaticPath(part) ? part->val : "/");
          part = advanceTok(__func__, reader);
        }

        if (!*path) {
          fatalOut(__FILE__, __func__, "bad import");
        }

        isBundlingMain = false;  // set state for $isMain
        auto_str inner = NULL;
        if (type == TkUnder) {
          // Local import
          if (!hasImported(path)) {
            addLocalImport(path);
            inner = bundle(path, true);
            buffer = join3(prevBuff, inner, ";");
          }
        } else {
          // Named import
          auto_str flag = join3(val, ":", path);
          if (!hasImported(flag)) {
            addNamedImport(flag);  // outer scope
            pushImportScope();
            addLocalImport(path);  //  inner scope
            inner = bundle(path, true);
            buffer = join5(prevBuff, val, "={;", inner, "};;");
            popImportScope();
          }
        }
        isBundlingMain = prevIsBundlingMain;  // restore state

      } else {
        // Is this another kind of import statement?
        if (isImport) {
          // If this is a dynamic import, alert, otherwise fail
          if (isDynamicPath(next->next)) {
            logAlert(__func__, "dynamic import", next->src);
          } else {
            logError(__func__, "invalid import", next->src);
            failBundling(filepath);
          }
        }

        char* separator = "";
        if (isPunctEol(next) || isCommTok(next)) {
          separator = ";";
        } else if (isPunctTok(next, ",")) {
          separator = ",";
        }

        buffer = join3(prevBuff, val, separator);
      }
    } else {
      buffer = join2(prevBuff, val);
    }

    // Cleanup
    if (val != tok->val) {
      free(val);
    }
    if (prevBuff != buffer) {
      free(prevBuff);
    }
  }

  // More cleanup
  if (!embedded) {
    // pop context path (revert to prev state)
    if (addPath) {
      if (prevPath) {
        free(ctxPath);
      }
      free(addPath);
    }
    ctxPath = prevPath;

    free(reader->source);
  }
  freeReaderAll(reader);

  return buffer;
}

void format(char* filepath, bool overwrite) {
  int indent = 0;
  char* buffer = strDup("");
  Reader* reader = getReader(filepath, NULL);

  // Set initial state flags
  bool wasLine = true;
  bool wasDot = false;
  bool wasOpening = false;
  bool wasIdentifier = false;
  bool wasUnary = false;
  bool wasMinus = false;
  bool wasOpenPar = false;
  bool wasClosePar = false;
  bool wasAssign = false;
  bool wasOper = false;
  bool wasClosCur = false;
  bool wasColon = false;
  bool wasSlash = false;
  bool wasType = false;
  bool isDynamic = false;
  bool wasDotTilde = false;
  int hasQuestion = false;

  // Other init
  char* tab = createWhites(INDENT);

  while (reader->current->type) {
    char* prevBuff = buffer;

    // Interpret current token to flags
    Token* tok = reader->current;
    TokType type = tok->type;
    bool isOper = type == TkOp;
    bool isLine = isPunctTok(tok, "\n") || isPunctTok(tok, ";");
    bool isComma = isPunctTok(tok, ",");
    bool isColon = isPunctTok(tok, ":");
    bool isChain = isOpTok(tok, ".") || isOpTok(tok, "?.");
    bool isDot = isChain || isOpTok(tok, "..") || isOpTok(tok, "..<");
    bool isComment = isCommTok(tok);
    bool isOpening = isPunctTok(tok, "[") || isPunctTok(tok, "{");
    bool isOpenSqB = isPunctTok(tok, "[");
    bool isOpenPar = isPunctTok(tok, "(");
    bool isClosCur = isPunctTok(tok, "}");
    bool isClosing = isPunctTok(tok, "]") || isClosCur;
    bool isClosPar = isPunctTok(tok, ")");
    bool isMinus = isOpTok(tok, "-");
    bool isQuest = isOpTok(tok, "?");
    bool isSlash = isOpTok(tok, "/");
    bool isType = type == TkName;

    // Apply formatting logic from flags (warning: logic bomb ahead)
    if (isClosing || isClosPar) {
      indent -= INDENT;
    }

    // Conditions
    bool exceptionRules = (!wasLine && isComment) || (wasOper && wasLine) ||
                          (wasLine && (isColon || isQuest || isChain));

    bool inclusiveRules =
        wasLine || isLine || isComma || (wasMinus && !wasAssign) ||
        (!hasQuestion && isColon) || isDot || wasDot || wasUnary ||
        wasOpenPar || isClosPar || ((wasOpening) && isLine) ||
        ((wasLine || wasOpening) && isClosing) || isDynamic ||
        (wasIdentifier && (isOpenSqB || isOpenPar)) || (wasLine && isOpening) ||
        (wasClosePar && isOpenPar) || (wasType && isSlash) ||
        (wasSlash && isType) || wasDotTilde;

    bool exclusiveRules = (wasOper && isOper && !wasDot) ||
                          (wasClosCur && isColon) || (wasColon && isMinus);

    char* prepend = " ";
    if (exceptionRules) {
      prepend = tab;
    } else if (inclusiveRules && !exclusiveRules) {
      prepend = "";
    }

    char* val = tokToVal(tok, false);

#ifdef DEBUG
    if (traceLogs) {
      if (exceptionRules) {
        logTrace2(__FILE__, __func__, "using exceptionRules", val);
      } else if (inclusiveRules && !exclusiveRules) {
        logTrace2(__FILE__, __func__, "using inclusiveRules", val);
      } else if (inclusiveRules && exclusiveRules) {
        logTrace2(__FILE__, __func__, "using exclusiveRules", val);
      } else {
        logTrace2(__FILE__, __func__, "using no rules apply", val);
      }
    }
#endif

    char* whites = wasLine ? createWhites(indent) : "";
    const Token* next = advanceTok(__func__, reader);
    bool willBeLine = isPunctTok(next, "\n") || isPunctTok(next, ";");
    buffer = wasLine && isLine && willBeLine
                 ? buffer
                 : join4(prevBuff, whites, prepend, val);
    if (wasLine) {
      free(whites);
    }

    // Update state flags
    isDynamic = isDynamic || (wasDot && isOpenSqB);
    if (isClosing) {
      isDynamic = false;
    }
    wasLine = isLine;
    wasDotTilde = wasDot && isOpTok(tok, "~");
    wasDot = isDot;
    wasOpening = isOpening;
    wasIdentifier =
        type == TkKey || type == TkId || type == TkName || type == TkUnder;
    wasUnary = isOpTok(tok, "!") || isOpTok(tok, "!!");
    wasMinus = ((type == TkPunct || isOpenPar || type == TkOp) &&
                isOpTok(next, "-")) ||
               (wasMinus && isMinus);
    wasOpenPar = isOpenPar;
    wasClosePar = isClosPar;
    wasClosCur = isClosCur;
    wasColon = isColon;
    wasSlash = isSlash;
    wasType = isType;
    wasAssign = isOpTok(tok, "=");
    wasOper = (isOper && !isOpTok(tok, "&") && !isOpTok(tok, "|")) ||
              (isLine && wasOper);

    if (hasQuestion && (isColon || isLine)) {
      hasQuestion--;
    } else if (isQuest) {
      hasQuestion++;
    }

    // Update indentation for next line
    if (isOpening || isOpenPar) {
      indent += INDENT;
    }

    // Cleanup
    if (val != tok->val) {
      free(val);
    }
    if (prevBuff != buffer) {
      free(prevBuff);
    }
  }

  // Align special symbols in group of lines
  buffer = reflowLines(buffer);

  // Output reformatted
  if (overwrite) {
    if (!writeFile(filepath, buffer, strlen(buffer), "w")) {
      fatalOut(__FILE__, __func__, filepath);
    }
    stderrStartLine(CL_GRN);
    fprintf(stderr, MRG_BLT "formatted: %s", filepath);
    stderrEndLine();
  } else {
    fputs(buffer, stdout);
  }

  // Cleanup
  free(buffer);
  free(reader->source);
  freeReaderAll(reader);
  free(tab);
}
