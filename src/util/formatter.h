/**
 * @file formatter.h
 * @brief Source code formatting utilities
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-21
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2023, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "../sdk/sdk.h"

/**
 * @brief Check if a string only has blank spaces.
 *
 * @param buffer string
 * @return boolean
 */
bool isBufferBlank(const char* buffer);

/**
 * @brief Create a new string of blank spaces.
 *
 * @param size number of blank spaces
 * @return string (use free)
 */
char* createWhites(int size);

/**
 * @brief Stack scope for import control.
 */
void pushImportScope(void);

/**
 * @brief Unstack scope for import control.
 */
void popImportScope(void);

/**
 * @brief Recursively bundles sources and imports into a single fat file.
 *
 * @param filepath string
 * @param recursive boolean
 * @return string
 */
char* bundle(char* filepath, bool recursive);

/**
 * @brief Reformats source on filepath and replaces original with formatted.
 *
 * @param filepath string
 * @param overwrite bool (if set to false prints result to stdout)
 */
void format(char* filepath, bool overwrite);
