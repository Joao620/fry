/**
 * @file repl.h
 * @brief Cool moves on the terminal for the fry repl
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.5.0
 * @date 2024-04-11
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include <stdbool.h>

#define H_BUFF_MAX 32

/**
 * @brief Get the input from console using readline
 *
 * @param buffer previous text
 * @return true if has input, false if EOF
 */
bool getInput(char* buffer);

/**
 * @brief Get a session header comment with timestamp
 *
 * @return static buffer (DON'T FREE)
 */
char* getSessionHeader(void);
