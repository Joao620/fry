# give me diamonds

# https://www.codewars.com/kata/5503013e34137eeeaa001648/javascript
# Return a string that looks like a diamond shape when printed on the screen,
# using asterisk (*) characters. Trailing spaces should be removed,
# and every line must be terminated with a newline character (\n).

diamond = (n: Number): Text -> {

  isEven = n % 2 == 0
  isNegative = n < 0

  isEven | isNegative => null

  _ => {
    ascii = { ~ art = '' }

    addStars = count -> {
      blanks = (n - count) / 2
      blanks ? 1..blanks @ -> ascii.art += ' '
      1..count @ -> { ascii.art += '*', () }
      ascii.art += '\n'
    }

    # draw upper part
    ~ stars = 1, (stars < n) @ {
      addStars(stars)
      stars += 2
    }

    # draw middle
    addStars(n)

    # draw lower part
    ~ stars = n - 2, (stars > 0) @ {
      addStars(stars)
      stars -= 2
    }

    ascii.art
  }

}

$result
diamond(1)  == '*\n'                           &
diamond(3)  == ' *\n***\n *\n'                 &
diamond(5)  == '  *\n ***\n*****\n ***\n  *\n' &
diamond(2)  == null                            &
diamond(-3) == null                            &
diamond(0)  == null => 'pass'
_                   => 'fail'
