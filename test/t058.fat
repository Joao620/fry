# inclusions and overrides

Cat = {
  hasManyLives = true
  speak = -> "meow"
}

Dog = {
  isLoyal = true
  speak = -> "woof"
}

Pet1 = { Cat, Dog }  # cat overrides dog

Pet2 = { Dog, Cat, isBlack = true }  # dog overrides cat, and is black

validatePet = pet -> {
  pet == Cat  => !pet.isLoyal & pet.hasManyLives & pet.speak == 'meow' & !pet.isBlack
  pet == Dog  => pet.isLoyal & !pet.hasManyLives & pet.speak == 'woof' & !pet.isBlack
  pet == Pet1 => pet.isLoyal & pet.hasManyLives & pet.speak == 'meow' & !pet.isBlack
  pet == Pet2 => pet.isLoyal & pet.hasManyLives & pet.speak == 'woof' & pet.isBlack
}

r1 = validatePet(Cat())
r2 = validatePet(Dog())
r3 = validatePet(Pet1())
r4 = validatePet(Pet2())

A = { x = 'a' }
B = { x = 'b' }
C = { x = 'c' }

X1 = { A, B, C }           # takes no argument, but inherits default
X2 = { B, C, A }           # takes no argument, but inherits default
X3 = { B, C, A, x }        # takes argument, and as B is first uses it's default
X4 = { C, A, B, x = 'd' }  # takes argument, with override

r5 = X1().x == 'a' & X2().x == 'b' & X3().x == 'b' & X4().x == 'd'

# you can't override x in X1, because it's only inherited...
# but you can on X2 and X3 where this prop is declared
r6 = X3('y').x == 'y' & X3('y').x == 'y'

# multi-inheritance
_ <- fat.type.Text
K = { Text, ~ val = 'default' }
L = { K, ~ val = 'other' }
k = K()
l = L()

r7 = k.size == 7 & l.size == 5

$result
r1 & r2 & r3 & r4 & r5 & r6 => 'pass'
_                           => 'fail'
