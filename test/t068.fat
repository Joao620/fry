{}  # empty brackets as very first input would seg fault

# regression test (pack b)

# run with --error

# these lines may cause issues on fry prior to v0.0.8

_ <- fat.type.Text
'a'.split('b')  # would seg-fault

a = { ~ b = 3 }
a.b = null  # would not get erased
r1 = a.b == null

_ <- fat.type.Number
r2 = Number('x') <= Error  # would not be an Error sub-type

# these lines may cause issues on fry prior to v1.0.0
# [].0  # would seg-fault - update now prevented at parse-time

# inconsistent behavior on fry prior to v1.0.1

a.~b = 3  # set mutable (valid syntax, since v1.0.0)
r3 = a.b == 3

a.b = ()  # would not get erased on nested value (fix)
r4 = a.b == null

# strange behaviour on fry prior to v1.1.0

r5 = (x: Text = null) != TypeError

# uncaught call to missing method name on fry prior to v1.2.0

r6 = a.c() == KeyError

# invalid method signature, repeated key on fry prior to v1.3.1
# fn = (n, n) -> {}  # prevented at parse-time 

# ensure standard behaviour on splitting empty string
_ <- fat.type.Text

r7 = ''.split('')      == []
r8 = ''.split('other') == [ '' ]

# would be CallError: unnamed call on fry prior to v2.0.0
r9 = self('a')        == a
r10 = [ 1, 2, 3 ] (1) == 2
r11 = (-> _ + 5)(4)   == 9

# would seg-fault on fry prior to v2.1.0
# () <- fat.type.Text

# would be silently skipped over on fry prior to v2.1.0
# (().() = 4) <= Error  # now prevented at parse time (v2.2.0)

# would create an infinite loop on fry prior to v2.1.0
x = [ 1, 2, 3 ]
y = -> _ + 2
z = -> _ * 2
r12 = (x @ y @ z) == [ 6, 8, 10 ]

# would alter the actual definition of X on deep nested values
# on fry prior to v2.1.0
X = (x = { x = { ~ x = 2 } })
x1 = X()
x1.x.x.x = 4
x2 = X()
r13 = x1 != x2

# would return null on fry after v1.3.5 and prior to v2.1.1
localResolution = -> {
  val = true
  val  # local entry can't be found!?
}

r14 = ([ false ] @ localResolution)(0)

# method would get mutable value by reference, fixed on v2.2.0
~ b = 5
fn1 = -> _ += 5
fn1(b)
r15 = b == 5
fn2 = x -> x += 5
r16 = fn2(b) == AssignError

# half-open range operator for selecting until zero would
# return the complete text, fixed on v2.6.0
r17 = 'hello' (..<0) == ''

$result
r1  & r2 & r3 & r4 & r5 & r6 & r7 & r8 & r9 & r10 &
r11 & r12 & r13 & r14 & r15 & r16 & r17 => 'pass'
_                                       => 'fail'
