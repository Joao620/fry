# text padding utils

_ <- fat.type.Text

## Create text with n of fill elements
fillWith = (fill: Text, n: Number): Text -> {
  n < 1 | fill.isEmpty ? '' : fill.repeat(n)(..<n)
}

## Add up to n of fill elements to the right-side of x
padRight = (x: Text, fill: Text, n: Number): Text -> {
  missing = n - x.size
  missing < 1 ? x : x + fillWith(fill, missing)
}

## Add up to n of fill elements to the left-side of x
padLeft = (x: Text, fill: Text, n: Number): Text -> {
  missing = n - x.size
  missing < 1 ? x : fillWith(fill, missing) + x
}

r1 = padRight('', ' ', 0)    == ''
r2 = padRight('', ' ', 1)    == ' '
r3 = padRight('abc', ' ', 1) == 'abc'
r4 = padRight('abc', ' ', 3) == 'abc'
r5 = padRight('abc', ' ', 4) == 'abc '

r6 = padLeft('50', '', 3)  == '50'
r7 = padLeft('50', '0', 3) == '050'
r8 = padLeft('', '0', 3)   == '000'

r9 = fillWith('ae', 3) == 'aea'

$result
r1 & r2 & r3 & r4 & r5 & r6 & r7 & r8 & r9 => 'pass'
__________________________________________ => 'fail'

# note: you can use one or many underscores :)
