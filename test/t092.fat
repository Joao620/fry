# break map example

_       <- fat.type.Error
failure <- fat.failure
color   <- fat.color
console <- fat.console
system  <- fat.system
sdk     <- fat.sdk

Break = Error

# make a mapper breakable (as curried method)
breakable = (mapper: Method) -> (arg: Any) -> {
  breakHandler = (error: Error) -> {
    error == Break => null
    _ => {
      # Otherwise crash!
      console.stderr(error, color.red)
      sdk.printStack(16)
      system.exit(system.failureCode)
    }
  }
  failure.trapWith(breakHandler)
  mapper(arg)
}

# if list contains '2' breaks mapping
myBreakableMapper = breakable(
  items -> {
    items @ item -> {
      item == 2 => Break()
      _         => item + 1  # effect executed until break
    }
  }
)

r1 = myBreakableMapper([ 1, 2, 3 ]) == null

r2 = myBreakableMapper([ 1, 3, 5 ]) == [ 2, 4, 6 ]

# crash example...
# myBreakableMapper([ 'a', 'b', 'c' ])

$result
r1 & r2 => 'pass'
_       => 'fail'
