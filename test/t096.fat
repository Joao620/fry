# calendar support

_ <- fat.extra.Date

# add one year to a leap day
dateLeapYear = Date(2020, 2, 29)
datePostLeapYear = dateLeapYear.addYears(1)
r1 = datePostLeapYear == Date(2021, 2, 28)

# add one month to the end of January
dateEndOfJan = Date(2023, 1, 31)
dateOneMonthAfterJan = dateEndOfJan.addMonths(1)
r2 = dateOneMonthAfterJan == Date(2023, 2, 28)

# subtract two months from January 31
dateTwoMonthsBeforeJan = dateEndOfJan.addMonths(-2)
r3 = dateTwoMonthsBeforeJan == Date(2022, 11, 30)

# add one day to February 28 in a non-leap year
dateFebNonLeapYear = Date(2023, 2, 28)
dateOneDayAfterFeb = dateFebNonLeapYear.addDays(1)
r4 = dateOneDayAfterFeb == Date(2023, 3, 1)

# add five days to January 28
dateJan = Date(2023, 1, 28)
dateFiveDaysAfterJan = dateJan.addDays(5)
r5 = dateFiveDaysAfterJan == Date(2023, 2, 2)

# add one month to April 30
dateEndOfApril = Date(2023, 4, 30)
dateOneMonthAfterApril = dateEndOfApril.addMonths(1)
r6 = dateOneMonthAfterApril == Date(2023, 5, 30)

# add one month to May 31
dateEndOfMay = Date(2023, 5, 31)
dateOneMonthAfterMay = dateEndOfMay.addMonths(1)
r7 = dateOneMonthAfterMay == Date(2023, 6, 30)

# add one year to December 31
dateEndOfYear = Date(2022, 12, 31)
dateOneYearAfterDec = dateEndOfYear.addYears(1)
r8 = dateOneYearAfterDec == Date(2023, 12, 31)

# add weeks
dateAddWeeks = Date(2023, 1, 1)
dateAfterAddingWeeks = dateAddWeeks.addWeeks(2)
r9 = dateAfterAddingWeeks == Date(2023, 1, 15)

# subtract months
dateSubtractMonths = Date(2023, 3, 31)
dateAfterSubtractingMonths = dateSubtractMonths.addMonths(-1)
r10 = dateAfterSubtractingMonths == Date(2023, 2, 28)

# toEpoch and fromEpoch methods
dateToEpoch = Date(2023, 1, 1)
epoch = dateToEpoch.toEpoch
dateFromEpoch = Date.fromEpoch(epoch)
r11 = dateFromEpoch == dateToEpoch

# truncate method
dateWithTime = Date(2023, 1, 1, Millis * 3600000)  # assuming 1 hour in milliseconds
truncatedDate = dateWithTime.truncate
r12 = truncatedDate == Date(2023, 1, 1)

# isValid method for an invalid date
r13 = !Date.isValid(2023, 2, 30, 0)
r14 = !Date.isValid(2023,-1, 10, 0)

# Final result
$result
r1 & r2 & r3 & r4 & r5 & r6 & r7 & r8 & r9 & r10 & r11 & r12 & r13 & r14 => 'pass'
_                                                                        => 'fail'
