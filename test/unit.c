/**
 * @file unit.c
 * @author Antonio Prates <hello@aprates.dev>
 * @brief Unit tests (a few, at least)
 * @version 2.6.0
 * @date 2024-05-30
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../src/interpreter.h"
#include "../src/lexer.h"
#include "../src/parser.h"

static void check(char *ref, bool pass) {
  int fill = 34 - strlen(ref);
  if (!pass) {
    printf(MRG_ERR "%s%*s", ref, fill, " ");
    stdoutLn(" fail", CL_RED);
    exit(EXIT_FAILURE);
  }
  printf(MRG_STR "%s%*s", ref, fill, " ");
  stdoutLn(" pass", CL_GRN);
}

// Note: no point in using frees on these tests

static void testSugar(void) {
  check("sugar: areSame",
        strEq("yes", "yes") == true && strEq("yes", "no") == false);

  check("sugar: ofBool", strEq(ofBool(1), "true") && strEq(ofBool(0), "false"));

  check("sugar: ofChar",
        strEq(ofChar('c'), "c") && strEq(ofChar('c'), "d") == false);

  check("sugar: ofInt", strEq(ofInt(1234), "1234") && strEq(ofInt(0), "0") &&
                            strEq(ofInt(-1), "-1"));

  char *list[4] = {"a", "b", "c", NULL};

  check("sugar: mkString", strEq(mkString(list), "abc"));

  check("sugar: joinSep", strEq(joinSep(list, ","), "a,b,c"));

  char **listFromSplit = splitSep("red<>green<>blue", "<>");

  check("sugar: splitSep", strEq(listFromSplit[0], "red") &&
                               strEq(listFromSplit[1], "green") &&
                               strEq(listFromSplit[2], "blue"));

  check("sugar: listCount", listCount((void **)listFromSplit) == 3);

  check("sugar: startsWith",
        startsWith("abc", "a") == true && startsWith("abc", "c") == false);

  char *phrase = "here we have 5 'e' and 3 'a'";
  check("sugar: countWord", countWord(phrase, "and") == 1 &&
                                countWord(phrase, "e") == 5 &&
                                countWord(phrase, "a") == 3);

  char *onePhrase = "what not not to do";
  onePhrase = replaceFirst(onePhrase, "not ", "");
  check("sugar: replaceFirst", strEq(onePhrase, "what not to do"));

  phrase = replaceAll(phrase, "here", "there");
  phrase = replaceAll(phrase, "we", "you");
  check("sugar: replaceAll", strEq(phrase, "there you have 5 'e' and 3 'a'"));

  char *filepath = ".test.txt";
  bool didSave = writeFile(filepath, phrase, strlen(phrase), "w");
  check("sugar: writeFile", didSave);
  if (didSave) {
    char *buff = NULL;
    readFile(&buff, filepath, "r");
    check("sugar: readFile", strEq(buff, phrase));
  }

  // try to remove the file create during test in mac/linux
  if (system(join2("rm ", filepath)) != EXIT_SUCCESS) {
    logAlert(__FILE__, __func__,  // didn't work... warn user :/
             join2("could not remove file generated during test: ", filepath));
  }

  check("sugar: jesteressHash", jesteressHash32("Antonio", 7) == 1927259496);
}

static void testCrypto(void) {
  char *ascii =  // all chars, excluding null char string termination (\0)
      "\001\002\003\004\005\006\007\010\011\012\013\014\015\016\017\020\021"
      "\022\023\024\025\026\027\030\031\032\033\034\035\036\037\040\041\042"
      "\043\044\045\046\047\050\051\052\053\054\055\056\057\060\061\062\063"
      "\064\065\066\067\070\071\072\073\074\075\076\077\100\101\102\103\104"
      "\105\106\107\110\111\112\113\114\115\116\117\120\121\122\123\124\125"
      "\126\127\130\131\132\133\134\135\136\137\140\141\142\143\144\145\146"
      "\147\150\151\152\153\154\155\156\157\160\161\162\163\164\165\166\167"
      "\170\171\172\173\174\175\176\177\200\201\202\203\204\205\206\207\210"
      "\211\212\213\214\215\216\217\220\221\222\223\224\225\226\227\230\231"
      "\232\233\234\235\236\237\240\241\242\243\244\245\246\247\250\251\252"
      "\253\254\255\256\257\260\261\262\263\264\265\266\267\270\271\272\273"
      "\274\275\276\277\300\301\302\303\304\305\306\307\310\311\312\313\314"
      "\315\316\317\320\321\322\323\324\325\326\327\330\331\332\333\334\335"
      "\336\337\340\341\342\343\344\345\346\347\350\351\352\353\354\355\356"
      "\357\360\361\362\363\364\365\366\367\370\371\372\373\374\375\376\377";
  char *encoded = xorEncode(NULL, ascii);
  char *decoded = xorDecode(NULL, encoded);
  check("crypto: encode/decode", strEq(ascii, decoded));

  char *uuid = generateUuid();
  bool isValidUuid = true;

  Byte *p = (Byte *)uuid;

  if (strlen(uuid) != 36) {
    isValidUuid = false;
  }

  for (int i = 0; i < 36; i++, p++) {
    if (i == 8 || i == 13 || i == 18 || i == 23) {
      if (*p != '-') {
        isValidUuid = false;
      }
    } else if (!isxdigit(*p)) {
      isValidUuid = false;
    }
  }

  if (uuid[14] != '4') {
    isValidUuid = false;
  }

  if (uuid[19] != '8' && uuid[19] != '9' && uuid[19] != 'a' &&
      uuid[19] != 'b') {
    isValidUuid = false;
  }

  check("crypto: generateUuid", isValidUuid);
}

static void testSdk(void) {
  // patterns (just a few tests, more like examples)
  check("patterns: isSpace", isSpace('\t') && !isSpace('x'));

  check("patterns: isPunct", isPunctuation('}') && !isPunctuation('x'));

  check("patterns: isDigit", isDigit('0') && isDigit('9') && !isDigit('x'));

  int opened = 0;
  int i = 0;
  char *br = "{{a}}b";  // will point index of last closing bracket
  while (br[i] && hasBracket(br[i], &opened)) {
    i++;
  }
  check("patterns: hasBracket", i == 4);

  bool hasDot = false;  // can have only one dot, like 2.3
  bool isExpo = false;  // is parsing an exponent
  check("patterns: isNumeric",
        !isNumeric(',', &hasDot, &isExpo) &&
            !isNumeric('e', &hasDot, &isExpo) &&
            isNumeric('2', &hasDot, &isExpo) &&
            !isNumeric('e', &hasDot, &isExpo) &&  // not ok before dot
            isNumeric('.', &hasDot, &isExpo) &&
            isNumeric('3', &hasDot, &isExpo) &&
            isNumeric('e', &hasDot, &isExpo) &&  // ok after dot
            isNumeric('+', &hasDot, &isExpo) &&  // ok after exponent
            !isNumeric('.', &hasDot, &isExpo));  // not ok after first dot

  check("patterns: isOneLine", isOneLine(MARK_COM) && !isOneLine('\n'));

  check("patterns: isId", isIdentifier('x') && isIdentifier('1'));

  // lexer
  check("lexer: readWhile", readWhile(&isSpace, " \t x") == 3);

  // types
  Node *node1 = createNode(FatText, "mock", NULL);
  Context *testCtx = createContext(0);
  pushStack(testCtx, "test", node1, NULL);
  node1->num.s = 7;  // string length
  node1->val = "example";
  Node *node2 = copyNode(node1, false, testCtx);
  popStack(testCtx, 1);
  check("structures: copyNode", nodeEq(node1, node2));
}

ExitCode main(void) {
  crashOnError = false;
  initLogger();
  testSugar();
  testCrypto();
  initMemoryManagement();
  testSdk();
  cleanMemoryLock();
  return EXIT_SUCCESS;
}
