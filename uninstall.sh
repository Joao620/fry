#!/usr/bin/env bash

# @file uninstall.sh
# @brief Uninstall script
# @author Antonio Prates <hello@aprates.dev>
# @version 1.3.4
# @date 2023-11-17
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2023, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

if [[ -f /usr/local/bin/fry ]]
then
    if [ "$(uname -s)" == "Darwin" ]
    then
        # MacOS
        sudo rm -v /usr/local/bin/fry
    else
        # assume Linux root
        rm -v /usr/local/bin/fry
    fi
elif [[ -f "$PREFIX/bin/fry" ]]
then
    # Termux
    rm -v "$PREFIX/bin/fry"
elif [[ -f "$HOME/.local/bin/fry" ]]
then
    # assume Linux regular user
    rm -v "$HOME/.local/bin/fry"
else
    echo 'fry not found'
    return 1
fi
