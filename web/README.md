# Web Build: Developer Guide

Running `fry`, the FatScript Interpreter in the browser.

## Building with Emscripten

Follow these steps to build the project using `emsdk`:

1. Ensure `emcc`, the [Emscripten](https://emscripten.org/) compiler is installed on your machine.
2. Modify the build script `web_build.sh` if needed:
   ```
   source ../../emsdk/emsdk_env.sh  # adjust the path to your emsdk installation
   ```
3. Execute the build script:
   ```bash
   ./web_build.sh
   ```

This process will generate the required web artifacts: `fry.data`, `fry.js`, and `fry.wasm`.

> Note that compiling with more aggressive optimization options in `emcc` can introduce bugs at link time. This is why `web_build.sh` uses a different optimization setup compared to the regular build.

## Starting the Web Server

To easily start a web server for testing, use the provided `web_start.sh` script. This starts a `nginx` service via Docker mapping the web folder into it:

```bash
./web_start.sh
```

Then head to:

```
http://localhost:5000
```

## Known Limitations

When FatScript is built for the web, it is linked against a custom-made `Webenv Library - Glue code for Emscripten`. This primarily serves as a replacement for `curses`, `libcurl`, and `readline`.

- **Console**: The `input` method works fine in the default mode. However, using modes like `plain`, `quiet`, and `secret` can result in repeated JavaScript prompts. If this occurs, click "OK" once, followed by "Cancel" for any subsequent prompts. While there have been improvements to enhance text input (e.g., using left and right arrow keys for cursor navigation), features like command history and other text input capabilities, which are typically provided by `readline`, remain absent.

- **Curses**: The `curses` module in the "webenv version" employs cursor movement logic that mimics the behavior of the curses backend when linked with the authentic ncurses library. This "glue code" offers fundamental features and broadly simulates the original's operations on [Xterm.js](https://xtermjs.org/). However, it lacks advanced features such as screen buffering and screen resizing events. Moreover, the `readKey` method might not recognize certain keypresses like Ctrl+C, back tab etc.

- **Http**: Methods such as `setHeaders`, `setName`, and `listen` aren't supported. Additionally, based on server feedback, there's potential to encounter CORS-related challenges during requests.

- **System**: Due to the limitations of the browser platform, methods like `shell`, `capture`, `fork`, and `kill` are implemented as dummy placeholder stubs.
