/**
 * @file script.js
 * @brief More glue code for Emscripten (see also: ../src/sdk/webenv.js)
 * @author Antonio Prates <hello@aprates.dev>
 * @version 1.3.3
 * @date 2023-10-29
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2023, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

// Integration global variables
var inputBuffer = "";
var isEchoOn = true;
var isBreakOn = true;
var isBlockOn = true;
var cursorPosition = 0;

// DOM elements
const terminalElement = document.getElementById("terminal");
const textInputElement = document.getElementById("textInput");
const sendButtonElement = document.getElementById("sendButton");
const restartButtonElement = document.getElementById("restartButton");
const runFatFileElement = document.getElementById("fatInput");
const loadFilesElement = document.getElementById("fileInput");
const spinnerElement = document.getElementById("spinner");
const statusElement = document.getElementById("status");

// Default term size to 80x24 if not specified via URLSearchParams
const urlParams = new URLSearchParams(window.location.search);
const cols = parseInt(urlParams.get("cols"), 10) || 80;
const rows = parseInt(urlParams.get("rows"), 10) || 24;

// Xterm.js theme override
const theme = {
  foreground: "#eff0eb",
  background: "#222222",
  selection: "#97979b33",
  black: "#222222",
  brightBlack: "#717171",
  red: "#cc0000",
  brightRed: "#ef2929",
  green: "#4e9a06",
  brightGreen: "#8ae234",
  yellow: "#c4a000",
  brightYellow: "#fce94f",
  blue: "#3465a4",
  brightBlue: "#729fcf",
  magenta: "#75507b",
  brightMagenta: "#ad7fa8",
  cyan: "#06989a",
  brightCyan: "#34e2e2",
  white: "#d3d7cf",
  brightWhite: "#eeeeec",
};

const fontFamily = "Hermit, monospace";

const term = new Terminal({ fontFamily, cols, rows, lineHeight: 0.9, theme });
term.open(terminalElement);
term.options.cursorBlink = true;

// Input functions

function applyEnterKey() {
  if (isEchoOn) term.write("\r\n");
  inputBuffer += "\n";
  cursorPosition++;
}

function applyBackspace() {
  if (cursorPosition > 0) {
    const afterPart = inputBuffer.slice(cursorPosition);
    if (isEchoOn) {
      const moveBack = "\b".repeat(afterPart.length);
      term.write("\b" + afterPart + " \b" + moveBack);
    }
    cursorPosition--;
    inputBuffer = inputBuffer.slice(0, cursorPosition) + afterPart;
  }
}

function applyMoveCursorLeft() {
  if (cursorPosition > 0) {
    term.write("\x1B[D");
    cursorPosition--;
  }
}

function applyMoveCursorRight() {
  if (cursorPosition < inputBuffer.length) {
    term.write("\x1B[C");
    cursorPosition++;
  }
}

function applyDelKey() {
  if (cursorPosition < inputBuffer.length) {
    const afterPart = inputBuffer.slice(cursorPosition + 1);
    if (isEchoOn) {
      const moveBack = "\b".repeat(afterPart.length + 1);
      term.write(afterPart + " " + moveBack);
    }
    inputBuffer = inputBuffer.slice(0, cursorPosition) + afterPart;
  }
}

function applyClearScreen() {
  term.write("\x1B[H\x1B[2J\x1B[3J ┌ ");
  cursorPosition = 0;
  inputBuffer = "";
}

function insertText(text) {
  const afterPart = inputBuffer.slice(cursorPosition);
  if (isEchoOn) {
    const moveBack = "\b".repeat(afterPart.length);
    term.write(text + afterPart + moveBack);
  }
  inputBuffer = inputBuffer.slice(0, cursorPosition) + text + afterPart;
  cursorPosition += text.length;
}

function readFileIntoEmscripten(file) {
  var reader = new FileReader();
  reader.onload = () => {
    const uint8Array = new Uint8Array(reader.result);
    FS.writeFile(file.name, uint8Array);
  };
  reader.readAsArrayBuffer(file);
}

function fryFatFile(file) {
  term.write("\r\n** running " + file + " **\r\n");
  inputBuffer += "\r\n_<-'" + file + "';\n";
}

const specialKeyMap = {
  "\r": "enter",
  "\b": "backspace",
  "\t": "tab",
  "\x7F": "backspace", // sometimes represents "delete"
  "\x1B": "esc",
  "\x1B[A": "up",
  "\x1B[B": "down",
  "\x1B[C": "right",
  "\x1B[D": "left",
  "\x1B[H": "home",
  "\x1B[F": "end",
  "\x1B[1~": "home", // alternate Home escape sequence
  "\x1B[4~": "end", // alternate End escape sequence
  "\x1B[2~": "insert",
  "\x1B[3~": "delete",
  "\x1B[5~": "pageUp",
  "\x1B[6~": "pageDown",
  " ": "space",
  // continue expanding for other special keys as required
};

// Event handling

term.onData((text) => {
  if (isBlockOn && isEchoOn) {
    switch (text.charCodeAt(0)) {
      case 12:
        applyClearScreen();
        return;
      case 13:
        applyEnterKey();
        return;
      case 8:
      case 127:
        applyBackspace();
        return;
      case 27: // other special characters (escaped)
        if (text === "\x1B[D") applyMoveCursorLeft();
        else if (text === "\x1B[C") applyMoveCursorRight();
        else if (text === "\x1B[3~") applyDelKey();
        return; // (ignored otherwise)
      default:
        if (text === "\t") {
          text = "  "; // replace tab with 2 spaces
        }
        insertText(text);
        return;
    }
  } else if (isBlockOn) {
    insertText(text);
  } else {
    // non-blocking mode
    inputBuffer = specialKeyMap[text] || text;
  }
});

textInputElement.addEventListener("paste", (event) => {
  event.preventDefault();
  const pastedText = event.clipboardData.getData("text");
  const processedText = pastedText
    .replace(/#.*\n/g, "") // remove comments
    .replace(/\r?\n\s*/g, ";"); // replace new lines
  textInputElement.value = processedText + ";";
});

textInputElement.addEventListener("keydown", (event) => {
  if (event.key === "Enter") {
    event.preventDefault();
    insertText(textInputElement.value);
    applyEnterKey();
    textInputElement.value = "";
  }
});

sendButtonElement.addEventListener("click", () => {
  insertText(textInputElement.value);
  textInputElement.value = "";
  term.focus();
});

restartButtonElement.addEventListener("click", () => {
  const url = new URL(location.href);
  url.searchParams.delete("sample");
  location.href = url.toString();
});

runFatFileElement.addEventListener("change", (event) => {
  const file = event.target.files[0];
  if (file) {
    readFileIntoEmscripten(file);
    setTimeout(() => fryFatFile(file.name), 500);
  }
  term.focus();
});

loadFilesElement.addEventListener("change", (event) => {
  Array.from(event.target.files).forEach((file) => {
    readFileIntoEmscripten(file);
    term.write("\r\n** " + file.name + " loaded **");
  });
  term.write("\r\n");
  inputBuffer = "\n";
  term.focus();
});

var Module = {
  print: (text, ...args) => {
    if (args.length) {
      text = [text, ...args].join(" ");
    }
    term.write(text + "\r\n");
  },

  printErr: (text, ...args) => {
    if (args.length) {
      text = [text, ...args].join(" ");
    }
    term.write(text + "\r\n");
  },

  onExit: (exitCode) => {
    term.write("\r\n** fry exited with code " + exitCode + " **\r\n");
  },

  setStatus: (text) => {
    if (!Module.setStatus.last) {
      Module.setStatus.last = { time: Date.now(), text: "" };
    }

    if (text === Module.setStatus.last.text) return;

    const m = text.match(/([^(]+)\((\d+(\.\d+)?)\/(\d+)\)/);
    const now = Date.now();
    if (m && now - Module.setStatus.last.time < 30) {
      return; // skip it if too soon
    }

    Module.setStatus.last.time = now;
    Module.setStatus.last.text = text;

    if (m) {
      spinnerElement.hidden = false;
    } else {
      if (!text) spinnerElement.style.display = "none";
    }
    statusElement.innerHTML = text;
  },
};

Module.setStatus("Loading...");

window.onerror = (event) => {
  Module.setStatus("Exception thrown, see JavaScript console");
  spinnerElement.style.display = "none";
  Module.setStatus = (text) => {
    if (text) {
      console.error("[post-exception status] " + text);
    }
  };
};

document.addEventListener("DOMContentLoaded", () => {
  const terminal = document.getElementById("terminal");

  // set the width of hr to match the width of the terminal
  document.querySelectorAll("hr").forEach((hr) => {
    hr.style.width = `${terminal.offsetWidth + 10}px`;
  });
});

// load with sample
const sample = urlParams.get("sample");
if (sample) Module.arguments = ["sample/" + sample + ".fat"];

// select terminal on start
term.focus();
