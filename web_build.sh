#!/usr/bin/env bash

# @file web_build.sh
# @brief Build script, web version using emscripten
# @author Antonio Prates <hello@aprates.dev>
# @version 2.5.0
# @date 2024-03-31
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

# Emscripten config
export EMSDK_QUIET=1
source ../../emsdk/emsdk_env.sh  # adjust the path to your emsdk installation
compiler="emcc"

FRY_VERSION="$(git describe --abbrev=6 --dirty --always --tags)"

sdk="bin/b64.o bin/huge.o bin/rle.o bin/embedded.o bin/fatcurses.o \
bin/logger.o bin/memory.o bin/sdk.o bin/patterns.o bin/structures.o \
bin/sugar.o bin/crypto.o bin/webenv.o"

fry="bin/repl.o bin/help.o bin/formatter.o bin/lexer.o bin/parser.o \
bin/libs.o bin/interpreter.o"

flags="-DFRY_VERSION=\"$FRY_VERSION\" -s USE_SDL=2 -s USE_SDL_MIXER=2 -Os $*"

links="-s INITIAL_MEMORY=512mb -s MAXIMUM_MEMORY=2gb -s ALLOW_MEMORY_GROWTH=1 \
-s ASYNCIFY -s ASYNCIFY_STACK_SIZE=67108864 -s TOTAL_STACK=256mb \
-s EXIT_RUNTIME=1 -s WASM_BIGINT -s FETCH -s USE_SDL=2 -s USE_SDL_MIXER=2 \
-lm -lpthread --js-library src/sdk/webenv.js \
--preload-file bin/256colors.fat@/sample/256colors.fat \
--preload-file bin/dice.fat@/sample/dice.fat \
--preload-file bin/guacano.fat@/sample/guacano.fat \
--preload-file bin/hyperlinks.fat@/sample/hyperlinks.fat \
--preload-file bin/menu.fat@/sample/games.fat \
--preload-file bin/pong.fat@/sample/pong.fat \
--preload-file bin/snake.fat@/sample/snake.fat \
--preload-file bin/triangle.fat@/sample/triangle.fat"

emitFinalBinary="$compiler -O1"

finishUp() {
    rm bin/*.o
    
    # minify JS payload
    ./web_minify.sh web/fry.js
    ./web_minify.sh web/script.js
    ./web_minify.sh web/styles.css
    ./web_minify.sh web/xterm.css
}

# clean target folder
[ -d "bin" ] && rm -r bin
[ -f "web/fry.data" ] && rm web/fry.data
[ -f "web/fry.js" ] && rm web/fry.js
[ -f "web/fry.min.js" ] && rm web/fry.min.js
[ -f "web/fry.wasm" ] && rm web/fry.wasm
[ -f "web/script.min.js" ] && rm web/script.min.js
[ -f "web/styles.min.css" ] && rm web/styles.min.css
[ -f "web/xterm.min.css" ] && rm web/xterm.min.css

echo 'Building fry [web]:' && mkdir bin && echo -n '   sdk.'                 && \
$compiler $flags -o bin/b64.o         -c src/sdk/b64.c                       && \
$compiler $flags -o bin/rle.o         -c src/sdk/rle.c                       && \
$compiler $flags -o bin/huge.o        -c src/sdk/huge.c                      && \
$compiler $flags -o bin/embedded.o    -c src/sdk/embedded.c   && echo -n '.' && \
$compiler $flags -o bin/fatcurses.o   -c src/sdk/fatcurses.c                 && \
$compiler $flags -o bin/logger.o      -c src/sdk/logger.c     && echo -n '.' && \
$compiler $flags -o bin/memory.o      -c src/sdk/memory.c     && echo -n '.' && \
$compiler $flags -o bin/patterns.o    -c src/sdk/patterns.c   && echo -n '.' && \
$compiler $flags -o bin/sdk.o         -c src/sdk/sdk.c        && echo -n '.' && \
$compiler $flags -o bin/structures.o  -c src/sdk/structures.c && echo -n '.' && \
$compiler $flags -o bin/sugar.o       -c src/sdk/sugar.c      && echo -n '.' && \
$compiler $flags -o bin/crypto.o      -c src/sdk/crypto.c     && echo -n '.' && \
$compiler $flags -o bin/webenv.o      -c src/sdk/webenv.c                    && \
echo $' \e[34mdone ✔ \e[0m' && echo -n '   front-end'                        && \
$compiler $flags -o bin/lexer.o       -c src/lexer.c          && echo -n '.' && \
$compiler $flags -o bin/parser.o      -c src/parser.c        && echo -n '..' && \
echo $' \e[34mdone ✔ \e[0m' && echo -n '   runtime'                          && \
$compiler $flags -o bin/libs.o        -c src/libs/libs.c     && echo -n '..' && \
$compiler $flags -o bin/interpreter.o -c src/interpreter.c  && echo -n '...' && \
echo $' \e[34mdone ✔ \e[0m' && echo -n '   tools'                            && \
$compiler $flags -o bin/repl.o        -c src/util/repl.c     && echo -n '..' && \
$compiler $flags -o bin/help.o        -c src/util/help.c     && echo -n '..' && \
$compiler $flags -o bin/formatter.o   -c src/util/formatter.c && echo -n '.' && \
$compiler $flags -o bin/fry.o         -c src/fry.c           && echo -n '..' && \
echo $' \e[34mdone ✔ \e[0m' && echo 'Packing samples:'                       && \
fry -z bin/256colors.fat sample/256colors.fat                                && \
fry -z bin/dice.fat sample/dice.fat                                          && \
fry -z bin/guacano.fat sample/guacano.fat                                    && \
fry -z bin/hyperlinks.fat sample/hyperlinks.fat                              && \
fry -z bin/menu.fat sample/menu.fat                                          && \
fry -z bin/pong.fat sample/pong.fat                                          && \
fry -z bin/snake.fat sample/snake.fat                                        && \
fry -z bin/triangle.fat sample/triangle.fat                                  && \
echo -n '   linking..'                                                       && \
$emitFinalBinary -o web/fry.js  bin/fry.o  $fry $sdk $links && echo -n '...' && \
echo $' \e[34mdone ✔ \e[0m' && finishUp "$@" || (echo ' -> failed!' && exit 1)
