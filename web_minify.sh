#!/usr/bin/env bash

# @file web_minify.sh
# @brief Rudimentary JS minifier written in bash (cuts emcc output to half)
# It simply aims to remove empty lines and lines that start with '//'
#
# @author Antonio Prates <hello@aprates.dev>
# @version 2.0.0
# @date 2023-12-15
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2023, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

# Output file with .min extension
FILE="$1"
OUTPUT_FILE="${FILE%.*}.min.${FILE##*.}"

{
    # Remove leading and trailing spaces
    # Remove lines that start with '//'
    # Remove block comments
    # Remove empty lines
    sed -e 's/^[ \t]*//' -e 's/[ \t]*$//' \
        -e '/^\/\//d' \
        -e ':a; /\/\*/ { :b; /.*\*\//! { N; bb }; s/\/\*.*\*\/// }; ta' \
        -e '/^$/d' "$FILE"

    # Additional minification steps could be added here
} > "$OUTPUT_FILE"
